import {createStackNavigator, createSwitchNavigator} from 'react-navigation';
import {
    SplashScreen
} from '../components/pages';
import {
    NAVIGATION_SPLASH_SCREEN,
    NAVIGATION_APP,
    NAVIGATION_DRAWER, NAVIGATION_ChooseLanguage_SCREEN, NAVIGATION_DRAWER_VENDOR,

} from './types';
import AppPAgesNavigatorCreator from './AppPagesNavigator'
import DrawerNavigatorCreator from './DrawerNavigation'
import DrawerNavigatorCreatorVendor from './DrawerNavigationVendor'
import ChooseLanguage from "../components/pages/ChooseLanguage";

const AppNavigator = createSwitchNavigator(
    {
        [NAVIGATION_SPLASH_SCREEN]: SplashScreen,
        [NAVIGATION_APP]: AppPAgesNavigatorCreator,
        [NAVIGATION_DRAWER]: DrawerNavigatorCreator,
        [NAVIGATION_DRAWER_VENDOR]: DrawerNavigatorCreatorVendor,
        [NAVIGATION_ChooseLanguage_SCREEN]: ChooseLanguage,
    }
);


export default AppNavigator;
