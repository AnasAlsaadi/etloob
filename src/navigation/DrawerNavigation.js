import {createAppContainer, createDrawerNavigator, createStackNavigator} from 'react-navigation';
import {
    ProductsScreen,
    CategoriesScreen,
    SearchScreen,
    HomeScreen,
    HomeScreen2,
    VendorHomeScreen,
    FavoriteScreen,
    OrderDetailsScreen,
    ProductScreen,
    MyOrdersScreen,
    CartScreen,
    ShippingAddressScreen,
    CheckoutScreen,
    ProfileScreen,
    EditProfileScreen,
    VendorsScreen,
    OfferScreen
} from '../components/pages/';
import {SideMenu} from '../components/template/';
import {I18nManager} from 'react-native'
import {
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_PRODUCT_SCREEN,
    NAVIGATION_SEARCH_SCREEN,
    NAVIGATION_Categories_SCREEN,
    NAVIGATION_PRODUCTS_SCREEN,
    NAVIGATION_CART_SCREEN,
    NAVIGATION_CHECKOUT_SCREEN,
    NAVIGATION_MYORDERS_SCREEN,
    NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_EDIT_PROFILE_SCREEN,
    NAVIGATION_PROFILE_SCREEN,
    NAVIGATION_ABOUTUS_SCREEN,
    NAVIGATION_VENDORS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN, NAVIGATION_ORDER_SCREEN,
    // NAVIGATION_VENDORS_SCREEN,
    NAVIGATION_VENDOR_HOME, NAVIGATION_OFFER_SCREEN,
} from './types';
import AboutUsScreen from "../components/pages/AboutUsScreen";


const MainAppNavigator = createStackNavigator({


    [NAVIGATION_HOME_SCREEN]: HomeScreen,
    [NAVIGATION_HOME2_SCREEN]: HomeScreen2,
    [NAVIGATION_PRODUCT_SCREEN]: ProductScreen,
    [NAVIGATION_SEARCH_SCREEN]: SearchScreen,
    [NAVIGATION_Categories_SCREEN]: CategoriesScreen,
    [NAVIGATION_PRODUCTS_SCREEN]: ProductsScreen,
    [NAVIGATION_CART_SCREEN]: CartScreen,
    [NAVIGATION_CHECKOUT_SCREEN]: CheckoutScreen,
    [NAVIGATION_MYORDERS_SCREEN]: MyOrdersScreen,
    [NAVIGATION_ORDER_SCREEN]: OrderDetailsScreen,
    [NAVIGATION_FAVORITES_SCREEN]: FavoriteScreen,
    [NAVIGATION_EDIT_PROFILE_SCREEN]: EditProfileScreen,
    [NAVIGATION_ABOUTUS_SCREEN]: AboutUsScreen,
    [NAVIGATION_VENDORS_SCREEN]: VendorsScreen,
    [NAVIGATION_PROFILE_SCREEN]: ProfileScreen,
    [NAVIGATION_SHIPPING_ADDRESS_SCREEN]: ShippingAddressScreen,
    [NAVIGATION_VENDOR_HOME]: VendorHomeScreen,
    [NAVIGATION_OFFER_SCREEN]: OfferScreen,

})


const DrawerNavigator = createDrawerNavigator({
        MainAppNavigator
    },
    {
        contentComponent: SideMenu,
        drawerPosition: I18nManager.isRTL ? 'right' : 'left',
    }
);
const DrawerNavigatorCreator = createAppContainer(DrawerNavigator);

export default DrawerNavigatorCreator;
