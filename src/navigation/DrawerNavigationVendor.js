import {createAppContainer, createDrawerNavigator, createStackNavigator} from 'react-navigation';
import {
    ProductsScreen,
    CategoriesScreen,
    SearchScreen,
    HomeScreen,
    HomeScreen2,
    VendorHomeScreen,
    FavoriteScreen,
    OrderDetailsScreen,
    ProductScreen,
    MyOrdersScreen,
    CartScreen,
    ShippingAddressScreen,
    CheckoutScreen,
    ProfileScreen,
    EditProfileScreen,
    VendorsScreen
} from '../components/pages/';
import {SideMenu} from '../components/template/';
import {I18nManager} from 'react-native'
import {
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_PRODUCT_SCREEN,
    NAVIGATION_SEARCH_SCREEN,
    NAVIGATION_Categories_SCREEN,
    NAVIGATION_PRODUCTS_SCREEN,
    NAVIGATION_CART_SCREEN,
    NAVIGATION_CHECKOUT_SCREEN,
    NAVIGATION_MYORDERS_SCREEN,
    NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_EDIT_PROFILE_SCREEN,
    NAVIGATION_PROFILE_SCREEN,
    NAVIGATION_ABOUTUS_SCREEN,
    NAVIGATION_VENDORS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN, NAVIGATION_ORDER_SCREEN,
    // NAVIGATION_VENDORS_SCREEN,
    NAVIGATION_VENDOR_HOME,
} from './types';
import AboutUsScreen from "../components/pages/AboutUsScreen";



console.log("customer role-======")
console.log(global.role)

const MainAppNavigatorVendor = createStackNavigator({
    [NAVIGATION_VENDOR_HOME]: VendorHomeScreen,
})


const DrawerNavigatorVendor = createDrawerNavigator({
        MainAppNavigatorVendor
    },
    {
        contentComponent: SideMenu,
        drawerPosition: I18nManager.isRTL ? 'right' : 'left',
    }
);
const DrawerNavigatorCreatorVendor = createAppContainer(DrawerNavigatorVendor);

export default DrawerNavigatorCreatorVendor;
