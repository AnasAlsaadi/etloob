import {createAppContainer, createStackNavigator} from 'react-navigation';
import {
    LoginScreen,
    SignUpScreen,
    Verification,
} from '../components/pages/';
import {
    NAVIGATION_ChooseLanguage_SCREEN,
    NAVIGATION_LOGIN_SCREEN,
    NAVIGATION_SIGNUP_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN,

} from './types';
import ChooseLanguage from "../components/pages/ChooseLanguage";


const AppPagesNavigator = createStackNavigator(
    {
        [NAVIGATION_LOGIN_SCREEN]: LoginScreen,
        [NAVIGATION_SIGNUP_SCREEN]: SignUpScreen,
        [NAVIGATION_VERIFICATION_SCREEN]: Verification,
    }
);

const AppPAgesNavigatorCreator = createAppContainer(AppPagesNavigator);

export default AppPAgesNavigatorCreator;
