import {
    FAILURE,
    PRODUCT_LIST_RESET,
    LOADING,
    SUCCESS,
    CUSTOMER_CART_REQUEST,
    CUSTOMER_CART_RESET,
    CUSTOMER_CART_SUCCESS,
    CUSTOMER_CART_FAIL,
    CUSTOMER_CART_LOADING,
    PRODUCT_REMOVE_FROM_CART_SUCCESS,
    PRODUCT_ADD_TO_CART_FAIL,
    CUSTOMER_FAVORITE_RESET,
    CUSTOMER_FAVORITE_LOADING, CUSTOMER_FAVORITE_SUCCESS, CUSTOMER_FAVORITE_FAIL, PRODUCT_ADD_TO_FAVORITE_FAIL
} from "../../constants/actionsType";

const getInitialState = product => ({
    isLoading: false,
    errorMessage: '',
    status: '',
    favorite: []

});

export default (state = getInitialState(null), {type, payload}) => {
    switch (type) {

        case CUSTOMER_FAVORITE_RESET:
            return getInitialState(null);

        case CUSTOMER_FAVORITE_LOADING:
            return {
                ...state,
                isLoading: true,
                status: LOADING,
                errorMessage: '',
            };

        case CUSTOMER_FAVORITE_SUCCESS:
            ////console.log("payload CUSTOMER_FAVORITE_SUCCESS")
            ////console.log(payload)
            return {
                ...state,
                status: SUCCESS,
                errorMessage: '',
                favorite:payload
            };


        case CUSTOMER_FAVORITE_FAIL:
            return {
                ...state,
                status: FAILURE,
                errorMessage: payload,
                favorite: [],
            };
        case PRODUCT_ADD_TO_FAVORITE_FAIL:
            return {
                ...state,
                status: FAILURE,
                errorMessage: "error add to favorite",
                favorite: [],
            };

        default:
            return state;
    }
};
