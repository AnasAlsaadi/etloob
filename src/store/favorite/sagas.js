import {takeLatest, takeEvery, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {ManageSys} from './../../utils'
import {
    CATEGORIES,
    CATEGORIES_FAIL,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CUSTOMER_CART_FAIL,
    CUSTOMER_CART_LOADING,
    CUSTOMER_CART_REQUEST,
    CUSTOMER_CART_SUCCESS, CUSTOMER_FAVORITE_FAIL, CUSTOMER_FAVORITE_REQUEST, CUSTOMER_FAVORITE_SUCCESS,
    LOADING,
    PRODUCT_LIST,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_REMOVE_FROM_CART,
    PRODUCT_REMOVE_FROM_CART_FAIL,
    PRODUCT_REMOVE_FROM_CART_LOADING,
    PRODUCT_REMOVE_FROM_CART_SUCCESS
} from "../../constants/actionsType";
import {config, url} from "../../api/api";


function* getCustomerFavorite({payload}) {
    try {

        ////console.log("customer Favorite ===========")

        ManageSys.setOptions({})
        // yield put({type: CUSTOMER_CART_LOADING});
        const cart = yield call({content: ManageSys, fn: ManageSys.cart.getFavorite});

        ////console.log("returned favorite")
        ////console.log(cart)
        yield put({type: CUSTOMER_FAVORITE_SUCCESS, payload: cart});

    } catch (error) {
        //////console.log(error)
        yield put({type: CUSTOMER_FAVORITE_FAIL, payload: "Fail to load cart"});


    }
}


export default function* watcherSaga() {
    yield takeLatest(CUSTOMER_FAVORITE_REQUEST, getCustomerFavorite);
}