import {
    HOME_FEATURE_PRODUCTS,
    HOME_RECENT_PRODUCTS,
    HOME_TRENDING_PRODUCTS,
    HOME_TOP_CATEGORIES,HOME_RANDOM_PRODUCTS,
    HOME_HOME_CATEGORIES, PRODUCT_RELATED, PRODUCT_LIST, VENDOR_LIST, NEW_REQUEST, HOME_HOME_SLIDERS
} from "../../constants/actionsType";

export const vendorsList = payload => ({
    type: VENDOR_LIST,
    payload,
});
export const RandomProduct = payload => ({
    type: HOME_RANDOM_PRODUCTS,
    payload,
});
export const FeatureProduct = payload => ({
    type: HOME_FEATURE_PRODUCTS,
    payload,
});
export const RecentProduct = payload => ({
    type: HOME_RECENT_PRODUCTS,
    payload,
});
export const TrendingProduct = payload => ({
    type: HOME_TRENDING_PRODUCTS,
    payload,
});
export const TopCategories = payload => ({
    type: HOME_TOP_CATEGORIES,
    payload,
});
export const HomeCategories = payload => ({
    type: HOME_HOME_CATEGORIES,
    payload,
});
export const HomeSliders = payload => ({
    type: HOME_HOME_SLIDERS,
    payload,
});
export const relatedAndMoreProduct = cartItem => ({
    type: PRODUCT_RELATED,
    payload: cartItem
});
export const NotFindRequest = payload => ({
    type: NEW_REQUEST,
    payload: payload
});
