import {
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_FEATURE_PRODUCTS_FAIL,
    FAILURE,
    LOADING,
    SUCCESS,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_HOME_CATEGORIES_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS,
    PRODUCT_RELATED_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET,
    VENDOR_LIST,
    VENDOR_LIST_LOADING,
    VENDOR_LIST_SUCCESS,
    VENDOR_LIST_FAIL,
    NEW_REQUEST_SUCCESS,
    NEW_REQUEST_LOADING,
    NEW_REQUEST_FAIL,
    NEW_REQUEST_RESET,
    HOME_HOME_SLIDERS_LOADING,
    HOME_HOME_SLIDERS_SUCCESS,
    HOME_HOME_SLIDERS_FAIL,
    HOME_RANDOM_PRODUCTS_LOADING, HOME_RANDOM_PRODUCTS_SUCCESS, HOME_RANDOM_PRODUCTS_FAIL
} from "../../constants/actionsType";

const INITIAL_STATE = {
    isLoadingFeatureProduct: false,
    statusFeatureProduct: "",
    isLoadingRandomProduct: false,
    statusRandomProduct: "",
    isLoadingVendor: false,
    statusVendor: "",
    isLoadingRecentProduct: false,
    statusRecentProduct: "",
    isLoadingTrendingProduct: false,
    statusTrendingProduct: "",
    isLoadingTopCategories: false,
    statusTopCategories: "",
    isLoadingNewRequest: false,
    statusNewRequest: "",
    MessageNewRequest: "",
    isLoadingHomeCategories: false,
    isLoadingHomeSliders: false,
    isLoading: false,
    statusHomeCategories: "",
    statusHomeSliders: "",
    ErrorMessage: "",
    featureProducts: [],
    randomProducts: [],
    recentProducts: [],
    trendingProducts: [],
    topCategories: [],
    homeCategories: [],
    homeSliders: [],
    vendors: [],
    isLoadingRelated: false,
    statusRelated: SUCCESS,
    ErrorMessageRelated: '',
    relatedList: [],
    moreList: [],
    productList: []
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {

        case HOME_FEATURE_PRODUCTS_LOADING:
            return {
                ...state,
                isLoadingFeatureProduct: true,
                statusFeatureProduct: LOADING,
            };

        case HOME_FEATURE_PRODUCTS_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingFeatureProduct: false,
                statusFeatureProduct: SUCCESS,
                ErrorMessage: '',
                featureProducts: payload
            };
        case HOME_FEATURE_PRODUCTS_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingFeatureProduct: false,
                statusFeatureProduct: FAILURE,
                ErrorMessage: payload,
            };
        case HOME_RANDOM_PRODUCTS_LOADING:
            return {
                ...state,
                isLoadingRandomProduct: true,
                statusRandomProduct: LOADING,
            };

        case HOME_RANDOM_PRODUCTS_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingRandomProduct: false,
                statusRandomProduct: SUCCESS,
                ErrorMessage: '',
                randomProducts: payload
            };
        case HOME_RANDOM_PRODUCTS_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingRandomProduct: false,
                statusRandomProduct: FAILURE,
                ErrorMessage: payload,
            };

        case VENDOR_LIST_LOADING:
            return {
                ...state,
                isLoadingVendor: true,
                statusVendor: LOADING,
            };

        case VENDOR_LIST_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingVendor: false,
                statusVendor: SUCCESS,
                ErrorMessage: '',
                vendors: payload
            };
        case VENDOR_LIST_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingVendor: false,
                statusVendor: FAILURE,
                ErrorMessage: payload,
            };

        case HOME_RECENT_PRODUCTS_LOADING:
            return {
                ...state,
                isLoadingRecentProduct: true,
                statusRecentProduct: LOADING,
            };

        case HOME_RECENT_PRODUCTS_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingRecentProduct: false,
                statusRecentProduct: SUCCESS,
                ErrorMessage: '',
                recentProducts: payload
            };
        case HOME_RECENT_PRODUCTS_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingRecentProduct: false,
                statusRecentProduct: FAILURE,
                ErrorMessage: payload,
            };


        case HOME_TRENDING_PRODUCTS_LOADING:
            return {
                ...state,
                isLoadingTrendingProduct: true,
                statusTrendingProduct: LOADING,
            };

        case HOME_TRENDING_PRODUCTS_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingTrendingProduct: false,
                statusTrendingProduct: SUCCESS,
                ErrorMessage: '',
                trendingProducts: payload
            };
        case HOME_TRENDING_PRODUCTS_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingTopCategories: false,
                statusTopCategories: FAILURE,
                ErrorMessage: payload,
            };


        case HOME_TOP_CATEGORIES_LOADING:
            return {
                ...state,
                isLoadingTopCategories: true,
                statusTopCategories: LOADING,
            };

        case HOME_TOP_CATEGORIES_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingTopCategories: false,
                statusTopCategories: SUCCESS,
                ErrorMessage: '',
                topCategories: payload
            };
        case HOME_TOP_CATEGORIES_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingTrendingProduct: false,
                statusTrendingProduct: FAILURE,
                ErrorMessage: payload,
            };


        case HOME_HOME_CATEGORIES_LOADING:
            return {
                ...state,
                isLoadingHomeCategories: true,
                statusHomeCategories: LOADING,
            };

        case HOME_HOME_CATEGORIES_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingHomeCategories: false,
                statusHomeCategories: SUCCESS,
                ErrorMessage: '',
                homeCategories: payload
            };
        case HOME_HOME_CATEGORIES_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingHomeCategories: false,
                statusHomeCategories: FAILURE,
                ErrorMessage: payload,
            };


        case HOME_HOME_SLIDERS_LOADING:
            return {
                ...state,
                isLoadingHomeSliders: true,
                statusHomeSliders: LOADING,
            };

        case HOME_HOME_SLIDERS_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoadingHomeSliders: false,
                statusHomeSliders: SUCCESS,
                ErrorMessage: '',
                homeSliders: payload
            };
        case HOME_HOME_SLIDERS_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingHomeSliders: false,
                statusHomeSliders: FAILURE,
                ErrorMessage: payload,
            };


        case PRODUCT_RELATED_LOADING:
            return {
                ...state,
                isLoadingRelated: true,
                statusRelated: LOADING,
            };

        case PRODUCT_RELATED_SUCCESS:
            //console.log("success");
            //console.log(payload);

            return {
                ...state,
                isLoadingRelated: false,
                statusRelated: SUCCESS,
                ErrorMessage: '',
                relatedList: payload.related,
                moreList: payload.more
            };

        case PRODUCT_RELATED_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoadingRelated: false,
                statusRelated: FAILURE,
                ErrorMessage: payload,
            };

        case NEW_REQUEST_LOADING:

            return {
                ...state,
                isLoadingNewRequest: true,
                statusNewRequest: LOADING,
            };

        case NEW_REQUEST_SUCCESS:
            //////console.log("success");
            //////console.log(payload);

            return {
                ...state,
                isLoadingNewRequest: false,
                statusNewRequest: SUCCESS,
                MessageNewRequest: '',
            };

        case NEW_REQUEST_FAIL:
            //////console.log("success");
            //////console.log(payload);

            return {
                ...state,
                isLoadingNewRequest: false,
                statusNewRequest: FAILURE,
                MessageNewRequest: payload,
            };

        case NEW_REQUEST_RESET:
            //////console.log("success");
            //////console.log(payload);

            return {
                ...state,
                isLoadingNewRequest: false,
                statusNewRequest: "",
                MessageNewRequest: "",
            };


        default:
            return state;
    }
};


