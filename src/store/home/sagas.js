import {takeLatest, call, put, delay} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    HOME_FEATURE_PRODUCTS,
    HOME_FEATURE_PRODUCTS_FAIL,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_HOME_CATEGORIES,
    HOME_HOME_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_HOME_SLIDERS,
    HOME_HOME_SLIDERS_FAIL,
    HOME_HOME_SLIDERS_LOADING,
    HOME_HOME_SLIDERS_SUCCESS,
    HOME_RANDOM_PRODUCTS,
    HOME_RANDOM_PRODUCTS_FAIL,
    HOME_RANDOM_PRODUCTS_LOADING,
    HOME_RANDOM_PRODUCTS_SUCCESS,
    HOME_RECENT_PRODUCTS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_TOP_CATEGORIES,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TRENDING_PRODUCTS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    LOADING,
    NEW_REQUEST,
    NEW_REQUEST_FAIL,
    NEW_REQUEST_LOADING,
    NEW_REQUEST_RESET,
    NEW_REQUEST_SUCCESS,
    PLACE_ORDER_REQUEST_FAIL,
    PLACE_ORDER_REQUEST_LOADING,
    PLACE_ORDER_REQUEST_SUCCESS,
    PLACE_ORDER_RESET,
    PRODUCT_LIST,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_RELATED,
    PRODUCT_RELATED_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS,
    VENDOR_LIST,
    VENDOR_LIST_FAIL,
    VENDOR_LIST_LOADING,
    VENDOR_LIST_SUCCESS
} from "../../constants/actionsType";
import {config, url, url_S} from "../../api/api";


const actionRelatedProductList = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    return axios.get(url + 'products/related?' + lang + "product_id=" + payload.product_id+"&",
        config)
        .then(function (response) {
            //////console.log("instide req");
            //////console.log(response);
            //////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            //////console.log("instide err");
            //////console.log(error.response.data);
            //////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


const actionGetProducts = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    console.log(url + 'products?' + lang + payload+"&status=publish&per_page=12");
    return axios.get(url + 'products?' + lang + payload+"&status=publish&per_page=12",
        config)
        .then(function (response) {
            console.log("instide req");
            console.log(payload);
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            console.log("instide err");
            console.log(error.response.data);
            console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}

const actionGetProductsRandom = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    console.log(url + 'products?' + lang + payload+"&status=publish&per_page=12");
    return axios.get(url + 'products/categories/random?' + lang + payload+"&status=publish&per_page=12",
        config)
        .then(function (response) {
            console.log("instide random req");
            console.log(payload);
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            // //console.log("instide err");
            // //console.log(error.response.data);
            // //console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}

const actionGetProductsTrendy = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    return axios.get(url + 'products/categories/trendy?' + lang + payload+"&status=publish&limit=12",
        config)
        .then(function (response) {
            console.log("instide req home trendy");
            console.log(payload);
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data.data, message: ""}
        })
        .catch(function (error) {
            //console.log("instide err");
            //console.log(error.response.data);
            //console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}
const actionVendors = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    return axios.get('https://etloob.com/wp-json/dokan/v1/stores?'+lang+"per_page=100",
        config)
        .then(function (response) {
            ////console.log("instide req");
            ////console.log(response);
            //////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            ////console.log("instide err");
            ////console.log(error.response.data);
            //////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}
const actionGetHomeCategories = (payload) => {


    var lang = I18nManager.isRTL ? "&lang=ar&" : "&lang=en&";
    return axios.get(url + 'products/categories/list?limit=4' + lang+"&status=publish",
        config)
        .then(function (response) {
            //////console.log("instide req");
            //////console.log(response);
            //////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            //////console.log("instide err");
            //////console.log(error.response.data);
            //////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}
const actionGetHomeSliders = (payload) => {


    var lang = I18nManager.isRTL ? "&lang=ar&" : "&lang=en&";
    return axios.get(url_S + 'smartslider3/v1/sliders/all/',
        config)
        .then(function (response) {
            //console.log("instide req");
            //console.log(response);
            //console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            //////console.log("instide err");
            //////console.log(error.response.data);
            //////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}
const actionGetTopCategories = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    return axios.get(url + 'products/categories?' + lang + payload,
        config)
        .then(function (response) {
            //////console.log("instide req");
            //////console.log(response);
            //////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            //////console.log("instide err");
            //////console.log(error.response.data);
            //////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}



const actionNewRequest = (payload) => {


    ////console.log("before request")
    //console.log(payload)

    return axios.post(url + 'newRequest',  payload,
        config)
        .then(function (response) {
            ////console.log("instide req");
            ////console.log(response);
            ////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            //console.log("instide err");



            ////console.log(error.response.data);
            //console.log(error);
            //console.log(error.response);
            //console.log(error.response.data.message);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}

function* getFeatureProduct({payload}) {
    try {

        yield put({type: HOME_FEATURE_PRODUCTS_LOADING});

        //////console.log(payload)
        payload = "featured=true";
        const data = yield call(actionGetProducts, payload);

        if (data.status === 200) {

            console.log("feature func 200 ");
            console.log(data.response)
            yield put({type: HOME_FEATURE_PRODUCTS_SUCCESS, payload: data.response});
        }
        else {

            console.log("feature func 400 ");
            console.log(data.message)
            yield put({type: HOME_FEATURE_PRODUCTS_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* getRandomProduct({payload}) {
    try {

        yield put({type: HOME_RANDOM_PRODUCTS_LOADING});

        //////console.log(payload)
        payload = "";
        const data = yield call(actionGetProductsRandom, payload);

        if (data.status === 200) {

            console.log("random func 200 ");
            console.log(data.response)
            yield put({type: HOME_RANDOM_PRODUCTS_SUCCESS, payload: data.response.data});
        }
        else {

            console.log("random func 400 ");
            console.log(data.message)
            yield put({type: HOME_RANDOM_PRODUCTS_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* getRecentProduct({payload}) {
    try {




        yield put({type: HOME_RECENT_PRODUCTS_LOADING});

        //////console.log(payload)
        payload = "";
        const data = yield call(actionGetProducts, payload);

        if (data.status === 200) {

            //////console.log("instide func 200 ");
            //////console.log(data.response)
            yield put({type: HOME_RECENT_PRODUCTS_SUCCESS, payload: data.response});
        }
        else {

            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: HOME_RECENT_PRODUCTS_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* getTrendingProduct({payload}) {
    try {

        yield put({type: HOME_TRENDING_PRODUCTS_LOADING});

        //////console.log(payload)
        payload = "trendy=true";
        const data = yield call(actionGetProductsTrendy, payload);

        if (data.status === 200) {

            //////console.log("instide func 200 ");
            //////console.log(data.response)
            yield put({type: HOME_TRENDING_PRODUCTS_SUCCESS, payload: data.response});
        }
        else {

            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: HOME_TRENDING_PRODUCTS_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* getTopCategories({payload}) {
    try {

        yield put({type: HOME_TOP_CATEGORIES_LOADING});

        //////console.log(payload)
        payload = "parent=0";
        const data = yield call(actionGetTopCategories, payload);

        if (data.status === 200) {

            //////console.log("instide func 200 ");
            //////console.log(data.response)
            yield put({type: HOME_TOP_CATEGORIES_SUCCESS, payload: data.response});
        }
        else {

            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: HOME_TOP_CATEGORIES_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* getHomeCategories({payload}) {
    try {

        yield put({type: HOME_HOME_CATEGORIES_LOADING});

        //////console.log(payload)
        payload = "parent=0";
        const data = yield call(actionGetHomeCategories, payload);

        if (data.status === 200) {

            //////console.log("instide func 200 ");
            //////console.log(data.response.data)
            yield put({type: HOME_HOME_CATEGORIES_SUCCESS, payload: data.response.data});
        }
        else {

            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: HOME_HOME_CATEGORIES_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}
function* getHomesliders({payload}) {
    try {

        yield put({type: HOME_HOME_SLIDERS_LOADING});

        //////console.log(payload)
        payload = "parent=0";
        const data = yield call(actionGetHomeSliders, payload);

        if (data.status === 200) {

            //////console.log("instide func 200 ");
            //////console.log(data.response.data)
            yield put({type: HOME_HOME_SLIDERS_SUCCESS, payload: data.response.data});
        }
        else {

            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: HOME_HOME_SLIDERS_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}


function* relatedProduct({payload}) {
    try {

        yield put({type: PRODUCT_RELATED_LOADING});

        //console.log(payload)
        if(payload.product_id===0)
        {
            payload = "featured=true";

            const data = yield call(actionGetProducts, payload);

            if (data.status === 200) {

                console.log("fetaure  func 200 ");
                console.log(data.response)
                yield put({type: PRODUCT_RELATED_SUCCESS,payload:{ related: data.response,more:[]}});
            }
            else {

                console.log("fetaure func 400 ");
                console.log(data.message)
                yield put({type: PRODUCT_RELATED_FAIL, payload: data.message});
            }

        }
        else {
            const data = yield call(actionRelatedProductList, payload);

            if (data.status === 200) {

                console.log("instide func 200 ");
                console.log(data.response)
                yield put({
                    type: PRODUCT_RELATED_SUCCESS,
                    payload: {related: data.response.data.related, more: data.response.data.more}
                });
            }
            else {

                console.log("instide func 400 ");
                console.log(data.message)
                yield put({type: PRODUCT_RELATED_FAIL, payload: data.message});
            }
        }


    } catch (error) {
        ////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* vendorList({payload}) {
    try {

        yield put({type: VENDOR_LIST_LOADING});

        ////console.log(payload)
        ////console.log(payload)
        payload = ""
        const data = yield call(actionVendors, payload);

        if (data.status === 200) {

            ////console.log("instide func 200 ");
            ////console.log(data.response)
            yield put({
                type: VENDOR_LIST_SUCCESS,
                payload: data.response
            });
        }
        else {

            ////console.log("instide func 400 ");
            ////console.log(data.message)
            yield put({type: VENDOR_LIST_FAIL, payload: data.message});
        }

    } catch (error) {
        ////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}




function* NewRequestAction({payload}) {
    try {

        yield put({type: NEW_REQUEST_LOADING});

        ////console.log("submit Order")
        ////console.log(payload)
        const data = yield call(actionNewRequest, payload);

        if (data.status === 200) {

            ////console.log("instide func 200 ");
            ////console.log(data.response)
            yield put({type: NEW_REQUEST_SUCCESS, payload: data.response});
        }
        else {

            //console.log("instide func 400 ");
            //console.log(data.message)

            yield delay(100)
            yield put({type: NEW_REQUEST_FAIL, payload: data.message});

            yield delay(100)
            yield put({type: NEW_REQUEST_RESET, payload: data.message});
        }

    } catch (error) {
        //console.log(error);
        yield put({type: NEW_REQUEST_FAIL, payload: error.errorMessage});
    }
}

export default function* watcherSaga() {
    yield takeLatest(HOME_RANDOM_PRODUCTS, getRandomProduct);
    yield takeLatest(HOME_FEATURE_PRODUCTS, getFeatureProduct);
    yield takeLatest(HOME_RECENT_PRODUCTS, getRecentProduct);
    yield takeLatest(HOME_TRENDING_PRODUCTS, getTrendingProduct);
    yield takeLatest(HOME_TOP_CATEGORIES, getTopCategories);
    yield takeLatest(HOME_HOME_CATEGORIES, getHomeCategories);
    yield takeLatest(PRODUCT_RELATED, relatedProduct);
    yield takeLatest(VENDOR_LIST, vendorList);
    yield takeLatest(NEW_REQUEST, NewRequestAction);
    yield takeLatest(HOME_HOME_SLIDERS, getHomesliders);


}
