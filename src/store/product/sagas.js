import {takeLatest, delay, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {ManageSys} from './../../utils'
import strings from './../../locales/i18n'
import {
    CATEGORIES,
    CATEGORIES_FAIL,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CUSTOMER_CART_REQUEST, GET_VARIANT_PRODUCT,
    LOADING,
    PRODUCT_ADD_TO_CART,
    PRODUCT_ADD_TO_CART_FAIL,
    PRODUCT_ADD_TO_CART_LOADING,
    PRODUCT_ADD_TO_CART_RESET,
    PRODUCT_ADD_TO_CART_RESET_FROM_SAGA,
    PRODUCT_ADD_TO_CART_SUCCESS,
    PRODUCT_ADD_TO_FAVORITE, PRODUCT_ADD_TO_FAVORITE_FAIL,
    PRODUCT_ADD_TO_FAVORITE_RESET,
    PRODUCT_ADD_TO_FAVORITE_SUCCESS,
    PRODUCT_LIST,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_RELATED,
    PRODUCT_RELATED_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS, PRODUCT_VARIANT_FAIL, PRODUCT_VARIANT_LOADING, PRODUCT_VARIANT_SUCCESS
} from "../../constants/actionsType";
import {config, url} from "../../api/api";

const actionGetProductVendor = (payload) => {


    return axios.post("https://etloob.com/wp-json/wc/v3/products/variant",
        {
            product_id: payload.product_id,
            attribute: payload.attribute,

        }
        ,
        config)
        .then(function (response) {
            console.log("instide req varianttttttttttt");
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            console.log("instide err variantttttttt");
            console.log(error.response.data);
            console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


function* addToCart({payload, more, related}) {
    try {
        //
        ManageSys.setOptions({})
        ////console.log("yeild add to cart ")
        ////console.log(payload)
        ////console.log(more)
        ////console.log(related)
        //yield put({type: PRODUCT_ADD_TO_CART_LOADING});
        //

        if (payload.stock_status === "outofstock") {

            yield delay(10)
            yield put({type: PRODUCT_ADD_TO_CART_SUCCESS, payload: "out of stock"});

        } else {

            const data = yield call({content: ManageSys, fn: ManageSys.cart.addItemToCart}, payload);

            ////console.log("after yeild add to cart ")
            yield delay(10)
            yield put({type: PRODUCT_ADD_TO_CART_SUCCESS, payload: strings.t("success_added_to_cart")});

        }
        yield delay(2000)
        yield put({type: PRODUCT_ADD_TO_CART_RESET, payload: {}});

        //yield put({type: CUSTOMER_CART_REQUEST, payload: payload});


    } catch (error) {
        yield delay(10)

        yield put({type: PRODUCT_ADD_TO_CART_FAIL, payload: payload});
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}


function* addToFavorite({payload}) {
    try {
        //
        ManageSys.setOptions({})
        ////console.log("yeild add to cart ")
        ////console.log(payload)
        //yield put({type: PRODUCT_ADD_TO_CART_LOADING});
        //
        const data = yield call({content: ManageSys, fn: ManageSys.cart.addItemToFavorite}, payload);

        ////console.log("after yeild add to cart ")

        yield delay(100)
        yield put({type: PRODUCT_ADD_TO_FAVORITE_SUCCESS, payload: {}});
        yield delay(2000)
        yield put({type: PRODUCT_ADD_TO_FAVORITE_RESET, payload: {}});

        //yield put({type: CUSTOMER_CART_REQUEST, payload: payload});




    } catch (error) {
        yield put({type: PRODUCT_ADD_TO_FAVORITE_FAIL, payload: payload});
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* getVariant({payload}) {
    try {

        yield put({type: PRODUCT_VARIANT_LOADING});

        //console.log(payload)

        var data = null;

        data = yield call(actionGetProductVendor, payload);


        if (data.status === 200) {

            //console.log("instide func 200 ");
            //console.log(data.response)
            yield put({type: PRODUCT_VARIANT_SUCCESS, payload: data.response.data});
        }
        else {

            //console.log("instide func 400 ");
            //console.log(data.message)
            yield put({type: PRODUCT_VARIANT_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

export default function* watcherSaga() {
    yield takeLatest(PRODUCT_ADD_TO_CART, addToCart);
    yield takeLatest(PRODUCT_ADD_TO_FAVORITE, addToFavorite);
    yield takeLatest(GET_VARIANT_PRODUCT, getVariant);

}
