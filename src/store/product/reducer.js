import {
    FAILURE,
    PRODUCT_LIST_RESET,
    LOADING,
    OPEN_SELECTED_PRODUCT,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    SUCCESS,
    PRODUCT_ADD_TO_CART_LOADING,
    PRODUCT_ADD_TO_CART_SUCCESS,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS,
    PRODUCT_RELATED_FAIL,
    PRODUCT_ADD_TO_CART_RESET,
    PRODUCT_ADD_TO_CART_RESET_FROM_SAGA,
    PRODUCT_ADD_TO_FAVORITE_LOADING,
    PRODUCT_ADD_TO_FAVORITE_RESET,
    PRODUCT_ADD_TO_FAVORITE_SUCCESS,
    PRODUCT_VARIANT_LOADING, PRODUCT_VARIANT_SUCCESS, PRODUCT_VARIANT_FAIL
} from "../../constants/actionsType";

const getInitialState = product => ({
    current: product,
    isLoading: false,
    // isLoadingJ: false,
    isLoadingRelated: false,
    isLoadingAddToCart: false,
    isLoadingAddToFavorite: false,
    status: '',
    ErrorMessageADDProductToCart: '',
    statusRelated: '',
    statusAddToCart: '',
    statusAddToFavorite: '',
    productList: [],
    relatedList: [],
    moreList: [],
    mediaErrorMessage: '',
    confOptionsLoading: false,
    isLoadingVariant: false,
    statusVariant: '',
    ErrorMessageVariant: '',
    confOptionsError: null,
    addToCartLoading: false,
    addToCartError: null,
    attributes: {},
    qtyInput: 1,
    selectedOptions: {}, variant: null
});

export default (state = getInitialState(null), action) => {
    switch (action.type) {
        case OPEN_SELECTED_PRODUCT:
            //////console.log("select item")
            //////console.log(action)
            //////console.log(action.payload)
            return {
                ...state,
                current: action.payload,
                addToCartLoading: false,
                addToCartError: null,
                qtyInput: 1,
                selectedOptions: {},
                ErrorMessageADDProductToCart: ''
            };

        case PRODUCT_ADD_TO_CART_LOADING:
            return {
                ...state,
                isLoadingAddToCart: true,
                statusAddToCart: LOADING,
                ErrorMessageADDProductToCart: ''
            };


        case PRODUCT_ADD_TO_CART_RESET:

            ////console.log("---------------PRODUCT_ADD_TO_CART_RESET")
            ////console.log(action)
            return {

                // isLoadingRelated: false,
                // statusRelated: SUCCESS,
                isLoadingAddToCart: false,
                statusAddToCart: '', ErrorMessageADDProductToCart: ''
                // ...state
            }
        case PRODUCT_ADD_TO_CART_RESET_FROM_SAGA:

            ////console.log("--------------- PRODUCT_ADD_TO_CART_RESET_FROM_SAGA")
            ////console.log(action)
            return {

                // isLoadingRelated: false,
                // statusRelated: SUCCESS,
                isLoadingAddToCart: false,
                statusAddToCart: '', ErrorMessageADDProductToCart: ''
                // ...state
            }

        case PRODUCT_ADD_TO_CART_SUCCESS:

            ////console.log("add to cart action reducer ")
            ////console.log( action.payload)

            return {
                ...state,
                isLoadingAddToCart: false,
                statusAddToCart: SUCCESS,
                ErrorMessage: action.payload,
                ErrorMessageADDProductToCart: action.payload,
            }


        case PRODUCT_ADD_TO_FAVORITE_LOADING:
            return {
                ...state,
                isLoadingAddToFavorite: true,
                statusAddToFavorite: LOADING,
            };


        case PRODUCT_ADD_TO_FAVORITE_RESET:

            ////console.log("---------------PRODUCT_ADD_TO_CART_RESET")
            ////console.log(action)
            return {

                // isLoadingRelated: false,
                // statusRelated: SUCCESS,
                isLoadingAddToFavorite: false,
                statusAddToFavorite: '',
                // ...state
            }

        case PRODUCT_ADD_TO_FAVORITE_SUCCESS:

            ////console.log("add to cart action reducer ")
            ////console.log(state)

            return {
                ...state,
                isLoadingAddToFavorite: false,
                statusAddToFavorite: SUCCESS,
                ErrorMessageFavorite: '',
            }


        case PRODUCT_VARIANT_LOADING:
            return {
                ...state,
                isLoadingVariant: true,
                statusVariant: LOADING,
                ErrorMessageVariant: '', variant: null
            };

        case PRODUCT_VARIANT_SUCCESS:

            ////console.log("add to cart action reducer ")
            ////console.log(state)

            return {
                ...state,
                isLoadingVariant: false,
                statusVariant: SUCCESS,
                ErrorMessageVariant: '',
                variant: action.payload
            }

        case PRODUCT_VARIANT_FAIL:

            ////console.log("add to cart action reducer ")
            ////console.log(state)

            return {
                ...state,
                isLoadingVariant: false,
                statusVariant: FAILURE,
                ErrorMessageVariant: '',
                variant: null
            }

        default:
            return state;
    }
};
