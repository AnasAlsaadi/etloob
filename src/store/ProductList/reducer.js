import {
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_FEATURE_PRODUCTS_FAIL,
    FAILURE,
    LOADING,
    SUCCESS,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_HOME_CATEGORIES_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS,
    PRODUCT_RELATED_FAIL, PRODUCT_LIST_LOADING, PRODUCT_LIST_SUCCESS, PRODUCT_LIST_FAIL, PRODUCT_LIST_RESET
} from "../../constants/actionsType";

const INITIAL_STATE = {
    isLoadingLIST: false,
    ErrorMessage: "",
    productList: [],
    status: '',
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {

        case PRODUCT_LIST_LOADING:
            return {
                ...state,
                isLoadingLIST: true,
                status: LOADING,
            };

        case PRODUCT_LIST_SUCCESS:
            //////console.log("success");
            // //console.log(action.payload);
            if (payload.refresh) {

                return {
                    ...state,
                    isLoadingLIST: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    productList: payload.data
                };
            }
            else {

                return {
                    ...state,
                    isLoadingLIST: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    productList: [...state.productList, ...payload.data]
                };
            }
        case PRODUCT_LIST_FAIL:
            //////console.log("fail");
            //////console.log(action.payload);

            return {
                ...state,
                isLoadingLIST: false,
                status: FAILURE,
                ErrorMessage: payload,
            };


        case PRODUCT_LIST_RESET:
            ////console.log("-------all--------")
            // ////console.log(action)
            return INITIAL_STATE


        default:
            return state;
    }
};


