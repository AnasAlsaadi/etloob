import {
    OFFER_LIST, OFFER_LIST_RESET
} from "../../constants/actionsType";

export const offerLists = payload => (
    {
        type: OFFER_LIST,
        payload: payload,
    });

export const offerListsRest = payload => (
    {
        type: OFFER_LIST_RESET,
        payload: payload,
    });
