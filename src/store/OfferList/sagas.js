import {takeLatest, call, put, takeEvery} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    HOME_FEATURE_PRODUCTS,
    HOME_FEATURE_PRODUCTS_FAIL,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_HOME_CATEGORIES,
    HOME_HOME_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_RECENT_PRODUCTS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_TOP_CATEGORIES,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TRENDING_PRODUCTS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    LOADING, OFFER_LIST, OFFER_LIST_FAIL, OFFER_LIST_LOADING, OFFER_LIST_SUCCESS,
    PRODUCT_LIST, PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING, PRODUCT_LIST_SUCCESS,
    PRODUCT_RELATED,
    PRODUCT_RELATED_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS
} from "../../constants/actionsType";
import {config, url, url_dokan} from "../../api/api";

const actionGetProductList = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    var urls = null

        urls = url + 'products/categories/offers?' + lang+ "&per_page=30&limit=30" + "&page=" + payload.page + "" ;

    console.log(urls)
    return axios.get(urls,
        config)
        .then(function (response) {
            console.log("offer req");
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            console.log("instide err");
            console.log(error.response.data);
            console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}

function* getProductList({payload}) {
    try {

        yield put({type: OFFER_LIST_LOADING});

        console.log(payload)

        var data = null;

        console.log("inside category")


        data = yield call(actionGetProductList, payload);

        if (data.status === 200) {

            //console.log("instide func 200 ");
            //console.log(data.response)
            yield put({type: OFFER_LIST_SUCCESS, payload: {refresh: payload.refresh, data: data.response.data}});
        }
        else {

            //console.log("instide func 400 ");
            //console.log(data.message)
            yield put({type: OFFER_LIST_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}


export default function* watcherSaga() {
    yield takeLatest(OFFER_LIST, getProductList);


}
