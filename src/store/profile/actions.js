import {
    EDIT_PROFILE,
    OPEN_SELECTED_ORDER, ORDER_LIST
} from "../../constants/actionsType";

// export const openSelectedOrder = order => (
//     {
//         type: OPEN_SELECTED_ORDER,
//         payload: order,
//     });

export const editProfile = payload => (
    {
        type: EDIT_PROFILE,
        payload: payload,
    });

