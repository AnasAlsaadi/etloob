import {
    FAILURE,
    LOADING,
    SUCCESS,
    OPEN_SELECTED_ORDER,
    ORDER_LIST_LOADING,
    ORDER_LIST_SUCCESS,
    ORDER_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET, EDIT_PROFILE_LOADING, EDIT_PROFILE_SUCCESS, EDIT_PROFILE_ERROR, EDIT_PROFILE_RESET
} from "../../constants/actionsType";

const getInitialState = order => ({

    isLoading: false,
    status: '',
    user: null,
});

export default (state = getInitialState(null), action) => {
    switch (action.type) {

        case EDIT_PROFILE_LOADING:
            return {
                ...state,
                isLoading: true,
                status: LOADING,
            };


        case EDIT_PROFILE_SUCCESS:
            //////console.log("success");
            //////console.log(action.payload);

            return {
                ...state,
                isLoading: false,
                status: SUCCESS,
                ErrorMessage: '',
                user: action.payload
            };

        case EDIT_PROFILE_ERROR:
            ////////console.log("fail");
            ////////console.log(action.payload);

            return {
                ...state,
                isLoading: false,
                status: FAILURE,
                ErrorMessage: action.payload,
            };


        case EDIT_PROFILE_RESET:
            //////console.log("RESET");
            ////console.log();

            return {
                ...state,
                isLoading: false,
                status: '',
                ErrorMessage: '',
            };


        default:
            return state;
    }
};
