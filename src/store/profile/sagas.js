import {takeLatest, call, put, delay} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    EDIT_PROFILE, EDIT_PROFILE_ERROR, EDIT_PROFILE_LOADING, EDIT_PROFILE_RESET, EDIT_PROFILE_SUCCESS,
    ORDER_LIST, ORDER_LIST_FAIL, ORDER_LIST_LOADING, ORDER_LIST_SUCCESS, PRODUCT_LIST_SUCCESS,

} from "../../constants/actionsType";
import {config, url} from "../../api/api";

const actionEditProfile = (payload) => {


    ////////console.log("get orders")
    ////////console.log(payload)
    //////console.log(url + 'customers/' + global.user.id)
    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    return axios.post(url + 'customers/' + global.user.id,
        payload,
        config)
        .then(function (response) {
            //////console.log("instide req");
            //////console.log(response);
            //////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            //////console.log("instide err");
            //////console.log(error.response.data);
            //////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


function* postEditProfile({payload}) {
    try {


        ////////console.log("get orders")
        ////////console.log("get orders fn")
        yield put({type: EDIT_PROFILE_LOADING});

        //////console.log("get edit profile fn1")
        //////console.log(payload)
        const data = yield call(actionEditProfile, payload);

        if (data.status === 200) {
            //
            //////console.log("instide func 200 ");
            //////console.log(data.response)

            yield delay(100)
            yield put({type: EDIT_PROFILE_SUCCESS, payload: data.response});

            yield delay(2000)
            yield put({type: EDIT_PROFILE_RESET, payload: data.response});
        }
        else {
            //
            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: EDIT_PROFILE_ERROR, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

export default function* watcherSaga() {
    yield takeLatest(EDIT_PROFILE, postEditProfile);
}