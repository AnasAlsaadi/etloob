import {
    FAILURE,
    LOADING,
    SUCCESS,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CATEGORIES_FAIL,
    OPEN_SELECTED_CATEGORY,
    OPEN_SELECTED_VENDORS,
    VENDORS_LOADING,
    VENDORS_SUCCESS,
    VENDORS_FAIL,
    REST_SELECTED_VENDORS, RESET_SELECTED_CATEGORY,
} from "../../constants/actionsType";

const INITIAL_STATE = {
    currentVendor: null,
    currentCategory: null,
    search: null,
    isLoadingVendor: false,
    status: "",
    ErrorMessage: "",
    vendorsAllList: [],
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case OPEN_SELECTED_VENDORS:
            return {
                ...state,
                currentCategory:null,
                search:null,
                currentVendor: payload,
            };


        case REST_SELECTED_VENDORS:
            //console.log("ffreset")
            return {
                ...state,
                currentVendor:null,
            };

        case VENDORS_LOADING:
            return {
                ...state,
                isLoadingVendor: true,
                status: LOADING,
            };

        case VENDORS_SUCCESS:
            ////console.log("success");
            ////console.log(payload);
            return {
                ...state,
                isLoadingVendor: false,
                status: SUCCESS,
                ErrorMessage: '',
                vendorsAllList: payload
            };
        case VENDORS_FAIL:
            ////////console.log("fail");
            ////////console.log(payload);

            return {
                ...state,
                isLoadingVendor: false,
                status: FAILURE,
                ErrorMessage: payload,
            };

        default:
            return state;
    }
};


