import {
    VENDORS, OPEN_SELECTED_CATEGORY, OPEN_SELECTED_VENDORS, REST_SELECTED_VENDORS,
} from "../../constants/actionsType";

export const Vendors = payload => ({
    type: VENDORS,
    payload,
});



export const openSelectedVendor = vendor => (
    {
        type: OPEN_SELECTED_VENDORS,
        payload: vendor,
    });



export const ResetSelectedVendor = vendor => (
    {
        type: REST_SELECTED_VENDORS,
        payload: vendor,
    });





