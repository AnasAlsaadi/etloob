import {takeLatest, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    VENDORS, VENDORS_FAIL, VENDORS_LOADING, VENDORS_SUCCESS
} from "../../constants/actionsType";
import {config, url, url_dokan} from "../../api/api";

const actionGetVendor = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    return axios.get(url_dokan + 'stores?' + lang+"per_page=100",
        config)
        .then(function (response) {
            ////////console.log("instide req");
            ////////console.log(response);
            ////////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            ////////console.log("instide err");
            ////////console.log(error.response.data);
            ////////console.log(error);


            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


function* getVendors({payload}) {
    try {

        yield put({type: VENDORS_LOADING});

        payload = "";
        const data = yield call(actionGetVendor, payload);

        if (data.status === 200) {

            ////console.log("instide func 200 ");
            ////console.log(data.response)
            yield put({type: VENDORS_SUCCESS, payload: data.response});
        }
        else {

            ////console.log("instide func 400 ");
            ////console.log(data.message)
            yield put({type: VENDORS_FAIL, payload: data.message});
        }

    } catch (error) {
        ////////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

export default function* watcherSaga() {
    yield takeLatest(VENDORS, getVendors);
}
