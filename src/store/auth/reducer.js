import {
    CUSTOMER_AUTH_STATE_RESET,
    CUSTOMER_SIGNUP_FAIL,
    CUSTOMER_SIGNUP_SUCCESS,
    FAILURE,
    LOADING,
    SUCCESS
} from "../../constants/actionsType";

const INITIAL_STATE = {
    isLoading: false,
    status: "",
    ErrorMessage: "",
    user: null,
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case LOADING:
            return {
                ...state,
                isLoading: true,
                status: LOADING,
            };

        case CUSTOMER_SIGNUP_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoading: false,
                status: SUCCESS,
                ErrorMessage: '',
                user: payload
            };
        case CUSTOMER_SIGNUP_FAIL:
            //////console.log("fail");
            //////console.log(payload);
            return {
                ...state,
                isLoading: false,
                status: FAILURE,
                ErrorMessage: payload,
            };



        case CUSTOMER_AUTH_STATE_RESET:
            return INITIAL_STATE;

        default:
            return state;
    }
};


