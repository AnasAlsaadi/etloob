import {takeLatest, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    HOME_FEATURE_PRODUCTS,
    HOME_FEATURE_PRODUCTS_FAIL,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_HOME_CATEGORIES,
    HOME_HOME_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_RECENT_PRODUCTS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_TOP_CATEGORIES,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TRENDING_PRODUCTS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    LOADING,
    PRODUCT_LIST,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SEARCH,
    PRODUCT_LIST_SEARCH_FAIL,
    PRODUCT_LIST_SEARCH_HSUCCESS, PRODUCT_LIST_SEARCH_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_RELATED,
    PRODUCT_RELATED_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS
} from "../../constants/actionsType";
import {config, url} from "../../api/api";

const actionGetProductListSearch = (payload) => {

    //orderby is not one of date, id, include, title, slug, price, popularity, rating
    var urls = ""
    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    if (payload.order_by) {

        payload.order_by === "price" ? payload.order_by = payload.order_by + "&order=asc" : null


        payload.order_by === "price-desc" ? payload.order_by = "price&order=desc" : null



        urls = url + 'products/?' + lang + "per_page=6&search=" + payload.search + "&status=publish&orderby=" +
            payload.order_by
    }
    else {
        urls = url + 'products/?' + lang + "per_page=6&search=" + payload.search + "&status=publish&"

    }
    console.log(urls)
    return axios.get(urls,
        config)
        .then(function (response) {
            //////console.log("instide req");
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            ////console.log("instide err");
            ////console.log(error.response.data);
            console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


function* getProductListSearch({payload}) {
    try {


        console.log("inside functions")
        console.log(payload)
        yield put({type: PRODUCT_LIST_SEARCH_LOADING});

        console.log("inside functions")
        console.log(payload)
        const data = yield call(actionGetProductListSearch, payload);

        if (data.status === 200) {

            ////console.log("instide func 200 ");
            ////console.log(data.response)
            yield put({type: PRODUCT_LIST_SEARCH_HSUCCESS, payload: {data: data.response}});
        }
        else {

            ////console.log("instide func 400 ");
            ////console.log(data.message)
            yield put({type: PRODUCT_LIST_SEARCH_FAIL, payload: data.message});
        }

    } catch (error) {
        ////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}


export default function* watcherSaga() {

    yield takeLatest(PRODUCT_LIST_SEARCH, getProductListSearch);


}