import {
    OPEN_SELECTED_CATEGORY,
    OPEN_SELECTED_SEARCH_WORD,
    PRODUCT_LIST_SEARCH, PRODUCT_LIST_SEARCH_RESET, RESET_SELECTED_CATEGORY, RESET_SELECTED_SEARCH_WORD
} from "../../constants/actionsType";

export const productListsSearch = payload => (
    {
        type: PRODUCT_LIST_SEARCH,
        payload: payload,
    });
export const productListsSearchReset = payload => (
    {
        type: PRODUCT_LIST_SEARCH_RESET,
        payload: payload,
    });

export const openSelectedSearchWord = word => (
    {
        type: OPEN_SELECTED_SEARCH_WORD,
        payload: word,
    });

export const ResetSelectedSearchWord = word => (
    {
        type: RESET_SELECTED_SEARCH_WORD,
        payload: word,
    });


