import {
    FAILURE,
    LOADING,
    SUCCESS,
    PRODUCT_LIST_SEARCH_LOADING,
    PRODUCT_LIST_SEARCH_HSUCCESS,
    PRODUCT_LIST_SEARCH_FAIL,
    PRODUCT_LIST_SEARCH_RESET,
    PRODUCT_LIST_SEARCH,
    OPEN_SELECTED_CATEGORY,
    OPEN_SELECTED_SEARCH_WORD,
    RESET_SELECTED_SEARCH_WORD,
    RESET_SELECTED_CATEGORY
} from "../../constants/actionsType";

const INITIAL_STATE = {
    isLoading: false,
    isLoadingj: false,
    ErrorMessage: "",
    productList: null,
    currentVendor: null,
    currentCategory: null,
    status: '',
    currentSearch: '',
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        //
        // case PRODUCT_LIST_SEARCH:
        //     ////console.log("ffffff")
        //     return {
        //         ...state,
        //         isLoadingj: true,
        //         isLoading: true,
        //         status: LOADING,
        //     };

        case OPEN_SELECTED_SEARCH_WORD:
            return {
                ...state,
                currentVendor:null,
                currentCategory:null,
                currentSearch: payload,
            };

        case RESET_SELECTED_SEARCH_WORD:
            console.log("ffreset")
            return {
                ...state,
                currentSearch: null,
            };

        case PRODUCT_LIST_SEARCH_LOADING:
            return {
                ...state,
                isLoadingj: true,
                isLoading: true,
                status: LOADING,
            };

        case PRODUCT_LIST_SEARCH_HSUCCESS:
            //console.log("success");
            //console.log(payload);
            return {
                ...state,
                isLoading: false,
                isLoadingj: false,
                status: SUCCESS,
                ErrorMessage: '',
                productList: payload.data
            };

        case PRODUCT_LIST_SEARCH_FAIL:
            //////console.log("fail");
            //////console.log(action.payload);

            return {
                ...state,
                isLoading: false,
                isLoadingj: false,
                status: FAILURE,
                ErrorMessage: payload,
            };


        case PRODUCT_LIST_SEARCH_RESET:
            ////console.log("-------all--------")
            // ////console.log(action)
            return INITIAL_STATE


        default:
            return state;
    }
};


