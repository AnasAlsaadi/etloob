import {takeLatest, call, put, takeEvery} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {REVIEW_LIST, REVIEW_LIST_FAIL, REVIEW_LIST_LOADING, REVIEW_LIST_SUCCESS
} from "../../constants/actionsType";
import {config, url, url_dokan} from "../../api/api";

const actionGetReviewList = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    var urls = null
    urls = 'https://etloob.com/wp-json/dokan/v1/stores/' + global.user.id + '/reviews';
    // console.log(urls)
    return axios.get(urls,
        config)
        .then(function (response) {
            // console.log("instide req");
            // console.log(response);
            // console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            // console.log("instide err");
            // console.log(error.response.data);
            // console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}

function* getReviewList({payload}) {
    try {

        yield put({type: REVIEW_LIST_LOADING});

        console.log(payload)

        var data = null;

        console.log("inside category")


        data = yield call(actionGetReviewList, payload);


        if (data.status === 200) {

            //console.log("instide func 200 ");
            //console.log(data.response)
            yield put({type: REVIEW_LIST_SUCCESS, payload: {refresh: payload.refresh, data: data.response}});
        }
        else {

            //console.log("instide func 400 ");
            //console.log(data.message)
            yield put({type: REVIEW_LIST_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}


export default function* watcherSaga() {
    yield takeEvery(REVIEW_LIST, getReviewList);


}
