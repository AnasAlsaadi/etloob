import {
    PRODUCT_LIST_RESET, REVIEW_LIST
} from "../../constants/actionsType";

export const reviewLists = payload => (
    {
        type: REVIEW_LIST,
        payload: payload,
    });
