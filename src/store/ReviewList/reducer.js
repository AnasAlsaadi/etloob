import {
    FAILURE,
    LOADING,
    SUCCESS,
    REVIEW_LIST_LOADING,
    REVIEW_LIST_SUCCESS, REVIEW_LIST_FAIL, REVIEW_LIST_RESET
} from "../../constants/actionsType";

const INITIAL_STATE = {
    isLoadingLIST: false,
    ErrorMessage: "",
    reviewList: [],
    status: '',
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {

        case REVIEW_LIST_LOADING:
            return {
                ...state,
                isLoadingLIST: true,
                status: LOADING,
            };

        case REVIEW_LIST_SUCCESS:
            //////console.log("success");
            // //console.log(action.payload);
            if (payload.refresh) {

                return {
                    ...state,
                    isLoadingLIST: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    reviewList: payload.data
                };
            }
            else {

                return {
                    ...state,
                    isLoadingLIST: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    reviewList: [...state.reviewList, ...payload.data]
                };
            }
        case REVIEW_LIST_FAIL:
            //////console.log("fail");
            //////console.log(action.payload);

            return {
                ...state,
                isLoadingLIST: false,
                status: FAILURE,
                ErrorMessage: payload,
            };

        default:
            return state;
    }
};


