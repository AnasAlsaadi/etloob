import {
    CUSTOMER_AUTH_STATE_RESET, CUSTOMER_LOGIN_FAIL, CUSTOMER_LOGIN_SUCCESS,
    FAILURE,
    LOADING,
    SUCCESS
} from "../../constants/actionsType";

const INITIAL_STATE = {
    isLoading: false,
    status: "",
    ErrorMessage: "",
    user: null,
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case LOADING:
            return {
                ...state,
                isLoading: true,
                status: LOADING,
            };


        case CUSTOMER_LOGIN_SUCCESS:
            ////////console.log("success");
            ////////console.log(payload);
            return {
                ...state,
                isLoading: false,
                status: SUCCESS,
                ErrorMessage: '',
                user: payload.customer_data
            };
        case CUSTOMER_LOGIN_FAIL:
            ////////console.log("fail");
            ////////console.log(payload);
            return {
                ...state,
                isLoading: false,
                status: FAILURE,
                ErrorMessage: "wrong username or password ",
            };


        case CUSTOMER_AUTH_STATE_RESET:
            return INITIAL_STATE;

        default:
            return state;
    }
};


