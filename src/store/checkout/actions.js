import {
    PLACE_ORDER_REQUEST,PLACE_ORDER_RESET
} from "../../constants/actionsType";

export const placeCartOrder = payload => ({
    type: PLACE_ORDER_REQUEST,
    payload,
});

export const resetPlaceOrder = payload => ({
    type: PLACE_ORDER_RESET,
    payload,
});
