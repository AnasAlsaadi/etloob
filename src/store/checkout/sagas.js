import {takeLatest, call, put, delay} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    CATEGORIES,
    CATEGORIES_FAIL,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    LOADING,
    PLACE_ORDER_REQUEST,
    PLACE_ORDER_REQUEST_FAIL,
    PLACE_ORDER_REQUEST_LOADING,
    PLACE_ORDER_REQUEST_SUCCESS,
    PLACE_ORDER_RESET
} from "../../constants/actionsType";
import {config, url} from "../../api/api";

const actionPlaceOrder = (payload) => {


    console.log("before order request")
    console.log(payload)
    return axios.post(url + 'orders?customer_id=' + global.user.id, payload,
        config)
        .then(function (response) {
            console.log("order req");
            console.log(response);
            console.log("order req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            ////console.log("instide err");
            ////console.log(error.response.data);
            ////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


function* placeOrder({payload}) {
    try {

        yield put({type: PLACE_ORDER_REQUEST_LOADING});

        console.log("submit Order")
        console.log(payload)
        console.warn(payload)
        console.log(JSON.stringify(payload))
        const data = yield call(actionPlaceOrder, payload);

        if (data.status === 200) {

            ////console.log("instide func 200 ");
            ////console.log(data.response)
            yield put({type: PLACE_ORDER_REQUEST_SUCCESS, payload: data.response});
        }
        else {

            ////console.log("instide func 400 ");
            ////console.log(data.message)

            yield delay(100)
            yield put({type: PLACE_ORDER_REQUEST_FAIL, payload: data.message});

            yield delay(2000)
           yield put({type: PLACE_ORDER_RESET, payload: data.message});
        }

    } catch (error) {
        ////console.log(error);
        yield put({type: PLACE_ORDER_REQUEST_FAIL, payload: error.errorMessage});
    }
}

export default function* watcherSaga() {
    yield takeLatest(PLACE_ORDER_REQUEST, placeOrder);
}
