import {
    FAILURE,
    LOADING,
    SUCCESS,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CATEGORIES_FAIL,
    OPEN_SELECTED_CATEGORY,
    PLACE_ORDER_REQUEST_LOADING,
    PLACE_ORDER_REQUEST_SUCCESS,
    PLACE_ORDER_REQUEST_FAIL, PLACE_ORDER_RESET,
} from "../../constants/actionsType";

const INITIAL_STATE = {
    isLoadingPlaceHolder: false,
    statusPlaceHolder: "",
    ErrorMessagePlaceHolder: "",
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case PLACE_ORDER_RESET:
            return {
                isLoadingPlaceHolder: false,
                statusPlaceHolder: "",
                ErrorMessagePlaceHolder: SUCCESS,
            };
        case PLACE_ORDER_REQUEST_LOADING:
            return {
                ...state,
                isLoadingPlaceHolder: true,
                statusPlaceHolder: LOADING,
            };

        case PLACE_ORDER_REQUEST_SUCCESS:
            console.log("success");
            console.log(payload);
            return {
                ...state,
                isLoadingPlaceHolder: false,
                statusPlaceHolder: SUCCESS,
                ErrorMessagePlaceHolder: '',
            };
        case PLACE_ORDER_REQUEST_FAIL:
            console.log("fail");
            console.log(payload);

            return {
                ...state,
                isLoadingPlaceHolder: false,
                statusPlaceHolder: FAILURE,
                ErrorMessagePlaceHolder: payload,
            };

        default:
            return state;
    }
};


