import {takeLatest, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    CATEGORIES, CATEGORIES_FAIL, CATEGORIES_LOADING, CATEGORIES_SUCCESS,
    LOADING
} from "../../constants/actionsType";
import {config, url} from "../../api/api";

const actionGetCategories = (payload) => {


    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";
    return axios.get(url + 'products/categories/all?' + lang + payload,
        config)
        .then(function (response) {
            console.log("instide req");
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            //////console.log("instide err");
            //////console.log(error.response.data);
            //////console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


function* getCategories({payload}) {
    try {

        yield put({type: CATEGORIES_LOADING});

        //////console.log(payload)
        payload = "";
        const data = yield call(actionGetCategories, payload);

        if (data.status === 200) {

            //////console.log("instide func 200 ");
            //////console.log(data.response)
            yield put({type: CATEGORIES_SUCCESS, payload: data.response});
        }
        else {

            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: CATEGORIES_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

export default function* watcherSaga() {
    yield takeLatest(CATEGORIES, getCategories);
}