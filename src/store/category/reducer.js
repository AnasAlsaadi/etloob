import {
    FAILURE,
    LOADING,
    SUCCESS,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CATEGORIES_FAIL, OPEN_SELECTED_CATEGORY, RESET_SELECTED_CATEGORY,
} from "../../constants/actionsType";

const INITIAL_STATE = {
    currentCategory: null,
    currentVendor: null,
    currentSearch: null,
    isLoading: false,
    status: "",
    ErrorMessage: "",
    categories: [],
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case OPEN_SELECTED_CATEGORY:
            return {
                ...state,
                currentVendor:null,
                currentCategory: payload,
            };
        case RESET_SELECTED_CATEGORY:
            console.log("ffreset")
            return {
                ...state,
                currentCategory:null,
            };
        case CATEGORIES_LOADING:
            return {
                ...state,
                isLoading: true,
                status: LOADING,
            };

        case CATEGORIES_SUCCESS:
            //////console.log("success");
            //////console.log(payload);
            return {
                ...state,
                isLoading: false,
                status: SUCCESS,
                ErrorMessage: '',
                categories: payload.data
            };
        case CATEGORIES_FAIL:
            //////console.log("fail");
            //////console.log(payload);

            return {
                ...state,
                isLoading: false,
                status: FAILURE,
                ErrorMessage: payload,
            };

        default:
            return state;
    }
};


