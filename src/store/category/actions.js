import {
    CATEGORIES, OPEN_SELECTED_CATEGORY, RESET_SELECTED_CATEGORY,
} from "../../constants/actionsType";

export const Categories = payload => ({
    type: CATEGORIES,
    payload,
});

export const openSelectedCategory = category => (
    {
        type: OPEN_SELECTED_CATEGORY,
        payload: category,
    });

export const ResetSelectedCategory = category => (
    {
        type: RESET_SELECTED_CATEGORY,
        payload: category,
    });
