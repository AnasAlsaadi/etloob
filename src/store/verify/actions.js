import {CUSTOMER_AUTH_STATE_RESET, CUSTOMER_VERIFY_PHONE} from "../../constants/actionsType";

export const verifyUser = payload => ({
    type: CUSTOMER_VERIFY_PHONE,
    payload,
});

export const resetAuthState = () => ({
    type: CUSTOMER_AUTH_STATE_RESET,
});

