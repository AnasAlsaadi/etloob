import {takeLatest, call, put} from 'redux-saga/effects';
import axios from "axios"
import {
    CUSTOMER_SIGNUP,
    CUSTOMER_SIGNUP_FAIL, CUSTOMER_SIGNUP_FAILl,
    CUSTOMER_SIGNUP_REQUEST,
    CUSTOMER_SIGNUP_SUCCESS, CUSTOMER_VERIFY_FAIL, CUSTOMER_VERIFY_PHONE, CUSTOMER_VERIFY_SUCCESS,
    LOADING
} from "../../constants/actionsType";
import {config, configForm, url} from "../../api/api";

const actionVerifyPhone = (payload) => {


    return axios.post(url + 'customers/checkCode',
        payload
        , config)
        .then(function (response) {
            ////////console.log("instide req");
            ////////console.log(response);
            ////////console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            ////////console.log("instide err");
            ////////console.log(error.response.data);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}

function* CustomerVerifyPhone({payload}) {
    try {

        yield put({type: LOADING});

        //////console.log(payload)
        const data = yield call(actionVerifyPhone, payload);

        if (data.status === 200) {

            //////console.log("instide func 200 ");
            //////console.log(data.response)
            yield put({type: CUSTOMER_VERIFY_SUCCESS, payload: data.response});
        }
        else {

            //////console.log("instide func 400 ");
            //////console.log(data.message)
            yield put({type: CUSTOMER_VERIFY_FAIL, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

export default function* watcherSaga() {
    yield takeLatest(CUSTOMER_VERIFY_PHONE, CustomerVerifyPhone);
}
