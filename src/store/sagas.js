import {fork} from 'redux-saga/effects';

import authSagas from './auth/sagas'
import homeSagas from './home/sagas'
import verifySagas from './verify/sagas'
import loginSagas from './login/sagas'
import categorySagas from './category/sagas'
import productSagas from './product/sagas'
import cartSagas from './cart/sagas'
import checkoutSagas from './checkout/sagas'
import orderSagas from './order/sagas'
import productListSagas from './ProductList/sagas'
import reviewListSagas from './ReviewList/sagas'
import searchSagas from './search/sagas'
import favoriteSagas from './favorite/sagas'
import profileSagas from './profile/sagas'
import vendorSagas from './vendor/sagas'
import OfferListSagas from './OfferList/sagas'

export default function* root() {

    yield fork(authSagas);
    yield fork(homeSagas);
    yield fork(verifySagas);
    yield fork(loginSagas);
    yield fork(categorySagas);
    yield fork(productSagas);
    yield fork(cartSagas);
    yield fork(checkoutSagas);
    yield fork(orderSagas);
    yield fork(productListSagas);
    yield fork(searchSagas);
    yield fork(favoriteSagas);
    yield fork(profileSagas);
    yield fork(vendorSagas);
    yield fork(reviewListSagas);
    yield fork(OfferListSagas);
}
