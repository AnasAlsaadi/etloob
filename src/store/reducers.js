import {combineReducers} from 'redux';

import authReducer from './auth/reducer';
import homeReducer from './home/reducer';
import verifyReducer from './verify/reducer';
import loginReducer from './login/reducer';
import categoryReducer from './category/reducer'
import cartReducer from './cart/reducer';
import productReducer from './product/reducer';
import checkoutReducer from './checkout/reducer';
import orderReducer from './order/reducer';
import productListReducer from './ProductList/reducer';
import reviewListReducer from './ReviewList/reducer';
import searchReducer from './search/reducer';
import favoriteReducer from './favorite/reducer';
import profileReducer from './profile/reducer';
import vendorReducer from './vendor/reducer';
import OfferListReducer from './OfferList/reducer';

export default combineReducers({
    auth: authReducer,
    home: homeReducer,
    product: productReducer,
    verify: verifyReducer,
    login: loginReducer,
    category: categoryReducer,
    cart: cartReducer,
    checkout: checkoutReducer,
    order: orderReducer,
    productList: productListReducer,
    search: searchReducer,
    favorite: favoriteReducer,
    profile: profileReducer,
    vendor: vendorReducer,
     reviewList: reviewListReducer,
    offerList: OfferListReducer,
});
