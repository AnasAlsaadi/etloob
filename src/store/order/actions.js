import {
    OPEN_SELECTED_ORDER, OPEN_SELECTED_PRODUCT, ORDER_LIST, ORDER_LIST_RESET, PRODUCT_LIST_RESET
} from "../../constants/actionsType";

// export const openSelectedOrder = order => (
//     {
//         type: OPEN_SELECTED_ORDER,
//         payload: order,
//     });

export const orderLists = payload => (
    {
        type: ORDER_LIST,
        payload: payload,
    });

export const orderListsRest = payload => (
    {
        type: ORDER_LIST_RESET,
        payload: payload,
    });

export const openSelectedOrder = payload => (
    {
        type: OPEN_SELECTED_ORDER,
        payload: payload,
    });

