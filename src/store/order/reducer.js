import {
    FAILURE,
    LOADING,
    SUCCESS,
    OPEN_SELECTED_ORDER,
    ORDER_LIST_LOADING,
    ORDER_LIST_SUCCESS,
    ORDER_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET, OPEN_SELECTED_PRODUCT, ORDER_LIST_RESET
} from "../../constants/actionsType";

const getInitialState = order => ({
    current: order,
    isLoading: false,
    isLoadingPro: false,
    status: '',
    orderList: [],
    productList: [],
});

export default (state = getInitialState(null), action) => {
    switch (action.type) {
        case OPEN_SELECTED_ORDER:
            //////console.log("select item")
            //////console.log(action)
            //////console.log(action.payload)
            return {
                ...state,
                current: action.payload,
            };
        case ORDER_LIST_LOADING:
            return {
                ...state,
                isLoadingPro: true,
                status: LOADING,
            };


        case ORDER_LIST_SUCCESS:
            //////console.log("success");
            //////console.log(action.payload);
            if (action.payload.refresh) {

                return {
                    ...state,
                    isLoadingPro: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    orderList: action.payload.data
                };
            }
            else {

                return {
                    ...state,
                    isLoadingPro: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    orderList: [...state.orderList, ...action.payload.data]
                };
            }
        case ORDER_LIST_FAIL:
            //////console.log("fail");
            //////console.log(action.payload);

            return {
                ...state,
                isLoadingPro: false,
                status: FAILURE,
                ErrorMessage: action.payload,
            };


        case ORDER_LIST_RESET:
            // console.log("-------all- reset-------")
            // console.log(getInitialState(null))
            return getInitialState(null)


        default:
            return state;
    }
};
