import {takeLatest, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native'
import axios from "axios"
import {
    ORDER_LIST, ORDER_LIST_FAIL, ORDER_LIST_LOADING, ORDER_LIST_SUCCESS, PRODUCT_LIST_SUCCESS,

} from "../../constants/actionsType";
import {config, url} from "../../api/api";

const actionGetOrderList = (payload) => {


    //////console.log("get orders")
    //////console.log(payload)
    //console.log(url + 'orders/?'+ "customer=" + global.user.id + "&per_page=25" + "&page=" + payload.page)
    var lang = I18nManager.isRTL ? "lang=ar&" : "lang=en&";

    var urls = "";
    if (payload.type_r && payload.type_r != null && payload.type_r === "vendor") {
        urls = "https://etloob.com/wp-json/wc/v2/orders?vendor_id=" + global.user.id + payload.getAttr + "&per_page=10" + "&page=" + payload.page;
    }
    else {
        urls = url + 'orders/?' + "customer=" + global.user.id + "&parent=0&per_page=25" + "&page=" + payload.page
    }


    console.log(urls)
    return axios.get(urls,
        config)
        .then(function (response) {
            console.log("instide req");
            console.log(response);
            console.log("instide req");
            return {status: 200, response: response.data, message: ""}
        })
        .catch(function (error) {
            console.log("instide err");
            console.log(error.response.data);
            console.log(error);
            return {status: 400, response: null, message: error.response.data.message}
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
}


function* getOrderList({payload}) {
    try {


        //////console.log("get orders")
        //////console.log("get orders fn")
        yield put({type: ORDER_LIST_LOADING});

        console.log("get orders fn1")
        console.log(payload)
        const data = yield call(actionGetOrderList, payload);

        if (data.status === 200) {
            //
            ////console.log("instide func 200 ");
            ////console.log(data.response)
            yield put({type: ORDER_LIST_SUCCESS, payload: {refresh: payload.refresh, data: data.response}});
        }
        else {
            //
            ////console.log("instide func 400 ");
            ////console.log(data.message)
            yield put({type: ORDER_LIST_FAIL, payload: data.message});
        }

    } catch (error) {
        ////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

export default function* watcherSaga() {
    yield takeLatest(ORDER_LIST, getOrderList);
}
