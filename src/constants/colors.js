export default {
    CustomBlack: '#42413d',
    Gray: '#e2e2e1',
    GrayTransparent: '#d4d4d4',
    GrayBorder: '#dadad991',
    white: '#fff',
    yellow: '#ffce2b',
    yellowBorder: '#ffdb6d',
    yellowTransparent: '#ffd140e0',
    red: '#f01518',
    blue: '#2da7dc',
    green: '#56be5d',

};
