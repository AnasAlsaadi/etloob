import colors from './colors';
import {heightPercentageToDP as hp ,widthPercentageToDP as wp} from "react-native-responsive-screen";
import Typography from './typography'
import {I18nManager} from 'react-native'
export default {

    opacity05:{

        opacity: 0.5,
    } ,
    Yellow:{
        color:colors.yellow
    },
    input: {
        borderBottomWidth: 1,
        borderColor: '#909090',
        height: wp('10%'),
        textAlign: I18nManager.isRTL?'right':'left',
        writingDirection: I18nManager.isRTL ? 'rtl' : 'ltr'
    },
    spinnerText:{
        ...Typography.Text30Bold,
        color:"white"
    }
};
