import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Text, ScrollView, Image, TouchableOpacity, View, TextInput, Platform} from 'react-native'
import Typography from './../../../constants/typography'
import AppStyles from "../../../constants/AppStyles";
import AuthHeader from './../../../containers/AuthHeader'
import {InputElement, SubmitButton, TextUnderLine} from '../../elements'
import CodeInput from 'react-native-confirmation-code-input';
import {NAVIGATION_DRAWER} from './../../../navigation/types'
import strings from "./../../../locales/i18n"
import {resetAuthState, verifyUser} from "../../../store/verify/actions";
import {connect} from "react-redux";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {NAVIGATION_VERIFICATION_SCREEN} from "../../../navigation/types";
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from "react-native-root-toast";
import axios from "axios";

class Verification extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    user = null

    constructor(props) {
        super(props);

        this.state = {
            code: null
        }


        //////console.log("user id retrived in verfify ")
        this.user = global.user
        //////console.log(this.user)
        this.renderVerMessages = this.renderVerMessages.bind(this)
    }

    componentDidMount() {


    }

    _onFinishCheckingCode1(code) {
        //////console.log(code)

        this.setState({code: code})


    }


    renderVerMessages() {

        if (this.props.status === SUCCESS) {


            //////console.log("user data ver")
            //////console.log(this.state)
            //////console.log(this.user)
            // global.storage.remove({
            //     key: 'user'
            // });

            global.storage.save({
                key: 'user',
                data: {user: this.user, logged: true, is_active: true},
                expires: null
            })

            global.logged = true
            global.is_active = true
            //////console.log(global.is_active);
            //////console.log(global.logged);
            //////console.log("true");


            //////console.log("after true");

            axios.post('https://etloob.com/wp-json/jwt-auth/v1/notifications_token',
                {
                    user_id: this.user.id,
                    os: Platform.OS,
                    token: global.token + "",
                }
                , null)
                .then(function (response) {
                    console.log("firebase save  req");
                    // console.log(response);
                    // console.log("instide req");
                    return {status: 200, response: response.data, message: ""}
                })
                .catch(function (error) {
                    console.log("firebase save  err");
                    // console.log(error.response.data);
                    return {status: 400, response: null, message: error.response.data.message}
                });


            this.props.reset()
            this.props.navigation.navigate(NAVIGATION_DRAWER);

        }

        if (this.props.status === FAILURE) {

            Toast.show(this.props.ErrorMessage, {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            this.props.reset()


        }

    }


    render() {

        return (

            <ScrollView style={{flex: 1,}} showsHorizontalScrollIndicator={false}>

                <AuthHeader {...this.props} noBack={true} isLoading={this.props.isLoading}>

                    <Grid>
                        <Row size={1.5} style={styles.container}>

                            <Text style={[Typography.Text30Bold, AppStyles.Yellow]}>{strings.t("verification")}</Text>

                            <Text
                                style={[Typography.Text12Regular, {paddingTop: hp("2%")}]}>{strings.t("digit_number")}</Text>
                            <Text style={[Typography.Text12Regular]}>{strings.t("enter_it_to_continue")}</Text>

                            <CodeInput
                                ref="codeInputRef2"
                                activeColor='rgb(5, 5, 5)'
                                inactiveColor='rgb(5, 5, 5)'
                                autoFocus={false}
                                ignoreCase={true}
                                inputPosition='center'
                                size={40}
                                codeLength={5}
                                // compareWithCode='12345'
                                containerStyle={{marginTop: 30}}
                                onFulfill={(code) => this._onFinishCheckingCode1(code)}
                                codeInputStyle={styles.CodeInputStyle}
                            />

                            <SubmitButton text={strings.t("continue")} onPress={() => {


                                //////console.log("inside onPresss");
                                //////console.log(this.user);
                                //////console.log(this.state.code);

                                this.props.verifyPhone({customer_id: this.user.id, verify_code: this.state.code})

                                // this.props.navigation.navigate(NAVIGATION_DRAWER);
                            }}/>
                        </Row>
                        {/*<Row size={0.2} style={styles.containerFooter}>*/}
                        {/*<TextUnderLine textStyle={'medium'} text={strings.t("resend_code")}/>*/}
                        {/*</Row>*/}
                        <Row size={2.3}/>
                    </Grid>


                    {this.renderVerMessages()}

                </AuthHeader>
            </ScrollView>


        );
    }
}

const styles = {

    container: {
        flexDirection: 'column', backgroundColor: 'white', padding: hp("2%")
    }, containerFooter: {
        justifyContent: 'center', paddingTop: hp("2%")
    },
    CodeInputStyle: {
        borderWidth: 1.5, borderColor: '#e4e4e4', borderRadius: 4
    }
}


const mapStateToProps = state => {
    return {
        isLoading: state.verify.isLoading,
        status: state.verify.status,
        ErrorMessage: state.verify.ErrorMessage,
        //user: state.auth.user,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        verifyPhone: (payload) => dispatch(verifyUser(payload)),
        reset: (payload) => dispatch(resetAuthState(payload))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Verification);
