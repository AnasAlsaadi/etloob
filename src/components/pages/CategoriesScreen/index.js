import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Switch, StyleSheet, Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'

import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import {FeatureProduct, RecentProduct, TopCategories, TrendingProduct} from "../../../store/home/actions";
import {openSelectedProduct, resetProductList} from "../../../store/product/actions";
import {connect} from "react-redux";
import {Categories, openSelectedCategory} from "../../../store/category/actions";
import Typeography from "../../../constants/typography"
import Color from "../../../constants/colors"
import {TagSelect} from 'react-native-tag-select';
import TagList from "./../../elements/TagList"
import {NAVIGATION_PRODUCTS_SCREEN} from "../../../navigation/types";
import TabedFooter from "../../template/TabedFooter";
import {ResetSelectedSearchWord} from "../../../store/search/actions";
import {ResetSelectedVendor} from "../../../store/vendor/actions";

class CategoriesScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};


    constructor(props) {
        super(props);
        this.state = {
            i: 1, Collapsed: false,
            selectedInside: [],
            isCollapsed: true, activeSections: [], multipleSelect: false,
            sections: [
                {
                    title: 'First',
                    content: 'Lorem ipsum...',
                },
                {
                    title: 'Second',
                    content: 'Lorem ipsum...',
                },
            ]


        }
        this.renderContent = this.renderContent.bind(this)
        this.renderSectionContent = this.renderSectionContent.bind(this)
        this.Expand = this.Expand.bind(this)
        this.onPressCategory = this.onPressCategory.bind(this)

        // this.renderContent = this.renderContent.bind(this)

    }

    componentDidMount() {

        this.props.getCategories({})
    }


    toggleExpanded = () => {
        this.setState({collapsed: !this.state.collapsed});
    };

    setSections = sections => {
        this.setState({
            activeSections: sections.includes(undefined) ? [] : sections,
        });
    };

    renderHeader = (section, _, isActive) => {
        return (
            <Grid
                duration={100}
                style={[styles.header, isActive ? styles.active : styles.inactive]}
                transition="backgroundColor"
            >
                <Col size={3.5}>
                    <Text style={[isActive ? styles.headerTextActive : styles.headerText]}>{section.name}</Text>
                </Col>
                <Col size={0.5} style={styles.icon}>
                    {isActive ?
                        <Image
                            style={{}}
                            source={require('./../../../assets/images/arrow_top.png')}
                        />
                        :
                        <Image
                            source={require('./../../../assets/images/arrow_bottom.png')}
                        />}
                </Col>
            </Grid>
        );
    };

    Expand(item) {

        //////console.log("clicked")
        //////console.log(item)
        var indexes = this.state.selectedInside
        // indexes=[]
        // indexes.push(item.id)
        // //////console.log(indexes)
        // //////console.log(this.state.selectedInside)
        // this.setState({selectedInside: indexes})
        if (indexes.indexOf(item.term_id) !== -1) {

            indexes.splice(indexes.indexOf(item.term_id), 1)
            this.setState({selectedInside: indexes})

            //////console.log("delete")
            //////console.log(indexes)
            //////console.log(this.state.selectedInside)
        }
        else {

            indexes.push(item.term_id)
            this.setState({selectedInside: indexes})

            //////console.log("new")
            //////console.log(indexes)
            //////console.log(this.state.selectedInside)
        }
    }

    ifSelected(id) {

        var indexes = this.state.selectedInside
        if (indexes.indexOf(id) !== -1) {

            return true
        }
        else {
            return false
        }
    }


    renderContent(section, _, isActive) {


        return (
            <View>
                <TouchableOpacity onPress={this.toggleExpanded}>
                    <View style={styles.header}>
                        <Text style={styles.headerText}>Single Collapsible</Text>
                    </View>
                </TouchableOpacity>
                <Collapsible collapsed={this.state.collapsed} align="center">
                    <View style={styles.content}>
                        <Text>
                            Bacon ipsum dolor amet chuck turducken landjaeger tongue spare
                            ribs
                        </Text>
                    </View>
                </Collapsible>
            </View>

        );
    }

    onPressCategory(category) {

        //////console.log("==============================")
        //////console.log(category)
        this.props.resetProductListAll({})
        this.props.ResetSelectedSearchWord(null)
        this.props.ResetSelectedVendor(null)


        this.props.selectedCategory({id: category.term_id})
        this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);

    }


    renderSectionContent(section, index) {


        var insides_sections = [];
        if (section.sub_categories.length > 0) {

            section.sub_categories.map((item, indexInside) => {

                const inside = (
                    <View key={"inside" + indexInside}>
                        <TouchableOpacity onPress={() => {
                            // this.Expand.bind(this, item)
                        }}>

                            <Grid
                                style={[styles.headerInside, !this.ifSelected(item.term_id) ? styles.activeInside : styles.inactive]}

                            >
                                <Col size={3.5}>
                                    <Text
                                        style={[!this.ifSelected(item.term_id) ? styles.headerTextActive : styles.headerText]}>{item.name}</Text>
                                </Col>
                                <Col size={0.5} style={styles.icon}>
                                    {this.ifSelected(item.term_id) ?
                                        <Image
                                            style={{}}
                                            source={require('./../../../assets/images/arrow_bottom_white.png')}
                                        />
                                        :
                                        <Image
                                            source={require('./../../../assets/images/arrow_top_white.png')}
                                        />}
                                </Col>
                            </Grid>
                        </TouchableOpacity>
                        <Collapsible collapsed={this.ifSelected(item.term_id)} align="center">
                            <View style={styles.content}>

                                <TagList

                                    setCategory={(category) => this.onPressCategory(category)}

                                    items={item.sub_categories}/>

                            </View>
                        </Collapsible></View>
                )

                insides_sections.push(inside)
            })

            return <View>{insides_sections}</View>
        }

    }

    render() {

        return (


            <DefaultHeader type={"home"} {...this.props}>

                <Grid>
                    <Row>

                        <View style={styles.container}>
                            <ScrollView contentContainerStyle={{paddingTop: 10}}>

                                <Accordion
                                    activeSections={this.state.activeSections}
                                    sections={this.props.categories}
                                    touchableComponent={TouchableOpacity}
                                    expandMultiple={this.state.multipleSelect}
                                    renderHeader={this.renderHeader}
                                    renderContent={this.renderSectionContent}
                                    duration={100}
                                    onChange={this.setSections}
                                />
                            </ScrollView>
                        </View>


                    </Row>
                </Grid>


            </DefaultHeader>


        );
    }
}


const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: Color.white,
        paddingStart: wp("4%"),
        paddingTop: wp("3%"),
        paddingBottom: wp("3%"),
        marginBottom: wp("1%"),
        marginTop: wp("1%"),
        marginStart: wp("4%"),
        marginEnd: wp("4%"),
        borderColor: Color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8

    }, headerInside: {
        backgroundColor: Color.white,
        paddingStart: wp("4%"),
        paddingTop: wp("3%"),
        paddingBottom: wp("3%"),
        marginBottom: wp("1%"),
        marginTop: wp("1%"),
        marginStart: wp("4%"),
        marginEnd: wp("4%"),
        borderColor: Color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8

    },
    headerText: {
        textAlign: I18nManager.isRTL ? 'right' : 'left',
        fontSize: 16,
        color: Color.CustomBlack,
        ...Typeography.Text16Light
    },
    headerTextActive: {
        textAlign: I18nManager.isRTL ? 'right' : 'left',
        fontSize: 16,
        color: Color.white,
        ...Typeography.Text16Light
    },
    content: {
        paddingTop: wp("1%"),
        paddingStart: wp("1%"),
        paddingEnd: wp("1%"),
        paddingBottom: wp("1%"),
        backgroundColor: '#fff',
        marginStart: wp("4%"),
        marginEnd: wp("4%"),
    },
    active: {
        backgroundColor: Color.yellow,
    },
    activeInside: {
        backgroundColor: Color.CustomBlack,
    },
    inactive: {
        backgroundColor: Color.white,
    },
    selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    selector: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    activeSelector: {
        fontWeight: 'bold',
    },
    selectTitle: {
        fontSize: 14,
        fontWeight: '500',
        padding: 10,
    },
    multipleToggle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 30,
        alignItems: 'center',
    },
    multipleToggle__title: {
        fontSize: 16,
        marginRight: 8,
    },
    icon: {
        paddingEnd: wp('3%'), alignItems: 'flex-end', justifyContent: 'center'
    },
});


const mapStateToProps = state => {
    return {
        isLoading: state.category.isLoading,
        status: state.category.status,
        categories: state.category.categories,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        selectedCategory: (payload) => dispatch(openSelectedCategory(payload)),
        resetProductListAll: (payload) => dispatch(resetProductList(payload)),
        getCategories: (payload) => dispatch(Categories(payload)),
        ResetSelectedSearchWord: (payload) => dispatch(ResetSelectedSearchWord(payload)),
        ResetSelectedVendor: (payload) => dispatch(ResetSelectedVendor(payload)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesScreen);