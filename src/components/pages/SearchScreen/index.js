import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    Text,
    FlatList,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    I18nManager
} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import colors from '../../../constants/colors'
import {productLists} from "../../../store/ProductList/actions";
import {addToCart, addToFavorite, openSelectedProduct} from "../../../store/product/actions";
import {connect} from "react-redux";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import {NAVIGATION_PRODUCT_SCREEN, NAVIGATION_PRODUCTS_SCREEN} from "../../../navigation/types";
import {openSelectedSearchWord, productListsSearch, productListsSearchReset} from "../../../store/search/actions";
import InputElement from "../../elements/TextInput";
import TabedFooter from "../../template/TabedFooter";
import Toast from "react-native-root-toast";
import {SUCCESS} from "../../../constants/actionsType";
import Typography from './../../../constants/typography'
import TagList from "../../elements/TagList";
import strings from './../../../locales/i18n'
import Product from "../../elements/Product";
import Swiper from "react-native-swiper";
import {relatedAndMoreProduct} from "../../../store/home/actions";
import color from '../../../constants/colors';
import Highlighter from 'react-native-highlight-words';
import RNPickerSelect from "react-native-picker-select";
import {ResetSelectedCategory} from "../../../store/category/actions";
import {ResetSelectedVendor} from "../../../store/vendor/actions";

class SearchScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    textInputSearch = null

    constructor(props) {
        super(props);
        this.state = {
            searchWord: '',
            order_by: '',
            searchType: 'write',
            typing: false,
            typingTimeout: 0
        }
    }

    componentDidMount() {
        this.props.resetSearchData()
        //   this.props.getRelatedProducts({product_id: 0})

    }

    onPressProductT(product) {

        ////console.log("pressed ")
        ////console.log(product)

        //////console.log(product)
        this.props.selectedProduct(product)
        this.props.navigation.navigate(NAVIGATION_PRODUCT_SCREEN);


    }

    onPressProductAddToCart(item) {

        ////console.log("add to cart ")
        ////console.log(item)

        if (item.attributes.length > 0) {

            let toast = Toast.show(strings.t("please_choose_attr"), {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,

            });
            setTimeout(function () {
                Toast.hide(toast);
            }, 1000);


        } else {

            this.props.addProductToCart(item)
        }

    }

    SearchCode() {


        const self = this;

        if (self.state.typingTimeout) {
            clearTimeout(self.state.typingTimeout);
        }

        self.setState({
            typingTimeout: setTimeout(function () {

                if (this.state.searchWord.length > 2) {

                    this.resetSearchOnSearching()
                    this.setState({searchType: "write"})

                    if (this.state.order_by !== null && this.state.order_by.length > 0) {
                        this.props.getProductsSearch({search: this.state.searchWord, order_by: this.state.order_by})

                    }
                    else {
                        this.props.getProductsSearch({search: this.state.searchWord})

                    }


                }
            }.bind(this), 200)
        });


        ////console.log("search")
    }

    SearchCodeByFilter() {


        ///
        ////console.log("search")

        if (this.state.searchWord.length > 2) {

            // this.resetSearchOnSearching()
            //console.log(this.state.order_by)
            if (this.state.order_by !== null && this.state.order_by.length > 0) {
                this.props.getProductsSearch({search: this.state.searchWord, order_by: this.state.order_by})

                // this.props.getProductsSearch({search: this.state.searchWord,order_by:this.state.order_by})

            }
            else {
                this.props.getProductsSearch({search: this.state.searchWord})

                // this.props.getProductsSearch({search: this.state.searchWord})

            }


        }


        ////console.log("search")
    }

    SearchCodeA() {

        ////console.log("search")

        if (this.state.searchWord.length > 2) {

            this.resetSearchOnSearching()
            this.setState({searchType: "button"})
            var serches = global.recentSearches
            serches.unshift({name: this.state.searchWord})

            global.storage.save({
                key: 'recentSearches', data: serches
            })
            global.recent_searches = serches

            //console.log(this.state.order_by)
            if (this.state.order_by !== null && this.state.order_by.length > 0) {
                this.props.getProductsSearch({search: this.state.searchWord, order_by: this.state.order_by})

            }
            else {
                this.props.getProductsSearch({search: this.state.searchWord})

            }

        }


        ////console.log("search")
    }

    resetSearch() {

        this.setState({searchType: 'write'})
        this.setState({searchWord: ''})
        this.props.resetSearchData()
    }

    resetSearchOnSearching() {

        this.props.resetSearchData()
    }

    onPressAddToCartPR(product) {


        ////console.log("add to cart from product page ")
        this.props.addProductToCartPR(product)

    }

    onPressAddToFavorite(product) {

        //////console.log("addto cart from home screen ")
        this.props.addProductToFavorite(product)

    }

    onPressProduct(product) {

        //////console.log(product)
        this.props.selectedProduct(product)
        this.props.navigation.navigate({
            routeName: NAVIGATION_PRODUCT_SCREEN,
            parms: {current: product, ...this.props},
            key: 'product' + product.id
        });

    }

    renderSwiperProducts(isLoading, ListAll) {

        //console.log(isLoading)
        //console.log(ListAll)
        var grids = [];
        if (isLoading) {

            return (
                <Grid>
                    <Row style={[styles.CenterContent]}>
                        <Text>Loading...</Text>
                    </Row>
                </Grid>)
        } else {

            if (!ListAll) return null

            if (I18nManager.isRTL) {
                for (var i = Math.ceil(ListAll.length / 4) - 1; i >= 0; i--) {
                    var grid = (
                        <Grid key={"grid" + i}>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 0) ?
                                        <Product key={((i * 4) + 0)} product={ListAll[((i * 4) + 0)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 0)])}

                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 1) ?
                                        <Product key={((i * 4) + 1)} product={ListAll[((i * 4) + 1)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 1)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 1)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 1)])}
                                        /> : null}

                                </Row>
                            </Col>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 2) ?
                                        <Product key={((i * 4) + 2)} product={ListAll[((i * 4) + 2)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 2) + 2)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 2)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 2)])}
                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 3) ?
                                        <Product key={((i * 4) + 3)} product={ListAll[((i * 4) + 3)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 3) + 3)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 3)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 3)])}
                                        /> : null}

                                </Row>
                            </Col>
                        </Grid>)

                    grids.push(grid)
                }
            }
            else {

                for (var i = 0; i < Math.ceil(ListAll.length / 4); i++) {

                    var grid = (
                        <Grid key={"grid" + i}>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 0) ?
                                        <Product key={((i * 4) + 0)} product={ListAll[((i * 4) + 0)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 0)])}

                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 1) ?
                                        <Product key={((i * 4) + 1)} product={ListAll[((i * 4) + 1)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 1)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 1)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 1)])}
                                        /> : null}

                                </Row>
                            </Col>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 2) ?
                                        <Product key={((i * 4) + 2)} product={ListAll[((i * 4) + 2)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 2) + 2)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 2)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 2)])}
                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 3) ?
                                        <Product key={((i * 4) + 3)} product={ListAll[((i * 4) + 3)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 3) + 3)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 3)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 3)])}
                                        /> : null}

                                </Row>
                            </Col>
                        </Grid>)

                    grids.push(grid)
                }
            }

            return (<Swiper style={{
                // flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row'

            }}
                            dot={<View
                                style={{
                                    backgroundColor: color.CustomBlack,
                                    width: 8, height: 8, borderRadius: 4,
                                    marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                    marginBottom: -hp("9%"),
                                }}/>}
                            activeDot={<View
                                style={{
                                    backgroundColor: color.yellow,
                                    width: 8, height: 8, borderRadius: 4,
                                    marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                    marginBottom: -hp("9%"),
                                }}/>}
                            loop={false}
                            showsButtons={false}>
                {grids}

            </Swiper>)

        }
    }


    openSearchProducts = () => {

        this.props.ResetSelectedCategory(null)
        this.props.ResetSelectedVendor(null)
        this.props.openSelectedSearchWord(this.state.searchWord)
        // this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);
        this.props.navigation.navigate({
            routeName: NAVIGATION_PRODUCTS_SCREEN,
            parms: {...this.props},
            key: 'productsMore' + this.state.searchWord


        });


    }
    render_footer_show_more = () => {

        var footer_View = (

            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}>
                <TouchableOpacity
                    onPress={this.openSearchProducts}
                    style={[Typography.Text14Light, {flex: 1, padding: wp("3%"),}]}><Text
                    style={[Typography.Text14Light, styles.textStyle]}>{strings.t("see_more")}</Text></TouchableOpacity>

            </View>

        );


        return footer_View;
    }


    _listEmptyComponent = () => {
        return (<Text style={{textAlign: 'center', padding: wp("6%")}}>{strings.t("empty_result")}</Text>)
    }


    render() {

        return (


            <DefaultHeader hideNavigationBar={true} type={"search"} {...this.props}>

                <View style={{flex: 1,}}>
                    <View style={{
                        flex: 1, flexDirection: "row", alignItems: 'center', justifyContent: "center",
                        backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                    }}>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>

                            <TouchableOpacity onPress={() => {
                                this.SearchCodeA()
                            }}>
                                <Image
                                    source={require("../../../assets/images/search.png")}
                                />
                            </TouchableOpacity>

                        </View>
                        <View style={{flex: 2,}}>

                            <TextInput style={[Typography.Text14Light, {
                                flex: 1,
                                paddingLeft: wp("3%"),
                                paddingRight: wp("3%"),
                                textAlign: I18nManager.isRTL ? "right" : "auto",
                                paddingStart: wp("3%"),
                                paddingEnd: wp("3%")
                            }]}
                                       returnKeyType={'search'}

                                       keyboardType={'default'}
                                       autoFocus={true}

                                       onSubmitEditing={() => {
                                           this.SearchCodeA()
                                       }}


                                       ref={(ref) => {
                                           this.textInputSearch = ref
                                       }}
                                       onKeyPress={(value) => {

                                           console.log(this.textInputSearch.props.value)

                                           this.setState({searchWord: this.textInputSearch.props.value}, function () {


                                               this.SearchCode()
                                           }.bind(this))

                                       }}
                                       onChangeText={(value) => {


                                           this.textInputSearch.props.value = value

                                           this.setState({searchWord: value}, function () {
                                               this.SearchCode()
                                           }.bind(this))

                                       }}
                                       value={this.state.searchWord}
                                       placeholder={strings.t("type_here")}/>
                        </View>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <TouchableOpacity style={[styles.CenterContentBottom, {padding: wp("3%")}]} onPress={() => {
                                this.resetSearch()
                            }}>
                                <Image
                                    source={require("../../../assets/images/close.png")}
                                />
                            </TouchableOpacity>

                        </View>
                    </View>
                    <View style={{
                        flex: 1, flexDirection: "row", alignItems: 'center',
                        paddingStart: wp("6%"),
                        paddingEnd: wp("6%"),
                        backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                    }}>
                        <View style={{flex: 2}}>
                            <Text
                                style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("order_by")}: </Text>
                        </View>
                        <View style={{flex: 2}}>
                            <RNPickerSelect
                                style={{marginTop: wp("3%"), marginBottom: wp("3%")}}
                                placeholder={this.oj}
                                onValueChange={(value) => {

                                    this.setState({order_by: value}, function () {
                                        this.SearchCodeByFilter()
                                    }.bind(this))

                                    ////console.log(value)
                                }}
                                items={[
                                    {label: strings.t("Popularity"), value: 'popularity'},
                                    {label: strings.t("Average_rating"), value: 'rating'},
                                    {label: strings.t("Newness"), value: 'date'},
                                    {label: strings.t("P_l_t_h"), value: 'price'},
                                    {label: strings.t("P_h_t_l"), value: 'price-desc'},
                                ]}
                            />
                        </View>
                    </View>
                    <View style={{}}>
                        {
                            this.props.isLoadingj &&
                            <Text style={{textAlign: 'center', padding: wp("6%")}}>{strings.t("loading")}</Text>
                        }
                        {
                            this.props.productList &&
                            <FlatList
                                ListEmptyComponent={this.props.productList!=null &&  this.props.productList.length==0 ?this._listEmptyComponent:null}

                                ListFooterComponent={this.props.productList.length > 0 && this.render_footer_show_more}
                                style={{backgroundColor: 'white'}}
                                keyExtractor={(item, index) => index.toString()}
                                data={this.props.productList}
                                // renderItem={({item}) => (
                                //     <ProductHorizontal product={item} onPress={this.onPressProductT.bind(this, item)}
                                //                        onPressAddToCart={this.onPressProductAddToCart.bind(this, item)}/>
                                // )}
                                renderItem={({item}) => {


                                    return this.state.searchType === "write" ? <TouchableOpacity
                                        product={item} onPress={this.onPressProductT.bind(this, item)}
                                        onPressAddToCart={this.onPressProductAddToCart.bind(this, item)}
                                        style={{
                                            backgroundColor: 'white',
                                            paddingStart: wp("6%"),
                                            paddingTop: wp("3%"),
                                            paddingBottom: wp("3%"),
                                            marginStart: wp("4%"),
                                            marginLeft: wp("3%"),
                                            // marginBottom: wp("1%"),
                                            marginTop: wp("1%"),
                                            // borderRadius: 5,
                                            // borderColor: color.GrayBorder,
                                            //
                                            // borderWidth: 1,
                                            // shadowColor: color.GrayBorder,
                                            // shadowOpacity: 0.8,
                                            // shadowRadius: 3,
                                            // shadowOffset: {
                                            //     height: 1,
                                            //     width: 1
                                            // }
                                        }}><Text style={[Typography.Text18Light, {color: '#9c9c9c'}]}>
                                        <Highlighter
                                            product={item}
                                            highlightStyle={{color: color.CustomBlack}}
                                            searchWords={[this.state.searchWord.toString()]}
                                            textToHighlight={item.name.toString()}
                                        /></Text></TouchableOpacity> : <ProductHorizontal product={item}
                                                                                          onPress={this.onPressProductT.bind(this, item)}
                                                                                          onPressAddToCart={this.onPressProductAddToCart.bind(this, item)}/>
                                }}


                                // keyExtractor={i => i.id.toString()}
                                refreshing={this.props.isLoading}
                                // onRefresh={this.handleRefresh}
                                // onEndReached={this.handleLoadMore}
                                onEndThreshold={0}
                            />
                        }

                    </View>
                    {/*<View style={{padding: wp("4%")}}>*/}
                    {/*<View style={{paddingBottom: wp("3%")}}><Text*/}
                    {/*style={[Typography.Text16Light, {}]}>{strings.t("recent_searches")}</Text></View>*/}
                    {/*<View style={{}}>*/}

                    {/*<TagList*/}
                    {/*setCategory={(category) => {*/}
                    {/*}}*/}

                    {/*items={global.recentSearches}/>*/}

                    {/*</View>*/}
                    {/*</View>*/}


                    {/*<View style={{padding: wp("4%")}}>*/}
                    {/*<View style={{paddingBottom: wp("3%")}}><Text*/}
                    {/*style={[Typography.Text16Light, {}]}>{strings.t("recommended")}</Text></View>*/}
                    {/*<View style={{}}>*/}


                    {/*<Grid style={[styles.paddingSectionPro, {height: hp("90%")}]}>*/}

                    {/*{this.renderSwiperProducts(this.props.isLoadingRelated, this.props.relatedList)}*/}

                    {/*</Grid>*/}


                    {/*</View>*/}
                    {/*</View>*/}


                </View>


                <Toast
                    visible={this.props.statusAddToCart === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{this.props.ErrorMessageADDProductToCart}</Toast>


                <Toast
                    visible={this.props.statusAddToCart === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{this.props.ErrorMessageADDProductToCart}</Toast>


                <Toast
                    visible={this.props.statusAddToFavorite === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{strings.t("success_favorite")}</Toast>

            </DefaultHeader>


        );
    }


}


const styles = StyleSheet.create({
    ContainerItem: {
        padding: 4,

    }
    ,
    CenterContentBottom: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end'
    }, paddingSectionPro: {

        paddingStart: wp("2%"),
        paddingEnd: wp("2%"),
        paddingTop: wp("2%"),
    },
});


const mapStateToProps = state => {
    return {
        productList: state.search.productList,
        // isLoading: state.search.isLoading,
        isLoadingj: state.search.isLoadingj,
        status: state.search.status,
        errorMessage: state.search.errorMessage,
        ErrorMessageADDProductToCart: state.search.ErrorMessageADDProductToCart,
        statusAddToCart: state.search.statusAddToCart,
        isLoadingRelated: state.home.isLoadingRelated,
        relatedList: state.home.relatedList,
        statusRelated: state.home.statusRelated,
        statusAddToFavorite: state.product.statusAddToFavorite,


    }
}
const mapDispatchToProps = dispatch => {
    return {
        // getRelatedProducts: (payload) => dispatch(relatedAndMoreProduct(payload)),
        getProductsSearch: (payload) => dispatch(productListsSearch(payload)),
        resetSearchData: (payload) => dispatch(productListsSearchReset(payload)),
        //  selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        addProductToCart: (payload) => dispatch(addToCart(payload)),
        addProductToCartPR: (payload) => dispatch(addToCart(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        addProductToFavorite: (payload) => dispatch(addToFavorite(payload)),

        ResetSelectedCategory: (payload) => dispatch(ResetSelectedCategory(payload)),

        ResetSelectedVendor: (payload) => dispatch(ResetSelectedVendor(payload)),
        openSelectedSearchWord: (payload) => dispatch(openSelectedSearchWord(payload)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)

(
    SearchScreen
)
;
