import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    Dimensions,
    I18nManager,
    StyleSheet,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    ImageBackground, Platform
} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import Slideshow from 'react-native-slideshow';
import color from '../../../constants/colors';
import Typography from './../../../constants/typography'
import {Product, CategoryHome, ProductHorizontal, ShowMore, Vendor} from '../../elements'
import {TabedFooter} from '../../template'
import Swiper from 'react-native-swiper'
import {
    NAVIGATION_APP,
    NAVIGATION_DRAWER,
    NAVIGATION_PRODUCT_SCREEN,
    NAVIGATION_PRODUCTS_SCREEN
} from "../../../navigation/types";
import strings from "./../../../locales/i18n"
import {CustomerSignUp} from "../../../store/auth/actions";
import {connect} from 'react-redux';
// import Toast, {DURATION} from 'react-native-easy-toast'
import Toast from 'react-native-root-toast'


import {
    FeatureProduct,
    RecentProduct,
    TrendingProduct,
    TopCategories,
    HomeCategories, vendors, vendorsList, NotFindRequest, HomeSliders, RandomProduct
} from "../../../store/home/actions";
import {
    addToCart,
    addToFavorite,
    openSelectedProduct,
    resetAddToCart,
    resetProductList
} from "../../../store/product/actions";
import CategoryList from "../../elements/CategoryList";
import {openSelectedCategory, ResetSelectedCategory} from "../../../store/category/actions";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import ApplicationLayout from "../../../containers/Layout";
import {openSelectedVendor, ResetSelectedVendor} from "../../../store/vendor/actions";
import SubmitButton from "../../elements/Buttons";
import FastImage from "react-native-fast-image";
import {ResetSelectedSearchWord} from "../../../store/search/actions";

class HomeScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {
            position: 1,
            interval: null,
            emailOrPhoneNew: "",
            requestNew: "",
            hideNavigationBar: false,
            showFloatingButtonBar: false,
            dataSourceSlideShow: [
                {
                    title: '',
                    caption: '',
                    url: require('./../../../assets/images/dumy/2.jpg'),
                }, {
                    title: '',
                    caption: '',
                    url: require('./../../../assets/images/dumy/3.jpg'),
                }, {
                    title: '',
                    caption: '',
                    url: require('./../../../assets/images/dumy/1.png'),
                },
            ],
        }


        this.onPressProduct = this.onPressProduct.bind(this)
    }

    componentDidMount() {
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.dataSourceSlideShow.length ? 0 : this.state.position + 1
                });
            }, 2000)
        });

        this.props.getRandomProducts({})
        this.props.getFeatureProducts({})
        this.props.getTopCategories({})
        this.props.getVendors({})
        this.props.getRecentProducts({})
        this.props.getTrendingProducts({})
        this.props.getHomeCategories({})
        this.props.getHomeSliders({})

    }

    componentWillUnmount() {

    }

    componentDidUpdate() {

    }

    submitNotFindRequest() {

        //console.log("submitNotFindRequest")
        this.props.NotFindRequest({
            text: this.state.requestNew,
            email: this.state.emailOrPhoneNew,
        })

    }

    onPressAddToCart(product) {

        // console.log("here product====== ===== =====")
        // console.log(product)
        if (product.attributes.length > 0) {

            let toast = Toast.show(strings.t("please_choose_attr"), {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,

            });
            setTimeout(function () {
                Toast.hide(toast);
            }, 1000);


        } else {

            //////console.log("addto cart from home screen ")
            this.props.addProductToCart(product)
        }


    }

    onPressAddToFavorite(product) {

        //////console.log("addto cart from home screen ")
        this.props.addProductToFavorite(product)

    }

    onPressProduct(product) {

        console.log("meee")
        console.log(product)

        this.props.navigation.navigate(NAVIGATION_PRODUCT_SCREEN);
        this.props.selectedProduct(product)


    }

    onPressCategory(category) {

        //////console.log(category)
        // this.props.productListsReset(category)
        this.props.ResetSelectedVendor(null)
        this.props.ResetSelectedSearchWord(null)
        this.props.selectedCategory(category)
        this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);

    }

    showMoreProductsCategory(category) {

        //////console.log(category)
        // this.props.productListsReset(category)
        this.props.ResetSelectedVendor(null)
        this.props.ResetSelectedSearchWord(null)
        this.props.selectedCategory({id: category.category_id})
        this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);
    }

    renderSwiperProducts(isLoading, ListAll) {

// return null
        var grids = [];
        if (isLoading) {

            return (
                <Grid>
                    <Row style={[styles.CenterContent]}>
                        <Text>Loading...</Text>
                    </Row>
                </Grid>)
        } else {
            if (Platform.OS === "ios") {
                for (var i = 0; i < Math.ceil(ListAll.length / 4); i++) {

                    ////console.log(ListAll[((i * 4) + 0)])
                    var grid = (
                        <Grid key={"grid" + i}>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 0) ?
                                        <Product key={((i * 4) + 0)} product={ListAll[((i * 4) + 0)]}
                                                 onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 0)])}
                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 1) ?
                                        <Product key={((i * 4) + 1)} product={ListAll[((i * 4) + 1)]}
                                                 onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 1)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 1)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 1)])}


                                        /> : null}

                                </Row>
                            </Col>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 2) ?
                                        <Product key={((i * 4) + 2)} product={ListAll[((i * 4) + 2)]}
                                                 onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 2)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 2)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 2)])}
                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 3) ?
                                        <Product key={((i * 4) + 3)} product={ListAll[((i * 4) + 3)]}
                                                 onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 3)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 3)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 3)])}
                                        /> : null}

                                </Row>
                            </Col>
                        </Grid>)

                    grids.push(grid)
                }

            }
            else {
                if (I18nManager.isRTL) {


                    for (var i = Math.ceil(ListAll.length / 4) - 1; i >= 0; i--) {

                        var grid = (
                            <Grid key={"grid" + i}>
                                <Col>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 0) ?
                                            <Product key={((i * 4) + 0)} product={ListAll[((i * 4) + 0)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 0)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 0)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 0)])}
                                            /> : null}

                                    </Row>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 1) ?
                                            <Product key={((i * 4) + 1)} product={ListAll[((i * 4) + 1)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 1)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 1)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 1)])}


                                            /> : null}

                                    </Row>
                                </Col>
                                <Col>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 2) ?
                                            <Product key={((i * 4) + 2)} product={ListAll[((i * 4) + 2)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 2)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 2)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 2)])}
                                            /> : null}

                                    </Row>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 3) ?
                                            <Product key={((i * 4) + 3)} product={ListAll[((i * 4) + 3)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 3)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 3)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 3)])}
                                            /> : null}

                                    </Row>
                                </Col>
                            </Grid>)

                        grids.push(grid)
                    }

                } else {

                    for (var i = 0; i < Math.ceil(ListAll.length / 4); i++) {

                        ////console.log(ListAll[((i * 4) + 0)])
                        var grid = (
                            <Grid key={"grid" + i}>
                                <Col>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 0) ?
                                            <Product key={((i * 4) + 0)} product={ListAll[((i * 4) + 0)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 0)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 0)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 0)])}
                                            /> : null}

                                    </Row>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 1) ?
                                            <Product key={((i * 4) + 1)} product={ListAll[((i * 4) + 1)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 1)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 1)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 1)])}


                                            /> : null}

                                    </Row>
                                </Col>
                                <Col>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 2) ?
                                            <Product key={((i * 4) + 2)} product={ListAll[((i * 4) + 2)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 2)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 2)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 2)])}
                                            /> : null}

                                    </Row>
                                    <Row style={styles.ContainerItem}>
                                        {ListAll.length > ((i * 4) + 3) ?
                                            <Product key={((i * 4) + 3)} product={ListAll[((i * 4) + 3)]}
                                                     onPressAddToCart={this.onPressAddToCart.bind(this, ListAll[((i * 4) + 3)])}
                                                     onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 3)])}
                                                     onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 3)])}
                                            /> : null}

                                    </Row>
                                </Col>
                            </Grid>)

                        grids.push(grid)
                    }
                }
            }


            return (<Swiper autoplayDirection={false} style={{

                // flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row'

                // flexDirection: Platform.OS === 'android' && I18nManager.isRTL ? 'column-reverse':'column-reverse'
            }}
                            dot={<View
                                style={{
                                    backgroundColor: color.CustomBlack,
                                    width: 8, height: 8, borderRadius: 4,
                                    marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                    marginBottom: -hp("9%"),
                                }}/>}
                            activeDot={<View
                                style={{
                                    backgroundColor: color.yellow,
                                    width: 8, height: 8, borderRadius: 4,
                                    marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                    marginBottom: -hp("9%"),
                                }}/>}
                            loop={false}
                            showsButtons={false}>
                {grids}

            </Swiper>)

        }
    }

    renderTopCategories() {


        // const arrayOfObj = Object.entries(this.props.).map((e) => ( { [e[0]]: e[1] } ));
        //

        if (this.props.isLoadingTopCategories) {
            return (
                <Grid>
                    <Row style={[styles.CenterContent]}>
                        <Text>Loading...</Text>
                    </Row>
                </Grid>)
        }
        else {

            const grids = [];
            for (var i = 0; i < Math.ceil(this.props.topCategories.length / 2); i++) {
                const grid = (
                    <Grid key={"grid_" + i} style={styles.categoryContainer}>
                        <Row>
                            {this.props.topCategories.length > ((i * 2) + 0) ?
                                <CategoryHome
                                    onPress={this.onPressCategory.bind(this, this.props.topCategories[((i * 2) + 0)])}
                                    category={this.props.topCategories[((i * 2) + 0)]}
                                    title={this.props.topCategories[((i * 2) + 0)].name}/> : null}
                        </Row>
                        <Row>
                            {this.props.topCategories.length > ((i * 2) + 1) ?
                                <CategoryHome

                                    onPress={this.onPressCategory.bind(this, this.props.topCategories[((i * 2) + 1)])}
                                    category={this.props.topCategories[((i * 2) + 1)]}
                                    title={this.props.topCategories[((i * 2) + 1)].name}/> : null}
                        </Row>
                    </Grid>
                )
                grids.push(grid)

            }


            return <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={true}>{grids}</ScrollView>
        }
    }

    renderHomeCategory(index) {


        if (this.props.isLoadingHomeCategories) {
            return (
                <Grid>
                    <Row style={[styles.CenterContent]}>
                        <Text>Loading...</Text>
                    </Row>
                </Grid>)
        }
        else {

            if (this.props.homeCategories[index]) {


                var used_cat = this.props.homeCategories[index]


                var products = []
                for (var i = 0; i < used_cat.products.length; i++) {
                    const product = (
                        <ProductHorizontal
                            key={"PRHR" + i}

                            onPressAddToFavorite={this.onPressAddToFavorite.bind(this, used_cat.products[i])}
                            onPressAddToCart={this.onPressAddToCart.bind(this, used_cat.products[i])}
                            onPress={this.onPressProduct.bind(this, used_cat.products[i])}
                            product={used_cat.products[i]}
                            forHome={true}/>)

                    products.push(product)
                }
                return (

                    <View style={[styles.paddingSectionTrending, {
                        // height: hp("70%"),
                        marginTop: hp("3%"),
                        flexDirection: 'column'
                    }]}>

                        <View size={0.35}
                              style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                            <Text style={[Typography.Text18OpenSansBold, {}]}>{used_cat.category_name}</Text>
                        </View>
                        <View size={3.65} style={{paddingTop: hp("2%"), flexDirection: 'column'}}>

                            {products}

                        </View>

                    </View>)

            }
            else {

                return null
            }
        }


    }

    renderAddToCart() {

        if (this.props.statusAddToCart === SUCCESS) {

            // this.refs.toast.show(strings.t("success_added_to_cart"),5000);


            // this.props.resetProducAddToCart({})
            // Toast.show(strings.t("success_added_to_cart"), {
            //     textStyle: Typography.Text14Light,
            //     duration: Toast.durations.LONG,
            //     position: Toast.positions.BOTTOM,
            //     shadow: true,
            //     animation: true,
            //     hideOnPress: true,
            //     delay: 0,
            // });

            // setTimeout(function () {
            //     this.props.resetProducAddToCart({})
            // }.bind(this), 3000)

            //

        }
    }

    onPressVendor(vendor) {

        //console.log(vendor)
        this.props.ResetSelectedCategory(null)
        this.props.ResetSelectedSearchWord(null)
        this.props.currentCategory = null
        this.props.selectedVendor(vendor)
        this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);


    }

    renderVendor() {


        if (this.props.isLoadingVendor) {
            return (
                <Grid>
                    <Row style={[styles.CenterContent]}>
                        <Text>Loading...</Text>
                    </Row>
                </Grid>)
        }
        else {


            const grids = [];
            for (var i = 0; i < this.props.vendors.length; i++) {
                const grid = (
                    <Grid key={"Vend" + i} style={styles.VendorContainer}>
                        <Col>

                            <Vendor

                                onPress={this.onPressVendor.bind(this, this.props.vendors[i])}
                                text={this.props.vendors[i].store_name}
                                image={this.props.vendors[i].gravatar}/>

                        </Col>

                    </Grid>
                )
                grids.push(grid)

            }


            return <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={true}>{grids}</ScrollView>

        }


    }

    handleScroll(event) {

        // if (event.nativeEvent.contentOffset.y > 1) {
        //
        //     this.setState({hideNavigationBar: true})
        //     this.setState({showFloatingButtonBar: true})
        //
        // }
        // else {
        //     this.setState({hideNavigationBar: false})
        //     this.setState({showFloatingButtonBar: false})
        // }
        // //console.log(event.nativeEvent.contentOffset.y);

    }

    render() {

        return (


            <DefaultHeader showFloatingButtonBar={this.state.showFloatingButtonBar}
                           hideNavigationBar={this.state.hideNavigationBar} type={"home"} {...this.props}>
                <Grid>
                    <Row>
                        <View style={{flex: 1}}>
                            <ScrollView style={{flex: 1}} onScroll={this.handleScroll.bind(this)}

                                        scrollEventThrottle={2}
                            >
                                <Grid style={{flex: 1,}}>
                                    <Row size={3.7} style={{flexDirection: 'column', paddingBottom: wp("3%")}}>


                                        <Grid style={[{height: hp("25%")}]}>

                                            {this.props.isLoadingHomeSliders ?
                                                <Row style={[styles.CenterContent]}>
                                                    <Text>{strings.t("loading")}</Text>
                                                </Row>
                                                :
                                                <Swiper style={{}}
                                                        dotColor={color.CustomBlack}
                                                        activeDotColor={color.yellow}
                                                        loop={true}
                                                        showsButtons={false}
                                                        autoplay={true}
                                                        autoplayTimeout={3}
                                                        autoplayDirection={true}
                                                        dot={<View
                                                            style={{
                                                                backgroundColor: color.CustomBlack,
                                                                width: 8, height: 8, borderRadius: 4,
                                                                marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                                                marginBottom: -hp("4%"),
                                                            }}/>}
                                                        activeDot={<View
                                                            style={{
                                                                backgroundColor: color.yellow,
                                                                width: 8, height: 8, borderRadius: 4,
                                                                marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                                                marginBottom: -hp("4%"),
                                                            }}/>}
                                                >

                                                    {this.props.homeSliders.map((e, i) => {
                                                        return (<Grid key={i}>
                                                            <Row key={"r" + i}>
                                                                <TouchableOpacity ref="touch"
                                                                                  style={[styles.CenterContent, {
                                                                                      alignItems: 'center',
                                                                                      justifyContent: 'center',
                                                                                      flex: 1
                                                                                  }]}>

                                                                    <FastImage
                                                                        style={{

                                                                            // width: wp("100%"), height: wp("100%"),

                                                                            width: wp("100%"), height: 200
                                                                            // resizeMode: 'contain',
                                                                        }}
                                                                        source={{uri: e.length > 0 ? e : "http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png"}}/>
                                                                </TouchableOpacity>
                                                            </Row>
                                                        </Grid>)
                                                    })}

                                                </Swiper>}
                                        </Grid>


                                        {/* Start Feature product*/}

                                        <Grid style={[styles.paddingSectionTrending, {
                                            marginTop: hp("3%"),
                                            height: hp("90%")
                                        }]}>

                                            <Row size={0.25}
                                                 style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                                                <Text
                                                    style={[Typography.Text18OpenSansBold]}>{strings.t("random_products")} </Text>
                                            </Row>
                                            <Row size={3.75} style={{paddingTop: hp("2%"),}}>


                                                {this.renderSwiperProducts(this.props.isLoadingRandomProduct, this.props.randomProducts)}
                                            </Row>

                                        </Grid>


                                        <Grid style={[styles.paddingSectionTrending, {
                                            marginTop: hp("3%"),
                                            height: hp("90%")
                                        }]}>

                                            <Row size={0.25}
                                                 style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                                                <Text
                                                    style={[Typography.Text18OpenSansBold]}>{strings.t("featured_products")} </Text>
                                            </Row>
                                            <Row size={3.75} style={{paddingTop: hp("2%"),}}>


                                                {this.renderSwiperProducts(this.props.isLoadingFeatureProduct, this.props.featureProducts)}
                                            </Row>

                                        </Grid>


                                        {/* End Feature product*/}


                                        {/* Start Category*/}
                                        <Grid style={[styles.paddingSectionNoStart, {
                                            marginTop: hp("2%"),
                                            height: hp("25%"),
                                        }]}>

                                            {this.renderTopCategories()}
                                            {/*<ScrollView*/}
                                            {/*showsHorizontalScrollIndicator={false}*/}
                                            {/*horizontal={true}>*/}
                                            {/*<Grid style={styles.categoryContainer}>*/}
                                            {/*<Row>*/}
                                            {/*<CategoryHome title={'Electronics & Computers'}/>*/}

                                            {/*</Row>*/}
                                            {/*<Row>*/}
                                            {/*<CategoryHome title={'Mens Fashion'}/>*/}

                                            {/*</Row>*/}
                                            {/*</Grid>*/}
                                            {/*<Grid style={styles.categoryContainer}>*/}
                                            {/*<Row>*/}
                                            {/*<CategoryHome title={'Furniture'}/>*/}

                                            {/*</Row>*/}
                                            {/*<Row>*/}
                                            {/*<CategoryHome title={'Womens Fashion'}/>*/}

                                            {/*</Row>*/}
                                            {/*</Grid>*/}

                                            {/*</ScrollView>*/}

                                        </Grid>
                                        {/* End Category*/}


                                        {/* Start Trending Now*/}

                                        <Grid style={[styles.paddingSectionTrending, {
                                            marginTop: wp("5%"), height: hp("90%")
                                        }]}>

                                            <Row size={0.25}
                                                 style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                                                <Text
                                                    style={[Typography.Text18OpenSansBold]}>{strings.t("trending_now")} </Text>
                                            </Row>

                                            <Row size={3.75} style={{paddingTop: hp("2%"),}}>

                                                {this.renderSwiperProducts(this.props.isLoadingTrendingProduct, this.props.trendingProducts)}

                                            </Row>

                                        </Grid>

                                        {/* End Trending Now*/}


                                        {/* Start Free Shipping */}
                                        <Image
                                            style={{
                                                flex: 1, width: wp("100%"), height: 190, marginTop: wp("16%")
                                            }}

                                            resizeMode="contain"

                                            source={require("./../../../assets/images/free_shipping.png")}/>


                                        {/* End Free Shipping */}


                                        {/* Start Recent Product*/}

                                        <Grid style={[styles.paddingSectionTrending, {
                                            marginTop: hp("3%"),
                                            height: hp("90%")
                                        }]}>

                                            <Row size={0.25}
                                                 style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                                                <Text
                                                    style={[Typography.Text18OpenSansBold]}>{strings.t("recent_products")} </Text>
                                            </Row>
                                            <Row size={3.75} style={{paddingTop: hp("2%"),}}>


                                                {this.renderSwiperProducts(this.props.isLoadingRecentProduct, this.props.recentProducts)}
                                            </Row>

                                        </Grid>


                                        {/* End Recent Product*/}


                                        {/* Start Contact  */}
                                        <Image

                                            style={{width: wp("100%"), height: 190, marginTop: wp("16%")}}

                                            resizeMode="contain"

                                            source={require("./../../../assets/images/contact.png")}/>


                                        {/* End Contact  */}


                                        {/* Start Compute Electronic */}

                                        <View style={{flexDirection: 'column'}}>
                                            <View>

                                                {this.renderHomeCategory(0)}
                                            </View>

                                            {/* End Compute Electronic*/}

                                            <View>
                                                <ShowMore
                                                    onPress={this.showMoreProductsCategory.bind(this, this.props.homeCategories[0])}/>


                                            </View>


                                            {/* Start Vendor  */}
                                        </View>


                                        {/* End Compute Electronic */}

                                        <Grid style={[styles.paddingSectionTrending, {
                                            height: hp("35%"),
                                            marginTop: hp("3%")
                                        }]}>

                                            <Row size={1}
                                                 style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                                                <Text
                                                    style={[Typography.Text18OpenSansBold, {}]}>{strings.t("vendors")}</Text>
                                            </Row>
                                            <Row size={3} style={{paddingTop: hp("0%"),}}>


                                                {this.renderVendor()}

                                            </Row>

                                        </Grid>

                                        {/* End Vendor */}


                                        {/* Start Makeup  */}


                                        {this.renderHomeCategory(1)}

                                        {/* End Makeup */}

                                        <ShowMore
                                            onPress={this.showMoreProductsCategory.bind(this, this.props.homeCategories[1])}/>


                                        {/* Start Shoes  */}


                                        {this.renderHomeCategory(2)}

                                        {/* End Shoes */}

                                        <ShowMore
                                            onPress={this.showMoreProductsCategory.bind(this, this.props.homeCategories[2])}/>

                                        {/* Start Tell Us */}

                                        <Grid style={[, {height: hp("35%"), marginTop: hp("3%")}]}>
                                            <ImageBackground style={{
                                                width: wp('100%'), flex: 1,
                                                resizeMode: 'contain',
                                            }}

                                                             source={require('../../../assets/images/tell_us.png')}>
                                                <Grid style={{flex: 1}}
                                                >
                                                    <Row size={1} style={[{
                                                        alignItems: 'flex-end',
                                                        justifyContent: 'center',
                                                        backgroundColor: ''
                                                    }]}>
                                                        <Text
                                                            style={[Typography.Text22OpenSansBold]}>{strings.t("cant_find_request")}</Text>
                                                    </Row>

                                                    <Row size={3}>

                                                        <Grid style={[styles.CenterContent]}>
                                                            <Col size={1.4} style={[{
                                                                justifyContent: 'center',
                                                                alignItems: 'center'
                                                            }]}><Text style={[Typography.Text16OpenSansBold, {
                                                                color: 'white',
                                                                textAlign: 'right',
                                                            }]}>{strings.t("tell_us_now")} </Text></Col>
                                                            <Col size={2.6} style={{
                                                                paddingTop: wp("5%"),
                                                                paddingBottom: wp("5%"),
                                                                paddingStart: wp("5%"),
                                                                paddingEnd: wp("5%"),
                                                            }}>
                                                                <TextInput

                                                                    onChangeText={(value) => {
                                                                        this.setState({requestNew: value})
                                                                    }}
                                                                    style={{
                                                                        paddingStart: wp("2%"),
                                                                        backgroundColor: 'white',
                                                                        borderRadius: 16,
                                                                        flex: 1,
                                                                        textAlign: I18nManager.isRTL ? "right" : "auto"
                                                                    }}
                                                                    placeholder={strings.t("your_request")}/>
                                                                <TextInput
                                                                    onChangeText={(value) => {
                                                                        this.setState({emailOrPhoneNew: value})
                                                                    }}
                                                                    style={{
                                                                        marginTop: wp("3%"),
                                                                        paddingStart: wp("2%"),
                                                                        backgroundColor: 'white',
                                                                        borderRadius: 16,
                                                                        flex: 1,
                                                                        textAlign: I18nManager.isRTL ? "right" : "auto"
                                                                    }}
                                                                    placeholder={strings.t("email_or_phone")}/>

                                                                <SubmitButton text={strings.t("send")} onPress={() => {

                                                                    this.submitNotFindRequest()
                                                                }}/>

                                                            </Col>
                                                        </Grid>
                                                    </Row>

                                                </Grid>
                                            </ImageBackground>
                                        </Grid>

                                        {/* End Tell Us */}


                                        {/* Start Lighting  */}


                                        {this.renderHomeCategory(3)}
                                        <ShowMore
                                            onPress={this.showMoreProductsCategory.bind(this, this.props.homeCategories[3])}/>

                                        {/* End Lighting */}

                                    </Row>
                                </Grid>
                            </ScrollView>

                        </View>
                    </Row>


                </Grid>


                {/*{this.renderAddToCart()}*/}

                {/*<Toast ref="toast"*/}
                {/*position='bottom'*/}
                {/*positionValue={180}*/}
                {/*/>*/}
                <Toast
                    visible={this.props.statusAddToCart === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{this.props.ErrorMessageADDProductToCart}</Toast>

                <Toast
                    visible={this.props.statusNewRequest === FAILURE || this.props.statusNewRequest === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{this.props.MessageNewRequest}</Toast>


                <Toast
                    visible={this.props.statusAddToFavorite === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{strings.t("success_favorite")}</Toast>

            </DefaultHeader>


        );
    }
}


const styles = StyleSheet.create({
    ContainerItem: {
        padding: 4,

    }
    ,
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        alignItems: 'center',
        flex: 1
    },
    productImage: {
        width: hp("18%"),
        resizeMode: 'contain',
    },
    categoryImage: {
        width: hp("4%"),
        resizeMode: 'contain',
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light, textAlign: 'center',
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    },
    paddingSection: {

        paddingStart: wp("6%"),
        paddingEnd: wp("6%"),
        paddingTop: wp("6%"),
    }, paddingSectionTrending: {

        paddingStart: wp("6%"),
        paddingEnd: wp("6%"),
        paddingTop: wp("0%"),
    }, paddingSectionStart: {

        paddingStart: wp("5%"),
        paddingTop: wp("4%"),
    }, paddingSectionNoStart: {

        paddingTop: wp("4%"),
    },
    categoryContainer: {
        width: wp("60%"),

    },
    VendorContainer: {
        width: wp("25%"),
        marginEnd: wp("1%"),

    }

})


const mapStateToProps = state => {
    return {
        isLoadingFeatureProduct: state.home.isLoadingFeatureProduct,
        statusFeatureProduct: state.home.statusFeatureProduct,
        featureProducts: state.home.featureProducts,
        isLoadingRandomProduct: state.home.isLoadingRandomProduct,
        statusRandomProduct: state.home.statusRandomProduct,
        randomProducts: state.home.randomProducts,
        isLoadingRecentProduct: state.home.isLoadingRecentProduct,
        statusRecentProduct: state.home.statusRecentProduct,
        recentProducts: state.home.recentProducts,
        isLoadingTrendingProduct: state.home.isLoadingTrendingProduct,
        statusTrendingProduct: state.home.statusTrendingProduct,
        trendingProducts: state.home.trendingProducts,
        isLoadingTopCategories: state.home.isLoadingTopCategories,
        statusTopCategories: state.home.statusTopCategories,
        topCategories: state.home.topCategories,
        isLoadingHomeCategories: state.home.isLoadingHomeCategories,
        statusHomeCategories: state.home.statusHomeCategories,
        isLoadingHomeSliders: state.home.isLoadingHomeSliders,
        statusHomeSliders: state.home.statusHomeSliders,
        MessageNewRequest: state.home.MessageNewRequest,
        isLoading: state.home.isLoadingNewRequest,
        statusNewRequest: state.home.statusNewRequest,
        isLoadingVendor: state.home.isLoadingVendor,
        statusVendor: state.home.statusVendor,
        vendors: state.home.vendors,
        homeSliders: state.home.homeSliders,
        homeCategories: state.home.homeCategories,
        ErrorMessage: state.home.ErrorMessage,
        ErrorMessageADDProductToCart: state.product.ErrorMessageADDProductToCart,
        statusAddToCart: state.product.statusAddToCart,
        statusAddToFavorite: state.product.statusAddToFavorite,
    }

}
const mapDispatchToProps = dispatch => {
    return {

        getFeatureProducts: (payload) => dispatch(FeatureProduct(payload)),
        getRandomProducts: (payload) => dispatch(RandomProduct(payload)),
        getRecentProducts: (payload) => dispatch(RecentProduct(payload)),
        getTrendingProducts: (payload) => dispatch(TrendingProduct(payload)),
        getTopCategories: (payload) => dispatch(TopCategories(payload)),
        getHomeCategories: (payload) => dispatch(HomeCategories(payload)),
        getHomeSliders: (payload) => dispatch(HomeSliders(payload)),
        getVendors: (payload) => dispatch(vendorsList(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        selectedCategory: (payload) => dispatch(openSelectedCategory(payload)),
        ResetSelectedCategory: (payload) => dispatch(ResetSelectedCategory(payload)),
        addProductToCart: (payload) => dispatch(addToCart(payload)),
        addProductToFavorite: (payload) => dispatch(addToFavorite(payload)),
        selectedVendor: (payload) => dispatch(openSelectedVendor(payload)),
        ResetSelectedVendor: (payload) => dispatch(ResetSelectedVendor(payload)),
        ResetSelectedSearchWord: (payload) => dispatch(ResetSelectedSearchWord(payload)),
        NotFindRequest: (payload) => dispatch(NotFindRequest(payload)),

        // productListsReset: (payload) => dispatch(resetProductList(payload))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

