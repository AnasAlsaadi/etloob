// Pages
import LoginScreen from './LoginScreen';
import SignUpScreen from './SignUpScreen';
import SplashScreen from './SplashScreen';
import Verification from './Verification';
import HomeScreen from './HomeScreen';
import HomeScreen2 from './HomeScreen2';
import ProductScreen from './ProductScreen';
import SearchScreen from './SearchScreen';
import CategoriesScreen from './CategoriesScreen';
import ProductsScreen from './ProductsScreen';
import CartScreen from './CartScreen';
import CheckoutScreen from './CheckoutScreen';
import MyOrdersScreen from './MyOrdersScreen';
import FavoriteScreen from './FavoriteScreen';
import ProfileScreen from './ProfileScreen';
import EditProfileScreen from './EditProfileScreen';
import AboutUsScreen from './AboutUsScreen';
import VendorsScreen from './VendorsScreen';
import ShippingAddressScreen from './ShippingAddressScreen';
import OrderDetailsScreen from './OrderDetailsScreen';
import VendorHomeScreen from './VendorHome';
import VendorTabProductsScreen from './VendorTabProductsScreen';
import VendorTabOrdersScreen from './VendorTabOrdersScreen';
import VendorTabSettingScreen from './VendorTabSettingScreen';
import VendorTabStatisticScreen from './VendorTabStatisticScreen';
import VendorTabReviewsScreen from './VendorTabReviewsScreen';
import OfferScreen from './OfferScreen';

export {
    LoginScreen,
    SignUpScreen,
    SplashScreen,
    Verification,
    HomeScreen,
    HomeScreen2,
    ProductScreen,
    SearchScreen,
    CategoriesScreen,
    CartScreen,
    CheckoutScreen,
    ProductsScreen,
    MyOrdersScreen,
    FavoriteScreen,
    EditProfileScreen,
    ProfileScreen,
    AboutUsScreen,
    VendorsScreen,ShippingAddressScreen,
    OrderDetailsScreen,
    VendorHomeScreen,
    VendorTabProductsScreen,
    VendorTabOrdersScreen,
    VendorTabSettingScreen,
    VendorTabStatisticScreen,
    VendorTabReviewsScreen,
    OfferScreen
};



