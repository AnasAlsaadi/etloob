import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    I18nManager,
    ActivityIndicator,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    FlatList
} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import {connect} from "react-redux";
import {Categories} from "../../../store/category/actions";
import {addToCart, addToFavorite, openSelectedProduct, resetProductList} from "../../../store/product/actions";
import {productLists, productListsRest} from "../../../store/ProductList/actions";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import colors from "../../../constants/colors";
import {NAVIGATION_PRODUCT_SCREEN} from "../../../navigation/types";
import TabedFooter from "../../template/TabedFooter";
import Toast from "react-native-root-toast";
import {SUCCESS} from "../../../constants/actionsType";
import strings from "../../../locales/i18n";
import Product from "../../elements/Product";
import RNPickerSelect from "react-native-picker-select";
import {Content} from 'native-base'

class ProductsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};
    category = null
    vendor = null
    search = null

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            productListAll: []
        }

        this.category = this.props.currentCategory
        this.vendor = this.props.currentVendor
        this.search = this.props.currentSearch
        // console.log("=======")
        // console.log(this.category)
        // console.log(this.vendor)
        // console.log(this.search)

    }

    componentDidMount() {

        this.props.resetProductListAll({})

        //////console.log("did amount")
        //////console.log({category: this.category.id, page: this.state.page})
        ////console.log({category: this.category.id, page: this.state.page})
        if (this.category) {
            this.props.getProducts({
                category: this.category.id,
                page: this.state.page,
                refresh: false,
                order_by: this.state.order_by
            })

        }
        else if (this.vendor) {
            this.props.getProducts({
                vendor: this.vendor.id,
                page: this.state.page,
                refresh: false,
                order_by: this.state.order_by
            })

        }
        else if (this.search) {
            this.props.getProducts({
                search: this.search,
                page: this.state.page,
                refresh: false,
                order_by: this.state.order_by
            })
        }
    };


    handleRefresh = () => {
        this.props.resetProductListAll({})

        this.setState({page: 1}, () => {

            if (this.category) {
                this.props.getProducts({
                    category: this.category.id,
                    page: this.state.page,
                    refresh: true,
                    order_by: this.state.order_by
                })

            }
            else if(this.vendor) {
                this.props.getProducts({
                    vendor: this.vendor.id,
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by
                })

            } else if(this.search) {
                this.props.getProducts({
                    search: this.search,
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by
                })

            }

        })
    };

    handleLoadMore = () => {

        var page = this.state.page;
        page = page + 1
        this.setState({page: page}, () => {

            if (this.category) {
                this.props.getProducts({
                    category: this.category.id,
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by
                })

            }
            else if(this.vendor){
                this.props.getProducts({
                    vendor: this.vendor.id,
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by
                })

            } else if(this.search){
                this.props.getProducts({
                    search: this.search,
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by
                })

            }

        })
    };

    onPressProduct(product) {

        //////console.log("pressed ")
        //////console.log(product)

        //////console.log(product)
        this.props.selectedProduct(product)
        // this.props.navigation.navigate(NAVIGATION_PRODUCT_SCREEN);

        this.props.navigation.navigate({
            routeName: NAVIGATION_PRODUCT_SCREEN,
            parms: {current: product, ...this.props},
            key: 'product' + product.id
        });
    }
    _listEmptyComponent = () => {
        return (<Text style={{textAlign: 'center', padding: wp("6%")}}>{strings.t("empty_result")}</Text>)
    }

    onPressProductAddToCart(item) {

        ////console.log("add to cart ")
        ////console.log(item)

        if (item.attributes.length > 0) {

            let toast = Toast.show(strings.t("please_choose_attr"), {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,

            });
            setTimeout(function () {
                Toast.hide(toast);
            }, 1000);


        } else {

            this.props.addProductToCart(item)
        }

    }


    onPressAddToFavorite(product) {

        //////console.log("addto cart from home screen ")
        this.props.addProductToFavorite(product)

    }

    render() {

        return (


            <DefaultHeader {...this.props} >


                <View style={{flex: 1,}}>

                    <View style={{
                        flex: 1, flexDirection: "row", alignItems: 'center',
                        paddingStart: wp("6%"),
                        paddingEnd: wp("6%"),
                        backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                    }}>
                        <View style={{flex: 2,}}>
                            <Text
                                style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("order_by")}: </Text>
                        </View>
                        <View style={{flex: 2}}>
                            <RNPickerSelect
                                style={{marginTop: wp("3%"), marginBottom: wp("3%")}}
                                placeholder={this.oj}
                                onValueChange={(value) => {

                                    this.setState({order_by: value}, function () {

                                        this.props.productListsRest()
                                        this.handleRefresh()

                                    }.bind(this))
                                    ////console.log(value)
                                    // this.props.productList = []
                                    // this.handleRefresh()

                                }}
                                items={[
                                    {label: strings.t("Popularity"), value: 'popularity'},
                                    {label: strings.t("Average_rating"), value: 'rating'},
                                    {label: strings.t("Newness"), value: 'date'},
                                    {label: strings.t("P_l_t_h"), value: 'price'},
                                    {label: strings.t("P_h_t_l"), value: 'price-desc'},
                                ]}
                            />
                        </View>
                    </View>

                    <View style={{flex: 8}}>

                        {
                            this.props.productList &&

                            <FlatList
                                ListEmptyComponent={this.props.productList!=null &&  this.props.productList.length==0  && !this.props.isLoadingLIST ?this._listEmptyComponent:null}

                                data={this.props.productList}
                                renderItem={({item}) => (

                                    <ProductHorizontal product={item} onPress={this.onPressProduct.bind(this, item)}
                                                       onPressAddToFavorite={this.onPressAddToFavorite.bind(this, item)}

                                                       onPressAddToCart={this.onPressProductAddToCart.bind(this, item)}

                                    />
                                )}
                                keyExtractor={(item, index) => item.id + ""}
                                refreshing={this.props.isLoadingLIST}
                                onRefresh={this.handleRefresh}
                                onEndReached={(xxx) => {
                                  //  console.log("reached")
                                  //  console.log(xxx)
                                    this.handleLoadMore()
                                }}

                                onEndThreshold={0}

                            />
                        }
                    </View>
                </View>

                {/*{this.props.isLoading && <View style={{flex:1, alignItems:'center'}}><ActivityIndicator size="large" color="#ff6a00" /></View>}*/}

                <Toast
                    visible={this.props.statusAddToCart === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{this.props.ErrorMessageADDProductToCart}</Toast>


                <Toast
                    visible={this.props.statusAddToFavorite === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{strings.t("success_favorite")}</Toast>

            </DefaultHeader>


        );
    }
}

const mapStateToProps = state => {
    return {
        currentCategory: state.category.currentCategory,
        currentSearch: state.search.currentSearch,
        currentVendor: state.vendor.currentVendor,
        productList: state.productList.productList,
        isLoadingLIST: state.productList.isLoadingLIST,
        status: state.productList.status,
        errorMessage: state.productList.errorMessage,
        statusAddToFavorite: state.product.statusAddToFavorite,
        ErrorMessageADDProductToCart: state.product.ErrorMessageADDProductToCart,

    }
}
const mapDispatchToProps = dispatch => {
    return {

        getProducts: (payload) => dispatch(productLists(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        resetProductListAll: (payload) => dispatch(resetProductList(payload)),
        addProductToCart: (payload) => dispatch(addToCart(payload)),
        addProductToFavorite: (payload) => dispatch(addToFavorite(payload)),
        productListsRest: (payload) => dispatch(productListsRest(payload)),

    }


}

export default connect(mapStateToProps, mapDispatchToProps)

(
    ProductsScreen
)
;
