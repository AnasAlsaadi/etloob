import React from 'react';
import {Platform, I18nManager, Image} from 'react-native';
import {Col, Grid} from "react-native-easy-grid";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RNLanguages from "react-native-languages";

import {
    NAVIGATION_APP,
    NAVIGATION_ChooseLanguage_SCREEN,
    NAVIGATION_DRAWER, NAVIGATION_DRAWER_VENDOR,
    NAVIGATION_VERIFICATION_SCREEN
} from './../../../navigation/types'
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
// import RestartAndroid from "react-native-restart-android";
import i18n from './../../../locales/i18n'
import firebase, {Notification, NotificationOpen} from 'react-native-firebase';
import axios from "axios"
import {config, urlAuth} from "../../../api/api";

class SplashScreen extends React.Component {

    constructor(props) {
        super(props);

        ////////console.log(global.storage)
        const storage = new Storage({
            size: 1000,

            storageBackend: AsyncStorage, // for web: window.localStorage

            defaultExpires: 1000 * 3600 * 102400,

            enableCache: true,

        });
        global.storage = storage


    }

    performTimeConsumingTask = async () => {
        return new Promise((resolve) =>
            setTimeout(
                () => {
                    resolve('result')
                },
                1900
            )
        )
    }

    async fetchToken() {

        console.warn('fetchToken');
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            console.warn(fcmToken);
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                console.warn("inside fcm geteeing")
                console.warn(fcmToken)


                global.token = fcmToken
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        } else {
            // console.warn("inside restore fcm ")
            global.token = fcmToken
            // console.log(global.token)

            console.warn(fcmToken);

        }
    }

    async askPermission() {
        try {
            await firebase.messaging().requestPermission();
            console.warn('Permissions allowed');
            this.fetchToken();
        } catch (error) {
            console.warn('Permissions denied');
        }
    }

    async componentDidMount() {

        const data = await this.performTimeConsumingTask();


        const granted = await firebase.messaging().hasPermission();
        if (granted) {
            console.warn('fetchToken');

            this.fetchToken();
        } else {

            this.askPermission();
        }


        global.current = 'home'
        if (data !== null) {


            AsyncStorage.getItem("lang")
                .then(language => {
                    I18nManager.allowRTL(true)
                    if (language === "ar" || language === "en") {

                        if (language === "ar") {

                            I18nManager.forceRTL(true)
                        } else if (language === "en") {
                            I18nManager.forceRTL(false)

                        }
                        storage.load({
                            key: 'user',
                            autoSync: true,
                            syncInBackground: true,

                        })
                            .then(ret => {
                                // console.log("ddd");
                                // console.log(ret);


                                if (ret.logged && ret.is_active) {
                                    global.logged = true
                                    global.is_active = true
                                    global.user = ret.user
                                    global.role = ret.user.role ? ret.user.role : 'customer'
                                    // if (global.role  === "seller") {
                                    //     this.props.navigation.navigate(NAVIGATION_DRAWER_VENDOR);
                                    //
                                    // } else {
                                    //     this.props.navigation.navigate(NAVIGATION_DRAWER);
                                    //
                                    // }


                                    // console.log({
                                    //     user_id: ret.user.id,
                                    //     os: Platform.OS,
                                    //     token: global.token + "",
                                    // })


                                    axios.post('https://etloob.com/wp-json/jwt-auth/v1/notifications_token',
                                        {
                                            user_id: ret.user.id,
                                            os: Platform.OS,
                                            token: global.token + "",
                                        }
                                        , null)
                                        .then(function (response) {
                                            // console.log("firebase save  req");
                                            // console.log(response);
                                            // console.log("instide req");
                                            return {status: 200, response: response.data, message: ""}
                                        })
                                        .catch(function (error) {
                                            // console.log("firebase save  err");
                                            // console.log(error.response.data);
                                            return {status: 400, response: null, message: error.response.data.message}
                                        });

                                    this.props.navigation.navigate(NAVIGATION_DRAWER);
                                }
                                else {


                                    this.props.navigation.navigate(NAVIGATION_APP);
                                }

                            })
                            .catch(err => {

                                switch (err.name) {
                                    case 'NotFoundError':
                                        // should go to choose language
                                        this.props.navigation.navigate(NAVIGATION_APP);
                                        break;
                                    case 'ExpiredError':

                                        break;
                                }
                            });


                    } else {

                        this.props.navigation.navigate(NAVIGATION_ChooseLanguage_SCREEN);

                    }
                });


            storage.load({
                key: 'cart',
                autoSync: true,
                syncInBackground: true,

            })
                .then(ret => {

                    global.cart = ret

                })
                .catch(err => {

                    switch (err.name) {
                        case 'NotFoundError':
                            break;
                        case 'ExpiredError':

                            break;
                    }
                });


            storage.load({
                key: 'recentSearches',
                autoSync: true,
                syncInBackground: true,

            })
                .then(ret => {

                    global.recentSearches = ret

                })
                .catch(err => {

                    switch (err.name) {
                        case 'NotFoundError':

                            global.recentSearches = []
                            break;
                        case 'ExpiredError':

                            global.recentSearches = []

                            break;
                    }
                });

            storage.load({
                key: 'favorite',
                autoSync: true,
                syncInBackground: true,

            })
                .then(ret => {
                    global.favorite = ret

                })
                .catch(err => {

                    switch (err.name) {
                        case 'NotFoundError':
                            break;
                        case 'ExpiredError':

                            break;
                    }
                });


            if (Platform.OS === "ios") {
                axios.get("http://etloob.com/check.php",
                    null)
                    .then(function (response) {
                        // console.log("instide req");
                        // console.log(response);

                        // pro or dev
                        global.mode = response.data.data + ""


                        // console.log(global.mode);
                    })
                    .catch(function (error) {
                        global.mode = "pro"
                    });

            }
            else {

                global.mode = "dev"

            }


        }




    }


    render() {
        return (
            <Grid>
                <Col>

                    <Image
                        style={styles.ImageBackground}
                        source={require('./../../../assets/images/intro2.gif')}
                        // source={require('./../../../assets/images/SplashScreen.jpg')}
                    />
                </Col>
            </Grid>
        );
    }
}

const styles = {

    ImageBackground: {
        height: hp('100%'),
        width: wp('100%')
    }
}

export default SplashScreen;
