import React, {useState, useEffect} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ApplicationLayout from './../../../containers/Layout'
import {Col, Row, Grid} from "react-native-easy-grid";
import {Linking, Text, Image, TouchableOpacity, View, ScrollView, Platform} from 'react-native'
import {
    NAVIGATION_DRAWER,
    NAVIGATION_DRAWER_VENDOR,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_SIGNUP_SCREEN
} from './../../../navigation/types'
import Typography from './../../../constants/typography'
import color from './../../../constants/colors'
import {InputElement, SubmitButton, TextUnderLine} from '../../elements'
import strings from "./../../../locales/i18n"
import {useSelector, useDispatch, connect} from 'react-redux';
import {CUSTOMER_SIGNUP, FAILURE, SUCCESS} from "../../../constants/actionsType";
import {CustomerLogin, resetAuthState} from "../../../store/login/actions";
import Toast from "react-native-root-toast";
import AuthHeader from "../../../containers/AuthHeader";
import {NAVIGATION_VERIFICATION_SCREEN} from "../../../navigation/types";
import {SlideButton, SlideDirection} from 'react-native-slide-button';
import axios from "axios";

class LoginScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
        this.state = {
            password: "",
            username: "",
            showPassword: true
        }

        this.ActionLogin = this.ActionLogin.bind(this)
    }

    ActionLogin() {

        if (this.state.username.length > 0
            && this.state.password.length > 0) {
            this.props.Login({
                username: this.state.username,
                password: this.state.password,
            })

        } else {

            Toast.show(strings.t("please_fill_all_inputs"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            // this.props.navigation.navigate(NAVIGATION_VERIFICATION_SCREEN);
        }


    }

    renderMessagesLogin() {

        if (this.props.status === SUCCESS) {


            this.props.reset()

            //////console.log("user data")
            //////console.log({user: this.props.user, logged: true, is_active: false})

            if (this.props.user.role === "customer") {
                if (this.props.user.is_verified === "yes") {

                    global.storage.save({
                        key: 'user',
                        data: {user: this.props.user, logged: true, is_active: true},
                        expires: null
                    })
                    global.user = this.props.user
                    global.logged = true
                    global.role = "customer"
                    global.is_active = true

                    axios.post('https://etloob.com/wp-json/jwt-auth/v1/notifications_token',
                        {
                            user_id: this.props.user.id,
                            os: Platform.OS,
                            token: global.token + "",
                        }
                        , null)
                        .then(function (response) {
                            console.log("firebase save  req");
                            // console.log(response);
                            // console.log("instide req");
                            return {status: 200, response: response.data, message: ""}
                        })
                        .catch(function (error) {
                            console.log("firebase save  err");
                            // console.log(error.response.data);
                            return {status: 400, response: null, message: error.response.data.message}
                        });

                    this.props.navigation.navigate(NAVIGATION_DRAWER)

                } else if (this.props.user.is_verified === false) {

                    global.storage.save({
                        key: 'user',
                        data: {user: this.props.user, logged: false, is_active: false},
                        expires: null
                    })
                    global.user = this.props.user
                    global.logged = false
                    global.is_active = false
                    global.role = "customer"

                    this.props.navigation.navigate(NAVIGATION_VERIFICATION_SCREEN)

                } else if (this.props.user.is_verified === "no") {

                    global.storage.save({
                        key: 'user',
                        data: {user: this.props.user, logged: false, is_active: false},
                        expires: null
                    })
                    global.user = this.props.user
                    global.logged = false
                    global.is_active = false

                    global.role = "customer"
                    this.props.navigation.navigate(NAVIGATION_VERIFICATION_SCREEN)

                } else {

                    this.props.navigation.navigate(NAVIGATION_SIGNUP_SCREEN)
                }
            }
            else {
                global.storage.save({
                    key: 'user',
                    data: {user: this.props.user, logged: true, is_active: true},
                    expires: null
                })
                global.user = this.props.user
                global.logged = true
                global.is_active = true
                global.role = "seller"

                this.props.navigation.navigate(NAVIGATION_DRAWER_VENDOR)
            }


        }

        if (this.props.status === FAILURE) {

            Toast.show(this.props.ErrorMessage, {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            this.props.reset()


        }
    }

    render() {

        return (

            <ApplicationLayout isLoading={this.props.isLoading}>
                <ScrollView style={{flex: 1,}} showsHorizontalScrollIndicator={false}>
                    <Grid style={{
                        flex: 1, height: hp("100%")
                    }}>
                        <Row size={0.7} style={styles.logoContainer}>
                            <Image
                                style={styles.ImageBackground}
                                source={require('./../../../assets/images/Logo.png')}
                            />
                        </Row>
                        <Row size={3.3} style={styles.formContainer}>
                            <Grid>
                                <Row size={2.5} style={styles.formInputContainer}>

                                    <Grid style={styles.backgroundWhite}>
                                        <Row size={1} style={styles.FlexDirectionColumn}>

                                            <Text
                                                style={[Typography.Text30Bold, color.Yellow]}>{strings.t("welcome")}</Text>
                                            <Text
                                                style={[Typography.Text14Light,]}>{strings.t("sing_in_to_continue")}</Text>
                                        </Row>

                                        <InputElement text={strings.t('email_or_mobile_number')}
                                                      onChangeText={(value) => {
                                                          this.setState({username: value})
                                                      }
                                                      }
                                        />

                                        <InputElement textLength={this.state.password.length} onPressShow={() => {

                                            this.setState({showPassword: !this.state.showPassword})
                                        }} type={"password"} secureTextEntry={this.state.showPassword}
                                                      text={strings.t("password")}
                                                      onChangeText={(value) => {
                                                          this.setState({password: value})
                                                      }
                                                      }/>


                                        <Row size={1} style={styles.FlexDirectionColumn}>
                                            <TouchableOpacity style={{}} onPress={() => {

                                                Linking.openURL('https://etloob.com/%d8%ad%d8%b3%d8%a7%d8%a8%d9%8a/lost-password/')

                                            }}>
                                                <Text
                                                    style={[Typography.Text14Light]}>{strings.t("forgot_password")}</Text>
                                            </TouchableOpacity>

                                            <SubmitButton text={strings.t("sign_in")}
                                                          onPress={() => {
                                                              this.ActionLogin()
                                                          }}
                                            />


                                        </Row>
                                    </Grid>
                                </Row>

                                <Row size={1.5} style={styles.SignUpContainer}>

                                    <View>
                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: "center",
                                            justifyContent: 'center'
                                        }}>
                                            <Text
                                                style={[Typography.Text12Medium]}>{strings.t("dont_have_account")}</Text>

                                            <TextUnderLine ifHome={true} textStyle={"Medium"} onPress={() => {

                                                // this.props.onLogin()
                                                this.props.navigation.navigate(NAVIGATION_SIGNUP_SCREEN);
                                            }} text={strings.t("sign_up")}/>


                                        </View>


                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: "center",
                                            justifyContent: 'center',
                                            marginTop: wp("2%")
                                        }}>
                                            <Text style={[Typography.Text12Medium]}>{strings.t("not_now")}</Text>

                                            <TextUnderLine ifHome={true} textStyle={"Medium"} onPress={() => {

                                                // this.props.onLogin()
                                                this.props.navigation.navigate(NAVIGATION_HOME_SCREEN);
                                            }} text={strings.t("continue_w")}/>

                                        </View>


                                    </View>

                                </Row>
                            </Grid>
                        </Row>
                    </Grid>
                </ScrollView>


                {this.renderMessagesLogin()}
            </ApplicationLayout>

        );
    }

}


const styles = {
    ImageBackground: {
        width: wp('50%'),
        resizeMode: 'contain'
    }
    , logoContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end',
    }
    , formContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    }
    , formInputContainer: {
        paddingLeft: wp('8%'),
        paddingRight: wp('8%'),
        alignItems: 'center',
        justifyContent: 'center',
    }
    , SignUpContainer: {
        paddingTop: wp('3%'),
        justifyContent: 'center',
    },

    FlexDirectionColumn: {

        flex: 1,
        flexDirection: 'column'

    },
    backgroundWhite: {
        backgroundColor: '#fff',
        padding: wp('5%'),
        borderRadius: 4,
    },


}

const mapStateToProps = state => {
    return {
        isLoading: state.login.isLoading,
        status: state.login.status,
        ErrorMessage: state.login.ErrorMessage,
        user: state.login.user,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        Login: (payload) => dispatch(CustomerLogin(payload)),
        reset: (payload) => dispatch(resetAuthState(payload))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
