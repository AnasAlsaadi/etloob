import React, {useState, useEffect} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ApplicationLayout from './../../../containers/Layout'
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    Button,
    Text,
    Image,
    TouchableOpacity,
    View,
    ScrollView,
    I18nManager,
    Platform,
    NativeModules, AsyncStorage
} from 'react-native'
import {NAVIGATION_DRAWER, NAVIGATION_SIGNUP_SCREEN} from './../../../navigation/types'
import Typography from './../../../constants/typography'
import color from './../../../constants/colors'
import {InputElement, SubmitButton, TextUnderLine} from '../../elements'
import strings from "./../../../locales/i18n"
import {useSelector, useDispatch, connect} from 'react-redux';
import {CUSTOMER_SIGNUP, FAILURE, SUCCESS} from "../../../constants/actionsType";
import {CustomerLogin, resetAuthState} from "../../../store/login/actions";
import Toast from "react-native-root-toast";
import AuthHeader from "../../../containers/AuthHeader";
import {NAVIGATION_VERIFICATION_SCREEN} from "../../../navigation/types";
import CategoryHome from "../../elements/CategoryHome";
// import RestartAndroid from "react-native-restart-android";
import CodePush from "react-native-code-push";

class ChooseLanguage extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
        this.state = {
            password: "",
            username: "",
        }

    }


    render() {

        return (

            <ApplicationLayout isLoading={this.props.isLoading}>
                <View style={styles.container}>
                    <Image
                        style={styles.ImageBackground}
                        source={require('./../../../assets/images/Logo.png')}
                    />
                </View>
                <View style={[styles.container, {paddingStart: wp("8%"), paddingEnd: wp("8%")}]}>

                    <View  >

                        <TouchableOpacity onPress={() => {

                            AsyncStorage.setItem('lang', "ar")
                                .then(() => {
                                    ////////console.log("ar")
                                    ////////console.log("ar")
                                    I18nManager.forceRTL(true)
                                    CodePush.restartApp();
                                    // if (Platform.OS === "android") {
                                    //     // RestartAndroid.restart();
                                    //
                                    //     CodePush.restartApp();
                                    // }
                                    // else {
                                    //
                                    //     NativeModules.DevSettings.reload();
                                    // }

                                })

                        }} style={[styles.CenterContent, styles.itemColor,]}><Text style={[,
                            Typography.Text14Light

                            , {
                                color: 'black',
                                textAlign: 'center'
                            }]}>العربية</Text>
                        </TouchableOpacity>

                    </View>
                    {/*<View style={styles.buttonContainer}>*/}

                    {/*<View style={{height: wp("15%")}}>*/}
                    {/*<CategoryHome onPress={() => {*/}


                    {/*AsyncStorage.setItem('lang', "ar")*/}
                    {/*.then(() => {*/}
                    {/*//////console.log("ar")*/}
                    {/*//////console.log("ar")*/}
                    {/*I18nManager.forceRTL(true)*/}
                    {/*if (Platform.OS === "android") {*/}
                    {/*// RestartAndroid.restart();*/}

                    {/*CodePush.restartApp();*/}
                    {/*}*/}
                    {/*else {*/}

                    {/*NativeModules.DevSettings.reload();*/}
                    {/*}*/}

                    {/*})*/}


                    {/*// I18nManager.forceRTL(true)*/}
                    {/*// //////console.log(I18nManager.isRTL)*/}
                    {/*//*/}
                    {/*// global.storage.save({*/}
                    {/*//     key: 'lang',*/}
                    {/*//     data: "ar",*/}
                    {/*//     expires: null,*/}
                    {/*//     autoSync: true,*/}
                    {/*//     syncInBackground: true,*/}
                    {/*// })*/}
                    {/*// if (Platform.OS === "android") {*/}
                    {/*//     RestartAndroid.restart();*/}
                    {/*// }*/}
                    {/*// else {*/}
                    {/*//*/}
                    {/*//     NativeModules.DevSettings.reload();*/}
                    {/*// }*/}

                    {/*}} IfCategoryColor={true} selected={false} title={"العربية"}/>*/}

                    {/*</View>*/}
                    {/*</View>*/}
                    <View  >


                        <TouchableOpacity onPress={() => {

                            AsyncStorage.setItem('lang', "en")
                                .then(() => {
                                    //////console.log("ar")
                                    //////console.log("ar")
                                    I18nManager.forceRTL(false)
                                    CodePush.restartApp();

                                    // if (Platform.OS === "android") {
                                    //     // RestartAndroid.restart();
                                    //
                                    //     CodePush.restartApp();
                                    // }
                                    // else {
                                    //
                                    //     NativeModules.DevSettings.reload();
                                    // }

                                })

                        }} style={[styles.CenterContent, styles.itemColor, styles.selectedColor,]}><Text style={[,
                            Typography.Text18Light
                            , {
                                color: 'black',
                                textAlign: 'center'
                            }]}>English</Text>
                        </TouchableOpacity>

                        {/*<View style={{}}>*/}
                        {/*<CategoryHome onPress={() => {*/}


                        {/*AsyncStorage.setItem('lang', "en")*/}
                        {/*.then(() => {*/}
                        {/*//////console.log("en")*/}
                        {/*//////console.log("en")*/}
                        {/*I18nManager.forceRTL(false)*/}
                        {/*if (Platform.OS === "android") {*/}
                        {/*// RestartAndroid.restart();*/}

                        {/*CodePush.restartApp();*/}
                        {/*}*/}
                        {/*else {*/}

                        {/*NativeModules.DevSettings.reload();*/}
                        {/*}*/}
                        {/*})*/}


                        {/*// I18nManager.forceRTL(false)*/}
                        {/*// //////console.log(I18nManager.isRTL)*/}
                        {/*// global.storage.save({*/}
                        {/*//     key: 'lang',*/}
                        {/*//     data: "en",*/}
                        {/*//     expires: null,*/}
                        {/*//     autoSync: true,*/}
                        {/*//     syncInBackground: true,*/}
                        {/*// })*/}
                        {/*// if (Platform.OS === "android") {*/}
                        {/*//     RestartAndroid.restart();*/}
                        {/*// }*/}
                        {/*// else {*/}
                        {/*//*/}
                        {/*//     NativeModules.DevSettings.reload();*/}
                        {/*// }*/}

                        {/*}}*/}
                        {/*IfCategoryColor={true} selected={true} title={"English"}/>*/}

                        {/*</View>*/}
                    </View>
                </View>

            </ApplicationLayout>

        );
    }
}


const styles = {
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        // flex: 1,
    }, ImageBackground: {
        width: wp('50%'),
        resizeMode: 'contain'
    },
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    item: {

        flex: 1,
        alignItems: 'center', justifyContent: 'center',
        margin: hp("1%"),
        backgroundColor: color.CustomBlack,
        borderRadius: 8
    },
    itemColor: {

        // flex: 1,
        alignItems: 'center', justifyContent: 'center',
        marginEnd: hp("1%"),
        marginTop: hp("1%"),
        marginBottom: hp("1%"),
        paddingStart: hp("6%"),
        paddingEnd: hp("6%"),
        paddingTop: hp("2%"),
        paddingBottom: hp("2%"),
        borderRadius: 8,
        // width:wp("20%"),
        backgroundColor: color.white,
        borderWidth: 1,
        borderColor: color.CustomBlack
    }
    ,
    selectedColor: {
        backgroundColor: color.yellow,
        borderWidth: 0
    }
}


export default ChooseLanguage;
