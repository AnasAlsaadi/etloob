import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {ActivityIndicator, Text, ScrollView, Image, TouchableOpacity, View, TextInput, FlatList} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import {connect} from "react-redux";
import TabedFooter from "../../template/TabedFooter";
import {openSelectedVendor, Vendors} from "../../../store/vendor/actions";
import FastImage from 'react-native-fast-image'
import colors from "../../../constants/colors";
import Typography from "../../../constants/typography";
import {NAVIGATION_PRODUCTS_SCREEN} from "../../../navigation/types";
import {ResetSelectedCategory} from "../../../store/category/actions";
import {ResetSelectedSearchWord} from "../../../store/search/actions";

class VendorsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {
            page: 1,

        }


    }

    componentDidMount() {


        this.props.getVendorsList({})
    };



    handleRefresh = () => {

            this.props.getVendorsList({})

    };

    onPressVendor(vendor) {

        ////console.log(vendor)

        this.props.ResetSelectedCategory(null)
        this.props.ResetSelectedSearchWord(null)

        this.props.currentCategory=null
        this.props.selectedVendor(vendor)
        this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);


    }
    renderVn() {

        ////console.log(this.props.vendorsAllList)


    }


    render() {

        return (

            <DefaultHeader type={"home"} {...this.props} >
                <Grid>
                    <Row >
                        {
                            this.props.vendorsAllList &&
                            <FlatList
                                style={styles.container}
                                data={this.props.vendorsAllList}
                                numColumns={2}
                                renderItem={({item}) => (

                                    <View style={{alignItems: 'center', alignContent: 'center', flex:1, margin:wp("3"), backgroundColor:'white', borderRadius:8, borderWidth:1,borderColor:colors.GrayBorder}}>

                                        <TouchableOpacity style={{}}
                                                          onPress={this.onPressVendor.bind(this, item)}>

                                            <View style={{alignItems: 'center', alignContent: 'center'}}>
                                                <FastImage
                                                    style={styles.productImage}
                                                    source={{
                                                        uri: item.gravatar.length > 0 ? item.gravatar : "https://static.thenounproject.com/png/220984-200.png",
                                                        priority: FastImage.priority.high,
                                                    }}
                                                />

                                                <Text style={[Typography.Text14OpenSansBold ,{marginVertical: wp("5%"),textAlign:'center'}]}>{item.store_name}</Text>

                                            </View>
                                        </TouchableOpacity>
                                    </View>



                                    )}
                                keyExtractor={i => i.id.toString()}
                                refreshing={this.props.isLoadingVendor}
                                onRefresh={this.handleRefresh}
                                onEndReached={this.handleLoadMore}
                                onEndThreshold={0}
                            />
                        }
                    </Row>
                </Grid>


                {/*{this.props.isLoading && <View style={{flex:1, alignItems:'center'}}><ActivityIndicator size="large" color="#ff6a00" /></View>}*/}


                {this.renderVn()}
            </DefaultHeader>


        );
    }
}

const styles = {
    productImage: {
        marginTop:wp("1"),
        width: wp("35%"),
        height: wp("35%"),
        resizeMode: 'contain',
    },
    container:{
        flex:1,
        marginVertical:wp("3%")
    }
}
const mapStateToProps = state => {
    return {
        vendorsAllList: state.vendor.vendorsAllList,
        isLoadingVendor: state.vendor.isLoadingVendor,
        status: state.vendor.status,
        errorMessage: state.vendor.errorMessage,

    }
}
const mapDispatchToProps = dispatch => {
        return {

            getVendorsList: (payload) => dispatch(Vendors(payload)),
            selectedVendor: (payload) => dispatch(openSelectedVendor(payload)),
            // resetVendorListAll: (payload) => dispatch(resetVendorList(payload)),
            ResetSelectedCategory: (payload) => dispatch(ResetSelectedCategory(payload)),
            ResetSelectedSearchWord: (payload) => dispatch(ResetSelectedSearchWord(payload)),

        }
    }

// export default VendorsScreen
;
export default connect(mapStateToProps, mapDispatchToProps)

(
    VendorsScreen
)
;
