import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {FlatList, Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import {Categories} from "../../../store/category/actions";
import {connect} from "react-redux";
import {
    getCustomerCart,
    minusProductFromCart,
    plusProductFromCart,
    removeProductFromCart
} from "../../../store/cart/actions";
import colors from "../../../constants/colors";
import strings from "../../../locales/i18n";
import Typography from "../../../constants/typography";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import ProductCart from "../../elements/ProductCart";
import SubmitButton from "../../elements/Buttons";
import {NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_VERIFICATION_SCREEN} from "../../../navigation/types";
import TabedFooter from "../../template/TabedFooter";
import Toast from "react-native-root-toast";
import NumberFormat from "react-number-format";

class CartScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {}
    }

    renderTotal = () => {
        let sum = 0;
        if (this.props.cart !== null && this.props.cart.line_items) {

            this.props.cart.line_items.forEach((item) => {
                sum += parseFloat(item.product.price) * item.quantity;
            });
        }
        return sum;
    };

    componentDidMount() {

        this.props.customerCart({})

        //////console.log("catrt")
        //////console.log("catrt")
        //////console.log(this.props.cart)
    }

    renderFooter = () => {

        if (this.props.cart !== null && this.props.cart.line_items) {
            if (this.props.cart.line_items.length > 0) {
                return (<View style={{
                    flexDirection: 'column',
                }}>

                    <View style={[styles.CenterContent, {
                        backgroundColor: colors.white,
                        marginStart: wp("6%"),
                        marginEnd: wp("6%"),
                        marginTop: wp("3%"),
                        padding: wp("2%"),
                        borderRadius: 8,
                        borderColor: colors.GrayBorder
                    }]}>

                        <NumberFormat value={this.renderTotal()} renderText={value => <Text
                            style={[Typography.Text18OpenSansBold, {color: colors.CustomBlack}]}>{strings.t("total")} : {value} {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>}
                                      displayType={'text'} thousandSeparator={true} prefix={""}/>


                    </View>
                    <View style={[{
                        marginStart: wp("3%"),
                        marginEnd: wp("3%"),
                    }]}>

                        <SubmitButton text={strings.t("checkout")} onPress={() => {

                            if (global.logged) {


                                //console.log(this.renderTotal())
                                if(this.renderTotal()>1000)
                                {
                                    this.props.navigation.navigate(NAVIGATION_CHECKOUT_SCREEN);

                                }
                                else
                                {
                                    Toast.show(strings.t("amount_not_valid"), {
                                        textStyle: Typography.Text14Light,
                                        duration: Toast.durations.LONG,
                                        position: Toast.positions.BOTTOM,
                                        shadow: true,
                                        animation: true,
                                        hideOnPress: true,
                                        delay: 0,
                                    });
                                }

                            }
                            else {

                                Toast.show(strings.t("please_login_first"), {
                                    textStyle: Typography.Text14Light,
                                    duration: Toast.durations.LONG,
                                    position: Toast.positions.BOTTOM,
                                    shadow: true,
                                    animation: true,
                                    hideOnPress: true,
                                    delay: 0,
                                });

                            }
                        }}/>
                    </View>

                </View>)
            }
            else {
                return null
            }

        } else return null
    }

    removeItem(item) {


        this.props.removeProductFromCart(item)
        // this.props.removeProductFromCart(item)
        setTimeout(function () {

            this.props.customerCart()
        }.bind(this), 10)
    }

    plusItem(item) {


        this.props.plusProductFromCart(item)
        // this.props.removeProductFromCart(item)
        setTimeout(function () {
            this.props.customerCart()
        }.bind(this), 10)
    }

    minusItem(item) {


        this.props.minusProductFromCart(item)
        // this.props.removeProductFromCart(item)
        setTimeout(function () {

            this.props.customerCart()
        }.bind(this), 10)
    }


    render() {

        return (


            <DefaultHeader type={"home"} {...this.props} isLoading={this.props.isLoading}>

                <Grid>
                    <Row>
                        <FlatList
                            data={this.props.cart !== null && this.props.cart !== undefined ? this.props.cart.line_items : []}
                            renderItem={({item}) => (

                                <ProductCart
                                    key={"pr"+item.product.id}
                                    if_meta_data={item.compare_data && item.compare_data.length > 0}
                                    meta_data={item.compare_data}
                                    product={item.product} count={item.quantity}
                                    onPress={() => {
                                        //////console.log("product pressed")
                                    }}
                                    onPressRemoveProduct={this.removeItem.bind(this, item)}
                                    onPressPlus={this.plusItem.bind(this, item)}
                                    onPressMinus={this.minusItem.bind(this, item)}

                                />


                            )}
                            ListFooterComponent={this.renderFooter}
                            ListEmptyComponent={<View style={[styles.CenterContent, {
                                flex: 1,
                                marginTop: wp("4%"),
                            }]}><Text>{strings.t("empty")}</Text></View>}
                        />
                    </Row>


                </Grid>

            </DefaultHeader>


        );
    }
}


const styles = {


    CenterContent: {

        alignItems: 'center', justifyContent: 'center'
    },
}

const mapStateToProps = state => {
    return {
        isLoading: state.cart.isLoading,
        status: state.cart.status,
        errorMessage: state.cart.errorMessage,
        cart: state.cart.cart,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        customerCart: (payload) => dispatch(getCustomerCart(payload)),
        removeProductFromCart: (payload) => dispatch(removeProductFromCart(payload)),
        plusProductFromCart: (payload) => dispatch(plusProductFromCart(payload)),
        minusProductFromCart: (payload) => dispatch(minusProductFromCart(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);
