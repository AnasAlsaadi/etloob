import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";

class EditProfileScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    userCurrent = null;

    constructor(props) {
        super(props);
        this.state = {

            first_name: "",
            last_name: "",
            email: "",
        }
    }

    componentDidMount() {


        this.userCurrent = global.user

        this.setState({
            first_name: this.userCurrent.first_name,
            last_name: this.userCurrent.last_name,
            email: this.userCurrent.email
        })
        //////console.log("catrt in cehcout")
        //////console.log(this.props.cart)
    }


    edit_profile() {


        ////console.log("edit profile")
        ////console.log(this.state)
        this.props.actionEditProfile(this.state)


    }

    renderMessageEditProfile() {

        if (this.props.status === SUCCESS) {


            // global.storage.remove({key: 'user'})
            global.user = null

            global.storage.save({
                key: 'user', data: {

                    user: this.props.user,
                    logged: true,
                    is_active: true,
                }
            })

            global.user = this.props.user


            ////console.log("data saved")
            ////console.log(global.user)


            Toast.show(strings.t("edit_profile_success"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });
        }

        if (this.props.status === FAILURE) {

            Toast.show(this.props.ErrorMessage, {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            //this.props.actionResetPlaceOrder()
            // this.props.reset()


        }

    }

    render() {


        return (


            <DefaultHeader  {...this.props} isLoading={this.props.isLoading}>

                <Grid>

                    <Row>

                        <Grid  style={{flex: 1}}>
                            <Row size={0.1}><View style={{marginTop: wp("3%"), marginStart: wp("6%")}}><Text
                                style={[Typography.Text18OpenSansBold, {}]}>{strings.t("edit_profile")}</Text></View>
                            </Row>
                            <Row size={3.9} style={{flex: 1, paddingStart: wp('5%'), paddingEnd: wp("5%")}}>
                                <ScrollView style={{flex: 1,}}>

                                    <Grid style={{
                                        flexDirection: 'column', backgroundColor: '#fff',
                                        paddingTop: wp('3%'),
                                        paddingStart: wp('5%'),
                                        paddingBottom: wp('5%'),
                                        paddingEnd: wp("5%"),
                                        borderRadius: 4,
                                    }}>
                                        <InputElement forCheckout={true} text={strings.t('first_name')}
                                                      onChangeText={(value) => {
                                                          this.setState({first_name: value})
                                                      }
                                                      }
                                                      value={this.state.first_name}
                                        />
                                        <InputElement forCheckout={true} text={strings.t('last_name')}
                                                      onChangeText={(value) => {
                                                          this.setState({last_name: value})
                                                      }
                                                      }
                                                      value={this.state.last_name}
                                        />


                                        <InputElement forCheckout={true} text={strings.t('email_address')}
                                                      onChangeText={(value) => {
                                                          this.setState({email: value})
                                                      }
                                                      }
                                                      value={this.state.email}
                                        />


                                        <SubmitButton text={strings.t("edit_profile")} onPress={() => {

                                            this.edit_profile()
                                        }}/>


                                    </Grid>
                                </ScrollView>

                            </Row>

                        </Grid>
                    </Row>

                </Grid>

                {this.renderMessageEditProfile()}

            </DefaultHeader>


        );
    }
}


const mapStateToProps = state => {
    return {

        isLoading: state.profile.isLoading,
        status: state.profile.status,
        errorMessage: state.profile.errorMessage,
        user: state.profile.user,

    }
}
const mapDispatchToProps = dispatch => {
    return {

        actionEditProfile: (payload) => dispatch(editProfile(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);