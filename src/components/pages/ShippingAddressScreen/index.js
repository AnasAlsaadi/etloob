import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {StyleSheet,Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import RNPickerSelect from "react-native-picker-select";

class ShippingAddressScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    userCurrent = null;

    constructor(props) {
        super(props);
        this.state = {

            first_name: "",
            last_name: "",
            address_1: "",
            address_2: "",
            phone: "",
            email: "",
            country:"",

        }
    }

    componentDidMount() {


        this.userCurrent = global.user

        this.setState({
            first_name: this.userCurrent.billing.first_name,
            last_name: this.userCurrent.billing.last_name,
            address_1: this.userCurrent.billing.address_1,
            address_2: this.userCurrent.billing.address_2,
            phone: this.userCurrent.billing.phone,
            email: this.userCurrent.billing.email,
            country: this.userCurrent.billing.country,
        })
        //////console.log("catrt in cehcout")
        //////console.log(this.props.cart)
    }


    edit_profile() {


        ////console.log("edit profile")
        ////console.log(this.state)
        var user = {
            first_name: this.userCurrent.first_name,
            last_name: this.userCurrent.last_name,
            email: this.userCurrent.email,
            billing: {

                first_name: this.state.first_name,
                last_name: this.state.last_name,
                address_1: this.state.address_1,
                address_2: this.state.address_2,
                phone: this.state.phone,
                email: this.state.email,
                country: this.state.country,
            }
        }
        this.props.actionEditProfile(user)


    }

    renderMessageEditProfile() {

        if (this.props.status === SUCCESS) {


            // global.storage.remove({key: 'user'})
            global.user = null

            global.storage.save({
                key: 'user', data: {

                    user: this.props.user,
                    logged: true,
                    is_active: true,
                }
            })

            global.user = this.props.user


            ////console.log("data saved")
            ////console.log(global.user)


            Toast.show(strings.t("edit_profile_success"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });
        }

        if (this.props.status === FAILURE) {

            Toast.show(this.props.ErrorMessage, {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            //this.props.actionResetPlaceOrder()
            // this.props.reset()


        }

    }

    render() {


        return (


            <DefaultHeader  {...this.props} isLoading={this.props.isLoading}>

                <Grid>

                    <Row >

                        <Grid style={{flex: 1}}>
                            <Row size={0.1}><View style={{marginTop: wp("3%"),marginBottom: wp("3%"), marginStart: wp("6%")}}><Text
                                style={[Typography.Text18OpenSansBold, {}]}>{strings.t("shipping_address")}</Text></View>
                            </Row>
                            <Row size={3.9} style={{flex: 1, paddingStart: wp('5%'), paddingEnd: wp("5%")}}>
                                <ScrollView style={{flex: 1,}}>

                                    <Grid style={{
                                        flexDirection: 'column', backgroundColor: '#fff',
                                        paddingTop: wp('3%'),
                                        paddingStart: wp('5%'),
                                        paddingBottom: wp('5%'),
                                        paddingEnd: wp("5%"),
                                        borderRadius: 4,
                                    }}>
                                        <InputElement forCheckout={true} text={strings.t('first_name')}
                                                      onChangeText={(value) => {
                                                          this.setState({first_name: value})
                                                      }
                                                      }
                                                      value={this.state.first_name}
                                        />
                                        <InputElement forCheckout={true} text={strings.t('last_name')}
                                                      onChangeText={(value) => {
                                                          this.setState({last_name: value})
                                                      }
                                                      }
                                                      value={this.state.last_name}
                                        />

                                        <RNPickerSelect
                                            style={{}}

                                            placeholder={this.oj}
                                            onValueChange={(value) => {

                                                this.setState({country: value})
                                                ////console.log(value)
                                            }}

                                            value={this.state.country}
                                            items={[
                                                {label: 'الحسكة', value: 'AI'},
                                                {label: 'القامشلي', value: 'BS'},
                                                {label: 'السويداء', value: 'AW'},
                                                {label: 'حلب', value: 'AL'},
                                                {label: 'دمشق', value: 'AF'},
                                                {label: 'درعا', value: 'DZ'},
                                                {label: 'دير الزور', value: 'AD'},
                                                {label: 'حماة', value: 'AO'},
                                                {label: 'حمص', value: 'AQ'},
                                                {label: 'إدلب', value: 'AZ'},
                                                {label: 'اللاذقية', value: 'AG'},
                                                {label: 'القنيطرة', value: 'AR'},
                                                {label: 'الرقة', value: 'AM'},
                                                {label: 'ريف دمشق', value: 'AX'},
                                                {label: 'طرطوس', value: 'AT'},
                                            ]}
                                        />


                                        <InputElement forCheckout={true} text={strings.t('street_address')}
                                                      onChangeText={(value) => {
                                                          this.setState({address_1: value})
                                                      }
                                                      }
                                                      value={this.state.address_1}
                                        />

                                        <InputElement forCheckout={true} text={strings.t('street_address2_placeholder')}
                                                      onChangeText={(value) => {
                                                          this.setState({address_2: value})
                                                      }
                                                      }
                                                      value={this.state.address_2}
                                        />


                                        <InputElement forCheckout={true} text={strings.t('mobile_number')}
                                                      onChangeText={(value) => {
                                                          this.setState({phone: value})
                                                      }
                                                      }
                                                      value={this.state.phone}
                                        />

                                        <InputElement forCheckout={true} text={strings.t('email_address')}
                                                      onChangeText={(value) => {
                                                          this.setState({email: value})
                                                      }
                                                      }
                                                      value={this.state.email}
                                        />


                                        <SubmitButton text={strings.t("save")} onPress={() => {

                                            this.edit_profile()
                                        }}/>


                                    </Grid>
                                </ScrollView>

                            </Row>

                        </Grid>
                    </Row>

                </Grid>

                {this.renderMessageEditProfile()}

            </DefaultHeader>


        );
    }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        marginTop: wp("3%"),
        marginBottom: wp("3%"),
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        marginTop: wp("3%"),
        marginBottom: wp("3%"),
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});
const mapStateToProps = state => {
    return {

        isLoading: state.profile.isLoading,
        status: state.profile.status,
        errorMessage: state.profile.errorMessage,
        user: state.profile.user,

    }
}
const mapDispatchToProps = dispatch => {
    return {




        actionEditProfile: (payload) => dispatch(editProfile(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShippingAddressScreen);