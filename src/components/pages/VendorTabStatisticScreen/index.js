import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput, FlatList, Linking} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER, NAVIGATION_EDIT_PROFILE_SCREEN, NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN, NAVIGATION_MYORDERS_SCREEN, NAVIGATION_PRODUCTS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import FastImage from "react-native-fast-image";
import colors from "../../../constants/colors";
import CodePush from "react-native-code-push";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import ProductVendor from "../../elements/ProductVendor";
import RNPickerSelect from "react-native-picker-select";
import {Categories, ResetSelectedCategory} from "../../../store/category/actions";
import {productLists} from "../../../store/ProductList/actions";
import color from '../../../constants/colors';
import axios from "axios";
import {config, url} from "../../../api/api";
import {ResetSelectedSearchWord} from "../../../store/search/actions";
import {openSelectedVendor} from "../../../store/vendor/actions";
import NumberFormat from "react-number-format";

class VendorTabStatisticScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};


    constructor(props) {
        super(props);
        this.state = {

            sales: "0",
            orders_count: "0",
            products_count: "0",
            pageviews: "0",
        }
    }

    componentDidMount() {

        axios.get('https://etloob.com/wp-json/dokan/v1/stores/summary?seller_id=' + global.user.id,
            config)
            .then(function (response) {
                console.log("instide req local");
                console.log(response);
                console.log("instide req");

                this.setState({
                    sales: parseFloat(response.data.earning).toFixed(2) + "",
                    orders_count: response.data.orders_count.total + "",
                    products_count: response.data.products_count + "",
                    pageviews: response.data.pageviews + "",


                })
            }.bind(this))
            .catch(function (error) {
                console.log("instide err local");
                console.log(error.response.data);
                console.log(error);
                return {status: 400, response: null, message: error.response.data.message}
            });

    }

    render() {


        return (
            <Grid>

                <Row>
                    <View style={{flex: 1}}>
                        <Grid style={{flex: 1,}}>

                            <Row style={styles.styleStatisticItem}>
                                <View style={styles.containerItem}>

                                    <NumberFormat value={this.state.sales} renderText={value => <Text
                                        style={[styles.subTitleText]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                                  displayType={'text'}
                                                  thousandSeparator={true} prefix={""}/>

                                    <Text style={styles.titleText}>{strings.t("sales")}</Text>


                                </View>
                            </Row>

                            <Row style={styles.styleStatisticItem}>
                                <View style={styles.containerItem}>

                                    <Text style={styles.subTitleText}>{this.state.orders_count}</Text>

                                    <Text style={styles.titleText}>{strings.t("total_orders")}</Text>


                                </View>
                            </Row>

                            <Row style={styles.styleStatisticItem}>
                                <View style={styles.containerItem}>

                                    <Text style={styles.subTitleText}>{this.state.products_count}</Text>

                                    <Text style={styles.titleText}>{strings.t("total_products")}</Text>


                                </View>
                            </Row>

                            <Row style={styles.styleStatisticItem}>
                                <View style={styles.containerItem}>

                                    <Text style={styles.subTitleText}>{this.state.pageviews}</Text>

                                    <Text style={styles.titleText}>{strings.t("total_views")}</Text>


                                </View>
                            </Row>


                        </Grid>
                    </View>
                </Row>
            </Grid>)

            ;

    }

}

const styles = {

    styleStatisticItem: {

        backgroundColor: colors.white,
        marginVertical: wp("6%"),
        marginHorizontal: wp("6%"),
        borderRadius: 8,
        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        },
        flex: 1, paddingStart: wp('5%'), paddingEnd: wp("5%"), alignItems: 'center', justifyContent: 'center'
    },
    titleText: {

        ...Typography.Text18Light,
        color: colors.CustomBlack,
    },
    subTitleText: {

        paddingTop: wp("2%"),
        ...Typography.Text18OpenSansBold,
        color: colors.CustomBlack,
    },
    containerItem:

        {
            flex: 1,

            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            backgroundColor: colors.white,
            marginVertical: wp("4%"),
            marginHorizontal: wp("4%"),
            borderRadius: 8,
        },
    profileImgContainer: {
        marginVertical: wp("3%"),
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        overflow: 'hidden'
    },
    productImage: {
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        resizeMode: 'contain',
    },
    icon: {
        width: wp("12%"),
        height: wp("12%"),
        resizeMode: 'contain',
    },
    iconArrow: {
        width: wp("4%"),
        height: wp("4%"),

        resizeMode: 'contain',
    },
    TextDetails: {
        flex: 1,
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },
    TextLi: {
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },

    item: {

        marginHorizontal: wp("5%"),
        marginVertical: wp("3%"),
        borderWidth: 1,
        borderRadius: 2,
        borderColor: colors.GrayBorder,
        backgroundColor: 'white',
    },
    itemContainer: {

        flexDirection: 'row',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    textContainer: {

        flex: 3,
        alignItems: 'flex-start',
        marginStart: wp("3%")
    }
}

const mapStateToProps = state => {
    return {}
}
const mapDispatchToProps = dispatch => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(VendorTabStatisticScreen);