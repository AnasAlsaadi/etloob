import React, {useState} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ApplicationLayout from './../../../containers/Layout'
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput, Linking} from 'react-native'
import Typography from './../../../constants/typography'
import AppStyle from './../../../constants/AppStyles'
import CheckBox from 'react-native-check-box'
import {NAVIGATION_VERIFICATION_SCREEN} from "../../../navigation/types";
import AppStyles from "../../../constants/AppStyles";
import AuthHeader from './../../../containers/AuthHeader'
import {InputElement, SubmitButton, TextUnderLine} from '../../elements'
import strings from "./../../../locales/i18n"
import {CustomerSignUp, resetAuthState} from "../../../store/auth/actions";
import {useSelector, useDispatch, connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import {FAILURE, SUCCESS} from "../../../constants/actionsType";

class SignUpScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
        this.state = {

            type: "customer",
            CustomerChecked: true,
            VendorChecked: false,
            isCheckedTerms: true,
            isCheckedCustomer: true,
            isCheckedVendor: false,
            loader: false,
            mobileNumber: "",
            email: "",
            password: "",
            showPassword: true

        }
        this.ActionSignup = this.ActionSignup.bind(this)
        this.renderMessages = this.renderMessages.bind(this)
    }


    ActionSignup() {


        if (!this.state.isCheckedCustomer) {
            Toast.show(strings.t("please_accept_terms"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });
            return
        }

        if (this.state.mobileNumber.length < 10 || this.state.mobileNumber.length > 10 || this.state.mobileNumber[0]!=="0" || this.state.mobileNumber[1]!=="9") {

            Toast.show(strings.t("please_write_true_phone_length"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,


                hideOnPress: true,
                delay: 0,
            });

            return
        }

        if (!this.state.email.length > 0) {
            Toast.show(strings.t("email_not_valid"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            return
        }
        if (!this.state.password.length > 0) {
            Toast.show(strings.t("password_not_valid"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            return
        }

        this.props.signUp({
            email: this.state.email,
            password: this.state.password,
            billing_phone: this.state.mobileNumber
        })


        // if (this.state.isCheckedCustomer
        //     && this.state.mobileNumber.length === 10
        //     && this.state.email.length > 0
        //     && this.state.password.length > 0) {
        //
        //
        // } else {
        //
        //     Toast.show(strings.t("please_fill_all_inputs"), {
        //         textStyle: Typography.Text14Light,
        //         duration: Toast.durations.LONG,
        //         position: Toast.positions.BOTTOM,
        //         shadow: true,
        //         animation: true,
        //         hideOnPress: true,
        //         delay: 0,
        //     });
        //
        //     // this.props.navigation.navigate(NAVIGATION_VERIFICATION_SCREEN);
        // }


    }

    renderMessages() {

        if (this.props.status === SUCCESS) {


            this.props.reset()

            //////console.log("user data")
            //////console.log({user: this.props.user, logged: false, is_active: false})
            global.storage.save({
                key: 'user',
                data: {user: this.props.user, logged: false, is_active: false},
                expires: null
            })
            global.user = this.props.user
            global.logged = false
            global.is_active = false

            this.props.navigation.navigate(NAVIGATION_VERIFICATION_SCREEN)

        }

        if (this.props.status === FAILURE) {

            Toast.show(this.props.ErrorMessage, {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });

            this.props.reset()


        }

    }

    render() {

        return (

            <ScrollView style={{flex: 1,}} showsHorizontalScrollIndicator={false}>
                <AuthHeader {...this.props} isLoading={this.props.isLoading}>

                    <Grid style={styles.container}>
                        <Row size={0.3} style={styles.SignUP}>
                            <Text style={[Typography.Text30Bold, AppStyle.Yellow]}>{strings.t("sign_up")}</Text>
                        </Row>
                        <Row size={0.2} style={{}}>
                            <Grid style={styles.ButtonChoose}>
                                <Col>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                type: "customer",
                                                CustomerChecked: true,
                                                VendorChecked: false,
                                            })
                                        }}
                                        style={
                                            this.state.CustomerChecked ? styles.checkedButton : styles.UncheckedButton
                                        }>

                                        <Text style={[Typography.Text14OpenSansBold]}>{strings.t("customer")}</Text>
                                    </TouchableOpacity>
                                </Col>
                                {/*<Col>*/}
                                {/*<TouchableOpacity*/}
                                {/*onPress={() => {*/}
                                {/*this.setState({*/}
                                {/*type: "vendor",*/}
                                {/*VendorChecked: true,*/}
                                {/*CustomerChecked: false*/}
                                {/*})*/}
                                {/*}}*/}
                                {/*style={*/}
                                {/*this.state.VendorChecked ? styles.checkedButton : styles.UncheckedButton*/}
                                {/*}>*/}
                                {/*<Text>{strings.t("vendor")}</Text>*/}
                                {/*</TouchableOpacity>*/}
                                {/*</Col>*/}
                            </Grid>
                        </Row>
                        <Row size={3.5} style={{paddingTop: hp("4%")}}>


                            {
                                this.state.type === 'customer' ?

                                    <Grid style={styles.FormContainer}>

                                        <Row>
                                            <Grid style={{flexDirection: 'column', flex: 1}}>
                                                <InputElement
                                                    keyboardType={"numeric"}
                                                    text={strings.t("mobile_number")}
                                                    onChangeText={(value) => {
                                                        this.setState({mobileNumber: value})
                                                    }
                                                    }

                                                    value={this.state.mobileNumber}/>
                                                <InputElement text={strings.t("email_address")}

                                                              onChangeText={(value) => {
                                                                  this.setState({email: value})
                                                              }
                                                              }
                                                              value={this.state.email}
                                                />
                                                <InputElement textLength={this.state.password.length}
                                                              onPressShow={() => {

                                                                  this.setState({showPassword: !this.state.showPassword})
                                                              }} type={"password"}
                                                              secureTextEntry={this.state.showPassword}

                                                              text={strings.t("password")}
                                                              onChangeText={(value) => {
                                                                  this.setState({password: value})
                                                              }
                                                              }
                                                              value={this.state.password}
                                                />

                                            </Grid>
                                        </Row>

                                        <Row>
                                            <Grid>
                                                <Row size={1} style={{flexDirection: 'column',}}>

                                                    <Grid>
                                                        <Col style={{flexDirection: 'row'}}>
                                                            <View style={[, {
                                                                flexDirection: 'row',
                                                                justifyContent: 'center',
                                                                alignItems: 'center'
                                                            }]}>
                                                                <CheckBox
                                                                    style={{paddingEnd: wp("1%")}}
                                                                    onClick={() => {
                                                                        this.setState({
                                                                            isCheckedCustomer: !this.state.isCheckedCustomer
                                                                        })
                                                                    }}
                                                                    isChecked={true}
                                                                />
                                                                <Text
                                                                    style={[Typography.Text13]}>{strings.t("i_have_read")}</Text>
                                                                <TextUnderLine onPress={()=>{

                                                                    Linking.openURL('https://etloob.com/%d8%b4%d8%b1%d9%88%d8%b7-%d8%a7%d8%b3%d8%aa%d8%ae%d8%af%d8%a7%d9%85-%d8%a7%d9%84%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a/');

                                                                }
                                                                }


                                                                               text={strings.t("terms_and_conditions")}
                                                                               style={{
                                                                                   marginStart: I18nManager.isRTL ? wp("0%") : wp("0.5%")

                                                                               }}/>

                                                            </View>
                                                            {/*<View style={styles.containerPrivacy}>*/}

                                                            {/**/}

                                                            {/*</View>*/}
                                                        </Col>
                                                    </Grid>


                                                </Row>
                                                <Row size={2}
                                                     style={styles.containerPolicy}>

                                                    <TouchableOpacity

                                                        onPress={()=>{

                                                            Linking.openURL('https://etloob.com/%d8%b4%d8%b1%d9%88%d8%b7-%d8%a7%d8%b3%d8%aa%d8%ae%d8%af%d8%a7%d9%85-%d8%a7%d9%84%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a/');

                                                        }
                                                        }
                                                    >
                                                        <Text
                                                            style={[Typography.Text12Medium, {lineHeight: hp("2%")}]}>{strings.t("privacy_policy_des")}
                                                            <Text
                                                                style={AppStyles.Yellow}> {strings.t("privacy_policy")}</Text></Text>

                                                    </TouchableOpacity>


                                                </Row>
                                                <Row size={1} style={{flex: 1, flexDirection: 'column'}}>
                                                    <SubmitButton text={strings.t("sign_up")} onPress={() => {
                                                        this.ActionSignup()
                                                    }}/>
                                                </Row>

                                            </Grid>
                                        </Row>

                                        <Row/>
                                    </Grid>
                                    :

                                    <Grid style={styles.FormContainer}>

                                        <Row size={2.7}>
                                            <Grid style={{flexDirection: 'column',}}>
                                                <InputElement text={strings.t("mobile_number")}></InputElement>
                                                <InputElement text={strings.t("email_address")}></InputElement>
                                                <InputElement text={strings.t("password")}
                                                              secureTextEntry={true}></InputElement>
                                                <InputElement text={strings.t("first_name")}></InputElement>
                                                <InputElement text={strings.t("last_name")}></InputElement>
                                                <InputElement text={strings.t("shop_name")}></InputElement>
                                            </Grid>
                                        </Row>

                                        <Row size={1.3}>
                                            <Grid>
                                                <Row size={1} style={{flexDirection: 'column',}}>

                                                    <Grid>
                                                        <Col style={{flexDirection: 'row'}}>
                                                            <View style={[, {
                                                                flexDirection: 'row',
                                                                justifyContent: 'center',
                                                                alignItems: 'center'
                                                            }]}>
                                                                <CheckBox
                                                                    style={{paddingEnd: wp("1%")}}
                                                                    onClick={() => {
                                                                        this.setState({
                                                                            isCheckedVendor: !this.state.isCheckedVendor
                                                                        })
                                                                    }}
                                                                    isChecked={this.state.isCheckedVendor}
                                                                />
                                                                <Text
                                                                    style={[Typography.Text13]}>{strings.t("i_have_read")}</Text>

                                                                <TextUnderLine
                                                                    onPress={()=>{

                                                                        Linking.openURL('https://etloob.com/%d8%b4%d8%b1%d9%88%d8%b7-%d8%a7%d8%b3%d8%aa%d8%ae%d8%af%d8%a7%d9%85-%d8%a7%d9%84%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a/');

                                                                    }
                                                                    }
                                                                    text={strings.t("terms_and_conditions")}
                                                                               style={{
                                                                                   marginStart: I18nManager.isRTL ? wp("0%") : wp("0.5%")

                                                                               }}/>

                                                            </View>
                                                            {/*<View style={styles.containerPrivacy}>*/}

                                                            {/*<TextUnderLine text={strings.t("terms_and_conditions")}*/}
                                                            {/*style={{*/}
                                                            {/*marginStart: I18nManager.isRTL ? wp("-7.50%") : wp("7.5%")*/}

                                                            {/*}}/>*/}

                                                            {/*</View>*/}
                                                        </Col>
                                                    </Grid>
                                                </Row>
                                                <Row size={2}
                                                     style={styles.containerPolicy}>

                                                    <TouchableOpacity
                                                        onPress={()=>{


                                                            Linking.openURL('https://etloob.com/%d8%b4%d8%b1%d9%88%d8%b7-%d8%a7%d8%b3%d8%aa%d8%ae%d8%af%d8%a7%d9%85-%d8%a7%d9%84%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a/');

                                                        }
                                                        }

                                                    >
                                                        <Text
                                                            style={[Typography.Text12Medium, {lineHeight: hp("2%")}]}>{strings.t("privacy_policy_des")}<Text
                                                            style={AppStyles.Yellow}>{strings.t("privacy_policy")}</Text></Text>

                                                    </TouchableOpacity>


                                                </Row>
                                                <Row size={1} style={{

                                                    flex: 1,
                                                    flexDirection: 'column'

                                                }}>
                                                    <SubmitButton text={strings.t("sign_up")} onPress={() => {

                                                        this.props.navigation.navigate(NAVIGATION_VERIFICATION_SCREEN);
                                                    }}/>
                                                </Row>

                                            </Grid>
                                        </Row>

                                    </Grid>
                            }

                        </Row>
                    </Grid>

                    {this.renderMessages()}


                </AuthHeader>
            </ScrollView>

        );
    }
}

const styles = {


    checkedButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffce2b',
        color: 'black',
        borderRadius: 5,
        marginEnd: wp("2%"),
        marginStart: wp("2%"),
        ...Typography.Text12OpenSansBold
    }
    , UncheckedButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        color: 'black',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'black',
        marginStart: wp("2%"),
        marginEnd: wp("2%"),
        ...Typography.Text14Light
    },
    container: {

        backgroundColor: 'white',
        flex: 1, height: hp("100%")
    },
    SignUP: {
        paddingStart: hp("4%"), paddingTop: hp("2%")
    },
    ButtonChoose: {
        paddingStart: wp("1.5%"), paddingEnd: wp("1.5%")
    },
    FormContainer: {
        flexDirection: 'column',
        flex: 1,
        padding: wp("4%"),
        backgroundColor: 'white',

    },
    containerPrivacy: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'

    },
    containerPolicy: {
        paddingTop: hp("3%"), paddingStart: wp("1%")
    }


}


const mapStateToProps = state => {
    return {
        isLoading: state.auth.isLoading,
        status: state.auth.status,
        ErrorMessage: state.auth.ErrorMessage,
        user: state.auth.user,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        signUp: (payload) => dispatch(CustomerSignUp(payload)),
        reset: (payload) => dispatch(resetAuthState(payload))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
// export default SignUpScreen
