import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput, FlatList, Linking} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER, NAVIGATION_EDIT_PROFILE_SCREEN, NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN, NAVIGATION_MYORDERS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import FastImage from "react-native-fast-image";
import colors from "../../../constants/colors";
import CodePush from "react-native-code-push";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import ProductVendor from "../../elements/ProductVendor";
import RNPickerSelect from "react-native-picker-select";
import {Categories} from "../../../store/category/actions";
import {productLists, productListsRest} from "../../../store/ProductList/actions";
import color from '../../../constants/colors';

class VendorTabProductsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};


    constructor(props) {
        super(props);
        this.state = {

            first_name: "",
            last_name: "",
            email: "",
            categoriesList: [],
            category: null,
            order_by: null,
            page: 1,
            productListAll: []
        }
    }

    componentDidMount() {
        this.props.getCategories({})


    }




    search_products() {

        this.props.productListsRest(null)
        this.props.getProducts({
            vendor: global.user.id,
            page:1,
            refresh: false,
            order_by: this.state.order_by,
            attGet: this.getSelectedAttrSearch()
        })

    }


    handleRefresh = () => {

        this.setState({page: 1}, () => {

            this.props.getProducts({
                vendor: global.user.id,
                page: this.state.page,
                refresh: false,
                order_by: this.state.order_by,
                attGet: this.getSelectedAttrSearch()

            })


        })
    };

    handleLoadMore = () => {

        var page = this.state.page;
        page = page + 1
        this.setState({page: page}, () => {

            this.props.getProducts({
                vendor: global.user.id,
                page: this.state.page,
                refresh: false,
                order_by: this.state.order_by,
                attGet: this.getSelectedAttrSearch()

            })

        })
    };

    getSelectedAttrSearch() {
        var getAttr = "";

        if (this.state.category) {
            getAttr = "&category=" + this.state.category + "&"
        }
        if (this.state.order_by) {
            getAttr += "order_by=" + this.state.order_by;
        }

        return getAttr
    }

    render() {


        return (
            <Grid style={{}}>

                <Row>
                    <View style={{flex: 1}}>
                        <Grid style={{flex: 1,}}>


                            <Row size={1.5} style={{flexDirection: 'column', paddingTop:wp("3%")}} >
                                <View style={{
                                    flex: 1, flexDirection: "row", alignItems: 'center',
                                    paddingStart: wp("6%"),
                                    paddingEnd: wp("6%"),
                                    backgroundColor: 'white', paddingTop: wp("3%"), paddingBottom: wp("3%")
                                }}>

                                    <View style={{flex: 2,}}>
                                        <Text
                                            style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("order_by")}: </Text>
                                    </View>
                                    <View style={{flex: 2,marginBottom:wp("3%"),marginTop:wp("3%")}}>
                                        <RNPickerSelect

                                            style={{marginTop: wp("3%"), marginBottom: wp("3%"),paddingTop:wp("3%"),paddingBottom:wp("3%")}}
                                            placeholder={this.oj}
                                            onValueChange={(value) => {

                                                this.setState({order_by: value}, function () {

                                                    // this.props.productListsRest()
                                                    // this.handleRefresh()

                                                }.bind(this))
                                                ////console.log(value)
                                                // this.props.productList = []
                                                // this.handleRefresh()

                                            }}
                                            items={[
                                                {label: strings.t("Popularity"), value: 'popularity'},
                                                {label: strings.t("Average_rating"), value: 'rating'},
                                                {label: strings.t("Newness"), value: 'date'},
                                                {label: strings.t("P_l_t_h"), value: 'price'},
                                                {label: strings.t("P_h_t_l"), value: 'price-desc'},
                                            ]}
                                        />
                                    </View>
                                </View>
                                <View style={{
                                    flex: 1, flexDirection: "row", alignItems: 'center',
                                    paddingStart: wp("6%"),
                                    paddingEnd: wp("6%"),
                                    backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                                }}>
                                    <View style={{flex: 2,}}>
                                        <Text
                                            style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("choose_category")}: </Text>
                                    </View>
                                    <View style={{flex: 2}}>
                                        <RNPickerSelect
                                            style={{marginTop: wp("3%"), marginBottom: wp("3%")}}
                                            placeholder={this.oj}
                                            onValueChange={(value) => {

                                                this.setState({category: value}, function () {

                                                    // this.props.productListsRest()
                                                    // this.handleRefresh()

                                                }.bind(this))
                                                ////console.log(value)
                                                // this.props.productList = []
                                                // this.handleRefresh()

                                            }}
                                            items={this.state.categoriesList}
                                        />
                                    </View>
                                </View>
                                <View style={{
                                    flex: 1, flexDirection: "row", alignItems: 'center',
                                    paddingStart: wp("6%"),
                                    paddingEnd: wp("6%"),
                                    backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                                }}>
                                    <View style={{flex: 2,}}>
                                        <Text
                                            style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("press_to_search")}: </Text>
                                    </View>
                                    <View style={{flex: 2}}>

                                        <TouchableOpacity style={{
                                            backgroundColor: color.yellow,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 8,
                                            height: hp('4.5%'),
                                            marginTop: hp('2%')
                                        }} onPress={() => {

                                            this.search_products()
                                        }}
                                        >
                                            <Text
                                                style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("search")}</Text>
                                        </TouchableOpacity>

                                    </View>
                                </View>

                                <View style={{
                                    flex: 1, flexDirection: "row", alignItems: 'center',
                                    paddingStart: wp("6%"),
                                    paddingEnd: wp("6%"),
                                    backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                                }}>
                                    <View style={{flex: 2,}}>
                                    </View>
                                    <View style={{flex: 2}}>

                                        <TouchableOpacity style={{
                                            backgroundColor: color.yellow,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 8,
                                            height: hp('4.5%'),
                                            marginTop: hp('2%')
                                        }} onPress={() => {

                                            Linking.openURL('https://etloob.com/btrf');

                                        }}
                                        >

                                            <Text
                                                style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("add_product")}</Text>
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </Row>


                            <Row size={2.5} style={{flexDirection: 'column'}}>

                                    <FlatList
                                        data={this.props.productList}
                                        renderItem={({item}) => (

                                            <ProductVendor

                                                product={item}
                                            />
                                        )}
                                        keyExtractor={(item, index) => item.id.toString()}
                                        refreshing={this.props.isLoadingLIST}
                                        onRefresh={this.handleRefresh}
                                        onEndReached={() => {
                                            this.handleLoadMore()
                                        }}
                                        onEndThreshold={0}

                                    />

                            </Row>
                        </Grid>
                    </View>
                </Row>
                {this.Rendercategories()}
            </Grid>)

            ;

    }

    Rendercategories() {

        if (this.props.statusC === SUCCESS) {
            var newCat = []
            // this.state.categoriesList
            this.props.categories.map((cat) => {
                newCat.push(
                    {label: cat.name, value: cat.term_id}
                )
                if (cat.sub_categories.length > 0) {
                    cat.sub_categories.map((cat2) => {

                        newCat.push(
                            {label: cat2.name, value: cat2.term_id}
                        )
                        if (cat2.sub_categories.length > 0) {
                            cat2.sub_categories.map((cat3) => {

                                newCat.push(
                                    {label: cat3.name, value: cat3.term_id}
                                )
                            })


                        }
                    })


                }

            })

            if (this.state.categoriesList.length === 0) {

                this.setState({categoriesList: newCat});
              //  console.log(SUCCESS)
              //  console.log(this.props.categories)
            }
        }
        else {
           // console.log("error")
        }
    }
}

const styles = {
    profileImgContainer: {
        marginVertical: wp("3%"),
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        overflow: 'hidden'
    },
    productImage: {
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        resizeMode: 'contain',
    },
    icon: {
        width: wp("12%"),
        height: wp("12%"),
        resizeMode: 'contain',
    },
    iconArrow: {
        width: wp("4%"),
        height: wp("4%"),

        resizeMode: 'contain',
    },
    TextDetails: {
        flex: 1,
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },
    TextLi: {
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },

    item: {

        marginHorizontal: wp("5%"),
        marginVertical: wp("3%"),
        borderWidth: 1,
        borderRadius: 2,
        borderColor: colors.GrayBorder,
        backgroundColor: 'white',
    },
    itemContainer: {

        flexDirection: 'row',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    textContainer: {

        flex: 3,
        alignItems: 'flex-start',
        marginStart: wp("3%")
    }
}

const mapStateToProps = state => {
    return {

        // isLoading: state.category.isLoading,
        statusC: state.category.status,
        categories: state.category.categories,
        productList: state.productList.productList,
        isLoadingLIST: state.productList.isLoadingLIST,
        status: state.productList.status,
        errorMessage: state.productList.errorMessage,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        getProducts: (payload) => dispatch(productLists(payload)),

        getCategories: (payload) => dispatch(Categories(payload)),
        productListsRest: (payload) => dispatch(productListsRest(payload)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VendorTabProductsScreen);
