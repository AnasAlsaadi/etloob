import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {FlatList, Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import {Categories} from "../../../store/category/actions";
import {connect} from "react-redux";
import {getCustomerCart,} from "../../../store/cart/actions";
import colors from "../../../constants/colors";
import strings from "../../../locales/i18n";
import Typography from "../../../constants/typography";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import ProductCart from "../../elements/ProductCart";
import SubmitButton from "../../elements/Buttons";
import {NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_VERIFICATION_SCREEN} from "../../../navigation/types";
import TabedFooter from "../../template/TabedFooter";
import {getCustomerFavorite} from "../../../store/favorite/actions";
import {addToFavorite} from "../../../store/product/actions";
import Toast from "react-native-root-toast";
import {SUCCESS} from "../../../constants/actionsType";

class FavoriteScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {}
    }


    componentDidMount() {

        this.props.customerFavorite({})

        //////console.log("catrt")
        //////console.log("catrt")
        //////console.log(this.props.cart)
    }


    removeItem(item) {

        this.props.removeProductFromFavorite(item)
        // this.props.removeProductFromCart(item)
        setTimeout(function () {

            this.props.customerFavorite()
        }.bind(this), 100)
    }


    render() {

        return (


            <DefaultHeader type={"home"} {...this.props} isLoading={this.props.isLoading}>

                <Grid>
                    <Row>
                        <FlatList
                            data={this.props.favorite !== null && this.props.favorite !== undefined ? this.props.favorite : []}
                            renderItem={({item}) => (

                                    <ProductCart product={item}
                                                 onPress={() => {
                                                     //////console.log("product pressed")
                                                 }}
                                                 favorite={true}
                                                 onPressRemoveProduct={this.removeItem.bind(this, item)}/>

                            )}
                            ListEmptyComponent={<View style={[styles.CenterContent, {
                                flex: 1,
                                marginTop: wp("4%"),
                            }]}><Text>{strings.t("empty_favorite")}</Text></View>}
                        />
                    </Row>


                </Grid>


                <Toast
                    visible={this.props.statusAddToFavorite===SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{strings.t("success_favorite")}</Toast>
            </DefaultHeader>


        );
    }
}


const styles = {


    CenterContent: {

        alignItems: 'center', justifyContent: 'center'
    },
}

const mapStateToProps = state => {
    return {
        isLoading: state.favorite.isLoading,
        status: state.favorite.status,
        errorMessage: state.favorite.errorMessage,
        favorite: state.favorite.favorite,
        statusAddToFavorite: state.product.statusAddToFavorite,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        customerFavorite: (payload) => dispatch(getCustomerFavorite(payload)),
        removeProductFromFavorite: (payload) => dispatch(addToFavorite(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteScreen);