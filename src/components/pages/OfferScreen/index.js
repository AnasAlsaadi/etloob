import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    I18nManager,
    ActivityIndicator,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    FlatList
} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import {connect} from "react-redux";
import {Categories} from "../../../store/category/actions";
import {addToCart, addToFavorite, openSelectedProduct, resetProductList} from "../../../store/product/actions";
import {productLists, productListsRest} from "../../../store/ProductList/actions";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import colors from "../../../constants/colors";
import {NAVIGATION_PRODUCT_SCREEN} from "../../../navigation/types";
import TabedFooter from "../../template/TabedFooter";
import Toast from "react-native-root-toast";
import {SUCCESS} from "../../../constants/actionsType";
import strings from "../../../locales/i18n";
import Product from "../../elements/Product";
import RNPickerSelect from "react-native-picker-select";
import {Content} from 'native-base'
import {offerLists, offerListsRest} from "../../../store/OfferList/actions";

class OfferScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};
    category = null
    vendor = null
    search = null

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            productListAll: []
        }


    }

    componentDidMount() {

        this.props.offerListsRest({})

        //////console.log("did amount")
        //////console.log({category: this.category.id, page: this.state.page})
        ////console.log({category: this.category.id, page: this.state.page})

        this.props.getProducts({
            page: this.state.page,
            refresh: false,
        })
    };


    handleRefresh = () => {
        this.props.offerListsRest({})

        this.setState({page: 1}, () => {

            this.props.getProducts({
                page: this.state.page,
                refresh: true,
            })

        })
    };

    handleLoadMore = () => {

        var page = this.state.page;
        page = page + 1
        this.setState({page: page}, () => {

            this.props.getProducts({
                page: this.state.page,
                refresh: false,
            })

        })
    };

    onPressProduct(product) {

        //////console.log("pressed ")
        //////console.log(product)

        //////console.log(product)
        this.props.selectedProduct(product)
        // this.props.navigation.navigate(NAVIGATION_PRODUCT_SCREEN);

        this.props.navigation.navigate({
            routeName: NAVIGATION_PRODUCT_SCREEN,
            parms: {current: product, ...this.props},
            key: 'productoffer' + product.id
        });
    }

    onPressProductAddToCart(item) {

        ////console.log("add to cart ")
        ////console.log(item)

        if (item.attributes.length > 0) {

            let toast = Toast.show(strings.t("please_choose_attr"), {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,

            });
            setTimeout(function () {
                Toast.hide(toast);
            }, 1000);


        } else {

            this.props.addProductToCart(item)
        }

    }


    onPressAddToFavorite(product) {

        //////console.log("addto cart from home screen ")
        this.props.addProductToFavorite(product)

    }

    render() {

        return (


            <DefaultHeader {...this.props} >


                <View style={{flex: 1,}}>


                    <View style={{flex: 1}}>

                        {
                            this.props.offerList &&

                            <FlatList
                                data={this.props.offerList}
                                renderItem={({item}) => (

                                    <ProductHorizontal offer={true} product={item} onPress={this.onPressProduct.bind(this, item)}
                                                       onPressAddToFavorite={this.onPressAddToFavorite.bind(this, item)}

                                                       onPressAddToCart={this.onPressProductAddToCart.bind(this, item)}

                                    />
                                )}
                                keyExtractor={(item, index) => item.id+"" }
                                refreshing={this.props.isLoadingLIST}
                                onRefresh={this.handleRefresh}
                                onEndReached={(xxx) => {
                                    console.log("reached")
                                    console.log(xxx)
                                    this.handleLoadMore()
                                }}


                            />
                        }
                    </View>
                </View>

                {/*{this.props.isLoading && <View style={{flex:1, alignItems:'center'}}><ActivityIndicator size="large" color="#ff6a00" /></View>}*/}

                <Toast
                    visible={this.props.statusAddToCart === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{this.props.ErrorMessageADDProductToCart}</Toast>


                <Toast
                    visible={this.props.statusAddToFavorite === SUCCESS}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{strings.t("success_favorite")}</Toast>

            </DefaultHeader>


        );
    }
}

const mapStateToProps = state => {
    return {
        offerList: state.offerList.offerList,
        isLoadingLIST: state.offerList.isLoadingLIST,
        status: state.offerList.status,
        errorMessage: state.offerList.errorMessage,
        statusAddToFavorite: state.product.statusAddToFavorite,
        ErrorMessageADDProductToCart: state.product.ErrorMessageADDProductToCart,

    }
}
const mapDispatchToProps = dispatch => {
    return {

        getProducts: (payload) => dispatch(offerLists(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        addProductToCart: (payload) => dispatch(addToCart(payload)),
        addProductToFavorite: (payload) => dispatch(addToFavorite(payload)),
        offerListsRest: (payload) => dispatch(offerListsRest(payload)),

    }


}

export default connect(mapStateToProps, mapDispatchToProps)

(
    OfferScreen
)
;
