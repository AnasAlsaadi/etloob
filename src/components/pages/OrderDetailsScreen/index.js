import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    I18nManager,
    ActivityIndicator,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    FlatList
} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import {connect} from "react-redux";
import Order from "../../elements/Order";
import colors from "../../../constants/colors";
import {NAVIGATION_PRODUCT_SCREEN} from "../../../navigation/types";
import {orderLists} from "../../../store/order/actions";
import TabedFooter from "../../template/TabedFooter";
import strings from "./../../../locales/i18n"
import color from '../../../constants/colors';
import Typography from './../../../constants/typography'
import NumberFormat from "react-number-format";

class OrderDetailsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    order = {};

    countries = [
        {label: 'الحسكة', value: 'AI'},
        {label: 'القامشلي', value: 'BS'},
        {label: 'السويداء', value: 'AW'},
        {label: 'حلب', value: 'AL'},
        {label: 'دمشق', value: 'AF'},
        {label: 'درعا', value: 'DZ'},
        {label: 'دير الزور', value: 'AD'},
        {label: 'حماة', value: 'AO'},
        {label: 'حمص', value: 'AQ'},
        {label: 'إدلب', value: 'AZ'},
        {label: 'اللاذقية', value: 'AG'},
        {label: 'القنيطرة', value: 'AR'},
        {label: 'الرقة', value: 'AM'},
        {label: 'ريف دمشق', value: 'AX'},
        {label: 'طرطوس', value: 'AT'},
    ];

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        }


        this.order = this.props.current
        console.log("OrderPressed")
        console.log(this.order)
    }

    findCountry(name) {
        for (var i = 0; i < this.countries.length; i++) {
            if (this.countries[i].value === name) {
                return this.countries[i].label
            }
        }
    }

    componentDidMount() {

        //////console.log("did amount")
        //////console.log({page: this.state.page})
        // this.props.getOrders({ page: this.state.page, refresh: false})
    };


    onPressOrder(order) {

        //////console.log("pressed ")
        //////console.log(order)

        //////console.log(order)
        // this.props.selectedOrder(Order)
        // this.props.navigation.navigate(NAVIGATION_PRODUCT_SCREEN);


    }


    render() {

        return (


            <DefaultHeader type={"order_details"} {...this.props} >

                <View style={{
                    flexDirection: 'column',
                    flex: 1,
                    paddingEnd: wp("4%"),
                    paddingStart: wp("4%"),
                    paddingTop: wp("4%")
                }}>

                    <View style={{
                        borderColor: color.GrayBorder,
                        borderWidth: 1,
                        shadowColor: color.GrayBorder,
                        shadowOpacity: 0.8,
                        shadowRadius: 3,
                        shadowOffset: {
                            height: 1,
                            width: 1
                        }, backgroundColor: 'white', padding: wp("2%"), borderRadius: 8
                    }}>
                        <Text style={[Typography.Text16Light, {}]}>{strings.t("order")} <Text
                            style={[, {backgroundColor: "#fcf8e3"}]}>{this.order.id}</Text> {strings.t("placed_on")}
                            <Text
                                style={[, {backgroundColor: "#fcf8e3"}]}>{this.order.date_created}</Text> {strings.t("currently")}
                            <Text
                                style={[, {backgroundColor: "#fcf8e3"}]}>{strings.t(this.order.status.split("-").join("_"))}</Text></Text>
                    </View>
                    <View style={{flexDirection: 'column', flex: 1,}}>

                        <View style={{marginTop: wp("3%"), marginBottom: wp("1%")}}><Text
                            style={[Typography.Text16OpenSansBold]}>{strings.t("order_details")}</Text></View>

                        <View style={{
                            borderWidth: 1,
                            borderColor: color.GrayBorder,
                            shadowColor: color.GrayBorder,
                            shadowOpacity: 0.8,
                            shadowRadius: 3,
                            shadowOffset: {
                                height: 1,
                                width: 1
                            }, backgroundColor: 'white',
                        }}>

                            {/* Header*/}

                            <View style={{
                                flexDirection: 'row',
                                flex: 1,
                                paddingStart: wp("2%"),
                                paddingTop: wp("2%"),
                                paddingBottom: wp("2%"),
                                borderBottomWidth: 1,
                                borderBottomColor: color.GrayBorder
                            }}>
                                <View style={{flex: 3}}><Text
                                    style={[Typography.Text14OpenSansBold]}>{strings.t("product")}</Text></View>
                                <View style={{flex: 1}}><Text
                                    style={[Typography.Text14OpenSansBold]}>{strings.t("subtotal")}</Text></View>
                            </View>

                            {/* Products*/}

                            {
                                this.order.line_items.map((e) => {

                                    return (
                                        <View key={e.name + ""} style={{
                                            flexDirection: 'row',
                                            flex: 1,
                                            paddingStart: wp("2%"),
                                            paddingTop: wp("2%"),
                                            paddingBottom: wp("2%"),
                                            borderBottomWidth: 1,
                                            borderBottomColor: color.GrayBorder
                                        }}>
                                            <View style={{flex: 3, flexDirection: 'column'}}>

                                                <Text style={[Typography.Text14Light]}>{e.name} * {e.quantity}</Text>
                                                <Text
                                                    style={[Typography.Text14Light]}>{strings.t("vendor")}: {e.shop_name}</Text>

                                                {e.meta_data.map((em) => {

                                                    return (
                                                        <Text
                                                            style={{textAlign: I18nManager.isRTL ? "left" : "left"}}><Text
                                                            style={[Typography.Text14OpenSansBold, {textAlign: I18nManager.isRTL ? "left" : "left"}]}>{em.key ? em.key.split("pa_").join("") : ""}</Text>: <Text
                                                            style={[Typography.Text14Light]}>{em.value ? em.value : ""}</Text></Text>
                                                    )
                                                })}

                                            </View>
                                            <View style={{flex: 1}}>
                                                <NumberFormat value={e.price} renderText={value => <Text
                                                    style={[Typography.Text14Light, {color: color.green}]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                                              displayType={'text'} thousandSeparator={true}
                                                              prefix={""}/>

                                            </View>
                                        </View>)
                                })
                            }


                            <View style={{
                                flexDirection: 'row',
                                flex: 1,
                                paddingStart: wp("2%"),
                                paddingTop: wp("2%"),
                                paddingBottom: wp("2%"),
                                borderBottomWidth: 1,
                                borderBottomColor: color.GrayBorder
                            }}>
                                <View style={{flex: 3}}><Text
                                    style={[Typography.Text14OpenSansBold]}>{strings.t("subtotal")}: </Text></View>
                                <View style={{flex: 1}}><Text>
                                    <NumberFormat value={this.order.total} renderText={value => <Text
                                        style={[Typography.Text14Light, {color: color.green}]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                                  displayType={'text'} thousandSeparator={true} prefix={""}/>
                                </Text></View>
                            </View>


                            <View style={{
                                flexDirection: 'row',
                                flex: 1,
                                paddingStart: wp("2%"),
                                paddingTop: wp("2%"),
                                paddingBottom: wp("2%"),
                                borderBottomWidth: 1,
                                borderBottomColor: color.GrayBorder
                            }}>
                                <View style={{flex: 3}}><Text
                                    style={[Typography.Text14OpenSansBold]}>{strings.t("payment_way")}:</Text></View>
                                <View style={{flex: 1, alignItems: "center"}}><Text
                                    style={[Typography.Text14OpenSansBold]}>{strings.t(this.order.payment_method)}</Text></View>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                flex: 1,
                                paddingStart: wp("2%"),
                                paddingTop: wp("2%"),
                                paddingBottom: wp("2%"),
                                borderBottomWidth: 1,
                                borderBottomColor: color.GrayBorder
                            }}>
                                <View style={{flex: 3}}><Text
                                    style={[Typography.Text14OpenSansBold]}>{strings.t("total")}: </Text></View>
                                <View style={{flex: 1}}><Text>
                                    <NumberFormat value={this.order.total} renderText={value => <Text
                                        style={[Typography.Text14Light, {color: color.green}]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                                  displayType={'text'} thousandSeparator={true} prefix={""}/>
                                </Text></View>
                            </View>


                        </View>
                    </View>

                    <View style={{flexDirection: 'column', flex: 1,}}>

                        <View style={{marginTop: wp("3%"), marginBottom: wp("1%")}}><Text
                            style={[Typography.Text16OpenSansBold]}>{strings.t("billing_address")}</Text></View>

                        <View style={{
                            borderWidth: 1,
                            borderColor: color.GrayBorder,
                            shadowColor: color.GrayBorder,
                            shadowOpacity: 0.8,
                            shadowRadius: 3,
                            shadowOffset: {
                                height: 1,
                                width: 1
                            }, backgroundColor: 'white',
                        }}>

                            {/* Header*/}

                            <View style={{
                                flexDirection: 'column',
                                flex: 1,
                                paddingStart: wp("2%"),
                                paddingTop: wp("2%"),
                                paddingBottom: wp("2%"),
                                borderBottomWidth: 1,
                                borderBottomColor: color.GrayBorder
                            }}>

                                <Text
                                    style={[Typography.Text14Light, {textAlign: I18nManager.isRTL ? "left" : "left"}]}>{this.order.billing.first_name} {this.order.billing.last_name}</Text>
                                <Text
                                    style={[Typography.Text14Light, {textAlign: I18nManager.isRTL ? "left" : "left"}]}>{this.order.billing.address_1} </Text>
                                <Text
                                    style={[Typography.Text14Light, {textAlign: I18nManager.isRTL ? "left" : "left"}]}>{this.findCountry(this.order.billing.country)} </Text>
                                <Text
                                    style={[Typography.Text14Light, {textAlign: I18nManager.isRTL ? "left" : "left"}]}>{strings.t("phone")}: {this.order.billing.phone} </Text>
                                <Text
                                    style={[Typography.Text14Light, {textAlign: I18nManager.isRTL ? "left" : "left"}]}>{strings.t("email_address")}: {this.order.billing.email} </Text>
                            </View>

                        </View>
                    </View>
                </View>


            </DefaultHeader>


        );
    }
}

const mapStateToProps = state => {
    return {
        current: state.order.current,

        // orderList: state.order.orderList,
        // isLoading: state.order.isLoading,
        // isLoadingPro: state.order.isLoadingPro,
        // status: state.order.status,
        // errorMessage: state.order.errorMessage,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        // getOrders: (payload) => dispatch(orderLists(payload)),
        // selectedOrder: (payload) => dispatch(openSelectedOrder(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)

(
    OrderDetailsScreen
)
;
