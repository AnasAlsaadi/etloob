import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER, NAVIGATION_EDIT_PROFILE_SCREEN, NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN, NAVIGATION_MYORDERS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import FastImage from "react-native-fast-image";
import colors from "../../../constants/colors";
import CodePush from "react-native-code-push";
import {TabBar, TabView, SceneMap, SceneRendererProps} from 'react-native-tab-view';
import {VendorTabProductsScreen, VendorTabOrdersScreen, VendorTabStatisticScreen,VendorTabReviewsScreen} from './../index'
import VendorTabSettingScreen from './../VendorTabSettingScreen'
class VendorHome extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    userCurrent = null;

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            routes: [
                {key: 'statistic', title: strings.t("statistic")},
                {key: 'products', title: strings.t("products")},
                {key: 'orders', title: strings.t("orders")},
                // {key: 'reviews', title: strings.t("reviews")},
                // {key: 'supports', title: strings.t("supports")},
                {key: 'reviews', title: strings.t("reviews")},
                {key: 'setting', title: strings.t("setting")},
            ],
        };
    }

    componentDidMount() {


        this.userCurrent = global.user

    }

    renderTabBar = (
        props: SceneRendererProps & { navigationState: State }
    ) => (
        <TabBar
            {...props}
            scrollEnabled
            indicatorStyle={styles.indicator}
            style={styles.tabbar}
            tabStyle={styles.tab}
            labelStyle={[styles.label, Typography.Text16Light]}
        />
    );

    render() {


        return (


            <DefaultHeader type={"vendor"} hideNavigationBar={true} {...this.props} isLoading={this.props.isLoading}>

                <View style={{flex: 1,}}>
                    <TabView
                        renderTabBar={this.renderTabBar}
                        navigationState={this.state}
                        renderScene={SceneMap({
                            statistic: VendorTabStatisticScreen,
                            products: VendorTabProductsScreen,
                            orders: VendorTabOrdersScreen,
                            reviews: VendorTabReviewsScreen,
                            // reviews: VendorTabSettingScreen,
                            // supports: VendorTabSettingScreen,
                            setting:()=> <VendorTabSettingScreen {...this.props} />,
                        })}
                        tabStyle={{width: 300}}
                        onIndexChange={index => this.setState({index})}
                        initialLayout={{width: wp("100%")}}
                    />
                </View>

            </DefaultHeader>


        );
    }
}

const styles = {
    tabbar: {
        backgroundColor: colors.yellow,

    },
    tab: {
        width: 120,
    },
    indicator: {
        backgroundColor: colors.Gray,
    },
    label: {
        fontWeight: '400',
    },
    scene: {
        flex: 1,
    },
    profileImgContainer: {
        marginVertical: wp("3%"),
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        overflow: 'hidden'
    },
    productImage: {
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        resizeMode: 'contain',
    },
    icon: {
        width: wp("12%"),
        height: wp("12%"),
        resizeMode: 'contain',
    },
    iconArrow: {
        width: wp("4%"),
        height: wp("4%"),

        resizeMode: 'contain',
    },
    TextDetails: {
        flex: 1,
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },
    TextLi: {
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },

    item: {

        marginHorizontal: wp("5%"),
        marginVertical: wp("3%"),
        borderWidth: 1,
        borderRadius: 2,
        borderColor: colors.GrayBorder,
        backgroundColor: 'white',
    },
    itemContainer: {

        flexDirection: 'row',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    textContainer: {

        flex: 3,
        alignItems: 'flex-start',
        marginStart: wp("3%")
    }
}

const mapStateToProps = state => {
    return {

        isLoading: state.profile.isLoading,

    }
}
const mapDispatchToProps = dispatch => {
    return {

        actionEditProfile: (payload) => dispatch(editProfile(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VendorHome);