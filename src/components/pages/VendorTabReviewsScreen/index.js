import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput, FlatList, Linking} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER, NAVIGATION_EDIT_PROFILE_SCREEN, NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN, NAVIGATION_MYORDERS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import FastImage from "react-native-fast-image";
import colors from "../../../constants/colors";
import CodePush from "react-native-code-push";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import ProductVendor from "../../elements/ProductVendor";
import RNPickerSelect from "react-native-picker-select";
import {Categories} from "../../../store/category/actions";
import {productLists, productListsRest} from "../../../store/ProductList/actions";
import color from '../../../constants/colors';
import ReviewVendor from "../../elements/ReviewVendor";
import {reviewLists} from "../../../store/ReviewList/actions";

class VendorTabReviewsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};


    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.props.getReviews({})
    }


    render() {


        return (
            <Grid style={{}}>

                <Row>
                    <View style={{flex: 1}}>
                        <Grid style={{flex: 1,}}>


                            <Row style={{flexDirection: 'column'}}>

                                <FlatList
                                    data={this.props.reviewList}
                                    renderItem={({item}) => (

                                        <ReviewVendor

                                            review={item}
                                        />
                                    )}
                                    keyExtractor={(item, index) => item.id.toString()}
                                    refreshing={this.props.isLoadingLIST}
                                    onEndThreshold={0}

                                />

                            </Row>
                        </Grid>
                    </View>
                </Row>
            </Grid>)

            ;

    }

}

const styles = {
    profileImgContainer: {
        marginVertical: wp("3%"),
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        overflow: 'hidden'
    },
    productImage: {
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        resizeMode: 'contain',
    },
    icon: {
        width: wp("12%"),
        height: wp("12%"),
        resizeMode: 'contain',
    },
    iconArrow: {
        width: wp("4%"),
        height: wp("4%"),

        resizeMode: 'contain',
    },
    TextDetails: {
        flex: 1,
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },
    TextLi: {
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },

    item: {

        marginHorizontal: wp("5%"),
        marginVertical: wp("3%"),
        borderWidth: 1,
        borderRadius: 2,
        borderColor: colors.GrayBorder,
        backgroundColor: 'white',
    },
    itemContainer: {

        flexDirection: 'row',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    textContainer: {

        flex: 3,
        alignItems: 'flex-start',
        marginStart: wp("3%")
    }
}

const mapStateToProps = state => {
    return {

        // isLoading: state.category.isLoading,
        reviewList: state.reviewList.reviewList,
        isLoadingLIST: state.reviewList.isLoadingLIST,
        status: state.reviewList.status,
        errorMessage: state.reviewList.errorMessage,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        getReviews: (payload) => dispatch(reviewLists(payload)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VendorTabReviewsScreen);