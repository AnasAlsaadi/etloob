import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput, FlatList} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER, NAVIGATION_EDIT_PROFILE_SCREEN, NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN, NAVIGATION_MYORDERS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import FastImage from "react-native-fast-image";
import colors from "../../../constants/colors";
import CodePush from "react-native-code-push";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import OrderVendor from "../../elements/OrderVendor";
import RNPickerSelect from "react-native-picker-select";
import ProductVendor from "../../elements/ProductVendor";
import DatePicker from 'react-native-date-picker';
import color from '../../../constants/colors';
import ModalShow from "react-native-modal";
import moment from "moment";
import {orderLists, orderListsRest} from "../../../store/order/actions";
import Order from "../../elements/Order";

class VendorTabOrdersScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};
    productList = [
        {
            id: 1,
            name: "ss",
        }, {
            id: 1,
            name: "ss",
        }, {
            id: 1,
            name: "ss",
        }, {
            id: 1,
            name: "ss",
        },
    ]

    constructor(props) {
        super(props);
        this.state = {
            first_name: "",
            last_name: "",
            isVisible: false,
            email: "",
            date: null,
            page: 1,
        }


        this.handleLoadMore= this.handleLoadMore.bind(this)
    }

    componentDidMount() {

        this.setState({date: new Date()})

        this.props.getOrders({type_r:"vendor", page: 1, refresh: false, getAttr: this.getSelectedAttrSearch()})



    }

    handleRefresh = () => {
        this.setState({page: 1}, () => {

            this.props.getOrders({type_r:"vendor", page: this.state.page, refresh: true, getAttr: this.getSelectedAttrSearch()})
        })
    };


    handleLoadMore() {
        //console.log("pageeee")
        //console.log(this.state.page)
        var page = this.state.page;
        page = page + 1
        this.setState({page: page}, () => {
            this.props.getOrders({ type_r:"vendor", page: this.state.page, refresh: false, getAttr: this.getSelectedAttrSearch()})
        })

    }

    formatDate() {
        var date = this.state.date
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();

        return day + '-' + monthIndex + '-' + year;
    }


    order_search() {

        this.props.resetOrderList(null)

        this.props.getOrders({type_r:"vendor", page: 1, refresh: false, getAttr: this.getSelectedAttrSearch()})

    }

    getSelectedAttrSearch() {
        var getAttr = "";

        if (this.state.date) {
            getAttr = "&order_date=" + this.formatDate() + "&"
        }
        if (this.state.order_by) {
            getAttr += "status=" + this.state.order_by;
        }

        return getAttr
    }


    render() {


        return (

            <Grid>

                <Row>
                    <View style={{flex: 1}}>
                        <Grid style={{flex: 1,}}>

                            <Row size={1} style={{flexDirection: 'column'}}>
                                <View style={{
                                    flex: 1, flexDirection: "row", alignItems: 'center',
                                    paddingStart: wp("6%"),
                                    paddingEnd: wp("6%"),
                                    backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                                }}>
                                    <View style={{flex: 2,}}>
                                        <Text
                                            style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("status")}: </Text>
                                    </View>
                                    <View style={{flex: 2}}>
                                        <RNPickerSelect
                                            style={{marginTop: wp("3%"), marginBottom: wp("3%")}}
                                            placeholder={this.oj}
                                            onValueChange={(value) => {

                                                this.setState({order_by: value}, function () {

                                                    // this.props.productListsRest()
                                                    // this.handleRefresh()

                                                }.bind(this))
                                                ////console.log(value)
                                                // this.props.productList = []
                                                // this.handleRefresh()

                                            }}
                                            items={[
                                                {label: strings.t("completed"), value: 'completed'},
                                                {label: strings.t("processing"), value: 'processing'},
                                                {label: strings.t("on_hold"), value: 'on-hold'},
                                                {label: strings.t("pending"), value: 'pending'},
                                                {label: strings.t("cancelled"), value: 'cancelled'},
                                                {label: strings.t("refunded"), value: 'refunded'},
                                            ]}
                                        />
                                    </View>
                                </View>
                                {/*<View style={{*/}
                                    {/*flex: 1, flexDirection: "row", alignItems: 'center',*/}
                                    {/*paddingStart: wp("6%"),*/}
                                    {/*paddingEnd: wp("6%"),*/}
                                    {/*backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")*/}
                                {/*}}>*/}
                                    {/*<View style={{flex: 2,}}>*/}
                                        {/*<Text*/}
                                            {/*style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("choose_category")}: </Text>*/}
                                    {/*</View>*/}
                                    {/*<View style={{flex: 2}}>*/}

                                        {/*<TouchableOpacity onPress={() => {*/}

                                            {/*console.log(new Date())*/}
                                            {/*this.setState({isVisible: true})*/}

                                        {/*}*/}
                                        {/*}><Text>{strings.t("date_select")}: {this.state.date ? this.formatDate() : null}</Text></TouchableOpacity>*/}
                                    {/*</View>*/}
                                {/*</View>*/}
                                <View style={{
                                    flex: 1, flexDirection: "row", alignItems: 'center',
                                    paddingStart: wp("6%"),
                                    paddingEnd: wp("6%"),
                                    backgroundColor: 'white', paddingTop: wp("2%"), paddingBottom: wp("2%")
                                }}>
                                    <View style={{flex: 2,}}>
                                        <Text
                                            style={{textAlign: I18nManager.isRTL ? 'left' : "right"}}>{strings.t("press_to_search")}: </Text>
                                    </View>
                                    <View style={{flex: 2}}>

                                        <TouchableOpacity style={{
                                            backgroundColor: color.yellow,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 8,
                                            height: hp('4.5%'),
                                            marginTop: hp('2%')
                                        }} onPress={() => {

                                            this.order_search()
                                        }}
                                        >
                                            <Text
                                                style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("search")}</Text>
                                        </TouchableOpacity>

                                    </View>
                                </View>
                            </Row>


                            <Row size={3} style={{flexDirection: 'column'}}>

                                {
                                    this.props.orderList &&
                                    <FlatList
                                        data={this.props.orderList}
                                        renderItem={({item}) => (
                                            <OrderVendor order={item}
                                            />
                                        )}
                                        keyExtractor={(item, index) => item.id.toString()}

                                        refreshing={this.props.isLoadingPro}
                                        onRefresh={this.handleRefresh}
                                        onEndReached={this.handleLoadMore}
                                        onEndThreshold={0}
                                    />
                                }

                            </Row>
                        </Grid>
                    </View>
                </Row>

                <ModalShow isVisible={this.state.isVisible}>
                    <View style={{flex: 1,}}>
                        <View style={{
                            borderWidth: 1,
                            marginTop: wp("10%"),
                            shadowColor: color.GrayBorder,
                            shadowOpacity: 0.8,
                            shadowRadius: 3,
                            shadowOffset: {
                                height: 1,
                                width: 1
                            },
                            borderRadius: 8,
                            flexDirection: 'column',
                            backgroundColor: 'white',
                            paddingTop: wp("3%"),
                            paddingBottom: wp("6%"),
                            height: hp("50%")
                        }}
                        >


                            <View style={{flex: 2}}>
                                <DatePicker
                                    date={this.state.date}
                                    mode={"date"}
                                    onDateChange={date => this.setState({date})}
                                />

                            </View>

                            <View style={{flexDirection: 'row', flex: 1,}}>

                                <View style={{flex: 2, paddingEnd: wp("3"), paddingStart: wp("3")}}>

                                    <TouchableOpacity style={{
                                        backgroundColor: color.yellow,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 8,
                                        height: hp('4.5%'),
                                        marginTop: hp('2%')
                                    }} onPress={() => {
                                        this.setState({isVisible: false})
                                    }}
                                    >
                                        <Text
                                            style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("save")}</Text>
                                    </TouchableOpacity>


                                </View>
                                <View style={{flex: 2, paddingEnd: wp("3"), paddingStart: wp("3")}}>

                                    <TouchableOpacity style={{
                                        backgroundColor: color.CustomBlack,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 8,
                                        height: hp('4.5%'),
                                        marginTop: hp('2%')
                                    }} onPress={() => {

                                        this.setState({isVisible: false})
                                    }}
                                    >
                                        <Text
                                            style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("close")}</Text>
                                    </TouchableOpacity>


                                </View>


                            </View>

                        </View>
                    </View>
                </ModalShow>

            </Grid>)

            ;
    }
}

const styles = {
    profileImgContainer: {
        marginVertical: wp("3%"),
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        overflow: 'hidden'
    },
    productImage: {
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        resizeMode: 'contain',
    },
    icon: {
        width: wp("12%"),
        height: wp("12%"),
        resizeMode: 'contain',
    },
    iconArrow: {
        width: wp("4%"),
        height: wp("4%"),

        resizeMode: 'contain',
    },
    TextDetails: {
        flex: 1,
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },
    TextLi: {
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },

    item: {

        marginHorizontal: wp("5%"),
        marginVertical: wp("3%"),
        borderWidth: 1,
        borderRadius: 2,
        borderColor: colors.GrayBorder,
        backgroundColor: 'white',
    },
    itemContainer: {

        flexDirection: 'row',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    textContainer: {

        flex: 3,
        alignItems: 'flex-start',
        marginStart: wp("3%")
    }
}

const mapStateToProps = state => {
    return {

        isLoading: state.profile.isLoading,
        orderList: state.order.orderList,
        isLoadingPro: state.order.isLoadingPro,
        status: state.order.status,
    }
}
const mapDispatchToProps = dispatch => {
    return {


        getOrders: (payload) => dispatch(orderLists(payload)),
        resetOrderList: (payload) => dispatch(orderListsRest(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VendorTabOrdersScreen);
