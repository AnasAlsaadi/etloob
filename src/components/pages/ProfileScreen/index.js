import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER, NAVIGATION_EDIT_PROFILE_SCREEN, NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN, NAVIGATION_MYORDERS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN, NAVIGATION_VENDOR_HOME,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import FastImage from "react-native-fast-image";
import colors from "../../../constants/colors";
import CodePush from "react-native-code-push";

class ProfileScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    userCurrent = null;

    constructor(props) {
        super(props);
        this.state = {

            first_name: "",
            last_name: "",
            email: "",
        }
    }

    componentDidMount() {


        this.userCurrent = global.user

    }


    edit_profile() {


        this.props.actionEditProfile(this.state)


    }


    render() {


        return (


            <DefaultHeader  {...this.props} isLoading={this.props.isLoading}>

                <Grid>

                    <Row>

                        <ScrollView style={{}}>

                            <Grid style={{flex: 1}}>

                                <Row size={2}>

                                    <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>


                                        <TouchableOpacity style={[styles.profileImgContainer]}>
                                            <FastImage
                                                style={styles.productImage}
                                                source={{
                                                    uri: global.user.avatar_url.length > 0 ? global.user.avatar_url : "https://static.thenounproject.com/png/220984-200.png",
                                                    priority: FastImage.priority.high,
                                                }}
                                            />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{flex: 1,}} onPress={() => {
                                            this.props.navigation.navigate(NAVIGATION_EDIT_PROFILE_SCREEN)
                                        }}>
                                            <View style={{alignItems: 'center', justifyContent: 'center'}}>

                                                <Text style={[styles.TextDetails]}>{global.user.email}</Text>
                                                <Text style={[styles.TextDetails]}>{global.user.billing.phone}</Text>
                                            </View>

                                        </TouchableOpacity>


                                    </View>
                                </Row>

                                <Row size={2}>

                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'column',
                                        alignSelf: 'stretch',
                                        marginTop: wp("6%")
                                    }}>


                                        <TouchableOpacity style={[styles.item]} onPress={() => {
                                            this.props.navigation.navigate(NAVIGATION_SHIPPING_ADDRESS_SCREEN)
                                        }}>
                                            <View style={[styles.itemContainer]}>
                                                <View style={{alignSelf: 'flex-start', alignItems: 'center'}}>
                                                    <Image style={styles.icon}
                                                           source={require("./../../../assets/images/Icon_Location.png")}/>
                                                </View>

                                                <View style={[styles.textContainer]}><Text
                                                    style={[styles.TextLi]}>{strings.t("shipping_address")}</Text></View>
                                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                                    {I18nManager.isRTL ?

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_left.png")}/>
                                                        :

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_right.png")}/>}
                                                </View>
                                            </View>
                                        </TouchableOpacity>


                                        <TouchableOpacity style={[styles.item]} onPress={() => {
                                            this.props.navigation.navigate(NAVIGATION_FAVORITES_SCREEN)
                                        }}>
                                            <View style={[styles.itemContainer]}>
                                                <View style={{alignSelf: 'flex-start', alignItems: 'center'}}>
                                                    <Image style={styles.icon}
                                                           source={require("./../../../assets/images/Icon_Wishlist.png")}/>
                                                </View>
                                                <View style={[styles.textContainer]}><Text
                                                    style={[styles.TextLi]}>{strings.t("wishlist")}</Text></View>
                                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                                    {I18nManager.isRTL ?

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_left.png")}/>
                                                        :

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_right.png")}/>}
                                                </View>
                                            </View>
                                        </TouchableOpacity>


                                        <TouchableOpacity style={[styles.item]} onPress={() => {
                                            this.props.navigation.navigate(NAVIGATION_MYORDERS_SCREEN)
                                        }}>
                                            <View style={[styles.itemContainer]}>
                                                <View style={{alignSelf: 'flex-start', alignItems: 'center'}}>
                                                    <Image style={styles.icon}
                                                           source={require("./../../../assets/images/Icon_History.png")}/>
                                                </View>
                                                <View style={[styles.textContainer]}><Text
                                                    style={[styles.TextLi]}>{strings.t("order_history")}</Text></View>
                                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                                    {I18nManager.isRTL ?

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_left.png")}/>
                                                        :

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_right.png")}/>}
                                                </View>
                                            </View>
                                        </TouchableOpacity>


                                        <TouchableOpacity style={[styles.item]}
                                                          onPress={() => {
                                                              global.storage.remove({
                                                                  key: 'user',
                                                              })


                                                              CodePush.restartApp();


                                                              // this.props.navigation.navigate(NAVIGATION_APP)

                                                          }}
                                        >
                                            <View style={[styles.itemContainer]}>
                                                <View style={{alignSelf: 'flex-start', alignItems: 'center'}}>
                                                    <Image style={styles.icon}
                                                           source={require("./../../../assets/images/Icon_Exit.png")}/>
                                                </View>
                                                <View style={[styles.textContainer]}><Text
                                                    style={[styles.TextLi]}>{strings.t("logout")}</Text></View>
                                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                                    {I18nManager.isRTL ?

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_left.png")}/>
                                                        :

                                                        <Image style={styles.iconArrow}
                                                               source={require("./../../../assets/images/arrow_right.png")}/>}
                                                </View>
                                            </View>
                                        </TouchableOpacity>


                                        {global.role === "seller" ?
                                            <TouchableOpacity style={[styles.item]} onPress={() => {
                                                this.props.navigation.navigate(NAVIGATION_VENDOR_HOME)
                                            }}>
                                                <View style={[styles.itemContainer]}>
                                                    <View style={{alignSelf: 'flex-start', alignItems: 'center'}}>
                                                        <Image style={styles.icon}
                                                               source={require("./../../../assets/images/Icon_History.png")}/>
                                                    </View>
                                                    <View style={[styles.textContainer]}><Text
                                                        style={[styles.TextLi]}>{strings.t("go_to_store")}</Text></View>
                                                    <View style={{
                                                        flex: 1,
                                                        alignItems: 'center',
                                                        justifyContent: 'center'
                                                    }}>
                                                        {I18nManager.isRTL ?

                                                            <Image style={styles.iconArrow}
                                                                   source={require("./../../../assets/images/arrow_left.png")}/>
                                                            :

                                                            <Image style={styles.iconArrow}
                                                                   source={require("./../../../assets/images/arrow_right.png")}/>}
                                                    </View>
                                                </View>
                                            </TouchableOpacity> : null}


                                    </View>
                                </Row>


                            </Grid>
                        </ScrollView>
                    </Row>

                </Grid>


            </DefaultHeader>


        );
    }
}

const styles = {
    profileImgContainer: {
        marginVertical: wp("3%"),
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        overflow: 'hidden'
    },
    productImage: {
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        resizeMode: 'contain',
    },
    icon: {
        width: wp("12%"),
        height: wp("12%"),
        resizeMode: 'contain',
    },
    iconArrow: {
        width: wp("4%"),
        height: wp("4%"),

        resizeMode: 'contain',
    },
    TextDetails: {
        flex: 1,
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },
    TextLi: {
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },

    item: {

        marginHorizontal: wp("5%"),
        marginVertical: wp("3%"),
        borderWidth: 1,
        borderRadius: 2,
        borderColor: colors.GrayBorder,
        backgroundColor: 'white',
    },
    itemContainer: {

        flexDirection: 'row',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    textContainer: {

        flex: 3,
        alignItems: 'flex-start',
        marginStart: wp("3%")
    }
}

const mapStateToProps = state => {
    return {

        isLoading: state.profile.isLoading,
        status: state.profile.status,
        errorMessage: state.profile.errorMessage,
        user: state.profile.user,

    }
}
const mapDispatchToProps = dispatch => {
    return {

        actionEditProfile: (payload) => dispatch(editProfile(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
