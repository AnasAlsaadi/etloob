import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {ActivityIndicator, Text, ScrollView, Image, TouchableOpacity, View, TextInput, FlatList} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import {connect} from "react-redux";
import Order from "../../elements/Order";
import colors from "../../../constants/colors";
import {NAVIGATION_ORDER_SCREEN, NAVIGATION_PRODUCT_SCREEN} from "../../../navigation/types";
import {openSelectedOrder, orderLists, orderListsRest} from "../../../store/order/actions";
import TabedFooter from "../../template/TabedFooter";

class MyOrdersScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};


    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        }



    }

    componentDidMount() {

        this.props.resetOrderList();
        //////console.log("did amount")
        //////console.log({page: this.state.page})

        this.props.getOrders({ page:1, refresh: false})
    };

    handleRefresh = () => {

        this.setState({page: 1}, () => {

            this.props.getOrders({ page: this.state.page, refresh: true})
        })
    };

    handleLoadMore = () => {

        var page = this.state.page;
        page = page + 1
        this.setState({page: page}, () => {
            this.props.getOrders({page: this.state.page, refresh: false})
        })
    };

    onPressOrder(order) {

        //////console.log("pressed ")
        //////console.log(order)

        //console.log(order)
        this.props.selectedOrder(order)
        this.props.navigation.navigate(NAVIGATION_ORDER_SCREEN);


    }


    render() {

        return (


            <DefaultHeader {...this.props} >

                <Grid>
                    <Row>
                        {
                            this.props.orderList &&
                            <FlatList
                                data={this.props.orderList}
                                renderItem={({item}) => (
                                     <Order key={""+item.id} order={item} onPress={this.onPressOrder.bind(this, item)}
                                    />


                                )}


                                keyExtractor={(item, index) => item.id+"" }
                                refreshing={this.props.isLoadingPro}
                                onRefresh={this.handleRefresh}
                                onEndReached={this.handleLoadMore}
                                onEndThreshold={0}
                            />
                        }

                    </Row>
                </Grid>

                {/*{this.props.isLoading && <View style={{flex:1, alignItems:'center'}}><ActivityIndicator size="large" color="#ff6a00" /></View>}*/}


            </DefaultHeader>


        );
    }
}

const mapStateToProps = state => {
    return {

        orderList: state.order.orderList,
        isLoading: state.order.isLoading,
        isLoadingPro: state.order.isLoadingPro,
        status: state.order.status,
        errorMessage: state.order.errorMessage,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        resetOrderList: (payload) => dispatch(orderListsRest(payload)),
        getOrders: (payload) => dispatch(orderLists(payload)),
        selectedOrder: (payload) => dispatch(openSelectedOrder(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)

(
    MyOrdersScreen
)
;
