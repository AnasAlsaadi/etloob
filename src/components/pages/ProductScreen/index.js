import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    InteractionManager,
    ActivityIndicator,
    StyleSheet,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    Button,
    I18nManager,Platform
} from 'react-native'
import {SlideButton, SlideDirection} from 'react-native-slide-button';
import {WebView} from 'react-native-webview';

import DefaultHeader from './../../../containers/DefaultHeader'
import Slideshow from 'react-native-slideshow';
import color from '../../../constants/colors';
import Typography from './../../../constants/typography'
import {Product, CategoryHome} from '../../elements'
import {TabedFooter} from '../../template'
import Swiper from 'react-native-swiper'
import {Modal} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import {NAVIGATION_PRODUCT_SCREEN, NAVIGATION_PRODUCTS_SCREEN} from "../../../navigation/types";
import strings from "../../../locales/i18n"
import {FeatureProduct, RecentProduct, TrendingProduct} from "../../../store/home/actions";
import {
    addToCart, addToFavorite, getVariant,
    openSelectedProduct,
    productLists
    , resetAddToCart,
    resetProductList
} from "../../../store/product/actions";
import RNPickerSelect from 'react-native-picker-select';

import {relatedAndMoreProduct} from '../../../store/home/actions';
import {connect} from "react-redux";
import ProductAttributes from "./../../elements/ProductAttributes"
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';

import FastImage from 'react-native-fast-image'
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import Toast from "react-native-root-toast";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import SubmitButton from "../../elements/Buttons";
import NumberFormat from "react-number-format";
import {openSelectedVendor} from "../../../store/vendor/actions";
import AppStyle from "../../../constants/AppStyles";
import TagList from "../../elements/TagList";
import ModalShow from "react-native-modal";
import {ResetSelectedCategory} from "../../../store/category/actions";

class ProductScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};
    swiper = null
    attributes = []
    product = {}
    data = [{
        value: 'Banana',
    }, {
        value: 'Mango',
    }, {
        value: 'Pear',
    }];
    meta_data = []
    selectedAttr = []

    constructor(props) {
        super(props);

        this.state = {
            step: 0,
            isReady: false,
            isLoading: true,
            isVisible: false,
            TabInfoType: "vendor",
            VendorSelected: true,
            AdditionalSelected: false,
            ShippingSelected: false,
            IndexPressed: null,
            ModalGallery: false,
            images: [],
            attributes: [],
            regular_price: null,
            variation_id: null,
        }


        this.onPressAddToCartThis = this.onPressAddToCartThis.bind(this)
        this.onPressAddToCartPR = this.onPressAddToCartPR.bind(this)
        this.vendorPressed = this.vendorPressed.bind(this)
        this.onPressAttr = this.onPressAttr.bind(this)

    }


    find_meta_data_by_value(value_) {

        for (i = 0; i < this.product.attributes.length; i++) {
            if (this.product.attributes[i].options.length > 0) {

                for (j = 0; j < this.product.attributes[i].options.length; j++) {

                    ////console.log(this.product.attributes[i].options[j])
                    ////console.log(value_)
                    if (value_ === this.product.attributes[i].options[j]) {
                        return {
                            state: true,
                            index: i,
                            item: this.product.attributes[i],
                        }
                    }
                }
            }
        }

        return {
            state: false,
            index: -1,
            item: null,
        }


    }

    renderSelectOption() {

        const selects = []
        if (this.product.attributes.length > 0) {

            for (i = 0; i < this.product.attributes.length; i++) {
                const item_title = this.product.attributes[i].name
                const item_value = []

                for (j = 0; j < this.product.attributes[i].options.length; j++) {
                    item_value.push({
                        label: this.product.attributes[i].options[j] + "",
                        value: this.product.attributes[i].options[j] + ""
                    })

                }
                const oj = {
                    label: item_title
                }
                const se = (

                    <RNPickerSelect
                        key={"fd" + i}
                        placeholder={oj}
                        onValueChange={(value, index) => {
                            ////console.log("onValueChange")
                            ////console.log(value)

                            ////console.log("out =====")
                            ////console.log(this.meta_data)

                            const find_item = this.find_meta_data_by_value(value)
                            if (this.meta_data.length > 0) {


                                //////console.log("find_item =====")
                                //////console.log(find_item)
                                if (find_item.state) {
                                    if (this.meta_data.findIndex(x => x.id === find_item.item.id) !== -1) {

                                        var index = this.meta_data.findIndex(x => x.id === find_item.item.id)

                                        //////console.log("find to detle =====")
                                        //////console.log(index)
                                        this.meta_data.splice(index, 1)
                                        this.meta_data.push({
                                            id: find_item.item.id,
                                            key: find_item.item.name,
                                            value: value
                                        })
                                    } else {

                                        this.meta_data.push({
                                            id: find_item.item.id,
                                            key: find_item.item.name,
                                            value: value
                                        })
                                    }

                                    //////console.log("after added to detle =====")
                                    //////console.log(this.meta_data)

                                }
                                else {

                                }

                            }
                            else {
                                this.meta_data.push({
                                    id: find_item.item.id,
                                    key: find_item.item.name,
                                    value: value
                                })

                                //////console.log(this.meta_data)
                            }


/// get price of attr
                            //console.log("added to product")
                            //console.log(this.meta_data)
                            var attr_to_send_to_get_price = [];
                            var attributes = this.state.attributes
                            this.meta_data.map((e) => {

                                attributes.map((m) => {
                                    if (e.key === m.name) {
                                        for (var i = 0; i < m.options.length; i++) {
                                            if (m.options[i].name === e.value) {

                                                attr_to_send_to_get_price.push(m.options[i].name_slog)
                                            }
                                        }
                                    }
                                })
                            })

                            //console.log(attr_to_send_to_get_price)
                            //console.log(attributes)

                            if (attr_to_send_to_get_price.length === attributes.length) {
                                this.props.getVariant({
                                    product_id: this.product.id,
                                    attribute: attr_to_send_to_get_price,
                                })

                            }

                        }}
                        items={item_value}
                    />

                )
                selects.push(se)
            }
            return (<View style={{flexDirection: 'column', flex: 1, margin: wp("3%")}}>{selects}</View>)
        }


    }

    componentDidMount() {


        var images = [];
        InteractionManager.runAfterInteractions(() => {
            // 2: Component is done animating
            // 3: Start fetching the team / or render the view
            // this.props.dispatchTeamFetchStart();

            //console.log("get readyyyyyyyyy")

            this.product = this.props.current

            this.setState({step: I18nManager.isRTL ? this.product.images.length - 1 : 0})
            // this.setState({step:0})
            this.setState({regular_price: this.product.regular_price})
            this.setState({price: this.product.price})
            this.setState({sku: this.product.sku})

            //console.log("selected prorrrr")
            //console.log(this.product)

            setTimeout(function () {
                this.setState({
                    isReady: true,
                    isLoading: false,
                })

            }.bind(this), 1)

            if (this.props.current.images) {

                this.props.current.images.map((item, index) => {

                    images.push({url: item.src})
                })
            }
            else {
                images.push({url: "http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png"})
            }
            this.setState({images: images})

            if (I18nManager.isRTL) {
                // this.swiper.scrollBy(1, true)
            }


            this.props.getRelatedProducts({product_id: this.product.id})

        });


    }


    onPressAttr(attr) {

        ////console.log(attr)
        this.selectedAttr = []
        var attrebutes = this.state.attributes

        attrebutes[attr.parent].options.map((e) => {

            e.selected = false
        })

        if (attrebutes[attr.parent].options.findIndex(x => x.name === attr.name) !== -1) {
            var index = attrebutes[attr.parent].options.findIndex(x => x.name === attr.name)

            attrebutes[attr.parent].options[index].selected = true

            // this.selectedAttr.push(attrebutes[attr.parent].options[index].name_slog)

        }


        attrebutes.map((e) => {
            e.options.map((m) => {
                m.selected ? this.selectedAttr.push(m.name_slog) : false
            })
        })
        //console.log("All attr======")
        //console.log(attrebutes)
        //console.log(this.selectedAttr)

        if (this.selectedAttr.length === attrebutes.length) {
            this.props.getVariant({
                product_id: this.product.id,
                attribute: this.selectedAttr,
            })

        }

        this.setState({attributes: attrebutes})


    }

    renderAttributes() {

        var grids = [];
        if (this.product.attributes.length > 0) {

            if (this.state.attributes.length > 0) {

            } else {
                var arr_attr = []
                for (var i = 0; i < this.product.attributes.length; i++) {
                    arr_attr.push({
                        name: this.product.attributes[i].name,
                        options: [],
                    })
                    for (var j = 0; j < this.product.attributes[i].options.length; j++) {
                        arr_attr[i].options.push({
                            name: this.product.attributes[i].options[j],
                            name_slog: this.product.attributes[i].options_slugs ? this.product.attributes[i].options_slugs[j] : "" + this.product.attributes[i].options[j],
                            selected: false,
                            parent: i
                        })
                    }
                }

                ////console.log("options")
                ////console.log(arr_attr)
                this.setState({attributes: arr_attr})
            }

            this.state.attributes.map((item, index) => {

                const grid = (
                    <ProductAttributes
                        key={"ind" + index}
                        setAttr={(attr) => this.onPressAttr(attr)}
                        index={"PRATT" + index} attribute={item}/>)
                grids.push(grid)
            })

            return (<View style={{flex: 1, alignItems: 'flex-start'}}>{grids}</View>)


        }
        else {
            return null
        }
    }


    onPressAddToFavoriteThis() {

        this.props.addProductToFavorite(this.product)
    }


    renderAttributesText() {


        var grids = []

        if (this.product.attributes.length > 0) {

            this.product.attributes.map((item, index) => {

                var options_text = "";
                item.options.map((elem, indexElem) => {
                    if (indexElem == item.options.length - 1) {


                        options_text += " " + elem + ""
                    } else {

                        options_text += " " + elem + ","
                    }

                })
                const grid = (

                    <Row>
                        <Text
                            style={[Typography.Text14Light, styles.TextDescription]}>
                            {item.name}: {options_text}
                        </Text></Row>)

                grids.push(grid)
            })


            return <Grid>{grids}</Grid>
        }
    }

    onPressAddToCartPR(product) {


        //////console.log("add to cart from product page ")
        this.props.addProductToCartPR(product)

    }

    onPressAddToFavorite(product) {

        ////////console.log("addto cart from home screen ")
        this.props.addProductToFavorite(product)

    }

    onPressProduct(product) {

        //console.log(product)
        //console.log('product' + this.product.id)
        this.props.selectedProduct(product)
        this.props.navigation.navigate({
            routeName: NAVIGATION_PRODUCT_SCREEN,
            parms: {current: product, ...this.props},
            key: 'product' + product.id


        });

    }

    onPressAddToCartThis() {


        ///// working on

        this.product.meta_data = this.meta_data


        //console.log("this.product.stock_status")
        //console.log(this.product.stock_status)
        if (this.product.stock_status !== false) {

            this.props.addProductToCartPR({
                id: this.product.id,
                item: this.product,
                if_meta: true,
                meta_data: this.meta_data,
                stock_status: this.product.stock_status

            })

            this.meta_data = []


        }
        else {
            let toast = Toast.show(strings.t("out_of_stock"), {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,

            });


            setTimeout(function () {
                Toast.hide(toast);
            }, 2000);
        }


    }


    renderSwiperProducts(isLoading, ListAll) {


        var grids = [];
        if (isLoading) {

            return (
                <Grid>
                    <Row style={[styles.CenterContent]}>
                        <Text>Loading...</Text>
                    </Row>
                </Grid>)
        } else {

            if (!ListAll) return null

            if (I18nManager.isRTL) {
                for (var i = Math.ceil(ListAll.length / 4) - 1; i >= 0; i--) {
                    var grid = (
                        <Grid key={"grid" + i}>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 0) ?
                                        <Product key={((i * 4) + 0)} product={ListAll[((i * 4) + 0)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 0)])}

                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 1) ?
                                        <Product key={((i * 4) + 1)} product={ListAll[((i * 4) + 1)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 1)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 1)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 1)])}
                                        /> : null}

                                </Row>
                            </Col>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 2) ?
                                        <Product key={((i * 4) + 2)} product={ListAll[((i * 4) + 2)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 2)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 2)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 2)])}
                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 3) ?
                                        <Product key={((i * 4) + 3)} product={ListAll[((i * 4) + 3)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 3)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 3)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 3)])}
                                        /> : null}

                                </Row>
                            </Col>
                        </Grid>)

                    grids.push(grid)
                }
            }
            else {

                for (var i = 0; i < Math.ceil(ListAll.length / 4); i++) {

                    var grid = (
                        <Grid key={"grid" + i}>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 0) ?
                                        <Product key={((i * 4) + 0)} product={ListAll[((i * 4) + 0)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 0)])}
                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 0)])}

                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 1) ?
                                        <Product key={((i * 4) + 1)} product={ListAll[((i * 4) + 1)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 1)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 1)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 1)])}
                                        /> : null}

                                </Row>
                            </Col>
                            <Col>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 2) ?
                                        <Product key={((i * 4) + 2)} product={ListAll[((i * 4) + 2)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 2)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 2)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 2)])}
                                        /> : null}

                                </Row>
                                <Row style={styles.ContainerItem}>
                                    {ListAll.length > ((i * 4) + 3) ?
                                        <Product key={((i * 4) + 3)} product={ListAll[((i * 4) + 3)]}
                                                 onPressAddToCart={this.onPressAddToCartPR.bind(this, ListAll[((i * 4) + 3)])}
                                                 onPress={this.onPressProduct.bind(this, ListAll[((i * 4) + 3)])}

                                                 onPressAddToFavorite={this.onPressAddToFavorite.bind(this, ListAll[((i * 4) + 3)])}
                                        /> : null}

                                </Row>
                            </Col>
                        </Grid>)

                    grids.push(grid)
                }
            }

            return (<Swiper style={{
                // flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row'

            }}
                            dot={<View
                                style={{
                                    backgroundColor: color.CustomBlack,
                                    width: 8, height: 8, borderRadius: 4,
                                    marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                    marginBottom: -hp("9%"),
                                }}/>}
                            activeDot={<View
                                style={{
                                    backgroundColor: color.yellow,
                                    width: 8, height: 8, borderRadius: 4,
                                    marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                    marginBottom: -hp("9%"),
                                }}/>}
                            loop={false}
                            showsButtons={false}>
                {grids}

            </Swiper>)

        }
    }

    closeModal() {
        this.setState({isVisible: false})
    }


    vendorPressed() {


        this.props.ResetSelectedCategory(null)
        this.props.currentCategory = null
        this.props.selectedVendor(this.product.store)
        // this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);


        this.props.navigation.navigate({
            routeName: NAVIGATION_PRODUCTS_SCREEN,
            parms: {currentVendor: this.product.store, ...this.props},
            key: 'vendor' + this.product.store.id
        });


    }

    AddToCartPressed() {

        /////
        if (this.product.attributes.length > 0) {

            this.setState({isVisible: true})

        } else {
            this.onPressAddToCartThis()
        }
    }

    render() {


        return (
            <DefaultHeader onPressAddToFavoriteThis={this.onPressAddToFavoriteThis.bind(this)}
                           AddToCartAction={this.AddToCartPressed.bind(this)} type={"product"} {...this.props}
                           isLoading={this.state.isLoading}>


                {!this.state.isReady ?
                    <ActivityIndicator style={{flex: 1, opacity: this.state.isReady ? 1.0 : 0.0}} animating={true}
                                       size="large"/> : <Grid>

                        <Row>
                            <View style={{flex: 1}}>
                                <ScrollView style={{flex: 1}}>
                                    <Grid style={{flex: 1,}}>
                                        <Row size={3.7} style={{flexDirection: 'column'}}>

                                            {/* Start Product Images*/}

                                            <Grid style={[{height: hp("60%")}]}>

                                                <Swiper
                                                    ref={(swiper) => {
                                                        this.swiper = swiper
                                                    }}
                                                    style={{}}
                                                    // onMomentumScrollEnd={this._onMomentumScrollEnd}
                                                    dotColor={color.CustomBlack}
                                                    activeDotColor={color.yellow}
                                                    loop={true}
                                                    showsButtons={false}
                                                    // index={this.state.step}
                                                    dot={<View
                                                        style={{
                                                            backgroundColor: color.CustomBlack,
                                                            width: 8, height: 8, borderRadius: 4,
                                                            marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                                            marginBottom: -hp("6%"),
                                                        }}/>}
                                                    activeDot={<View
                                                        style={{
                                                            backgroundColor: color.yellow,
                                                            width: 8, height: 8, borderRadius: 4,
                                                            marginLeft: 3, marginRight: 3, marginTop: hp("0%"),
                                                            marginBottom: -hp("6%"),
                                                        }}/>}
                                                >
                                                    {this.state.images.map((item, index, []) => {
                                                        return (
                                                            <Grid key={index}>
                                                                <Row>
                                                                    <TouchableOpacity
                                                                        ref="touch"
                                                                        onPress={(item) => {
                                                                            // alert(parseInt(index))
                                                                            this.setState({IndexPressed: index})
                                                                            this.setState({ModalGallery: true})
                                                                        }}
                                                                        style={[{flex: 1,}]}>
                                                                        <FastImage
                                                                            style={{
                                                                                width: wp("100%"),
                                                                                height: hp("55%")
                                                                            }}
                                                                            source={{
                                                                                uri: item.url,
                                                                                priority: FastImage.priority.high,
                                                                            }}/>
                                                                    </TouchableOpacity>
                                                                </Row>
                                                            </Grid>
                                                        )

                                                    })}

                                                </Swiper>

                                                <Modal visible={this.state.ModalGallery} transparent={true}>
                                                    <ImageViewer
                                                        index={this.state.IndexPressed}

                                                        renderFooter={
                                                            () =>
                                                                <TouchableOpacity style={styles.CloseContainer}
                                                                                  onPress={() => {
                                                                                      this.setState({ModalGallery: false})

                                                                                  }}>
                                                                    <Text
                                                                        style={styles.Close}> {strings.t("close")} </Text>
                                                                </TouchableOpacity>}
                                                        imageUrls={this.state.images}/>
                                                </Modal>

                                            </Grid>

                                            {/* End Product Images*/}

                                            {/* Start Product desc price */}

                                            <Grid style={[styles.paddingSection, {
                                                // height: hp("18%"),
                                                marginBottom: hp("2%"),
                                                flex: 1
                                            }]}>

                                                <Row size={0.8} style={[styles.CenterContent, {}]}>
                                                    <Grid style={[styles.CenterContent, {}]}>
                                                        <Col size={3} style={{}}><Text
                                                            style={[Typography.Text18OpenSansBold]}>{this.product.name}</Text></Col>
                                                        <Col size={1} style={[{alignItems: 'flex-end'}]}>

                                                            {this.product.on_sale && this.state.regular_price > 0 ?
                                                                <NumberFormat
                                                                    value={this.state.regular_price}
                                                                    renderText={value => <Text
                                                                        style={[styles.oldPrice, Typography.Text12OpenSansBold, {}]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                                                    displayType={'text'} thousandSeparator={true}
                                                                    prefix={""}/> : null}
                                                        </Col>
                                                    </Grid>
                                                </Row>
                                                <Row size={0.8} style={[styles.CenterContent, {}]}>
                                                    <Grid style={[styles.CenterContent, {}]}>
                                                        <Col size={2.5}><TouchableOpacity
                                                            onPress={this.vendorPressed}><Text
                                                            style={[Typography.Text16Light, {
                                                                color: color.blue,
                                                                alignSelf: 'flex-start'
                                                            }]}>{this.product.store.name}</Text></TouchableOpacity></Col>
                                                        <Col size={1.5} style={[{alignItems: 'flex-end'}]}>

                                                            <NumberFormat
                                                                value={this.state.price}
                                                                renderText={value => <Text
                                                                    style={[Typography.Text18OpenSansBold]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                                                displayType={'text'} thousandSeparator={true}
                                                                prefix={""}/>

                                                        </Col>
                                                    </Grid>
                                                </Row>
                                                <Row size={0.8} style={[styles.CenterContent, {}]}>
                                                    <Grid style={[styles.CenterContent, {}]}>
                                                        <Col><Text
                                                            style={[Typography.Text14Light, {color: color.CustomBlack}]}>{strings.t("sku")}: {this.state.sku}</Text></Col>
                                                    </Grid>
                                                </Row>

                                                {this.renderAttributes()}


                                            </Grid>


                                            {/* End Product desc  price*/}


                                            {/* Start Product desc */}

                                            <Grid style={[styles.paddingSection, {
                                                paddingBottom: hp("2%"),
                                                flex: 1,
                                                alignSelf: 'stretch',
                                                flexDirection: 'column',
                                                borderBottomColor: color.GrayTransparent,
                                                borderBottomWidth: 0,

                                            }]}>
                                                <Row style={{alignItems: 'flex-start'}}>
                                                    {this.product.short_description.length > 0 ?

                                                        <HTMLView stylesheet={stylesHTML}
                                                                  value={this.product.short_description + ""}/>
                                                        : null}


                                                </Row>
                                            </Grid>


                                            <Grid style={[styles.paddingSection, {
                                                paddingBottom: hp("2%"),
                                                flex: 1,
                                                alignSelf: 'stretch',
                                                flexDirection: 'column',
                                                borderBottomColor: color.GrayTransparent,
                                                borderBottomWidth: 0,

                                            }]}>
                                                <Row style={{alignItems: 'flex-start'}}>
                                                    {this.product.description.length > 0 ?
                                                        <HTMLView stylesheet={stylesHTML}
                                                                  value={this.product.description + ""}/>
                                                        : null}
                                                </Row>
                                            </Grid>


                                            {/* End Product desc  */}


                                            {/* Start Information Shipping   */}


                                            <Grid style={[styles.paddingSection, {
                                                marginTop: hp("3%"),
                                                paddingBottom: hp("2%"),
                                                flex: 1,
                                                alignSelf: 'stretch',
                                                flexDirection: 'column',
                                            }]}
                                            >
                                                <View style={{paddingBottom: hp("1%")}}>
                                                    <View style={{flex: 1, alignItems: 'flex-start'}}>

                                                        <ScrollView
                                                            style={{paddingBottom: wp("2%"),}}
                                                            horizontal={true}
                                                        >


                                                            <View style={{
                                                                flexDirection: I18nManager.isRTL ? 'row' : "row",


                                                                justifyContent: 'center',
                                                                alignItems: 'flex-start',
                                                                flex: 1
                                                            }}>
                                                                <TouchableOpacity
                                                                    onPress={() => {
                                                                        this.setState({
                                                                            TabInfoType: "vendor",
                                                                            VendorSelected: true,
                                                                            AdditionalSelected: false,
                                                                            ShippingSelected: false,
                                                                        })
                                                                    }}><Text
                                                                    style={[
                                                                        this.state.VendorSelected ? styles.selectedTabText : styles.UnSelectedTabText
                                                                    ]}
                                                                >{strings.t("vendor_inof")}</Text></TouchableOpacity>
                                                                <TouchableOpacity
                                                                    onPress={() => {
                                                                        this.setState({
                                                                            TabInfoType: "additional",
                                                                            VendorSelected: false,
                                                                            AdditionalSelected: true,
                                                                            ShippingSelected: false,
                                                                        })
                                                                    }}
                                                                    style={[styles.CenterContent]}
                                                                ><Text
                                                                    style={[
                                                                        this.state.AdditionalSelected ? styles.selectedTabText : styles.UnSelectedTabText
                                                                    ]}
                                                                >{strings.t("additional_information")}</Text></TouchableOpacity>

                                                                <TouchableOpacity
                                                                    onPress={() => {
                                                                        this.setState({
                                                                            TabInfoType: "shipping",
                                                                            VendorSelected: false,
                                                                            AdditionalSelected: false,
                                                                            ShippingSelected: true,
                                                                        })

                                                                    }}
                                                                    style={[{alignItems: 'flex-end'}]}
                                                                ><Text
                                                                    style={[
                                                                        this.state.ShippingSelected ? styles.selectedTabText : styles.UnSelectedTabText
                                                                    ]}
                                                                >{strings.t("shipping")}</Text></TouchableOpacity>

                                                            </View>
                                                        </ScrollView>


                                                    </View>
                                                </View>


                                                {
                                                    this.state.TabInfoType === "vendor" ?
                                                        <Row>
                                                            <Grid>
                                                                <Row><Text
                                                                    style={[Typography.Text14OpenSansBold, {paddingBottom: hp("1%")}]}>{strings.t("vendor_informations")}</Text></Row>
                                                                <Row>
                                                                    <Grid>
                                                                        <Row><Text
                                                                            style={[Typography.Text14Light, styles.TextDescription]}>
                                                                            {strings.t("store_name")}: {this.product.store.shop_name}
                                                                        </Text></Row>
                                                                        <Row>
                                                                            <TouchableOpacity
                                                                                onPress={this.vendorPressed}>
                                                                                <View style={{flexDirection: 'row'}}>
                                                                                    <Text
                                                                                        style={[Typography.Text14Light, styles.TextDescription]}>
                                                                                        {strings.t("al_vendor")}:
                                                                                    </Text>
                                                                                    <Text
                                                                                        style={[Typography.Text14OpenSansBold, styles.TextDescription, {color: color.blue}]}> {this.product.store.name} </Text>
                                                                                </View></TouchableOpacity></Row>
                                                                        <Row><Text
                                                                            style={[Typography.Text14Light, styles.TextDescription]}>
                                                                            {strings.t("address")}: {this.product.store.address ? this.product.store.address.city : ""}
                                                                        </Text></Row>


                                                                    </Grid>
                                                                </Row>
                                                            </Grid>
                                                        </Row>
                                                        : this.state.TabInfoType === "additional" ?
                                                        <Row>
                                                            <Grid>
                                                                <Row>

                                                                    {this.renderAttributesText()}
                                                                </Row>
                                                            </Grid>
                                                        </Row> :
                                                        <Row>
                                                            <Grid>
                                                                <Row>
                                                                    <Grid>
                                                                        <Row><Text
                                                                            style={[Typography.Text14Light, styles.TextDescription]}>
                                                                            {strings.t("no_shipping_details")}
                                                                        </Text></Row>
                                                                    </Grid>
                                                                </Row>
                                                            </Grid>
                                                        </Row>
                                                }

                                            </Grid>

                                            {/* End Information Shipping */}


                                            {/* Start More product*/}

                                            <Grid style={[styles.paddingSectionPro, {height: hp("90%")}]}>

                                                <Row size={0.25}
                                                     style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                                                    <Text
                                                        style={[Typography.Text18OpenSansBold]}>{strings.t("more_products")}</Text>
                                                </Row>
                                                <Row size={3.75} style={{paddingTop: hp("2%"),}}>

                                                    {this.renderSwiperProducts(this.props.isLoadingRelated, this.props.moreList)}


                                                </Row>

                                            </Grid>

                                            {/* End More product*/}


                                            {/* Start Related  product*/}


                                            <Grid style={[styles.paddingSectionPro, {height: hp("90%")}]}>

                                                <Row size={0.25}
                                                     style={{borderBottomColor: color.yellow, borderBottomWidth: 2}}>
                                                    <Text
                                                        style={[Typography.Text18OpenSansBold]}>{strings.t("related_products")}</Text>
                                                </Row>
                                                <Row size={3.75} style={{paddingTop: hp("2%"),}}>


                                                    {this.renderSwiperProducts(this.props.isLoadingRelated, this.props.relatedList)}


                                                </Row>

                                            </Grid>


                                            {/* End Related product*/}


                                        </Row>
                                    </Grid>
                                </ScrollView>

                            </View>
                        </Row>


                        <Toast
                            visible={this.props.statusAddToCart === SUCCESS}
                            position={-50}
                            duration={3000}
                            shadow={false}
                            animation={false}
                            hideOnPress={true}
                        >{this.props.ErrorMessageADDProductToCart}</Toast>


                        <Toast
                            visible={this.props.statusAddToFavorite === SUCCESS}
                            position={-50}
                            duration={3000}
                            shadow={false}
                            animation={false}
                            hideOnPress={true}
                        >{strings.t("success_favorite")}</Toast>


                        <ModalShow isVisible={this.state.isVisible}>
                            <View style={{flex: 1}}>
                                <ScrollView>


                                    <View style={{
                                        borderWidth: 1,
                                        shadowColor: color.GrayBorder,
                                        shadowOpacity: 0.8,
                                        shadowRadius: 3,
                                        shadowOffset: {
                                            height: 1,
                                            width: 1
                                        },
                                        borderRadius: 8,
                                        flexDirection: 'column',
                                        backgroundColor: 'white',
                                        paddingTop: wp("3%"),
                                        paddingBottom: wp("6%"),
                                        marginTop:Platform.OS==="android"?wp("15%"):wp("15%"),
                                    }}
                                    >

                                        {this.renderSelectOption()}

                                        <View style={{flexDirection: 'row', flex: 1,}}>

                                            <View style={{flex: 2, paddingEnd: wp("3"), paddingStart: wp("3")}}>

                                                <TouchableOpacity style={{
                                                    backgroundColor: color.yellow,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    borderRadius: 8,
                                                    height: hp('4.5%'),
                                                    marginTop: hp('2%')
                                                }} onPress={() => {

                                                    //console.log(this.meta_data.length + "" + this.product.attributes.length)

                                                    if (this.meta_data.length === this.product.attributes.length) {


                                                        if(!this.state.isLoading){

                                                            this.setState({isVisible: false})

                                                            this.onPressAddToCartThis()
                                                        }
                                                    }
                                                    else {


                                                        let toast = Toast.show(strings.t("please_choose_all_attr"), {
                                                            duration: Toast.durations.LONG,
                                                            position: Toast.positions.BOTTOM,
                                                            shadow: true,
                                                            animation: true,
                                                            hideOnPress: true,
                                                            delay: 0,

                                                        });


                                                        setTimeout(function () {
                                                            Toast.hide(toast);
                                                        }, 2000);


                                                    }


                                                }}
                                                >
                                                    <Text
                                                        style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("add_to_cart")}</Text>
                                                </TouchableOpacity>


                                            </View>
                                            <View style={{flex: 2, paddingEnd: wp("3"), paddingStart: wp("3")}}>

                                                <TouchableOpacity style={{
                                                    backgroundColor: color.CustomBlack,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    borderRadius: 8,
                                                    height: hp('4.5%'),
                                                    marginTop: hp('2%')
                                                }} onPress={() => {

                                                    this.setState({isVisible: false})
                                                }}
                                                >
                                                    <Text
                                                        style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("close")}</Text>
                                                </TouchableOpacity>


                                            </View>


                                        </View>

                                    </View>

                                </ScrollView>

                            </View>
                        </ModalShow>

                        {this.renderVariant()}
                    </Grid>}

            </DefaultHeader>

        );


    }

    getSelectedItem(elem) {

        //////console.log("getSelectedItem")
        //////console.log(elem)
    }

    selectedIndex(elem) {

        //////console.log("selectedIndex")
        //////console.log(elem)
    }

    renderVariant() {
        if (this.props.isLoadingVariant) {

            if (!this.state.isLoading) {

                //this.swiper.scrollBy(0,true)

                this.setState({isLoading: true})
            }

            // this.setState({isLoading: true})
        } else {


            if (this.state.isLoading) {

                ////console.log("======variant=====")
                ////console.log(this.props.variant)
                this.setState({isLoading: false})
                if (this.props.variant) {
                    this.setState({regular_price: this.props.variant.display_regular_price})
                    this.setState({price: this.props.variant.display_price})
                    this.setState({sku: this.props.variant.sku})
                    this.setState({variation_id: this.props.variant.variation_id})

                    this.product.price = this.props.variant.display_price
                    this.product.stock_status = this.props.variant.stock_status
                    this.product.variation_id = this.props.variant.variation_id
                    // this.product.regular_price=this.props.variant.display_regular_price
                    ////console.log("after varian")
                    ////console.log(this.product)
                    this.product.images.map((e, index) => {

                        if (e.id === parseInt(this.props.variant.image_id)) {
                            ////console.log("===index===")
                            ////console.log(index)


                            var step = this.state.step
                            if (index > step) {

                                this.swiper.scrollBy(parseInt(index - step), true)
                                this.setState({step: index})
                            }
                            else {

                                this.swiper.scrollBy(-parseInt(step - index), true)
                                this.setState({step: index})


                            }

                        }

                    })



                    if(this.product.stock_status===false)
                    {
                        let toast = Toast.show(strings.t("out_of_stock"), {
                            duration: Toast.durations.LONG,
                            position: Toast.positions.BOTTOM,
                            shadow: true,
                            animation: true,
                            hideOnPress: true,
                            delay: 0,

                        });


                        setTimeout(function () {
                            Toast.hide(toast);
                        }, 2000);
                    }


                }
            }


        }
    }

}


const stylesHTML = StyleSheet.create({
    p: {
        flex: 1,
        alignSelf: I18nManager.isRTL ? 'flex-start' : 'flex-end',
        ...Typography.Text12Light,

    }, a: {
        flex: 1,
        alignSelf: I18nManager.isRTL ? 'flex-start' : 'flex-end',
        ...Typography.Text12Light,

    },
});

const
    styles = StyleSheet.create({
        ContainerItem: {
            padding: 4,

        }
        ,
        CenterContent: {
            alignItems: 'center', justifyContent: 'center'
        },

        TouchableFlex: {

            alignItems: 'center', justifyContent: 'center',
            flex: 1
        },
        CloseContainer: {
            width: wp("100%"),
            marginTop: hp("5%"), textAlign: 'center', alignItems: 'center', justifyContent: 'center',
        },
        Close: {
            ...Typography.Text18OpenSansBold,
            paddingBottom: hp("1%"),
            color: 'white',
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',
        },
        ArrwoLeft: {

            width: wp('4%'),
            resizeMode: 'contain',
        },
        paddingSection: {

            marginStart: wp("6%"),
            marginEnd: wp("6%"),
        },
        oldPrice: {
            ...Typography.Text13,
            color: color.red,
            textDecorationLine: 'line-through'

        },

        TextDescription: {
            marginBottom: hp("1%")

        },
        selectedTabText: {
            color: color.yellow,
            paddingEnd: wp("3%"),
            ...Typography.Text14Medium,
        },
        UnSelectedTabText: {

            paddingEnd: wp("3%"),
            color: color.black,
            ...Typography.Text14Medium,
        }
        ,
        paddingSectionPro: {

            paddingStart: wp("6%"),
            paddingEnd: wp("6%"),
            paddingTop: wp("6%"),
        },
    })

const
    mapStateToProps = state => {
        return {
            current: state.product.current,
            isLoadingRelated: state.home.isLoadingRelated,
            statusRelated: state.home.statusRelated,
            relatedList: state.home.relatedList,
            moreList: state.home.moreList,
            statusAddToFavorite: state.product.statusAddToFavorite,
            ErrorMessageADDProductToCart: state.product.ErrorMessageADDProductToCart,
            isLoadingVariant: state.product.isLoadingVariant,
            statusVariant: state.product.statusVariant,
            ErrorMessageVariant: state.product.ErrorMessageVariant,
            variant: state.product.variant,
        }
    }
const
    mapDispatchToProps = dispatch => {
        return {

            getRelatedProducts: (payload) => dispatch(relatedAndMoreProduct(payload)),
            addProductToCartPR: (payload) => dispatch(addToCart(payload)),
            selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
            getVariant: (payload) => dispatch(getVariant(payload)),
            addProductToFavorite: (payload) => dispatch(addToFavorite(payload)),
            selectedVendor: (payload) => dispatch(openSelectedVendor(payload)),
            ResetSelectedCategory: (payload) => dispatch(ResetSelectedCategory(payload)),

        }
    }

export default connect(mapStateToProps, mapDispatchToProps)

(
    ProductScreen
)
;

