import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import TabedFooter from "../../template/TabedFooter";
import Typography from "../../../constants/typography";

class AboutUsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        return (


            <DefaultHeader type={"home"} {...this.props}>

                <Grid>
                    <Row >

                        <ScrollView>
                            {I18nManager.isRTL ?
                                <View style={styles.CenterContent}>


                                    <Text style={styles.TextAbout}>
                                        {"\n"}{"\n"}انتشرت مواقع التجارة الإلكترونية في مختلف دول العالم، حتى أصبحت من
                                        ضمن ثقافة
                                        المجتمع التي يتحدث بها الجميع كما هو الحال في السوق العادي تماماً، ومن هنا
                                        انطلقَتْ
                                        الحاجة لوجود متجر إلكتروني احترافي بمواصفات عالمية في سوريا.

                                        (اطلب . كوم) هو أكبر متجر إلكتروني مسجّل في سوريا، تم بناؤه بمعايير وخبرات
                                        عالمية لتوفير
                                        آلية تسوق فريدة من نوعها تشمل عمليات الدفع والتوصيل بسهولة وأمان تام. كما يضم
                                        الموقع
                                        فئات عديدة ومتنوعة من المنتجات.{"\n"}{"\n"}

                                        نتجنَّب في (اطلب . كوم) كل ما هو مُضرٌّ أو مسيء أو يؤثر سلباً على المجتمع أو
                                        البيئة،{"\n"}{"\n"}
                                        ونسعى لتفاديه باستمرار، ونرحّب بكافة المقترحات التي تساعدنا في تحقيق هذه
                                        الغاية.{"\n"}{"\n"}{"\n"}

                                    </Text>

                                    <Text style={styles.TextAbout}>
                                        أكبر متجر إلكتروني مسجل في سوريا. {"\n"}{"\n"}
                                        التعامل بالمنتجات الجديدة لضمان أفضل.{"\n"}{"\n"}
                                        عرض المنتجات من مصادرها الأصلية ووكالاتها المعتمدة وفق اتفاقيات مسبقة تضمن حقوق
                                        جميع
                                        الأطراف.{"\n"}{"\n"}
                                        اعتماد المنتجات المسجلة لدى الغرف والنقابات (الصناعية أو التجارية أو المهنية)
                                        والمستوردة
                                        ببيانات جمركية.{"\n"}{"\n"}
                                        التأكد من مطابقة المواصفات والصلاحية التامة للمنتج.{"\n"}{"\n"}
                                        وجود صور حقيقية تظهر المنتجات بطريقة واضحة.{"\n"}{"\n"}
                                        وجود إمكانية للإرجاع والتبديل وفق سياسات واضحة.{"\n"}{"\n"}
                                        إمكانية الدفع بعدة طرق منها (الدفع عند الاستلام).{"\n"}{"\n"}
                                        متجر (اطلب) الإلكتروني هو عبارة عن شركة محدودة المسؤولية تم تأسيسها في عام
                                        2019{"\n"}{"\n"}

                                    </Text>
                                </View>
                                :

                                <View style={styles.CenterContent}>


                                    <Text style={styles.TextAbout}>
                                        {"\n"} {"\n"} E-commerce sites spread around the world, until they became
                                        Within a culture
                                        Society that everyone speaks as in the normal market just like here
                                        Launched
                                        The need for a professional electronic store with international specifications
                                        in Syria.

                                        (Otlab.com) is the largest registered online store in Syria, built with
                                        standards and expertise
                                        Universal to provide
                                        Unique shopping mechanism with easy and secure payment and delivery. Also
                                        features
                                        Site
                                        Many different product categories. {"\n"} {"\n"}

                                        We avoid anything that is harmful, offensive, or adversely affects the
                                        community,
                                        Environment, {"\n"} {"\ n"}
                                        We constantly seek to avoid it, and we welcome all suggestions that help us
                                        achieve this
                                        Goal. {"\n"} {"\n"} {"\n"}

                                    </Text>

                                    <Text style={styles.TextAbout}>
                                        The largest registered electronic store in Syria. {"\n"} {"\n"}
                                        Deal with new products for better security. {"\n"} {"\n"}
                                        Display products from their original sources and approved agencies in accordance
                                        with prior agreements guaranteeing rights
                                        all
                                        Parties. {"\n"} {"\n"}
                                        Approval of products registered with chambers and trade unions (industrial,
                                        commercial or professional)
                                        And imported
                                        With customs data. {"\n"} {"\n"}
                                        Ensure that the specification and the validity of the product are
                                        met. {"\n"} {"\n"}
                                        There are real images that show the products in a clear way. {"\n"} {"\n"}
                                        There is a possibility of return and exchange with clear policies. {"\n"} {"\n"}
                                        You can pay in a variety of ways (pay on delivery). {"\n"} {"\n"}
                                        Online Store (Ask) is a limited liability company established in 1998
                                        2019 {"\n"} {"\n"}

                                    </Text>
                                </View>
                            }

                        </ScrollView>
                    </Row>
                </Grid>
            </DefaultHeader>


        );
    }
}

export const styles = {

    CenterContent: {
        alignItems: 'center', justifyContent: 'center',
        paddingTop: wp("3%"),
        paddingStart: wp("6%"),
        paddingEnd: wp("6%"),
    },
    TextAbout: {
        textAlign: 'center',
        ...Typography.Text14OpenSansBold

    }
}

export default AboutUsScreen;