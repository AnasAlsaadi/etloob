import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput, FlatList, Linking} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_APP,
    NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_DRAWER, NAVIGATION_EDIT_PROFILE_SCREEN, NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN, NAVIGATION_MYORDERS_SCREEN, NAVIGATION_PRODUCTS_SCREEN, NAVIGATION_SHIPPING_ADDRESS_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import {editProfile} from "../../../store/profile/actions";
import TabedFooter from "../../template/TabedFooter";
import FastImage from "react-native-fast-image";
import colors from "../../../constants/colors";
import CodePush from "react-native-code-push";
import ProductHorizontal from "../../elements/ProudctHorizontal";
import ProductVendor from "../../elements/ProductVendor";
import RNPickerSelect from "react-native-picker-select";
import {Categories, ResetSelectedCategory} from "../../../store/category/actions";
import {productLists} from "../../../store/ProductList/actions";
import color from '../../../constants/colors';
import axios from "axios";
import {config, url} from "../../../api/api";
import {ResetSelectedSearchWord} from "../../../store/search/actions";
import {openSelectedVendor} from "../../../store/vendor/actions";

class VendorTabSettingScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};


    constructor(props) {
        super(props);
        this.state = {

            first_name: "",
        }
    }

    componentDidMount() {

        axios.get('https://etloob.com/wp-json/dokan/v1/settings?vendor_id=' + global.user.id,
            config)
            .then(function (response) {
                // console.log("instide req local");
                // console.log(response);
                // console.log("instide req");

                this.setState({
                    store_name:response.data.store_name,
                    phone:response.data.phone,
                    product_count_in_page:response.data.store_ppp+"",
                    address:response.data.address.street_1,
                    street_address:response.data.address.street_2,
                    city:response.data.address.city,

                })
            }.bind(this))
            .catch(function (error) {
                // console.log("instide err local");
                // console.log(error.response.data);
                // console.log(error);
                return {status: 400, response: null, message: error.response.data.message}
            });

    }

    render() {


        return (
            <Grid>

                <Row>
                    <View style={{flex: 1}}>
                        <Grid style={{flex: 1,}}>
                            <Row style={{flex: 1, paddingStart: wp('5%'), paddingEnd: wp("5%")}}>
                                <ScrollView style={{flex: 1,}}>

                                    <Grid style={{
                                        flexDirection: 'column', backgroundColor: '#fff',
                                        paddingTop: wp('5%'),
                                        paddingStart: wp('5%'),
                                        paddingBottom: wp('5%'),
                                        paddingEnd: wp("5%"),
                                        borderRadius: 4,
                                    }}>

                                        <InputElement forCheckout={true} text={strings.t('store_name')}
                                                      onChangeText={(value) => {
                                                          this.setState({store_name: value})
                                                      }
                                                      }
                                                      value={this.state.store_name}
                                        />

                                        <InputElement forCheckout={true} text={strings.t('product_count_in_page')}
                                                      onChangeText={(value) => {
                                                          this.setState({product_count_in_page: value})
                                                      }
                                                      }
                                                      value={this.state.product_count_in_page}
                                        />

                                        <InputElement forCheckout={true} text={strings.t('address')}
                                                      onChangeText={(value) => {
                                                          this.setState({address: value})
                                                      }
                                                      }
                                                      value={this.state.address}
                                        />
                                        <InputElement forCheckout={true} text={strings.t('street_address')}
                                                      onChangeText={(value) => {
                                                          this.setState({street_address: value})
                                                      }
                                                      }
                                                      value={this.state.street_address}
                                        />

                                        <InputElement forCheckout={true} text={strings.t('city')}
                                                      onChangeText={(value) => {
                                                          this.setState({city: value})
                                                      }
                                                      }
                                                      value={this.state.city}
                                        />
                                        <InputElement forCheckout={true} text={strings.t('phone')}
                                                      onChangeText={(value) => {
                                                          this.setState({phone: value})
                                                      }
                                                      }
                                                      value={this.state.phone}
                                        />

                                        <Row>
                                            <Grid>
                                                <Col style={{padding:wp("1%")}}>

                                                    <TouchableOpacity style={{
                                                        backgroundColor: color.yellow,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        borderRadius: 8,
                                                        height: hp('4.5%'),
                                                        marginTop: hp('2%')
                                                    }} onPress={() => {
                                                        global.storage.remove({
                                                            key: 'user',
                                                        })


                                                        CodePush.restartApp();
                                                    }}
                                                    >
                                                        <Text
                                                            style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("logout")}</Text>
                                                    </TouchableOpacity>

                                                </Col>
                                                <Col style={{padding:wp("1%")}}>

                                                    <TouchableOpacity style={{
                                                        backgroundColor: color.yellow,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        borderRadius: 8,
                                                        height: hp('4.5%'),
                                                        marginTop: hp('2%')
                                                    }} onPress={() => {



                                                        this.props.ResetSelectedCategory(null)
                                                        this.props.ResetSelectedSearchWord(null)

                                                        this.props.currentCategory=null
                                                        this.props.selectedVendor({id:global.user.id})
                                                        this.props.navigation.navigate(NAVIGATION_PRODUCTS_SCREEN);

                                                        // Linking.openURL('http://etloob.com/store/'+this.state.store_name);

                                                    }}
                                                    >
                                                        <Text
                                                            style={[Typography.Text14OpenSansBold, {color: 'white'}]}>{strings.t("visit_store")}</Text>
                                                    </TouchableOpacity>

                                                </Col>
                                            </Grid>
                                        </Row>
                                    </Grid>
                                </ScrollView>
                            </Row>
                        </Grid>
                    </View>
                </Row>
            </Grid>)

            ;

    }

}

const styles = {
    profileImgContainer: {
        marginVertical: wp("3%"),
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        overflow: 'hidden'
    },
    productImage: {
        width: wp("30%"),
        height: wp("30%"),
        borderRadius: 80,
        resizeMode: 'contain',
    },
    icon: {
        width: wp("12%"),
        height: wp("12%"),
        resizeMode: 'contain',
    },
    iconArrow: {
        width: wp("4%"),
        height: wp("4%"),

        resizeMode: 'contain',
    },
    TextDetails: {
        flex: 1,
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },
    TextLi: {
        textAlign: 'center',
        ...Typography.Text16Light,
        color: colors.CustomBlack
    },

    item: {

        marginHorizontal: wp("5%"),
        marginVertical: wp("3%"),
        borderWidth: 1,
        borderRadius: 2,
        borderColor: colors.GrayBorder,
        backgroundColor: 'white',
    },
    itemContainer: {

        flexDirection: 'row',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    textContainer: {

        flex: 3,
        alignItems: 'flex-start',
        marginStart: wp("3%")
    }
}

const mapStateToProps = state => {
    return {}
}
const mapDispatchToProps = dispatch => {
    return {
        selectedVendor: (payload) => dispatch(openSelectedVendor(payload)),

        ResetSelectedCategory: (payload) => dispatch(ResetSelectedCategory(payload)),
        ResetSelectedSearchWord: (payload) => dispatch(ResetSelectedSearchWord(payload)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VendorTabSettingScreen);
