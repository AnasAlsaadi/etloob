import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Text, ScrollView, Image, TouchableOpacity, View, TextInput, I18nManager, Modal} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import strings from './../../../locales/i18n'
import Typography from './../../../constants/typography'
import InputElement from "../../elements/TextInput";
import {
    NAVIGATION_CHECKOUT_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_VERIFICATION_SCREEN
} from "../../../navigation/types";
import SubmitButton from "../../elements/Buttons";
import {getCustomerCart, removeProductFromCart} from "../../../store/cart/actions";
import {connect} from "react-redux";
import RadioGroup from 'react-native-radio-buttons-group';
import Toast from "react-native-root-toast";
import {placeCartOrder, resetPlaceOrder} from "../../../store/checkout/actions";
import AuthHeader from "../../../containers/AuthHeader";
import {FAILURE, SUCCESS} from "../../../constants/actionsType";
import RNPickerSelect from "react-native-picker-select";
import CheckBox from "react-native-check-box";
import TextUnderLine from "../../elements/TextUnderLine";
import color from "../../../constants/colors";
import {styles} from "../AboutUsScreen";

class CheckoutScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    oj = {label: "اختر محافظة"}
    userCurrent = null

    constructor(props) {
        super(props);
        this.state = {
            termsChecked: true,
            isVisible: false,
            first_name: "",
            last_name: "",
            address_1: "",
            coupon: "",
            address_2: "",
            phone: "" + global.user.billing.phone,
            email: "" + global.user.email,
            country: "",
            customer_note: "",
            radioGroupData: [
                {
                    label: strings.t("cheque"),
                    value: "cheque",
                    hint:strings.t("hint_cheque"),
                },
                {
                    label: strings.t("cod"),
                    value: "cod",
                    hint:"",
                },
            ],
        }

        this.userCurrent = global.user
    }

    componentDidMount() {

        this.userCurrent = global.user

        this.props.customerCart({})

        this.setState({
            phone: this.userCurrent.billing.phone,
            email: this.userCurrent.email,
            first_name: this.userCurrent.billing.first_name,
            last_name: this.userCurrent.billing.last_name,
            address_1: this.userCurrent.billing.address_1,
            address_2: this.userCurrent.billing.address_2,
            country: this.userCurrent.billing.country,
        })
      //  console.log("user in cehcout")
       // console.log(this.userCurrent)
    }


    onPressRadio = data => this.setState({radioGroupData: data});

    place_Order() {


        if (!this.state.termsChecked) {
            Toast.show(strings.t("please_accept_terms"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });
            return
        }
        ////console.log(this.state)
        if (
            this.state.first_name.length > 0 &&
            this.state.last_name.length > 0 &&
            this.state.address_1.length > 0 &&
            this.state.address_2.length > 0 &&
            this.state.phone.length > 0 &&
            this.state.email.length > 0 &&
            this.state.country.length > 0

        ) {

            let selectedButton = this.state.radioGroupData.find(e => e.selected === true);
            selectedButton = selectedButton ? selectedButton.value : this.state.radioGroupData[0].value;

            var cart_sumbit = this.props.cart


            if (selectedButton === "cod") {

                cart_sumbit.status = "processing"
            }
            else if (selectedButton === "cheque") {

                cart_sumbit.status = "on-hold"
            }
            cart_sumbit.payment_method = selectedButton
            cart_sumbit.billing.first_name = this.state.first_name
            cart_sumbit.billing.last_name = this.state.last_name
            cart_sumbit.billing.address_1 = this.state.address_1
            cart_sumbit.billing.address_2 = this.state.address_2
            cart_sumbit.billing.phone = this.state.phone
            cart_sumbit.billing.email = this.state.email
            cart_sumbit.billing.country = this.state.country
            cart_sumbit.shipping.first_name = this.state.first_name
            cart_sumbit.shipping.last_name = this.state.last_name
            cart_sumbit.shipping.address_1 = this.state.address_1
            cart_sumbit.shipping.address_2 = this.state.address_2
            cart_sumbit.shipping.phone = this.state.phone
            cart_sumbit.shipping.email = this.state.email
            cart_sumbit.shipping.country = this.state.country
            cart_sumbit.customer_note = this.state.email
            cart_sumbit.coupon_lines = []
            if (this.state.coupon.length > 0) {

                cart_sumbit.coupon_lines.push({
                    code: this.state.coupon
                })
            }

            ////console.log("cart submited")
            ////console.log(cart_sumbit)
            this.props.actionPlaceCartOrder(cart_sumbit)

        } else {
            //
            Toast.show(strings.t("please_fill_all_inputs"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });







        }
    }

    renderMessagesPlaceOrder() {
        // console.log("fdfefefe")
        // console.log(this.props.statusPlaceHolder)
        // console.log(this.props.ErrorMessagePlaceHolder)
        // console.log(this.props.isLoadingPlaceHolder)
        if (this.props.statusPlaceHolder === SUCCESS) {


            global.storage.remove({key: 'cart'})
            global.cart = null
            this.props.actionResetPlaceOrder()

                this.props.navigation.navigate(NAVIGATION_HOME_SCREEN)



            Toast.show(strings.t("success_place_order"), {
                textStyle: Typography.Text14Light,
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });
        } else {

            ////console.log("fdfefefe")
            // Toast.show(this.props.ErrorMessagePlaceHolder, {
            //     textStyle: Typography.Text14Light,
            //     duration: Toast.durations.LONG,
            //     position: Toast.positions.BOTTOM,
            //     shadow: true,
            //     animation: true,
            //     hideOnPress: true,
            //     delay: 0,
            // });
        }


    }

    render() {


        return (


            <DefaultHeader  {...this.props} isLoading={this.props.isLoadingPlaceHolder}>

                <Grid style={{flex: 1}}>
                    <Row size={0.15}><View style={{marginTop: wp("3%"), marginStart: wp("6%")}}><Text
                        style={[Typography.Text18OpenSansBold, {}]}>{strings.t("pay_shipping")}</Text></View>
                    </Row>
                    <Row size={3.9} style={{flex: 1, paddingStart: wp('5%'), paddingEnd: wp("5%")}}>
                        <ScrollView style={{flex: 1,}}>

                            <Grid style={{
                                flexDirection: 'column', backgroundColor: '#fff',
                                paddingTop: wp('3%'),
                                paddingStart: wp('5%'),
                                paddingBottom: wp('5%'),
                                paddingEnd: wp("5%"),
                                borderRadius: 4,
                            }}>
                                <InputElement forCheckout={true} text={strings.t('first_name')}

                                              onChangeText={(value) => {
                                                  this.setState({first_name: value})
                                              }
                                              }
                                              value={this.state.first_name}
                                />
                                <InputElement forCheckout={true} text={strings.t('last_name')}
                                              onChangeText={(value) => {
                                                  this.setState({last_name: value})
                                              }
                                              }
                                              value={this.state.last_name}
                                />
                                <RNPickerSelect
                                    style={{marginTop: wp("3%"), marginBottom: wp("3%")}}
                                    placeholder={this.oj}
                                    onValueChange={(value) => {

                                        this.setState({country: value})
                                        ////console.log(value)
                                    }}

                                    value={this.state.country}
                                    items={[
                                        {label: 'الحسكة', value: 'AI'},
                                        {label: 'القامشلي', value: 'BS'},
                                        {label: 'السويداء', value: 'AW'},
                                        {label: 'حلب', value: 'AL'},
                                        {label: 'دمشق', value: 'AF'},
                                        {label: 'درعا', value: 'DZ'},
                                        {label: 'دير الزور', value: 'AD'},
                                        {label: 'حماة', value: 'AO'},
                                        {label: 'حمص', value: 'AQ'},
                                        {label: 'إدلب', value: 'AZ'},
                                        {label: 'اللاذقية', value: 'AG'},
                                        {label: 'القنيطرة', value: 'AR'},
                                        {label: 'الرقة', value: 'AM'},
                                        {label: 'ريف دمشق', value: 'AX'},
                                        {label: 'طرطوس', value: 'AT'},
                                    ]}
                                />

                                <InputElement forCheckout={true} text={strings.t('street_address')}
                                              onChangeText={(value) => {
                                                  this.setState({address_1: value})
                                              }
                                              }
                                              value={this.state.address_1}

                                />

                                <InputElement forCheckout={true} text={strings.t('street_address2_placeholder')}
                                              onChangeText={(value) => {
                                                  this.setState({address_2: value})
                                              }
                                              }
                                              value={this.state.address_2}

                                />

                                {/*<InputElement forCheckout={true} text={strings.t('mobile_number')}*/}
                                              {/*value={this.state.phone}*/}
                                              {/*onChangeText={(value) => {*/}
                                                  {/*this.setState({phone: value})*/}
                                              {/*}*/}
                                              {/*}*/}
                                {/*/>*/}
                                {/*<InputElement forCheckout={true} text={strings.t('email_address')}*/}
                                              {/*value={this.state.email}*/}

                                              {/*onChangeText={(value) => {*/}
                                                  {/*this.setState({email: value})*/}
                                              {/*}*/}
                                              {/*}*/}
                                {/*/>*/}
                                <InputElement forCheckout={true} multiline={true}
                                              numberOfLines={4}
                                              text={strings.t('customer_note')}
                                              onChangeText={(value) => {
                                                  this.setState({customer_note: value})
                                              }
                                              }
                                />


                                <Row>
                                    <RadioGroup radioButtons={this.state.radioGroupData} onPress={this.onPressRadio}/>

                                </Row>

                                <InputElement forCheckout={true} text={strings.t('u_have_coupon')}
                                              onChangeText={(value) => {
                                                  this.setState({coupon: value})
                                              }
                                              }
                                />




                                <Row>
                                    <View style={[, {
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }]}>
                                        <CheckBox
                                            style={{paddingEnd: wp("1%")}}
                                            onClick={() => {
                                                this.setState({
                                                    termsChecked: !this.state.termsChecked
                                                })
                                            }}
                                            isChecked={true}
                                        />
                                        <Text
                                            style={[Typography.Text13]}>{strings.t("i_have_read")}</Text>
                                        <TextUnderLine onPress={() => {

                                            this.setState({isVisible: true})
                                        }} text={strings.t("terms_and_conditions")}
                                                       style={{
                                                           marginStart: I18nManager.isRTL ? wp("0%") : wp("0.5%")

                                                       }}/>

                                    </View>
                                </Row>


                                <SubmitButton text={strings.t("place_order")} onPress={() => {

                                    this.place_Order()
                                }}/>


                            </Grid>
                        </ScrollView>

                    </Row>

                </Grid>


                {this.renderMessagesPlaceOrder()}

                <Toast
                    visible={this.props.statusPlaceHolder === FAILURE}
                    position={-50}
                    duration={3000}
                    shadow={false}
                    animation={false}
                    hideOnPress={true}
                >{this.props.ErrorMessagePlaceHolder}</Toast>


                <Modal transparent={true}
                       visible={this.state.isVisible}
                       onRequestClose={this.closeModal}>
                    <View style={{
                        backgroundColor: 'white',
                        borderRadius: 8,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: hp("60%"),
                        margin: wp("4%"),
                        borderColor: color.GrayBorder,
                        borderWidth: 1
                    }}>
                        <View style={{
                            width: wp("80%"),
                            height: hp("50%"),
                            flexDirection: 'column'
                        }}>

                            <View>
                                <ScrollView>


                                    <View style={{flexDirection: 'column'}}>
                                        <View style={{flexDirection: 'column', flex: 1}}>


                                            {I18nManager.isRTL ?
                                                <View style={styles.CenterContent}>


                                                    <Text style={styles.TextAbout}>
                                                        {"\n"} مقدمة:

                                                        {"\n"} السادة الزوار الكرام نرحب بكم في المتجر الالكتروني “أطلب”
                                                        ونورد لكم البنود والشروط التي تخص استخدامك ودخولكم لمتجرنا
                                                        الالكتروني وكافة الصفحات والروابط والأدوات والخواص المتفرعة عنه.
                                                        إن استخدامكم لمتجر “أطلب” الالكتروني هو موافقة منك على القبول
                                                        ببنود وشروط هذه الاتفاقية والذي يتضمن كافة التفاصيل أدناه وهو
                                                        تأكيد لإلتزامك بالاستجابة لمضمون هذه الاتفاقية الخاصة بمتجرنا
                                                        الالكتروني.

                                                        {"\n"} 1. تعتبر هذه المقدمة جزء لا يتجزأ من هذه الاتفاقية وهي
                                                        تتكامل وتتوافق مع كافة بنودها وفقراتها.

                                                        {"\n"} 2. المصطلحات

                                                        كون للمصطلحات المعرفة أو المكتوبة بخط عريض ذات المعاني الواردة
                                                        في هذه المادة.

                                                        2.1. المتجر الإلكتروني (الموقع): هو الموقع الالكتروني المثبت على
                                                        الشبكة العنكبوتية باسم…. والمملوك من قبل الوكيل التجاري وحده
                                                        المختص بتسويق وبيع المنتجات وفق الشروط والأحكام الخاصة بالموقع
                                                        والمعروضة والمثبتة به.

                                                        2.2. الشركات (الموكلين): هي الشركات التي تقدم المنتجات أو المواد
                                                        التابعة لها لعرضها باسمها وتسويقها وبيعها على المتجر الالكتروني
                                                        (الموقع) مقابل عمولة محددة وتخضع هذه الشركات (الموكلين) لإحكام
                                                        الوكالة التجارية العادية سواء ظهرت هذه الشركات باسمها المباشر أو
                                                        باسم شركات تابعة لها وبأي صفة كانت.

                                                        2.3. الزبائن: كل شخص طبيعي أو اعتباري يسعى لتسوق المنتجات
                                                        المعروضة على المتجر الالكتروني (الموقع) وفق القواعد والشروط
                                                        القانونية الخاصة بالمتجر والقواعد القانونية المحلية والدولية
                                                        ويعتبر أي زائر للموقع زبوناً طالما أنه لم يباشر بشراء أي منتج من
                                                        المتجر الالكتروني(الموقع) وليس للزبائن على المتجر الالكتروني أي
                                                        إلزام قانوني تبعاً لعدم انعقاد أي علاقة تعاقدية بينهم.

                                                        2.4. المشتري: كل شخص طبيعي أو اعتباري يتسوق من المتجر الالكتروني
                                                        أي من المنتجات و/أو المواد المعروضة عليه وفقاً للأحكام والشروط و
                                                        البروتوكولات الخاصة بالمتجر الالكتروني (الموقع) ، ويقر الزبون
                                                        بأن رغبته بتسوق أي منتج من المتجر الالكتروني (الموقع) عن طريق
                                                        ضغطه على المنتج لتحويله إلى سلة المشتريات يعتبر إقراراً صريحاً
                                                        منه باطلاعه وقبوله لكافة الشروط و الإحكام القانونية و
                                                        البروتوكولات الخاصة بالمتجر الالكتروني (الموقع) ويعتبر استكمال
                                                        الزبون لقائمة البيانات الشخصية الخاصة به و تحديد المنتجات أو
                                                        المواد التي يرغب بتسوقها و إرسال طلب إلى المتجر بهذه المواد
                                                        إيجاباً مقدم للمتجر الالكتروني برغبة الشراء ما يعني تعذر العودة
                                                        عن هذا الإيجاب إن اقترن بقبول المتجر الالكتروني والذي يثبت
                                                        بإرسال رسالة من المتجر الالكتروني تفيد بتثبيت هذه المنتجات على
                                                        اسم الزبون و لصالحه وعند وصول هذه الرسالة يكون الزبون قد حمل صفة
                                                        المشتري وحقوقه و يكون التعاقد قد تم بين المشتري والمتجر
                                                        الالكتروني.

                                                        2.5. كلمات السر: وهي الكلمات والرموز الأمنية الخاصة (كاسم
                                                        المستخدم أو كلمة السر أو أية معلومة أخرى) التي قد تمنحها إدارة
                                                        المتجر الالكتروني (الموقع) لبعض أو كل الشركات (الموكلين) و/أو
                                                        المشترين اختيارياً و/أو إلزامياً والتي يتم تزويدها كجزء من
                                                        الإجراء الأمنية للمتجر الالكتروني (الموقع)، ويجب في هذه الحالة
                                                        على الشخص الطبيعي أو الاعتباري الذي يحصل على هذه الكلمات والرموز
                                                        الأمنية التعامل معها بسرية تامة، حيث يتحمل وحده المسؤولية عن
                                                        الحفاظ على المعلومات والرموز الأمنية الخاصة به. ولا يجوز له
                                                        الإفصاح عن كلمة السر للغير (بخلاف الأطراف الأخرى المفوضة من طرف
                                                        المتجر الالكتروني (الموقع) لاستخدام حسابه حسب شروط الموقع هذه)،
                                                        كما يتحمل الشخص الذي زود بهذه الكلمات والرموز الأمنية من المتجر
                                                        الالكتروني (الموقع) المسؤولية وحده عن أي استخدام أو عمل متخذ
                                                        بموجب كلمة السر الخاصة به. وفي حالة اختراق كلمة السر الخاصة به،
                                                        يجب عليه تغييرها على الفور. ويحق للمتجر الالكتروني (الموقع)
                                                        إيقاف أي اسم تعريف أو كلمة سر، سواء تم اختيارها من طرف الشركة
                                                        (الموكل) و/أو الزبون، أو قام المتجر الالكتروني (الموقع)
                                                        بتخصيصها، في أي وقت إذا ارتأى المتجر الالكتروني (الموقع) حسب
                                                        رأيه المعقول أن الشخص الطبيعي أو الاعتباري (الشركة (الموكل) و
                                                        /أو الزبون) قد أخفق في الالتزام بأي من شروط المتجر الالكتروني
                                                        (الموقع) هذه.

                                                        {"\n"} 3. التزامات الشركات (الموكلين):

                                                        3.1. موافاة المتجر الالكتروني (الموقع) بالوثائق القانونية
                                                        الأساسية السارية والأصلية ومنها صورة مصدقة عن السجل التجاري
                                                        الخاص بالشركة والمفوض بالتوقيع عنها وقائمة بالبضائع المراد
                                                        تسويقها بواسطة الوكيل المتجر الالكتروني (الموقع) ويتعين أن يكون
                                                        لدى الشركة القدرة على ممارسة الأعمال في الدولة المختارة حسب
                                                        القوانين المعمول بها في الجمهورية العربية السورية والتقييد
                                                        بقوانينها.

                                                        3.2. ضمان عدم تسبب منتجاتها في حدوث أية إصابة شخصية أو تلف
                                                        بالممتلكات وتامين العناية الخاصة التي قد تتطلب المنتجات التي
                                                        ممكن أن تمثل خطراً بما في ذلك على سبيل المثال لا الحصر (صحة
                                                        وسلامة الأفراد) إلى ضمانات إضافية تعطى للمتجر الالكتروني
                                                        (الموقع) من أجل السماح بالبيع وقد تنطبق قوانين معينة على بيع هذا
                                                        النوع من المنتجات وتقع مسؤولية الالتزام بهذه القوانين على عاتق
                                                        الشركة (الموكل) وتتحمل بمفردها مسؤولية و /أو تكاليف أي التزامات
                                                        نتيجة عدم إتباع المتطلبات الصحية ومتطلبات السلامة ذات الصلة و/أو
                                                        بيع المنتجات التي تمثل خطورة على الأفراد أو ممتلكات الأشخاص.
                                                        وتعد الشركة (الموكل) وحدها المسؤولة عن أي من منتجاتها التي يتم
                                                        طلبها أو توزيعها عبر الموقع ويحتفظ المتجر الالكتروني (الموقع)
                                                        بالحق في رفض المنتجات التي تصنف ضمن قائمة البنود المحظورة لديه
                                                        وتعديلاتها من حين لآخر. ويجب أن تلتزم المنتجات المعروضة للبيع
                                                        على المتجر الالكتروني (الموقع) بجميع القوانين واللوائح وسياسات
                                                        وبروتوكولات التي يتم تحديدها في وتحديثها من حين لآخر. ويمنع
                                                        منعاً باتاً بيع منتجات غير قانونية أو غير آمنة أو غير ذلك من
                                                        المنتجات المحظورة من الجهات المختصة، بما في ذلك المنتجات التي
                                                        تتاح فقط بموجب وصفة طبية أو رخصة معينة أو غير ذلك من الشروط
                                                        والقيود الخاصة للبيع. وينبغي قبل إدراج أو طلب إدراج المنتجات إلى
                                                        المتجر الالكتروني (الموقع) القيام بمراجعة دقيقة لصفحات الإرشادات
                                                        ذات الصلة ، ويستثنى من هذا الحالات التي تفترض عناية الشخص المعني
                                                        علم الشركة (الموكل) المسبق بضرورة وجود ضمانات معينة عند بيع هذه
                                                        المنتجات أو عدم جوز بيعها وتسويقها للعموم إلا بموجب كتاب إضافي
                                                        مرفق (كالوصفة الطبية أو ما شابه) أو عدم قانونية بيع هذه المنتجات
                                                        أصلاً وفي هذه الحالة تتحمل الشركة (الموكل) المسؤولية كاملة عن
                                                        طلب أو عرض هذه المنتجات على المتجر الالكتروني (الموقع) سواء ذكرت
                                                        هذه المنتجات بالإرشادات ذات الصلة أم لن تذكر وذلك كون الأمثلة
                                                        الواردة في صفحات المساعدة ليست أمثلة شاملة، بل تم تقديمها فقط
                                                        كنوع من المعلومات الإرشادية.

                                                        3.3. تقديم الضمانات المطلوبة والصيانة الخاصة بالمنتجات التي تم
                                                        بيعها بواسطة الوكيل التجاري ومتجره الالكتروني(الموقع) والاستمرار
                                                        في تحمل مسؤولية خدمات ما بعد البيع والكفالات والضمانات والصيانة
                                                        وأي عيوب قد تظهر فيما يتعلق بمنتجاتكم، حسب القانون المعمول به
                                                        حيث لا يجوز لهم أن يطلبوا من المتجر الالكتروني (الموقع) القيام
                                                        بذلك نيابة عنهم.

                                                        3.4. تعبئة كل منتج من المنتجات المباعة عبر الوكيل التجاري أو
                                                        متجره الالكتروني بصورة تجارية معقولة بحيث تتوافق مع جميع متطلبات
                                                        التعبئة.

                                                        3.5. تأمين المنتجات للوكيل التجاري خلال يومين من تاريخ طلب
                                                        المشتري للمنتج وفي حال كان المنتج مصنف ضمن فئة المنتجات التي يجب
                                                        تسليمها للمشتري خلال مدة أقصاها ثلاثة أيام عمل من تاريخ تنظيم
                                                        الطلبية ذات الصلة من قبل المشتري فان الموكل يقر بوجوب تسليم هذه
                                                        البضاعة للوكيل التجاري خلال 24 ساعة من تاريخ عمل الطلبية.

                                                        3.6. يلتزم الموكل بتأمين منتج بمقاس أكبر وأصغر أن كانت الطلبية
                                                        تتعلق بمنتج له عدة مقاسات ويكون التزام الموكل هذا محدد لمدة لا
                                                        تتجاوز 7 أيام عمل.

                                                        3.7. يلتزم الموكل بتعويض الوكيل التجاري عن أي خسارة متحملة أو
                                                        متكبدة وذلك في الأحوال التالية:

                                                        3.7.1. تأخر الموكل عن تسليم المنتج في الوقت المحدد للتسليم.

                                                        3.7.2. إلغاء طلب مؤكد للمشتري.

                                                        3.7.3. أي ضرر أو خسارة ناتجة عن خطأ الموكل.

                                                        3.8. يلتزم الموكل بإعادة أي منتج تم تسويقه عبر الوكيل التجاري أو
                                                        متجره الالكتروني (الموقع) وذلك خلال مدة أقصاها عشرة أيام عمل إن
                                                        كانت الإعادة ناتجة عن رفض الزبون استلام المنتج أو عيب أو نقص
                                                        سببه الموكل وتقر الشركات (الموكلين) بأن الموكل ومتجره الالكتروني
                                                        (الموقع) لا تمتلك الملكية أو ملكية المنتجات أو مخزون المنتجات في
                                                        أية مرحلة من المراحل بحكم تقديمنا للخدمات الخاصة بنا حسب شروط
                                                        المتجر الالكتروني (الموقع) هذه. حيث تظل المنتجات مملوكة للبائع
                                                        حتى إجراء وإتمام عملية التسليم بنجاح إلى المشتري أو إعادتها إلى
                                                        الشركة (الموكل).

                                                        3.9. إعلام الموكل بكمية البضاعة المتوفرة من كل منتج ومواصفاته
                                                        بشكل دقيق ولا يحق للوكيل تعديل هذه الكميات إلا بعد إعلام الموكل
                                                        المسبق والحصول على موافقته بهذا الخصوص.

                                                        3.10. مطابقة المنتجات إلى المواصفات المصرح عنها وللمواصفات
                                                        والصور المقدمة للوكيل التجاري، ويحظر على الشركات (الموكلين) وضع
                                                        أي إعلان أو معلومات مضللة عن منتجاتهم أو عن أية علامة تجارية على
                                                        المتجر الالكتروني (الموقع). وفي حالة اكتشاف مخالفتهم لشروط
                                                        المتجر الالكتروني (الموقع) هذه، يجب عليهم قبول إرجاع المنتجات
                                                        وتحمل المسؤولية أمام المتجر الالكتروني(الموقع).

                                                        3.11. إعلام الوكيل التجاري بجميع التعليمات التي تحددها الجهة
                                                        المصنعة، الموزع، و/أو جهة ترخيص المنتج، إن وجدت، والتي تحدد
                                                        التاريخ الذي لا يجوز قبله إتاحة المعلومات المحددة عن ذلك المنتج
                                                        (مثال: عنوان كتاب)، أو الذي لا يجوز قبله تسليم المنتج أو خلافاً
                                                        لذلك إتاحته للزبائن.

                                                        3.12. إرفاق بطاقة تعبئة مخصصة للطلبية، وحصر تسليم المنتج بموظف
                                                        الوكيل المختص والذي يحمل كتاب رسمي ممهور بختم الوكيل التجاري
                                                        بالإضافة لتسليمه أي فواتير ضريبية، في كل عملية شحن لمنتجاتكم.

                                                        3.13. يحظر على الشركات (الموكلين)التسجيل باسم مستعار و/أو انتحال
                                                        بيانات اعتماد تسجيل الدخول الخاصة بأي بائع آخر أو كلمة (كلمات)
                                                        السر الخاصة به وتتحمل الشركة (الموكل) كامل المسؤولية المدنية
                                                        والجزائية عن هذا السلوك الاحتيالي وعن أي مخالفة للقوانين
                                                        واللوائح الوطنية أو الإقليمية أو الدولية المعمول به) كما وتقر
                                                        الشركات (الموكلين)بأن تسجيلها لحساب بائع خاص بها على المتجر
                                                        الالكتروني (الموقع) و/أو قبول عرض منتجاتها أو المواد والمنتجات
                                                        التابعة لها عليه يعني إقراراً صريح منها بقبول جميع شروط المتجر
                                                        الالكتروني (الموقع) وأحكامه والبروتوكولات الخاصة به.

                                                        3.14. باستثناء ما تسمح به شروط الموقع هذه صراحة، الالتزام بعدم
                                                        الاتصال بالعملاء (سواء عن طريق الهاتف أو البريد الالكتروني أو
                                                        غير ذلك من وسائل الاتصال) لتأكيد الطلبيات أو لتنفيذ منتجاتكم أو
                                                        لأي سبب آخر.

                                                        3.15. تقر الشركة (الموكل) بمسؤوليته عن سداد الضرائب المرتبطة
                                                        بالمنتجات (بما في ذلك أية فوائد أو غرامات تفرضها أية سلطة مختصة
                                                        بسبب تأخر أو عدم سداد تلك الضرائب، وذلك حسب القوانين المعمول بها
                                                        في الجمهورية العربية السورية أو الدولة المختارة في كل حالة) بما
                                                        في ذلك دون قيد، ضريبة القيمة المضافة، وضريبة المبيعات، وغير ذلك
                                                        من الضرائب المماثلة على المعاملات، وضرائب الإنتاج والعائدات
                                                        الإجمالية والضرائب غير المباشرة.

                                                        3.16. تقر الشركات (الموكلين) بأن البيع من خلال المتجر الالكتروني
                                                        (الموقع) يتم حصراً بالليرة السورية وليس لهم المطالبة بأي تعديل
                                                        بخصوص هذه المادة، ما يعني انعكاس ذلك على التعامل النقدي بين
                                                        الوكيل والشركات(الموكل).

                                                        {"\n"} 4. سياسات المتجر الإلكتروني (الموقع):

                                                        4.1. الملكية الفكرية:

                                                        ويعد طلب الشركة أو الوكيل عرض أي منتج على المتجر الالكتروني
                                                        (الموقع) هو ترخيص دائم وشامل وغير حصري وعلى مسؤولية الشركة
                                                        (الموكل) باستعمال ونسخ وتوزيع وتعديل والإفصاح إلى الغير عن أي
                                                        محتوى أو علامات تجارية أو مواد أو صور تتعلق بالمنتج المراد عرضه
                                                        على المتجر الالكتروني (الموقع) وتضمن الشركة (الموكل) أنه يحق له
                                                        منح ذلك الترخيص.

                                                        وفي حالة قيام البائع أو مالك علامة تجارية من الغير بإخطار المتجر
                                                        الالكتروني (الموقع) أو من خلال إحدى المحاكم أو أمر إداري أو
                                                        حكومي بأن المنتجات أو أي جزء منها ينتهك حقوق الملكية الفكرية
                                                        طبقاً للقوانين المعمول بها في الدولة المختارة (ومنها على سبيل
                                                        المثال لا الحصر)

                                                        4.1.1. حقوق النشر أو التأليف، وبراءات الاختراع وحقوق قواعد
                                                        البيانات والحقوق في العلامات التجارية، والتصميمات، والمعرفة
                                                        الفنية، والمعلومات السرية (سواء المسجلة أو غير المسجلة)

                                                        4.1.2. طلبات التسجيل، وحق التقدم بالتسجيل، الخاص بأي من تلك
                                                        الحقوق.

                                                        4.1.3. جميع حقوق الملكية الفكرية الأخرى، وأشكال الحماية المساوية
                                                        أو المماثلة الموجودة في أي مكان آخر في العالم) أو أي حقوق أخرى
                                                        للغير أو بموجب أي قوانين معمول بها في الدولة المختارة

                                                        فإن للمتجر الالكتروني (الموقع) الحق بإزالة تلك المنتجات من
                                                        الموقع و / أو تقديم الشركة (الموكل) طلب إزالة تلك المنتجات وذلك
                                                        وفقاً لتقدير إدارة المتجر الالكتروني وحده بالإضافة إلى ذلك فإن
                                                        المتجر الالكتروني (الموقع) غير ملزم بإعادة أي معلومات أو مواد أو
                                                        مستندات إليكم، سواء قبل أو بعد إنهاء شروط الموقع هذه أو إلغاء
                                                        حساب البائع. وتعتبر هذه المعلومات أو المواد أو المستندات ملكاً
                                                        خالصاً وخاصاً بالمتجر الالكتروني (الموقع).

                                                        وتقر الشركات (الموكلين) بأن استخدامهم لبعض المعلومات السرية التي
                                                        لا تظهر للزبائن أو المشترين لا يعني تملكهم لأي من هذه المعلومات
                                                        و تبقى هذه المعلومات ملكية حصرية للمتجر الالكتروني (الوقع) حالها
                                                        كحال كل المعلومات أو البيانات الخاصة بالمتجر الالكتروني أو
                                                        أعماله و لا يحق للشركات (الموكلين) إلا استخدام القدر المتاح من
                                                        هذه المعلومات من المتجر الالكتروني(الموقع) دون جواز استعمالها أو
                                                        تسريبها للغير بأي شكل من الأشكال إلا بناء على إجازة خطية تصدر من
                                                        الجهة التي تفوضها إدارة المتجر الالكتروني بهذا التصريح.

                                                        4.2. التغييرات والتحديثات والترقيات:

                                                        يحق لإدارة المتجر الالكتروني (الموقع) في أي وقت وحسب تقديرها
                                                        المطلق أن تغيير وتحدث وترقي الموقع أو البروتوكولات الناظمة لعمله
                                                        وتعتبر هذه الأعمال سارية المفعول من تاريخ نشر التعديلات على
                                                        الموقع و/أو إرسال إخطار للشركة (الموكل) بما في ذلك وسائل الاتصال
                                                        الالكترونية، ويعتبر استخدام الشركة (الموكل) المستمر للموقع (وهو
                                                        ما يظهر من خلال دخولها إلى الحساب الخاص بها) وخدماتنا عقب نشر أي
                                                        تغييرات، إخطارات و/أو طلب الضغط للموافقة على الاستمرار، بمثابة
                                                        قبول من طرفها لتلك التغييرات.

                                                        ويتوجب على الشركة (الموكل) في حالة عدم قبولها لأي من تلك
                                                        التغييرات التي تتم على شروط الموقع هذه عدم الاستمرار في استعمال
                                                        الموقع أو خدماته.. وتقر الشركات (الموكلين) بأنه يجوز لإدارة
                                                        المتجر الالكتروني (الموقع) من وقت لآخر ترقية الخصائص الموجودة
                                                        فيه و/أو بعض الجوانب في خدماته، ويجوز لإدارة المتجر الالكتروني
                                                        (الموقع) أن تغيير من وقت لآخر أي من خدماتها بالقدر اللازم بغرض
                                                        الالتزام بأي تغيير يطرأ على القوانين المعمول بها.

                                                        4.3. مسؤولية المتجر الالكتروني:

                                                        إن إعلان الشركة أو الموكل على المتجر الالكتروني(الموقع) يعتبر
                                                        تصريحاً منه بأنه اطلع على الخصائص الموجودة في المتجر الالكتروني
                                                        وقبل بها كما هي وبوضعها سواء أكانت تتوافق مع متطلباته أم لا و/أو
                                                        سواء أكانت موجودة في الوقت المناسب و آمنة ولا تحتوي أي من
                                                        الأخطاء أم لا ما يعني قبولها [الشركة (الموكل)] بهذه الخصائص كما
                                                        هي واعدم جواز تحميل المتجر الالكتروني(الموقع) أو إدارته أو
                                                        مالكيه لأي مسؤولية تبعاً لهذه الخصائص وطبيعتها ويخلي المتجر
                                                        الالكتروني(الموقع) ومالكيه و إدارته مسؤوليتهم من أي أضرار أو
                                                        أخطاء تنجم عن أعطال الكترونية ما لم تكن ناتجة عن خطأ مهني جسيم
                                                        في النظام الأساسي للمتجر أما ما عدى ذلك من أعطال النظام وغيرها
                                                        من الانقطاعات التي قد تؤثر على البيع بواسطة المتجر
                                                        الالكتروني(الموقع) فلا يعتد بها و لا يمكن مسائلة المتجر
                                                        الالكتروني أو إدارته أو مالكيه بأي مسائلة بهذا الخصوص.

                                                        ولا يجوز بأي من الأحوال أن تتجاوز قيمة التعويض المطلوب من المتجر
                                                        الالكتروني (الموقع) أو إدارته أو مالكيه لقيمة العمولة التي يتحصل
                                                        عليها المتجر الالكتروني من الشركة (الموكل) وذلك على حساب القطعة
                                                        المفردة.

                                                        4.4. الإجراءات الاحترازي:

                                                        يحق للمتجر الالكتروني أن يعمد إلى إبلاغ الجهات المختصة عن أي
                                                        نشاط مشبوه يتم وذلك وفقاً للأصول القانونية المتبعة في الجمهورية
                                                        العربية السورية ، كما يجوز للمتجر الالكتروني (الموقع) تعليق
                                                        تنفيذ خدماته أو إمكانية الدخول إلى حساب الشركة (الموكل) دون أن
                                                        يرتب ذلك أية مسؤولية عليها في حال تم تحديد خطر شخصي أو مالي أو
                                                        قانوني فعلي أو محتمل (ومن ذلك على سبيل المثال لا الحصر:مخالفة
                                                        البائع لشروط المتجر الالكتروني (الموقع) أو أي من سياساته و / أو
                                                        عندما ترى إدارة المتجر الالكتروني (الموقع) و بشكل معقول أن
                                                        استمرارها في تقديم أي من خدماتها سوف يترتب عليه خطورة أمنية و /
                                                        أو مادية و/أو إجراء تنظيمي.

                                                        {"\n"} 5. القانون الناظم وطرق حل النزاعات:

                                                        يعتبر القانون السوري هو القانون الناظم للعلاقات في المتجر
                                                        الالكتروني(الموقع) وتسري قواعد هذا القانون على التعاملات الخاصة
                                                        والعامة فيه إلا ما استثني بنص خاص. وتحل الخلافات التي تنشئ بين
                                                        المتجر الالكتروني والغير المتعاقد معهم عن طريق الحل الودي وذلك
                                                        خلال عشرة أيام من تاريخ تقديم الشكوى أو الاعتراض الخطي من أي طرف
                                                        للطرف الآخر فإن تعسر ذلك يتم اللجوء إلى التحكيم خلال 10 أيام من
                                                        تاريخ تعذر الوصول للحل الودي ، على أن يتم التحكيم بواسطة محكم
                                                        مفرد يتفق عليه المحتكمين وفي حال تعذر ذلك يلجئ المحتكمين إلى
                                                        هيئة تحكيم ثلاثية تعين من قبلهم فيكون لكل محتكم أن يسمي محكمه
                                                        وتكون تسمية المحكم المرجح للمحكمين اللذان عينا من قبل المحتكمين
                                                        وفي حال عدم اتفاقهما على محكم مرجح يتم اللجوء إلى المحكمة
                                                        المختصة وفق أحكام القانون الناظم للتحكيم في الجمهورية العربية
                                                        السورية لتعين المحكم المرجح ، على أن يلتزم المحكمين بإصدار حكمهم
                                                        خلال مدة شهر من تسليمهم المهمة التحكيمية و ذلك وفقاً لأحكام
                                                        القانون الناظم للتحكيم في الجمهورية العربية السورية و وفقاً
                                                        لأحكام قانون الموضوع و الإجراءات التي يحدده المحتكمين أو
                                                        المحكمين وفق الأصول القانونية ، ويعتبر الحكم الصادر عن المحكمين
                                                        قطعياً ويلتزم المحتكمين بتطبيقه وقفاً للأصول القانونية ومبدأ حسن
                                                        النية.

                                                        {"\n"} 6. تجزئة البطلان:

                                                        يعتبر بطلان أي بند أو فقرة من أحكام هذه الشروط و/أو أي من
                                                        البروتوكولات و/أو أي من العقود و/أو الاتفاقيات الموقعة بين
                                                        المتجر الالكتروني والغير بطلاناً مقصوراً على الفقرة التي انطوت
                                                        على البطلان دون أن يتعداه لغيره من الفقرات أو الشروط وتعتبر
                                                        إلزامية باقي الفقرات كما هي دون أن ينتقص أي من أثارها بهذا
                                                        البطلان.

                                                        {"\n"} 7. التبليغات:

                                                        يعتبر العنوان …….هو العنوان المختار لأي تبليغ يخص المتجر
                                                        الالكتروني، كما يعتبر العنوان المصرح فيه كعنوان مختار للتبليغ
                                                        بالنسبة للشركات (الموكلين) و / أو الزبائن عنواناً للتبليغ يعطي
                                                        التبليغ عليه كافة الآثار القانونية.

                                                    </Text>
                                                </View>
                                                :

                                                <View style={styles.CenterContent}>


                                                    <Text style={styles.TextAbout}>

                                                        {"\n"} Introduction:

                                                        {"\n"} Dear Visitors, We welcome you to the online store
                                                        “ETLOOB” and we provide you with the terms and conditions
                                                        related to your use and access to our store, all pages, links,
                                                        tools and the branched properties. Your use of the “ETLOOB ”
                                                        Online store is your acceptance of the terms and conditions of
                                                        this agreement which includes all the details below and is an
                                                        affirmation of your obligation to respond to the content of this
                                                        agreement for our online store.

                                                        1. This Introduction is an integral part of this Agreement and
                                                        is integrated
                                                        https://accounts.google.com/SignOutOptions?hl=en-GB&continue=https://docs.google.com/document/u/0/d/1w1L-mT8IZwIT_1i81U73mOTP9-347rFY-vfoiqsafNI/edit&service=writely
                                                        and complies with all its terms and conditions.

                                                        2. Terms:

                                                        The terms defined or written in bold shall have the meanings set
                                                        out in this Article.

                                                        2.1 The online store (the website): is the website installed on
                                                        the Internet in the name of “ETLOOB” and owned by the commercial
                                                        agent alone, the competent in marketing and selling of products
                                                        in accordance with the terms and conditions displayed and
                                                        installed in the website.

                                                        2.2 The Companies (Clients): Companies that offer products or
                                                        materials to display them on their behalf, market them and sell
                                                        them on the online store (Website) for a specific commission.
                                                        These Companies (Clients) are subject to the normal commercial
                                                        agency, whether they appear in their own name or in the name of
                                                        their subsidiaries or in any capacity.

                                                        2.3 Customers: Any natural or legal person seeking to market the
                                                        products displayed on the online store (The Website) according
                                                        to the legal rules and conditions of the store and the local and
                                                        international legal rules. Any visitor to the Website shall be
                                                        considered a customer as long as he has not started to purchase
                                                        any product from the online store, The online store doesn’t have
                                                        any legal obligation towards the customers as a result of the
                                                        absence of any contractual relationship between them.

                                                        2.4 Purchaser : Any natural or legal person who purchases from
                                                        the online store any of the products and / or materials offered
                                                        to him in accordance with the terms and conditions and protocols
                                                        of the online store (the website). The customer acknowledges
                                                        that he wishes to purchase any product from the online store by
                                                        clicking on the product to transfer it to the shopping basket,
                                                        is an explicit acknowledgment of his and acceptance of all the
                                                        conditions, legal provisions and protocols of the online store
                                                        (the website). The completion of the customer’s personal data
                                                        list and identifying the products or materials that he wishes to
                                                        purchase and sending a request to the store by these materials
                                                        is considered as an advanced affirmation for purchasing wish,
                                                        which is proved by sending a message from the online store
                                                        confirming the installation of these products under the
                                                        customer’s name and in his favor. Upon the arrival of this
                                                        message, the customer has acquired the purchaser’s status and
                                                        rights. Then, Contracting is completed between purchaser and the
                                                        online store.

                                                        2.5 Passwords: These are passwords and special security symbols
                                                        (such as user name, password or any other information) which may
                                                        be given by the online store management (the website) to some or
                                                        all of the companies (the clients) and / or purchasers
                                                        optionally and / or compulsively provided as a part of the
                                                        security procedures. In this case, the natural or legal person
                                                        who obtains these words and security symbols must deal with them
                                                        in strict confidence. He is solely responsible for maintaining
                                                        the information and his security symbols. He may not disclose
                                                        the password to third parties. (Other than the other parties
                                                        authorized by online store to use his account) .The person who
                                                        was provided by these words and security symbols from the online
                                                        store (the website) is solely responsible for any use or action
                                                        taken under his password, and in case of hacking of his
                                                        password, He must change it immediately. The online store may
                                                        stop any identifying name or password, whether chosen by the
                                                        company (the client) and / or the customer, or the online store
                                                        allocates in its reasonable opinion at any time if the online
                                                        store (Website) that the natural or legal person (the company
                                                        (the client) and / or the customer) has failed to comply with
                                                        any of the conditions of this online store (Website).

                                                        {"\n"} 3. Corporate Obligations:

                                                        3.1 Providing the online store by the basic and valid legal
                                                        documents including a certified copy of the trade registry of
                                                        the company and the authorized signatory, and a list of the
                                                        goods to be marketed by the online store agent (The Website).
                                                        The company must have the ability to do business in the selected
                                                        country according to the applicable laws In the Syrian Arab
                                                        Republic and the restriction of its laws.

                                                        3.2 Ensure that their products do not cause any personal injury
                                                        or damage to properties and provide special care that may
                                                        require products that may pose a risk, including but not limited
                                                        to (individual health and safety) additional guarantees given to
                                                        the online store (the website) to allow the sale. Certain laws
                                                        may apply to the sale of this type of products. Responsibility
                                                        for compliance with these laws rests with the Company (the
                                                        client) and assumes sole responsibility and / or costs for any
                                                        liability as a result of non-compliance with health requirements
                                                        and related safety requirements and / or selling products that
                                                        pose a risk to individuals or Property of persons. The Company
                                                        (the client) is solely responsible for any of its products
                                                        ordered or distributed through the Website and the Online Store
                                                        reserves the right to refuse products that are classified as
                                                        prohibited and amended from time to time. Products offered for
                                                        sale on the Online Store (the website) shall comply with all
                                                        laws, regulations, policies and protocols that are specified in
                                                        and updated from time to time. It is strictly prohibited to sell
                                                        illegal, unsafe or other prohibited products from the competent
                                                        authorities, including products that are only available under a
                                                        prescription, certain license or other special conditions and
                                                        restrictions for sale. Before the inclusion or requesting for
                                                        inclusion of products in the online store (the website), a
                                                        thorough review of the relevant guidelines pages should be made.
                                                        Excludes those cases where the care of the person concerned
                                                        prescribes the company’s (the client) prior knowledge of the
                                                        necessity of certain safeguards when selling these products or
                                                        not selling them and marketing them to the public (Such as
                                                        medical prescription or similar) or the illegal sale of such
                                                        products. In this case, the company (the client) shall bear full
                                                        responsibility for the request or offer of these products on the
                                                        online store (the website) whether or not these products are
                                                        mentioned in the relevant guidelines. Help pages are not
                                                        exhaustive examples, but presented only as a kind of guidance
                                                        information.

                                                        3.3Provide the warranties required and maintenance of the
                                                        products sold by the commercial agent and its online store (the
                                                        website) and continue to take responsibility for the after-sale
                                                        services, warranties, guarantees and maintenance and any defects
                                                        that may appear in relation to your products, according to
                                                        applicable law where they may not ask the online store (The
                                                        website) to do so on their behalf.

                                                        3.4 Fill each product sold through a commercial agent or store
                                                        in a commercially reasonable manner to meet all packaging
                                                        requirements.

                                                        3.5 Securing of products of the commercial agent within two days
                                                        from the date of the purchaser’s request for the product. If the
                                                        product is classified in the category of products that must be
                                                        delivered to the purchaser within a maximum of three working
                                                        days from the date of organizing the relevant order by the
                                                        purchaser, the client acknowledges that the goods must be
                                                        delivered to the commercial agent within 24 hours From the date
                                                        of the order.

                                                        3.6 The customer is obliged to insure a product of a larger and
                                                        smaller size if the order relates to a product with multiple
                                                        sizes and the commitment of this client is limited to a period
                                                        not exceeding 7 working days.

                                                        3.7 The Client shall indemnify the Commercial Agent for any loss
                                                        incurred or in the following cases:

                                                        3.7.1. Delaying delivery of the product on time for delivery.

                                                        3.7.2. Canceling a confirmed order for the purchaser.

                                                        3.7.3. Any damage or loss resulting from the fault of the
                                                        client.

                                                        3.8 The client is obliged to return any product marketed through
                                                        the commercial agent or his online store (the website) within a
                                                        maximum period of ten working days if the return is due to the
                                                        customer’s refusal to receive the product or a defect or
                                                        shortage caused by the client. (the clients) admit that (the
                                                        website) do not have ownership or own the products or the
                                                        inventory of the products at any stage by virtue of providing
                                                        our own services in accordance with the terms of the online
                                                        store (the website). Where the products remain the property of
                                                        the seller until the delivery is successfully completed and
                                                        completed by the purchaser or returned to the company (the
                                                        client).

                                                        3.9 Informing the client of the quantity of goods available from
                                                        each product and its specifications accurately. The agent shall
                                                        not be entitled to amend these quantities until after informing
                                                        the customer in advance and obtaining his consent in this
                                                        regard.

                                                        3.10 The products are identical to the declared specifications,
                                                        specifications and images provided to the commercial agent, and
                                                        the companies (the clients) are prohibited from placing any
                                                        misleading advertising or information about their products or
                                                        any trademark on the online store (the website). If they are
                                                        found to be in breach of the terms of this online store, they
                                                        must accept the return of the products and take responsibility
                                                        for the online store (the website).

                                                        3.11 Notify the commercial agent of all instructions given by
                                                        the manufacturer, distributor, and / or product licensing
                                                        authority, if any, specifying the date on which the specific
                                                        information about that product (Eg. title of a book) may not be
                                                        made available, Otherwise make it available to customers.

                                                        3.12 Attach a special packing card to the order, and limit the
                                                        delivery of the product to the agent of the competent agent, who
                                                        holds an official letter stamped with the stamp of the
                                                        commercial agent in addition to delivery of any tax invoices, in
                                                        each shipment of your products.

                                                        3.13 Companies (the clients) are prohibited from registering
                                                        with a false name and / or impersonating any other vendor’s
                                                        credentials or passwords. The Company assumes full civil and
                                                        criminal liability for such fraudulent behavior and for any
                                                        violation of national or regional laws and regulations. (the
                                                        clients) of the online store (the website) recognizes that
                                                        registering to offer their products or products and materials
                                                        that belong to them (the clients) shall mean an express
                                                        acknowledgment of acceptance of all the conditions of the online
                                                        store (the website) and its provisions and the special protocols
                                                        with it.

                                                        3.14 Except as expressly permitted by These terms of this
                                                        website, the obligation to not contact the costumer (Through the
                                                        telephone , E-mail or other means of) To confirm your order or
                                                        performances of your products or for any other cause.

                                                        3.15 The Company shall be liable for the payment of taxes
                                                        associated with the Products (including any interest or
                                                        penalties imposed by any competent authority due to delay or
                                                        non-payment of such taxes, in accordance with the laws of the
                                                        Syrian Arab Republic or the State selected in each case) Tax,
                                                        sales tax, and other similar taxes on transactions, production
                                                        taxes, gross proceeds and indirect taxes.

                                                        3.16 The companies (the clients) acknowledge that the sale
                                                        through the online store (the website) is exclusively in Syrian
                                                        pounds and they do not have to demand any amendment regarding
                                                        this article, which means that this reflects the cash
                                                        transaction between the agent and the company.

                                                        {"\n"} 4. Online Store Policies (The Website):

                                                        4.1. Intellectual property:

                                                        The request of the company or the agent to display any product
                                                        on the online store (the website) is a permanent, comprehensive
                                                        and non-exclusive license and the responsibility of the company
                                                        (the client) to use, copy, distribute, modify and disclose to
                                                        third parties any content, trademarks, materials or images
                                                        related to the product to be displayed on the online store (The
                                                        website) and the company (the client) guarantees that it is
                                                        entitled to grant such license.
                                                        In the event that the seller or the owner of a trademark from
                                                        third parties notify the online store (website) or through a
                                                        court or administrative or governmental order that the products
                                                        or any part thereof infringes the intellectual property rights
                                                        in accordance with the laws in force in the selected country )

                                                        4.1.1. Copyright or authorship, patents, database rights,
                                                        trademark rights, designs, technical knowledge, confidential
                                                        information (both registered and unregistered)

                                                        4.1.2. Applications for registration, and the right to register,
                                                        for any such rights.

                                                        4.1.3, All other intellectual property rights, equal or similar
                                                        protections found elsewhere in the world) or any other rights of
                                                        third parties or under any laws applicable in the selected State

                                                        The online store (the website) shall have the right to remove
                                                        such products from the website and / or to provide the company
                                                        (the client) with the request to remove those products at the
                                                        discretion of the department of the online store alone. In
                                                        addition, the online store does not have to return any
                                                        information, materials or documents to you , Either before or
                                                        after termination of these Website Terms or cancellation of the
                                                        Seller’s account.
                                                        Such information, materials or documents shall be deemed to
                                                        belong exclusively to the online store (the website).

                                                        The companies acknowledge that their use of certain confidential
                                                        information that does not appear to customers or purchasers does
                                                        not mean their ownership of any such information. This
                                                        information remains the exclusive property of the online store
                                                        (the Website), as is the case with all information or data
                                                        related to the online store or its business. The use of the
                                                        information available from the online store without the
                                                        possibility of using it or leaking it to others in any way
                                                        except on the basis of a written leave issued by the authority
                                                        authorized by the department of the online store.

                                                        4.2 Changes, updates, and upgrades:

                                                        The management of the online store (the website) have the right
                                                        at any time, at its absolute discretion, change, update and
                                                        upgrade the website or the protocols governing its work. Such
                                                        works shall be valid from the date of publication of amendments
                                                        to the website and / or sending notice to the company (the
                                                        client) including the means of electronic communication. The
                                                        continued use of the Website by the Company (the client) (as
                                                        evidenced by its access to its Account) and our Services
                                                        following the posting of any changes, notifications and / or
                                                        request for continued approval, shall constitute acceptance by
                                                        such party of such changes.

                                                        The Company (the Client), in the event that it does not accept
                                                        any of the changes made to these Terms of the Website, shall not
                                                        continue to use the Website or its services. (the website) may
                                                        be changed from time to time by any of its services to the
                                                        extent necessary to comply with any change in applicable laws.

                                                        4.3 Online Store Responsibility:

                                                        The advertisement of the company or the client on the online
                                                        store (the website) is considered to be a license to view the
                                                        properties in the online store and accept them as they are and
                                                        their position whether or not they comply with their
                                                        requirements and / or whether they are in a timely and secure
                                                        manner and contain no errors which the company (the client)
                                                        should accept. The website, its management or its owners are not
                                                        liable for any liability according to these characteristics and
                                                        their nature. The online store (the website) and its owners and
                                                        management shall not be responsible for any damages or errors
                                                        resulting from electronic failures. Was not the result of a
                                                        serious professional error The system of the store or any other
                                                        system failures and other interruptions that may affect the sale
                                                        through the online store (the website) is not reliable and
                                                        cannot be held accountable for the online store or its
                                                        management or its owners any accountability in this regard.

                                                        In no case shall the value of the compensation requested by the
                                                        online store (the website), its management or its owners exceed
                                                        the commission received by the online store from the company
                                                        (the client) at the expense of the individual item.

                                                        4.4 Precautions:

                                                        The online store (the website) shall be entitled to inform the
                                                        competent authorities of any suspicious activity carried out in
                                                        accordance with the legal principles in the Syrian Arab
                                                        Republic. The online store (the website) may suspend the
                                                        execution of its services or access to the account of the
                                                        company (the client) without giving any responsibility for it in
                                                        the event that a personal, financial or legal risk is
                                                        identified, whether actual or potential (including, but not
                                                        limited to: Seller’s violation of the terms of the online store
                                                        (the website) or any of its policies and / In the provision of
                                                        any of its services will entail a security risk / Material and /
                                                        or regulatory action.

                                                        {"\n"} 5. Governing Law and Dispute Resolution Methods:

                                                        The Syrian law is the law governing relations in the online
                                                        store (the website). The rules of this law apply to private and
                                                        public transactions therein, except as excluded by special
                                                        provision. Disputes that arise between the online store and
                                                        non-contracting parties shall be resolved by friendly solution
                                                        within ten days from the date of filing the complaint or written
                                                        objection from either party to the other party, the arbitration
                                                        shall be made during the IO days from the date of the failure of
                                                        the friendly solution. Arbitration by a single arbitrator agreed
                                                        upon by the arbitrators. In the event that this is not possible,
                                                        the arbitrators shall resort to a tripartite arbitral tribunal
                                                        appointed by them, so that each arbitrator may designate an
                                                        arbitrator and the name of the arbitrator appointed by the
                                                        arbitrators shall be referred to the competent court in
                                                        accordance with the provisions of The law The arbitrator shall
                                                        appoint the arbitrator in the Syrian Arab Republic, provided
                                                        that the arbitrators shall be bound to issue their judgment
                                                        within one month of the handover of the arbitral assignment in
                                                        accordance with the provisions of the law governing arbitration
                                                        in the Syrian Arab Republic and in accordance with the
                                                        provisions of the law of the subject and the procedures
                                                        determined by the arbitrators or arbitrators , And the judgment
                                                        issued by the arbitrators is final and the obligation of the
                                                        parties to apply it is a suspension of legal assets and the
                                                        principle of good faith.

                                                        {"\n"} 6. Fragmentation of nullity:

                                                        The invalidity of any of the provisions of these terms and / or
                                                        any of the protocols and / or any of the contracts and / or
                                                        agreements signed between the online store and the third party
                                                        shall be null and void in the paragraph that contains the
                                                        nullity, without being subject to other clauses or conditions,
                                                        Paragraphs as they are without detracting any of the effects of
                                                        this invalidity.

                                                        {"\n"} 7. Notifications:

                                                        The address Al Hariqah, mou’aeiyah st., Malas bldg. 2nd floor is
                                                        the chosen address of any notification relating to the online
                                                        store. The authorized address as the chosen address for the
                                                        notification for the companies (the clients) and / or the
                                                        customers shall be considered as the address of notification, to
                                                        which all legal effects shall be notified.

                                                    </Text>


                                                </View>
                                            }


                                        </View>

                                    </View>


                                </ScrollView>
                            </View>
                            <View>

                                <SubmitButton text={strings.t("close")} onPress={() => {

                                    this.setState({isVisible: false})
                                }}/>

                            </View>


                        </View>
                    </View>
                </Modal>

            </DefaultHeader>


        );
    }
}


const mapStateToProps = state => {
    return {

        isLoadingPlaceHolder: state.checkout.isLoadingPlaceHolder,
        statusPlaceHolder: state.checkout.statusPlaceHolder,
        ErrorMessagePlaceHolder: state.checkout.ErrorMessagePlaceHolder,
        cart: state.cart.cart,
    }
}
const mapDispatchToProps = dispatch => {
    return {

        customerCart: (payload) => dispatch(getCustomerCart(payload)),
        actionPlaceCartOrder: (payload) => dispatch(placeCartOrder(payload)),
        actionResetPlaceOrder: (payload) => dispatch(resetPlaceOrder(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutScreen);
