import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Platform, Linking, I18nManager, Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import Typography from './../../../constants/typography'
import AppStyles from "../../../constants/AppStyles";
import Color from "../../../constants/colors";
import {NavigationActions} from 'react-navigation';
import {NativeModules} from "react-native";
import strings from "./../../../locales/i18n"
import {
    NAVIGATION_ABOUTUS_SCREEN,
    NAVIGATION_APP,
    NAVIGATION_CART_SCREEN,
    NAVIGATION_Categories_SCREEN,
    NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_MYORDERS_SCREEN,
    NAVIGATION_EDIT_PROFILE_SCREEN,
    NAVIGATION_VENDORS_SCREEN,
    NAVIGATION_PROFILE_SCREEN, NAVIGATION_OFFER_SCREEN
} from "../../../navigation/types";
// import {RNRestart} from 'react-native-restart'; // Import package from node modules
// import RestartAndroid from 'react-native-restart-android'

import i18n from './../../../locales/i18n'
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';

import CodePush from 'react-native-code-push';


class SideMenu extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    logged = false

    constructor(props) {
        super(props);
        this.state = {


            //https://www.npmjs.com/package/react-native-confirmation-code-field
        }

        this.logged = global.logged


    }

    // navigateToScreen = (route) => () => {
    //     const navigateAction = NavigationActions.navigate({
    //         routeName: route
    //     });
    //     this.props.navigation.dispatch(navigateAction);
    // }


    render() {

        return (

            <Grid>
                <Row size={0.5} style={{

                    paddingTop: hp("4%"),
                    paddingStart: hp("4%"),
                    borderBottomWidth: 1,
                    borderColor: Color.Gray,
                }}>
                    <Grid>
                        <Row size={1}>
                            <TouchableOpacity
                                onPress={() => {

                                    this.props.navigation.closeDrawer()
                                    // this.props.navigation.navigate("HomeScreen2")
                                }}>
                                <Image
                                    style={{
                                        width: hp("6%"),
                                        justifyContent: 'center',
                                        alignItems: 'center',

                                        resizeMode: 'contain'
                                    }}

                                    source={require('./../../../assets/images/close.png')}
                                />
                            </TouchableOpacity>

                        </Row>
                        <Row size={3} style={{
                            alignItems: 'center',
                        }}>
                            <Image
                                style={{
                                    width: hp("13%"),

                                    resizeMode: 'contain'
                                }}
                                source={require('./../../../assets/images/Logo.png')}
                            />
                        </Row>

                    </Grid>

                </Row>
                <Row size={3}>
                    <ScrollView style={{flex: 1,}}>
                        <Grid style={styles.container}>

                            <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}
                                                  onPress={() => {

                                                      global.current = 'home'
                                                      this.props.navigation.navigate(NAVIGATION_HOME_SCREEN)
                                                  }}
                                >
                                    <Grid>
                                        <Col size={0.7} style={styles.icon}>
                                            <Image
                                                source={require('./../../../assets/images/home.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'home' ? styles.itemText : styles.itemTextNon}>{strings.t("home")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Row>

                            <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}
                                                  onPress={() => {

                                                      global.current = 'offer'
                                                      this.props.navigation.navigate(NAVIGATION_OFFER_SCREEN)
                                                  }}
                                >
                                    <Grid>
                                        <Col size={0.7} style={styles.icon}>
                                            <Image
                                                style={{
                                                    width:20,
                                                    height:20,
                                                }}
                                                source={require('./../../../assets/images/offer.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'offer' ? styles.itemText : styles.itemTextNon}>{strings.t("offer")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Row>


                            <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}
                                                  onPress={() => {

                                                      global.current = 'categories'
                                                      this.props.navigation.navigate(NAVIGATION_Categories_SCREEN)
                                                  }}
                                >
                                    <Grid>
                                        <Col size={0.7} style={styles.icon}>
                                            <Image
                                                source={require('./../../../assets/images/shop_by_category.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'categories' ? styles.itemText : styles.itemTextNon}>{strings.t("shop_by_category")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Row>

                            <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}
                                                  onPress={() => {

                                                      global.current = 'vendors'
                                                      this.props.navigation.navigate(NAVIGATION_VENDORS_SCREEN)
                                                  }}>
                                    <Grid>
                                        <Col size={0.7} style={styles.icon}>
                                            <Image
                                                source={require('./../../../assets/images/shop_by_brand.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'vendors' ? styles.itemText : styles.itemTextNon}>{strings.t("shop_by_brand")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Row>
                            <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}
                                                  onPress={() => {

                                                      global.current = 'favorite'
                                                      this.props.navigation.navigate(NAVIGATION_FAVORITES_SCREEN)
                                                  }}
                                >
                                    <Grid>
                                        <Col size={0.7} style={styles.icon}>
                                            <Image
                                                style={{

                                                }}
                                                source={require('./../../../assets/images/wishlist_menu_side.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'favorite' ? styles.itemText : styles.itemTextNon}>{strings.t("wishlist")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Row>


                            {/*<Row size={0.06} style={styles.item}>*/}
                            {/*<TouchableOpacity style={{flex: 1}}>*/}
                            {/*<Grid>*/}
                            {/*<Col size={0.7} style={styles.icon}>*/}
                            {/*<Image*/}
                            {/*source={require('./../../../assets/images/daily_offer.png')}*/}
                            {/*/>*/}
                            {/*</Col>*/}
                            {/*<Col size={3} style={styles.itemTextContainer}>*/}
                            {/*<Text style={styles.itemTextNon}>{strings.t("daily_offers")}</Text>*/}
                            {/*</Col>*/}
                            {/*</Grid>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</Row>*/}

                            {this.logged ? <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}
                                                  onPress={() => {

                                                      global.current = 'my_orders'
                                                      this.props.navigation.navigate(NAVIGATION_MYORDERS_SCREEN)
                                                  }}
                                >
                                    <Grid>
                                        <Col size={0.7} style={[styles.icon]}>
                                            <Image
                                                style={{
                                                    width:25,
                                                    height:20,
                                                }}
                                                source={require('./../../../assets/images/my_order_icon.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'my_orders' ? styles.itemText : styles.itemTextNon}>{strings.t("my_orders")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Row> : null}


                            <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}

                                                  onPress={() => {

                                                      global.current = 'about_us'
                                                      this.props.navigation.navigate(NAVIGATION_ABOUTUS_SCREEN)
                                                  }}

                                >
                                    <Grid>
                                        <Col size={0.7} style={styles.icon}>
                                            <Image
                                                source={require('./../../../assets/images/about_us.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'about_us' ? styles.itemText : styles.itemTextNon}>{strings.t("about_us")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>

                            </Row>

                            {/*<Row size={0.06} style={styles.item}>*/}
                            {/*<TouchableOpacity style={{flex: 1}}>*/}
                            {/*<Grid>*/}
                            {/*<Col size={0.7} style={styles.icon}>*/}
                            {/*<Image*/}
                            {/*source={require('./../../../assets/images/newsfeed.png')}*/}
                            {/*/>*/}
                            {/*</Col>*/}
                            {/*<Col size={3} style={styles.itemTextContainer}>*/}
                            {/*<Text style={styles.itemTextNon}>{strings.t("newsFeed")}</Text>*/}
                            {/*</Col>*/}
                            {/*</Grid>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</Row>*/}
                            {I18nManager.isRTL ?
                                <Row size={0.06} style={styles.item}>
                                    <TouchableOpacity

                                        onPress={() => {

                                            // I18nManager.forceRTL(false)
                                            // AsyncStorage.setItem("lang", "en")
                                            //     .then(data => {
                                            //         RestartAndroid.restart();
                                            //     })
                                            //     .catch(err => {
                                            //         ////console.log("err");
                                            //     });


                                            AsyncStorage.setItem('lang', "en")
                                                .then(() => {
                                                    ////console.log("en")
                                                    ////console.log("en")
                                                    I18nManager.forceRTL(false)

                                                    CodePush.restartApp();
                                                    // if (Platform.OS === "android") {
                                                    //
                                                    //     CodePush.restartApp();
                                                    //     // setTimeout(function () {
                                                    //     //
                                                    //     //    // RestartAndroid.restart();
                                                    //     //     CodePush.restartApp();
                                                    //     //
                                                    //     // },2000)
                                                    //
                                                    // }
                                                    // else {
                                                    //
                                                    //     NativeModules.DevSettings.reload();
                                                    // }

                                                })

                                            // I18nManager.forceRTL(false)
                                            // ////console.log(I18nManager.isRTL)
                                            // global.storage.save({
                                            //     key: 'lang',
                                            //     data: "en",
                                            //     expires: null,
                                            //     autoSync: true,
                                            //     syncInBackground: true,
                                            // })
                                            // if(Platform.OS==="android")
                                            // {
                                            //     RestartAndroid.restart();
                                            // }
                                            // else {
                                            //
                                            //     NativeModules.DevSettings.reload();
                                            // }

                                            // i18n.locale = "en";

                                            // setTimeout(function () {
                                            //     RestartAndroid.restart();
                                            // },1000)

                                            // NativeModules.DevSettings.reload();

                                        }
                                        }
                                        style={{flex: 1}}>
                                        <Grid>
                                            <Col size={0.7} style={styles.icon}>
                                                <Image
                                                    style={{
                                                        width:22,
                                                        height:22,
                                                    }}
                                                    source={require('./../../../assets/images/english.png')}
                                                />
                                            </Col>
                                            <Col size={3} style={styles.itemTextContainer}>
                                                <Text style={styles.itemTextNon}>English</Text>
                                            </Col>
                                        </Grid>
                                    </TouchableOpacity>
                                </Row>
                                : <Row size={0.06} style={styles.item}>
                                    <TouchableOpacity
                                        onPress={() => {

                                            //
                                            // I18nManager.forceRTL(true)
                                            // AsyncStorage.setItem("lang", "ar")
                                            //     .then(data => {
                                            //
                                            //         RestartAndroid.restart();
                                            //     })
                                            //     .catch(err => {
                                            //         ////console.log("err");
                                            //     });


                                            AsyncStorage.setItem('lang', "ar")
                                                .then(() => {
                                                    ////console.log("ar")
                                                    ////console.log("ar")
                                                    I18nManager.forceRTL(true)
                                                    CodePush.restartApp();

                                                    // if (Platform.OS === "android") {
                                                    //
                                                    //     CodePush.restartApp();
                                                    //     // setTimeout(function () {
                                                    //     //
                                                    //     //    // RestartAndroid.restart();
                                                    //     //     CodePush.restartApp();
                                                    //     // },2000)
                                                    // }
                                                    // else {
                                                    //
                                                    //     NativeModules.DevSettings.reload();
                                                    // }
                                                })


                                            // I18nManager.forceRTL(true)
                                            //
                                            // // ////console.log(I18nManager.isRTL)
                                            // global.storage.save({
                                            //     key: 'lang',
                                            //     data: "ar",
                                            //     expires: null,
                                            //     autoSync: true,
                                            //     syncInBackground: true,
                                            // })
                                            // i18n.locale = "ar";
                                            // setTimeout(function () {
                                            //     RestartAndroid.restart();
                                            // },1000)
                                            //
                                            // if(Platform.OS==="android")
                                            // {
                                            //     RestartAndroid.restart();
                                            // }
                                            // else {
                                            //
                                            //     NativeModules.DevSettings.reload();
                                            // }

                                            // NativeModules.DevSettings.reload();

                                        }
                                        }
                                        style={{flex: 1}}>
                                        <Grid>
                                            <Col size={0.7} style={styles.icon}>
                                                <Image
                                                    style={{
                                                        width:24,
                                                        height:24,
                                                    }}
                                                    source={require('./../../../assets/images/arabic.png')}
                                                />
                                            </Col>
                                            <Col size={3} style={styles.itemTextContainer}>
                                                <Text style={styles.itemTextNon}>العربية</Text>
                                            </Col>
                                        </Grid>
                                    </TouchableOpacity>
                                </Row>
                            }


                            {!this.logged ? <Row size={0.06} style={styles.item}>
                                <TouchableOpacity style={{flex: 1}}
                                                  onPress={() => {

                                                      global.current = 'login'
                                                      this.props.navigation.navigate(NAVIGATION_APP)
                                                  }}
                                >
                                    <Grid>
                                        <Col size={0.7} style={styles.icon}>
                                            <Image
                                                source={require('./../../../assets/images/user_menu.png')}
                                            />
                                        </Col>
                                        <Col size={3} style={styles.itemTextContainer}>
                                            <Text
                                                style={global.current === 'my_orders' ? styles.itemText : styles.itemTextNon}>{strings.t("login_register")}</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Row> : null}


                            {/*{this.logged ?*/}
                            {/*<Row size={0.06}>*/}
                            {/*<TouchableOpacity style={{flex: 1}}*/}

                            {/*onPress={() => {*/}
                            {/*global.storage.remove({*/}
                            {/*key: 'user',*/}
                            {/*})*/}
                            {/*this.props.navigation.navigate(NAVIGATION_APP)*/}

                            {/*}}>*/}
                            {/*<Grid>*/}
                            {/*<Col size={0.7} style={styles.icon}>*/}
                            {/*{I18nManager.isRTL ?*/}
                            {/*<Image*/}
                            {/*source={require('./../../../assets/images/logout_ar.png')}*/}
                            {/*/> :*/}
                            {/*<Image*/}
                            {/*source={require('./../../../assets/images/logout_en.png')}*/}
                            {/*/>}*/}

                            {/*</Col>*/}
                            {/*<Col size={3} style={styles.itemTextContainer}>*/}
                            {/*<Text style={styles.itemTextNon}>{strings.t("logout")}</Text>*/}
                            {/*</Col>*/}
                            {/*</Grid>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</Row>*/}
                            {/*: null}*/}


                        </Grid>
                    </ScrollView>
                </Row>
                <Row size={0.5} style={{
                    paddingStart: hp("3%"), paddingEnd: hp("3%"),
                }}>

                    <Grid>
                        <Row>
                            <Grid>
                                <Col style={styles.itemTextContainerCenter}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            Linking.openURL(`tel:00963116089`)


                                        }}>
                                        <Image style={styles.iconFooter}
                                               source={require('./../../../assets/images/call.png')}/>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={styles.itemTextContainerCenter}>
                                    <TouchableOpacity onPress={() => {

                                        Linking.openURL('mailto:cs@etloob.com')
                                    }}>
                                        <Image style={styles.iconFooter}
                                               source={require('./../../../assets/images/email.png')}/>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={styles.itemTextContainerCenter}>
                                    <TouchableOpacity onPress={() => {

                                        //  https://www.facebook.com/Etloob
                                        Linking.openURL('https://www.facebook.com/Etloob');

                                    }}>
                                        <Image style={styles.iconFooter}
                                               source={require('./../../../assets/images/fb.png')}/>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={styles.itemTextContainerCenter}
                                >
                                    <TouchableOpacity
                                        onPress={() => {

                                            //  https://www.instagram.com/etloob/
                                            Linking.openURL('https://www.instagram.com/etloob/');

                                        }}
                                    >
                                        <Image style={styles.iconFooter}
                                               source={require('./../../../assets/images/insta.png')}/>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={styles.itemTextContainerCenter}>
                                    <TouchableOpacity
                                        onPress={() => {

                                            //  https://t.me/etloob
                                            Linking.openURL('https://t.me/etloob');

                                        }}
                                    >
                                        <Image style={styles.iconFooter}
                                               source={require('./../../../assets/images/telegram.png')}/>
                                    </TouchableOpacity>
                                </Col>
                            </Grid>
                        </Row>
                        <Row style={{paddingTop: hp("2.5%"), paddingBottom: hp("2.5%")}}>

                            <Grid>

                                {this.logged ? <Col size={I18nManager.isRTL ? 2.5 : 2}
                                                    style={{borderEndWidth: 1, borderEndColor: Color.yellowBorder}}>
                                    <TouchableOpacity style={{flex: 1, paddingStart: wp("1.1%")}}

                                                      onPress={() => {
                                                          this.props.navigation.navigate(NAVIGATION_PROFILE_SCREEN)
                                                      }}

                                    >
                                        <Grid style={styles.itemTextContainerCenter}>
                                            <Col size={1} style={{paddingStart: wp("1%")}}>
                                                <Image
                                                    source={require('./../../../assets/images/user_menu.png')}/>
                                            </Col>
                                            <Col size={3}>
                                                <Text style={styles.itemTextFooter}>{strings.t("profile")}</Text>
                                            </Col>
                                        </Grid>
                                    </TouchableOpacity>
                                </Col> : null}

                                <Col size={I18nManager.isRTL ? 1.5 : 2}>

                                    <TouchableOpacity style={{flex: 1}}

                                                      onPress={() => {
                                                          this.props.navigation.navigate(NAVIGATION_CART_SCREEN)
                                                      }}>
                                        <Grid style={styles.itemTextContainerCenter}>
                                            <Col size={1}
                                                 style={{paddingStart: I18nManager.isRTL ? hp("2%") : hp("5%")}}>

                                                <Image
                                                    source={require('./../../../assets/images/cart_product.png')}/>
                                            </Col>
                                            <Col size={3}>
                                                <Text style={styles.itemTextFooter}>{strings.t("cart")}</Text>
                                            </Col>
                                        </Grid>
                                    </TouchableOpacity>

                                </Col>
                            </Grid>
                        </Row>
                    </Grid>
                </Row>
            </Grid>

        );
    }
}

const styles = {
    container: {

        paddingTop: hp("3%"),
        paddingStart: hp("4%"),
        paddingEnd: hp("4%"),
        backgroundColor: 'white',
        flex: 1, height: hp("100%")
    },
    item: {
        borderBottomWidth: 1,
        borderBottomColor: Color.yellowBorder
    },
    icon: {
        paddingStart: hp('1.2%'), alignItems: 'flex-start', justifyContent: 'center'
    },
    itemTextContainer: {
        alignItems: 'flex-start', justifyContent: 'center'
    },
    itemTextContainerCenter: {
        alignItems: 'center', justifyContent: 'center'
    }, itemText: {
        color: Color.yellow,
        ...Typography.Text16OpenSansBold
    }, itemTextNon: {
        color: Color.CustomBlack,
        ...Typography.Text16Light
    }, itemTextFooter: {
        color: Color.CustomBlack,
        paddingStart: wp("1.2%"),
        ...Typography.Text16Light,
    },
    iconFooter: {
        width: hp("4%"),
        resizeMode: 'contain'
    }

}

export default SideMenu;
