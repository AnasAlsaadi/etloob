import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import Typography from './../../../constants/typography'
import AppStyles from "../../../constants/AppStyles";
import color from "../../../constants/colors";
import {NavigationActions} from 'react-navigation';
import {
    NAVIGATION_CART_SCREEN,
    NAVIGATION_Categories_SCREEN,
    NAVIGATION_FAVORITES_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_EDIT_PROFILE_SCREEN,
    NAVIGATION_VENDORS_SCREEN,
    NAVIGATION_PROFILE_SCREEN,
    NAVIGATION_APP
} from "../../../navigation/types";

class TabedFooter extends React.Component {

    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        return (
            <Row size={0.3} style={styles.footer}>

                <Grid style={styles.CenterContent}>
                    <Col size={0.5} style={styles.CenterContent}>

                        <TouchableOpacity style={styles.TouchableFlex}
                                          onPress={() => {
                                              global.current = 'vendors'
                                              this.props.navigation.navigate(NAVIGATION_VENDORS_SCREEN)
                                          }}
                        >
                            <Image
                                style={styles.footerImageCart}
                                source={require("./../../../assets/images/shopf.png")}
                            />
                        </TouchableOpacity>
                    </Col>
                    <Col size={0.5} style={styles.CenterContent}>

                        <TouchableOpacity style={styles.TouchableFlex}

                                          onPress={() => {

                                              global.current = 'favorite'
                                              this.props.navigation.navigate(NAVIGATION_FAVORITES_SCREEN)
                                          }}


                        >
                            <Image
                                style={styles.footerImageL}
                                source={require("./../../../assets/images/wishlist_product.png")}
                            />
                        </TouchableOpacity>
                    </Col>
                    <Col size={1} style={styles.CenterContent}>

                        <TouchableOpacity style={styles.TouchableFlex}
                                          onPress={() => {
                                              global.current = 'home'
                                              this.props.navigation.navigate(NAVIGATION_HOME_SCREEN)
                                          }}
                        >
                            <Image
                                style={styles.footerImage}
                                source={require("./../../../assets/images/homef.png")}
                            />
                        </TouchableOpacity>
                    </Col>
                    <Col size={0.5} style={styles.CenterContent}>

                        <TouchableOpacity style={styles.TouchableFlex}
                                          onPress={() => {

                                              this.props.navigation.navigate(NAVIGATION_CART_SCREEN)
                                          }}
                        >
                            <Image
                                style={styles.footerImageCart}
                                source={require("./../../../assets/images/cart.png")}
                            />
                        </TouchableOpacity>
                    </Col>
                    <Col size={0.5} style={styles.CenterContent}>

                        <TouchableOpacity style={styles.TouchableFlex}
                                          onPress={() => {
                                              if( global.logged){
                                                  this.props.navigation.navigate(NAVIGATION_PROFILE_SCREEN)

                                              }
                                              else {
                                                  this.props.navigation.navigate(NAVIGATION_APP)


                                              }
                                          }}
                        >
                            <Image
                                style={styles.footerImageP}
                                source={require("./../../../assets/images/user_menu.png")}
                            />
                        </TouchableOpacity>

                    </Col>
                </Grid>
            </Row>

        );
    }
}

const styles = {

    footer: {
        backgroundColor: 'white',
        paddingStart: wp("4%"),
        paddingEnd: wp("4%"),

        borderTopWidth: 1,
        borderTopColor: color.GrayBorder,
        shadowColor: color.GrayBorder,
        shadowOpacity: 1,
        shadowRadius: 6,
        shadowOffset: {
            height: 2,
            width: 1
        }
    }, CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    footerImageL: {
        width: hp("4%"),
        resizeMode: 'contain',
    },
    footerImageCart: {
        width: hp("3%"),
        resizeMode: 'contain',
    },
    footerImageP: {
        width: hp("8%"),
        height: hp("3.2%"),
        resizeMode: 'contain',
    },
}

export default TabedFooter;
