import React from 'react';
import {Grid, Row} from "react-native-easy-grid";
import {TouchableOpacity, Text, TextInput} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import strings from "./../../../locales/i18n"

const InputElement = ({
                          sizeRow,
                          text,
                          secureTextEntry,
                          onClick,
                          type,
                          disabled,
                          buttonType,
                          onChangeText,
                          multiline, numberOfLines,
                          value,
                          keyboardType,
                          forCheckout,
                          onPressShow,
                          textLength,
                      }) => (
    <Row size={sizeRow ? sizeRow : 1} style={[styles.FlexDirectionColumn, forCheckout ? styles.CustomMargin : null]}>
        {forCheckout ?
            <Text style={
                [Typography.Text16Light]
            }>{text}</Text>
            :
            <Text style={
                [Typography.Text12Regular, AppStyle.opacity05]
            }>{text}</Text>}

        <TextInput
            keyboardType={keyboardType ? "numeric" : "default"}

            multiline={multiline} numberOfLines={numberOfLines} value={value} onChangeText={onChangeText}
            secureTextEntry={secureTextEntry} style={[Typography.Text14OpenSansRegular, AppStyle.input]}/>

        {type == "password" && textLength>0 ?
            <TouchableOpacity onPress={onPressShow} style={{
                marginTop: -wp("8%"),
                textAlign: 'right',
                alignSelf: 'flex-end'
            }}><Text style={[Typography.Text12OpenSansBold]}>{secureTextEntry?strings.t("show"):strings.t("hide")}</Text></TouchableOpacity> : null}
    </Row>
);

const styles = {

    FlexDirectionColumn: {

        flex: 1,
        flexDirection: 'column'

    },
    CustomMargin: {
        marginBottom: wp("4%")

    }
}

export default InputElement;