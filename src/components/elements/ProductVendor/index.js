import React from 'react';
import {I18nManager, View, Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import strings from "../../../locales/i18n";
import FastImage from 'react-native-fast-image'
import NumberFormat from 'react-number-format';

const ProductVendor = ({
                           text,
                           onPress, onPressAddToCart,
                           onPressAddToFavorite,
                           product

                       }) => (
    <View style={styles.Item}>
        <View style={[styles.CenterContent]}>
            <TouchableOpacity onPress={onPress}>
                <FastImage
                    style={styles.productImage}
                    source={{
                        uri: Array.isArray(product.images) && product.images.length > 0 ? product.images[0].src : "https://static.thenounproject.com/png/220984-200.png",
                        priority: FastImage.priority.high,
                    }}
                />


            </TouchableOpacity>
        </View>
        <View style={[{flex: 1, alignSelf: 'flex-start', textAlign: 'right', paddingHorizontal: wp("4%")}]}>

            <TouchableOpacity onPress={onPress} style={{}}>

                <View style={[{flex: 1,}]}>
                    <Text
                        style={styles.productName}>{strings.t("product_name")}: {product.name} </Text>
                </View>

                <View style={[{flex: 1,}]}>
                    <Text
                        style={styles.productName}>{strings.t("product_status")}: {product.status} </Text>
                </View>

                <View style={[{flex: 1,}]}>
                    <Text
                        style={styles.productName}>{strings.t("sku")}: {product.sku} </Text>
                </View>

                <View style={[{flex: 1,}]}>
                    <NumberFormat value={product.regular_price} renderText={value => <Text
                        style={[styles.productName]}>{strings.t("price")}: {value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                  displayType={'text'}
                                  thousandSeparator={true} prefix={""}/>
                </View>

                <View style={[{flex: 1,}]}>
                    <NumberFormat value={product.regular_price} renderText={value => <Text
                        style={[styles.productName]}>{strings.t("earning")}: {value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                  displayType={'text'}
                                  thousandSeparator={true} prefix={""}/>
                </View>

                <View style={[{flex: 1,}]}>
                    <Text
                        style={styles.productName}>{strings.t("stock_status")}: {product.in_stock ? strings.t("in_stock") : strings.t("not_in_stock")} </Text>
                </View>

                <View style={[{flex: 1,}]}>
                    <Text
                        style={styles.productName}>{strings.t("views")}: {product.meta_data.map((e) => {
                        if (e.key === "pageview") return e.value
                    })} </Text>
                </View>

                <View style={[{flex: 1,}]}>
                    <Text
                        style={styles.productName}>{strings.t("date")}: {product.date_created} </Text>
                </View>


            </TouchableOpacity>
        </View>

    </View>
);

const styles = {

    Item: {
        padding: 4,
        flexDirection: 'column',
        borderRadius: 5,
        backgroundColor: 'white',
        borderColor: color.GrayBorder,
        marginBottom: wp("4%"),
        marginHorizontal: wp("4%"),
        flex: 1,
        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
    ,
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        paddingLeft: wp("3%"),
        paddingRight: wp("3%"),
        // alignItems: 'center',
        // flex: 1
    },
    productImage: {
        width: wp("25%"),
        height: wp("30%"),
        // resizeMode: 'contain',
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light,
        paddingStart: wp("1%"),
        paddingEnd: wp("1%"),
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    }

}


export default ProductVendor;