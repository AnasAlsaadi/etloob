import React from 'react';
import {View, Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import FastImage from 'react-native-fast-image'
import strings from "./../../../locales/i18n"
import NumberFormat from "react-number-format";

const OrderVendor = ({
                         text,
                         onPress,
                         order
                     }) => (

    <View style={[styles.CenterContentCon, {}]}>
        <View style={{
            flexDirection: "column",
            flex: 0.2,
        }}>

            <TouchableOpacity disabled={true} style={styles.TouchableFlex}>

                <Image
                    style={{

                        opacity:0.15,
                        // width: wp("22%"),
                        // resizeMode: 'contain',
                    }}
                    // resizeMode="contain"
                    source={require("./../../../assets/images/my_order_icon.png")}

                />
            </TouchableOpacity>

        </View>
        <View style={{
            flexDirection: "column",
            flex: 0.8,
        }}>
            <TouchableOpacity onPress={onPress}>
                <Text
                    style={[Typography.Text14OpenSansBold, {paddingBottom: wp("2%")}]}>{strings.t("order_number")}{": "}ET-{order.id}</Text>

                <Text
                    style={[Typography.Text14Light, {paddingBottom: wp("1%")}]}>{strings.t("customer")}: {order.shipping.first_name} {order.shipping.last_name} </Text>


                <Text
                    style={[styles.productName, {paddingBottom: wp("1%")}]}><NumberFormat value={order.total}
                                                                                          renderText={value => <Text
                                                                                              style={[styles.productName]}>{strings.t("price")}: {value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                                                                          displayType={'text'}
                                                                                          thousandSeparator={true}
                                                                                          prefix={""}/></Text>


                <Text style={[Typography.Text14Light, {
                    paddingBottom: wp("1%"),
                    paddingTop: wp("1%")
                }]}>{strings.t("status")}{": "} {order.status}</Text>
                <Text style={[Typography.Text14Light, {
                    paddingBottom: wp("1%"),
                    paddingTop: wp("1%")
                }]}>{strings.t("created_at")}{": "} {order.date_created}</Text>


            </TouchableOpacity>
        </View>

    </View>

);

const styles = {

    Item: {
        borderRadius: 5,
        backgroundColor: 'white',
        borderColor: color.GrayBorder,

        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
    ,
    CenterContentCon: {
        backgroundColor: color.white,
        marginBottom: wp("0.5%"),
        marginStart: wp("3%"),
        marginEnd: wp("3%"),
        marginTop: wp("1.5%"),
        paddingTop: wp("2%"),
        paddingBottom: wp("2%"),
        borderColor: color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8
        ,
        flexDirection: "row",
        alignItems: 'center', justifyContent: 'center'
    },
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        alignItems: 'center',
        flex: 1
    },
    productImage: {
        width: hp("18%"),
        resizeMode: 'contain',
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light,
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    },
    FlexIcon: {
        paddingTop: wp("3%"), paddingBottom: wp("3%"),
        alignItems: 'center', justifyContent: 'center',
        flex: 1,

    }

}


export default OrderVendor;
