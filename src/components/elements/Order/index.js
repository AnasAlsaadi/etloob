import React from 'react';
import {View, Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import FastImage from 'react-native-fast-image'
import strings from "./../../../locales/i18n"

const Order = ({
                   text,
                   onPress,
                   order
               }) => (

    <View  key={order.id+"order"} style={[styles.CenterContentCon, {}]}>
        <View style={{
            flexDirection: "column",
            flex: 0.2,
        }}>

            <TouchableOpacity disabled={true} style={styles.TouchableFlex}>

                <Image
                    style={{

                        opacity:0.15,
                        // width: wp("22%"),
                        // resizeMode: 'contain',
                    }}
                    // resizeMode="contain"
                    source={require("./../../../assets/images/my_order_icon.png")}

                />
            </TouchableOpacity>

        </View>
        <View style={{
            flexDirection: "column",
            flex: 0.8,
        }}>
            <TouchableOpacity onPress={onPress}>
                <Text
                    style={[Typography.Text14OpenSansBold, {paddingBottom: wp("2%")}]}>{strings.t("order_number")}{": "}ET-{order.number}</Text>

                <Text
                    style={[Typography.Text12Light, {paddingBottom: wp("1%")}]}>{strings.t("name")}{": "}{order.billing.first_name}{" "}{order.billing.last_name}</Text>
                <Text style={[Typography.Text12Light, {
                    paddingBottom: wp("1%"),
                    paddingTop: wp("1%")
                }]}>{strings.t("address")}{": "}{order.billing.address_1}{"-"}{order.billing.address_2}{"-"}{order.billing.country}</Text>
                <Text style={[Typography.Text12Light, {
                    paddingBottom: wp("1%"),
                    paddingTop: wp("1%")
                }]}>{strings.t("payment_way")}{": "}{strings.t(order.payment_method)}</Text>
                <Text style={[Typography.Text12Light, {
                    paddingBottom: wp("1%"),
                    paddingTop: wp("1%")
                }]}>{strings.t("status")}{": "}{strings.t(order.status.split("-").join("_"))}</Text>
                <Text style={[Typography.Text12Light, {
                    paddingBottom: wp("1%"),
                    paddingTop: wp("1%")
                }]}>{strings.t("created_at")}{": "}{new Date(order.date_created+"").getFullYear() + "-"+ new Date(order.date_created+"").getMonth()+ "-"+ new Date(order.date_created+"").getDay() +" "+  new Date(order.date_created+"").getHours() + ":"+ new Date(order.date_created+"").getMinutes()+ ":"+ new Date(order.date_created+"").getSeconds()}</Text>

                {order.coupon_lines.length > 0 ?
                    (<View>
                            <Text
                                style={[Typography.Text16OpenSansBold, {color: color.red}]}>{strings.t("discount")} {": "} {order.discount_total}
                                {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>
                            <Text
                                style={[Typography.Text16OpenSansBold, {color: color.green}]}>{strings.t("total")} {": "} {order.total}
                                {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>
                        </View>


                    )

                    :
                    <Text
                        style={[Typography.Text16OpenSansBold, {color: color.green}]}>{strings.t("total")} {": "} {order.total}
                        {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>
                }

            </TouchableOpacity>
        </View>

    </View>

);

const styles = {

    Item: {
        borderRadius: 5,
        backgroundColor: 'white',
        borderColor: color.GrayBorder,

        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
    ,
    CenterContentCon: {
        backgroundColor: color.white,
        marginBottom: wp("0.5%"),
        marginStart: wp("3%"),
        marginEnd: wp("3%"),
        marginTop: wp("1.5%"),
        paddingTop: wp("2%"),
        paddingBottom: wp("2%"),
        borderColor: color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8
        ,
        flexDirection: "row",
        alignItems: 'center', justifyContent: 'center'
    },
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        alignItems: 'center',
        flex: 1
    },
    productImage: {
        width: hp("18%"),
        resizeMode: 'contain',
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light, textAlign: 'center',
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    },
    FlexIcon: {
        paddingTop: wp("3%"), paddingBottom: wp("3%"),
        alignItems: 'center', justifyContent: 'center',
        flex: 1,

    }

}


export default Order;
