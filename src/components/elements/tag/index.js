import React from 'react';
import {Image, Text, TextInput, View, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import HTML from 'react-native-render-html'

const Tag = ({
                 IfCategoryColor,
                 selected,
                 title,
    onPress

             }) => (
    <TouchableOpacity style={[styles.CenterContent,styles.itemColor]}
                      onPress={onPress}>


            <Text style={[Typography.Text14Light
                , {
                    color: 'black',
                    textAlign: 'center'
                }]}> {title}</Text>


    </TouchableOpacity>
);

const styles = {

    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    itemColor: {

        alignItems: 'center', justifyContent: 'center',
        marginEnd: hp("1%"),
        marginBottom: hp("1%"),
        paddingStart: hp("2%"),
        paddingEnd: hp("2%"),
        paddingTop: hp("1%"),
        paddingBottom: hp("1%"),
        borderRadius: 5,
        // width:wp("20%"),
        backgroundColor: color.white,
        borderWidth: 1,
        borderColor: color.CustomBlack
    }
    ,
    selectedColor: {
        backgroundColor: color.yellow,
        borderWidth: 0
    }


}


export default Tag;