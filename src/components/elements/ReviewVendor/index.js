import React from 'react';
import {View, Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import FastImage from 'react-native-fast-image'
import strings from "./../../../locales/i18n"
import NumberFormat from "react-number-format";

const ReviewVendor = ({
                          text,
                          onPress,
                          review
                      }) => (

    <View style={[styles.CenterContentCon, {}]}>
        <View style={{
            flexDirection: "column",
            flex: 0.2,
        }}>

            <TouchableOpacity disabled={true} style={styles.TouchableFlex}>

                <FastImage
                    style={styles.productImage}
                    source={{
                        uri: review.author.avatar.length > 0 ? review.author.avatar : "https://static.thenounproject.com/png/220984-200.png",
                        priority: FastImage.priority.high,
                    }}
                />
            </TouchableOpacity>

        </View>
        <View style={{
            flexDirection: "column",
            flex: 0.8,
        }}>
            <TouchableOpacity onPress={onPress}>
                <Text
                    style={[Typography.Text14OpenSansBold, {paddingBottom: wp("2%")}]}>{review.author.name} - {review.date}</Text>

                <Text
                    style={[Typography.Text14Light, {paddingBottom: wp("1%")}]}>{review.title}</Text>

                <Text
                    style={[Typography.Text14Light, {paddingBottom: wp("1%")}]}>{review.content}</Text>

                <Text
                    style={[Typography.Text14Light, {paddingBottom: wp("1%")}]}>{strings.t("rating")}: {review.rating}</Text>

            </TouchableOpacity>
        </View>

    </View>

);

const styles = {

    Item: {
        borderRadius: 5,
        backgroundColor: 'white',
        borderColor: color.GrayBorder,

        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
    ,
    CenterContentCon: {
        backgroundColor: color.white,
        marginBottom: wp("0.5%"),
        marginStart: wp("3%"),
        marginEnd: wp("3%"),
        marginTop: wp("1.5%"),
        paddingTop: wp("2%"),
        paddingBottom: wp("2%"),
        borderColor: color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8
        ,
        flexDirection: "row",
        alignItems: 'center', justifyContent: 'center'
    },
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        alignItems: 'center',
        flex: 1
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light,
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    },
    FlexIcon: {
        paddingTop: wp("3%"), paddingBottom: wp("3%"),
        alignItems: 'center', justifyContent: 'center',
        flex: 1,

    },

    productImage: {
        width: 45,
        height: 45,
        // resi
        // zeMode: 'contain',
    },
}


export default ReviewVendor;