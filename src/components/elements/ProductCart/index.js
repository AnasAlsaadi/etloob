import React from 'react';
import {View, Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import FastImage from 'react-native-fast-image'
import strings from "./../../../locales/i18n"
import NumberFormat from "react-number-format";

const ProductCart = ({

                         text,
                         onPress, onPressRemoveProduct,
                         product,
                         meta_data,
                         if_meta_data,
                         count,
                         favorite,
                         onPressPlus,
                         onPressMinus
                     }) => (

    <View style={styles.CenterContentConBig}>
        <View style={[styles.CenterContentCon, {}]}>
            <View style={{
                flexDirection: "column",
                flex: 0.4,
            }}>

                <TouchableOpacity disabled={true} style={styles.TouchableFlex}>
                    <FastImage
                        style={{
                            width: wp("18%"),
                            height: wp("18%"),
                            resizeMode: 'contain',
                        }}
                        source={{
                            uri: Array.isArray( product.images) && product.images.length > 0? product.images[0].src : "http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png",
                            priority: FastImage.priority.high,
                        }}

                    />
                </TouchableOpacity>

            </View>
            <View style={{
                flexDirection: "column",
                flex: 0.6,
            }}>
                <TouchableOpacity onPress={onPress}>
                    <Text
                        style={[Typography.Text12OpenSansBold, {paddingBottom: wp("2%")}]}>{product.name}</Text>

                    <Text
                        style={[Typography.Text12Light, {paddingBottom: wp("1%")}]}>{product.categories ? product.categories.map((item, index) => {


                        return index === product.categories.length - 1 ? item.name + "" : item.name + ", "
                    }) : null}</Text>

                    <NumberFormat value={product.price} renderText={value => <Text
                        style={[Typography.Text16OpenSansBold, {color: color.green}]}>{value} {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>}
                                  displayType={'text'} thousandSeparator={true} prefix={""}/>


                    {if_meta_data ?
                        meta_data.map((item) => {
                            return (<Text key={"dd"+item.key}
                                style={[Typography.Text12Light, {paddingBottom: wp("1%")}]}>{item.key + ": " + item.value}</Text>)

                        })
                        : null}
                </TouchableOpacity>
            </View>
            <View style={{
                flexDirection: "column",
                flex: 0.2,
            }}>
                <Grid>
                    {favorite?null:<Row style={styles.CenterContent}>
                        <TouchableOpacity style={[styles.FlexIcon]} onPress={onPressPlus}>
                            <Image
                                source={require("../../../assets/images/plus.png")}/>
                        </TouchableOpacity>
                    </Row>}

                    {favorite?null: <Row style={styles.CenterContent}>
                        <TouchableOpacity style={[styles.FlexIcon]} onPress={onPressMinus}>
                            <Image
                                source={require("../../../assets/images/minus.png")}/>
                        </TouchableOpacity>
                    </Row>}


                    <Row style={styles.CenterContent}>
                        <TouchableOpacity style={[styles.FlexIcon]} onPress={onPressRemoveProduct}>
                            <Image
                                source={require("../../../assets/images/remove.png")}/>
                        </TouchableOpacity>
                    </Row>
                </Grid>

            </View>


        </View>

        {favorite ? null :
            <View style={styles.CenterContent}><Text
                style={[Typography.Text16OpenSansBold, {
                    paddingTop: wp("2%"),
                    paddingBottom: wp("2%"),
                    color: color.green,
                }]}>{count} × <NumberFormat value={product.price} renderText={value => <Text
                style={[Typography.Text16OpenSansBold, {color: color.green}]}>{value} {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>}
                                            displayType={'text'} thousandSeparator={true} prefix={""}/>
            </Text></View>}

    </View>


);

const styles = {


    CenterContentConBig: {
        flexDirection: 'column',
    },
    CenterContentCon: {
        backgroundColor: color.white,
        marginBottom: wp("0.5%"),
        marginStart: wp("3%"),
        marginEnd: wp("3%"),
        marginTop: wp("1.5%"),
        paddingTop: wp("2%"),
        paddingBottom: wp("2%"),
        borderColor: color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8
        ,
        flexDirection: "row",
        alignItems: 'center', justifyContent: 'center'
    },
    Item: {
        borderRadius: 5,
        backgroundColor: 'white',
        borderColor: color.GrayBorder,

        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
    ,
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        alignItems: 'center',
        flex: 1
    },
    productImage: {
        width: hp("18%"),
        resizeMode: 'contain',
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light, textAlign: 'center',
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    },
    FlexIcon: {
        paddingTop: wp("3%"), paddingBottom: wp("3%"),
        alignItems: 'center', justifyContent: 'center',
        flex: 1,

    }

}


export default ProductCart;