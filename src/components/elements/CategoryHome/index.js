import React from 'react';
import {Image, Text, TextInput, View, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import HTML from 'react-native-render-html'

const CategoryHome = ({
                          IfCategoryColor,
                          selected,
                          title,
                            onPress

                      }) => (
    <TouchableOpacity onPress={onPress} style={[styles.CenterContent, {flex: 1,}]}>
        <Row style={
            [IfCategoryColor ? styles.itemColor : styles.item,
                selected && IfCategoryColor ? styles.selectedColor : null,
                , {flex: 1}]
        }>

            <Text style={[,
                IfCategoryColor ? Typography.Text14Light :
                    Typography.Text16OpenSansBold
                , {
                    color: IfCategoryColor ? 'black' : 'white',
                    flex: 1,
                    textAlign: 'center'
                }]}> {title}</Text>

        </Row>
    </TouchableOpacity>
);

const styles = {

    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    item: {

        flex: 1,
        alignItems: 'center', justifyContent: 'center',
        margin: hp("1%"),
        backgroundColor: color.CustomBlack,
        borderRadius: 8
    },
    itemColor: {

        flex: 1,
        alignItems: 'center', justifyContent: 'center',
        marginEnd: hp("1%"),
        marginTop: hp("1%"),
        marginBottom: hp("1%"),
        paddingStart: hp("4%"),
        paddingEnd: hp("4%"),
        paddingTop: hp("1%"),
        paddingBottom: hp("1%"),
        borderRadius: 8,
        // width:wp("20%"),
        backgroundColor: color.white,
        borderWidth: 1,
        borderColor: color.CustomBlack
    }
    ,
    selectedColor: {
        backgroundColor: color.yellow,
        borderWidth: 0
    }


}


export default CategoryHome;