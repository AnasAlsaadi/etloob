import React from 'react';
import {Grid, Row} from "react-native-easy-grid";
import {Text, TextInput, TouchableOpacity, View} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {NAVIGATION_SIGNUP_SCREEN} from "../../../navigation/types";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const TextUnderLine = ({
                           onPress,
                           text,
                           style,
                           textStyle,
                           ifHome
                       }) => (
    <TouchableOpacity style={[styles.TextContainer, {height: ifHome ? wp("5%") : 'auto'}]}
                      onPress={onPress}>
        <View style={[style, styles.underlineTextContainer]}>
            <Text style={
                textStyle === "Medium" ?
                    [Typography.Text12Medium, AppStyle.Yellow] :
                    [Typography.Text13, AppStyle.Yellow]


            }>{text}</Text>
        </View>
    </TouchableOpacity>
);

const styles = {
    underlineTextContainer: {
        borderBottomWidth: 1,
        borderColor: '#ffc30f', alignSelf: 'flex-start'
    },
}
export default TextUnderLine;