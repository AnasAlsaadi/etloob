import React from 'react';
import {View, Image, Text, TextInput, TouchableOpacity, I18nManager} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import FastImage from 'react-native-fast-image'
import strings from "./../../../locales/i18n"
import NumberFormat from "react-number-format";

const ProductHorizontal = ({
                               text,
                               onPress, onPressAddToCart,
                               onPressAddToFavorite,
                               product,
                               forHome,
    offer
                           }) => (

    !forHome ?

        <View key={product.id+""} style={[styles.CenterContentCon, {}]}>
            <View style={{
                flexDirection: "column",
                flex: 0.4,
            }}>

                <TouchableOpacity  onPress={onPress}  style={styles.TouchableFlex}>
                    <FastImage
                        style={{
                            width: wp("23%"),
                            height: wp("23%"),
                            resizeMode: 'contain',
                        }}
                        source={{
                            uri:Array.isArray( product.images) && product.images.length > 0 ? product.images[0].src : "http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png",
                            priority: FastImage.priority.high,
                        }}

                    />
                </TouchableOpacity>

            </View>
            <View style={{
                flexDirection: "column",
                flex: 0.6,
            }}>
                <TouchableOpacity onPress={onPress}>
                    <Text
                        style={[Typography.Text12OpenSansBold, {paddingBottom: wp("2%")}]}>{product.name}</Text>

                    <Text
                        style={[Typography.Text12Light, {paddingBottom: wp("1%")}]}>{product.categories ? product.categories.map((item, index) => {


                        return index === product.categories.length - 1 ? item.name + "" : item.name + ", "
                    }) : null}</Text>

                    {product.on_sale && product.regular_price.length>0 ?
                        <NumberFormat value={product.regular_price}  renderText={value => <Text style={[styles.oldPrice]}>{value} {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>} displayType={'text'} thousandSeparator={true} prefix={""} />
                        : null}


                    <NumberFormat value={product.price}  renderText={value => <Text style={[Typography.Text16OpenSansBold, {color: color.green}]}>{value} {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>} displayType={'text'} thousandSeparator={true} prefix={""} />

                </TouchableOpacity>
            </View>
            <View style={{
                flexDirection: "column",
                flex: 0.2,
            }}>

                <Grid>
                    <Row style={styles.CenterContent}>
                        <TouchableOpacity style={[styles.FlexIcon]} onPress={onPressAddToCart}>
                            <Image
                                source={require("../../../assets/images/cart_product.png")}/>
                        </TouchableOpacity>
                    </Row>
                    <Row style={styles.CenterContent}>

                        <TouchableOpacity style={[styles.FlexIcon]} onPress={onPressAddToFavorite}>
                            <Image
                                source={require("../../../assets/images/love.png")}/>
                        </TouchableOpacity>

                    </Row>
                </Grid>

            </View>

            {offer && parseInt(product.regular_price)>parseInt(product.price)?<View style={{
                position:'absolute',
                bottom:wp("4%"),
                right:wp("12%"),
                borderRadius:5,
                paddingHorizontal:2,
                paddingVertical:2,
                backgroundColor:color.green,
            }}>
                <Text style={[Typography.Text12Light,{color:color.white}]}> {strings.t("discountـ")+"" + parseInt(((parseInt(product.regular_price)-parseInt(product.price))*100)/parseInt(product.regular_price) )+"%"}</Text>
            </View>:null}

        </View>
        :


        <View key={product.id+""} style={[styles.CenterContentConTrans, {}]}>
            <View style={{
                flexDirection: "column",
                flex: 0.4,
            }}>


                <TouchableOpacity  onPress={onPress}   style={styles.TouchableFlex}>
                    <FastImage
                        style={{
                            width: wp("23%"),
                            height: wp("23%"),
                            resizeMode: 'contain',
                        }}
                        source={{
                            uri: Array.isArray( product.images) && product.images.length > 0? product.images[0].src : "http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png",
                            priority: FastImage.priority.high,
                        }}

                    />
                </TouchableOpacity>

            </View>
            <View style={{
                flexDirection: "column",
                flex: 0.6,
            }}>



                <TouchableOpacity onPress={onPress}>
                    <Text
                        style={[Typography.Text12OpenSansBold, {paddingBottom: wp("2%")}]}>{product.name}</Text>

                    <Text
                        style={[Typography.Text12Light, {paddingBottom: wp("1%")}]}>{product.categories ? product.categories.map((item, index) => {


                        return index === product.categories.length - 1 ? item.name + "" : item.name + ", "
                    }) : null}</Text>

                    {product.on_sale  && product.regular_price.length>0  ?
                        <NumberFormat value={product.regular_price}  renderText={value => <Text style={[styles.oldPrice]}>{value} {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>} displayType={'text'} thousandSeparator={true} prefix={""} />

                        : null}

                    <NumberFormat value={product.price}  renderText={value => <Text style={[Typography.Text16OpenSansBold, {color: color.green}]}>{value} {global.mode === "pro" ?"":strings.t("syrian_p")}</Text>} displayType={'text'} thousandSeparator={true} prefix={""} />

                </TouchableOpacity>
            </View>
            <View style={{
                flexDirection: "column",
                flex: 0.2,
            }}>


                <Grid>
                    <Row style={styles.CenterContent}>
                        <TouchableOpacity style={[styles.FlexIcon]} onPress={onPressAddToCart}>
                            <Image
                                source={require("../../../assets/images/cart_product.png")}/>
                        </TouchableOpacity>
                    </Row>
                    <Row style={styles.CenterContent}>

                        <TouchableOpacity style={[styles.FlexIcon]} onPress={onPressAddToFavorite}>
                            <Image
                                source={require("../../../assets/images/love.png")}/>
                        </TouchableOpacity>

                    </Row>
                </Grid>

            </View>
        </View>
);

const styles = {

    CenterContentCon: {
        backgroundColor: color.white,
        marginBottom: wp("0.5%"),
        marginStart: wp("3%"),
        marginEnd: wp("3%"),
        marginTop: wp("1.5%"),
        paddingTop: wp("2%"),
        paddingBottom: wp("2%"),
        borderColor: color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8
        ,
        flexDirection: "row",
        alignItems: 'center', justifyContent: 'center'
    },
    CenterContentConTrans: {
        backgroundColor:'white',
        marginBottom: wp("0.5%"),
        marginStart: wp("3%"),
        marginEnd: wp("3%"),
        marginTop: wp("1.5%"),
        paddingTop: wp("2%"),
        paddingBottom: wp("2%"),
        borderColor: color.GrayBorder,
        borderWidth: 1,
        borderRadius: 8
        ,
        flexDirection: "row",
        alignItems: 'center', justifyContent: 'center'
    },

    Item: {
        borderRadius: 5,
        backgroundColor: 'white',
        borderColor: color.GrayBorder,

        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
    ,
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        alignItems: 'center',
        flex: 1
    },
    productImage: {
        width: hp("18%"),
        resizeMode: 'contain',
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light, textAlign: 'center',
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    },
    FlexIcon: {
        paddingTop: wp("3%"), paddingBottom: wp("3%"),
        alignItems: 'center', justifyContent: 'center',
        flex: 1,

    }

}


export default ProductHorizontal;
