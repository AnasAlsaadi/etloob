import React from 'react';
import {View, Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import strings from "./../../../locales/i18n"

const ShowMore = ({
                      text,
                      onPress

                  }) => (
    <View style={{marginTop: hp("2%")}}>
        <View style={[styles.CenterContent]}><TouchableOpacity
            onPress={onPress}
            style={[, {
                borderRadius: 8,
                backgroundColor: color.CustomBlack,
                paddingStart: wp("8%"),
                paddingEnd: wp("8%"),
                paddingTop: wp("1.5%"),
                paddingBottom: wp("1.5%")
            }]}
        ><Text
            style={[Typography.Text18OpenSansBold, {color: color.yellow}]}>{strings.t("see_more")}</Text></TouchableOpacity></View>

    </View>
);

const styles = {


    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },


}


export default ShowMore;