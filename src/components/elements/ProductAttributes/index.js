import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Text, ScrollView, Image, TouchableOpacity, View, TextInput} from 'react-native'
import DefaultHeader from './../../../containers/DefaultHeader'
import Typography from "../../../constants/typography";
import color from "../../../constants/colors";
import CategoryList from "../CategoryList";
import CategoryHome from "../CategoryHome";

class ProductAttributes extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {


        const {attribute} = this.props

        //////console.log("attr")
        //////console.log(attribute)
        //////console.log(attribute.options)
        return (

            <Row size={2.4} style={{marginTop: wp("1%")}}>
                <View>
                    <View size={1.5} style={[{alignItems: 'flex-start'}]}>
                        <Text
                            style={[Typography.Text18OpenSansBold, {color: color.yellow}]}>{attribute.name}</Text>
                    </View>
                    <View size={2.5} style={{alignItems: 'flex-start'}}>


                        <ScrollView
                            horizontal={true}>

                            <Grid style={[{justifyContent: 'flex-start', alignItems: 'flex-start'}]}>

                                {attribute.options.map((item, index) => {

                                    return (

                                        <CategoryHome
                                            IfCategoryColor={true}
                                            onPress={() => {
                                                this.props.setAttr(item)
                                            }}
                                            key={"cT" + index + item.name}
                                            title={item.name}
                                            selected={item.selected}/>
                                    )
                                })
                                }

                            </Grid>
                            {/*<CategoryList*/}

                            {/*onPressitem={(item)=>{this.props.setAttr(item)}}*/}
                            {/*IfCategoryColor={true}*/}
                            {/*items={attribute.options}*/}
                            {/*/>*/}

                        </ScrollView>
                    </View>
                </View>
            </Row>

        );
    }
}


export default ProductAttributes;