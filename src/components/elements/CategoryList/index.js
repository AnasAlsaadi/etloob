import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import CategoryHome from "../CategoryHome";

class CategoryList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        return (
            <Grid style={[{justifyContent: 'flex-start', alignItems: 'flex-start'}]}>

                {this.props.items.map((item, index) => {

                    return (

                        <CategoryHome
                            onPress={() => {
                                this.props.onPressitem(item)
                            }}
                            key={"cT" + index + item.title} IfCategoryColor={this.props.IfCategoryColor}
                            title={item}
                            selected={false}/>
                    )
                })
                }

            </Grid>
        );
    }
}


export default CategoryList;