import React from 'react';
import {Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import FastImage from 'react-native-fast-image'

const Vendor = ({
                    text,
                    onPress,
                    image
                }) => (
    <TouchableOpacity style={[, {flex: 1}]} onPress={onPress}>
        <Grid>
            <Row size={3} style={[styles.CenterContent]}>
                <FastImage
                    style={{
                        width: wp("17%"),
                        height: wp("17%"),
                        resizeMode: 'contain'
                    }}
                    source={{
                        uri: image ? image : "https://static.thenounproject.com/png/220984-200.png", priority: FastImage.priority.high,
                    }}
                />

            </Row>
            <Row size={1} style={[styles.CenterContent]}><Text
                style={[Typography.Text14Light]}>{text}</Text></Row>
        </Grid>
    </TouchableOpacity>
);

const styles = {

    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },

}


export default Vendor;