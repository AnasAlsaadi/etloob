import React from 'react';
import {Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";

const SubmitButton = ({
                          text,
                          onPress

                      }) => (
    <TouchableOpacity style={styles.ActionContainer} onPress={onPress}>
        <Text style={[Typography.Text14OpenSansBold, AppStyle.Yellow]}>{text}</Text>
    </TouchableOpacity>
);

const styles = {

    ActionContainer: {
        backgroundColor: '#42413d',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        height: hp('4.5%'),
        marginTop: hp('2%')
    }

}


export default SubmitButton;