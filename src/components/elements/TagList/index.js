import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Row, Grid} from "react-native-easy-grid";
import {View} from "react-native"
import Tag from "../tag";

class TagList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    setCategory(){
        //////console.log("fffff")
    }

    render() {

        return (
            <View style={[{
                flexDirection: 'row',
                flexWrap: 'wrap',
                alignItems: 'flex-start',
                flex:1,
            }]}>

                {this.props.items.map((item, index) => {

                    return (
                        <Tag onPress={()=>{
                            this.props.setCategory(item)

                        }} key={index} IfCategoryColor={true} title={item.name}
                                      selected={false}/>
                    )
                })
                }

            </View>
        );
    }
}


export default TagList;