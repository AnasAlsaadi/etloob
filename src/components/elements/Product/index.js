import React from 'react';
import {I18nManager, View, Image, Text, TextInput, TouchableOpacity} from "react-native";
import Typography from "../../../constants/typography";
import AppStyle from "../../../constants/AppStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Col, Grid, Row} from "react-native-easy-grid";
import color from "../../../constants/colors";
import strings from "../../../locales/i18n";
import FastImage from 'react-native-fast-image'
import NumberFormat from 'react-number-format';

const Product = ({
                     text,
                     onPress, onPressAddToCart,
                     onPressAddToFavorite,
                     product

                 }) => (
    <View style={styles.Item}>
        <View style={[styles.CenterContent]}>
            <TouchableOpacity onPress={onPress}>
                <FastImage
                    style={styles.productImage}
                    source={{
                        uri: Array.isArray(product.images) && product.images.length > 0 ? product.images[0].src : "https://static.thenounproject.com/png/220984-200.png",
                        priority: FastImage.priority.high,
                    }}
                />


            </TouchableOpacity>
        </View>
        <View style={[styles.CenterContent, {flex: 1,}]}>

            <TouchableOpacity onPress={onPress}>

                <View style={[styles.CenterContent, {flex: 1,}]}>
                    <Text
                        style={styles.productName}>{product.name}</Text>
                </View>
                {product.on_sale && product.regular_price.length > 0 ?
                    <View style={[styles.CenterContent, {flex: 1}]}>
                        <NumberFormat value={product.regular_price} renderText={value => <Text
                            style={[styles.oldPrice]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                      displayType={'text'}
                                      thousandSeparator={true} prefix={""}/>
                    </View>
                    : null}

            </TouchableOpacity>
        </View>

        <View>
            <View style={[styles.CenterContent, {flexDirection: 'row'}]}>
                <View size={1} style={[styles.CenterTopContent, {flex: 0.2}]}>

                    <TouchableOpacity style={styles.TouchableFlexTop} onPress={onPressAddToFavorite}>
                        <Image
                            source={require('./../../../assets/images/love.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View size={2} style={[styles.CenterTopContent, {flex: 0.6}]}>
                    <NumberFormat value={product.price} displayType={'text'}
                                  renderText={value => <Text
                                      style={[Typography.Text12OpenSansBold, {color: color.green}]}>{value} {global.mode === "pro" ? "" : strings.t("syrian_p")}</Text>}
                                  thousandSeparator={true} prefix={""}/>
                    {/*<Text style={[Typography.Text14OpenSansBold]}></Text>*/}
                </View>
                <View size={1} style={[styles.CenterTopContent, {flex: 0.2}]}>


                    <TouchableOpacity style={[styles.TouchableFlexTop]} onPress={onPressAddToCart}>
                        <Image
                            source={require('./../../../assets/images/cart_product.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </View>
);

const styles = {

    Item: {
        padding: 4,
        flexDirection: 'column',
        borderRadius: 5,
        backgroundColor: 'white',
        borderColor: color.GrayBorder,

        flex: 1,
        borderWidth: 1,
        shadowColor: color.GrayBorder,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
    ,
    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    CenterTopContent: {
        alignItems: 'center'
    },
    TouchableFlex: {

        alignItems: 'center', justifyContent: 'center',
        flex: 1
    }, TouchableFlexTop: {

        paddingLeft: wp("3%"),
        paddingRight: wp("3%"),
        // alignItems: 'center',
        // flex: 1
    },
    productImage: {
        width: wp("35%"),
        height: wp("35%"),
        resizeMode: 'contain',
    },
    footerImage: {
        width: hp("5%"),
        resizeMode: 'contain',
    },
    productName: {
        ...Typography.Text14Light, textAlign: 'center',
        paddingStart: wp("1%"),
        paddingEnd: wp("1%"),
    },
    oldPrice: {
        ...Typography.Text13,
        color: color.red,
        textDecorationLine: 'line-through'

    }

}


export default Product;