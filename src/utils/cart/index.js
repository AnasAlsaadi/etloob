import {NAVIGATION_APP, NAVIGATION_DRAWER} from "../../navigation/types";

export default cartManagement => ({

    minusItemCart(item) {
        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                //////console.log(ret)

                if (ret.line_items.findIndex(x => x.product_id === item.product_id) !== -1) {


                    if (item.compare_data.length > 0) {
                        var array_index = ret.line_items.reduce(function (a, e, i) {
                            if (e.product_id === item.product_id)
                                a.push(i);
                            return a;
                        }, []);

                        array_index.map((e, index) => {

                            if (JSON.stringify(ret.line_items[e].compare_data) === JSON.stringify(item.compare_data)) {
                                ret.line_items[e].quantity = ret.line_items[e].quantity -1

                            }
                        })

                    }else {

                        var index = ret.line_items.findIndex(x => x.product_id === item.product_id)

                        if (ret.line_items[index].quantity > 1) {
                            ret.line_items[index].quantity = ret.line_items[index].quantity - 1


                        }
                    }

                }

                //////console.log("before save remove item")

                global.cart = ret

                global.storage.save({
                    key: 'cart',
                    data: ret
                })

            })
            .catch(err => {

                //////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },

    plusItemCart(item) {

        console.log("add item plus")
        console.log(item)
        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                console.log("ret")
                console.log(ret)

                if (ret.line_items.findIndex(x => x.product_id === item.product_id) !== -1) {


                    if (item.compare_data.length > 0) {
                        var array_index = ret.line_items.reduce(function (a, e, i) {
                            if (e.product_id === item.product_id)
                                a.push(i);
                            return a;
                        }, []);

                        array_index.map((e, index) => {

                            if (JSON.stringify(ret.line_items[e].compare_data) === JSON.stringify(item.compare_data)) {
                                ret.line_items[e].quantity = ret.line_items[e].quantity + 1

                            }
                        })

                    }
                    else {

                        var index = ret.line_items.findIndex(x => x.product_id === item.product_id)
                        ret.line_items[index].quantity = ret.line_items[index].quantity + 1
                    }


                    //
                    //
                    //
                    // console.log("if meta true")
                    // console.log(item.meta_data)
                    // console.log(ret.line_items[index].meta_data)
                    // if (JSON.stringify(item.meta_data) === JSON.stringify(ret.line_items[index].meta_data)) {
                    //
                    //
                    //
                    // }

                }

                //////console.log("before save remove item")

                global.cart = ret

                global.storage.save({
                    key: 'cart',
                    data: ret
                })

            })
            .catch(err => {

                //////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },
    removeItemCart(item) {

        //////console.log("remove product cart action Cart Management")
        //////console.log(item)


        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                //////console.log(ret)

                if (ret.line_items.findIndex(x => x.product_id === item.product_id) !== -1) {
                    var index = ret.line_items.findIndex(x => x.product_id === item.product_id)

                    ret.line_items.splice(index, 1)
                }

                //////console.log("before save remove item")

                global.cart = ret

                global.storage.save({
                    key: 'cart',
                    data: ret
                })

            })
            .catch(err => {

                //////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        break;
                    case 'ExpiredError':

                        break;
                }
            });


    },
    addItemToCart(item) {

        console.log("add to cart action Cart Management")
        console.log(item)

        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                console.log(item)

                if (ret.line_items.findIndex(x => x.product_id === item.id) !== -1) {
                    var index = ret.line_items.findIndex(x => x.product_id === item.id)


                    if (item.if_meta) {

                        console.log("if meta true")
                        console.log(item.meta_data)
                        console.log(ret.line_items[index].compare_data)
                        if (JSON.stringify(item.meta_data) === JSON.stringify(ret.line_items[index].compare_data)) {

                            ret.line_items[index].quantity = ret.line_items[index].quantity + 1

                        }
                        else {

                            ret.line_items.push({
                                product_id: item.id,
                                quantity: 1,
                                variation_id: item.if_meta ? item.item.variation_id : item.variation_id,
                                compare_data: item.if_meta ? item.meta_data : [],
                                product: item.if_meta ? item.item : item,
                            })


                        }
                    }
                    else {
                        ret.line_items[index].quantity = ret.line_items[index].quantity + 1

                    }


                    //console.log("after save3")

                    //console.log(ret)
                }
                else {

                    ret.line_items.push({
                        product_id: item.id,
                        quantity: 1,
                        variation_id: item.if_meta ? item.item.variation_id : item.variation_id,
                        compare_data: item.if_meta ? item.meta_data : [],
                        product: item.if_meta ? item.item : item,
                    })

                    ////console.log("after save2")

                    ////console.log(ret)
                }


                //console.log("before save")
                global.storage.save({
                    key: 'cart',
                    data: ret
                })
                //////console.log("after save1")
                //
                //console.log(ret)

                global.cart = ret

            })
            .catch(err => {

                //////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        const cartContaint = {
                            payment_method: "",
                            payment_method_title: "",
                            status: "",
                            set_paid: true,
                            billing: {
                                first_name: "",
                                last_name: "",
                                address_1: "",
                                state: "",
                                address_2: "",
                                city: "",
                                postcode: "",
                                country: "",
                                email: "",
                                phone: "",
                            },
                            shipping: {
                                first_name: "",
                                last_name: "",
                                address_1: "",
                                state: "",
                                address_2: "",
                                city: "",
                                postcode: "",
                                country: "",
                            },
                            line_items: [],
                            shipping_lines: [],
                            customer_note: "",
                            coupon_lines: [],
                        }

                        cartContaint.line_items.push({
                            product_id: item.id,
                            quantity: 1,
                            variation_id: item.if_meta ? item.item.variation_id : item.variation_id,
                            compare_data: item.if_meta ? item.meta_data : [],
                            product: item.if_meta ? item.item : item,
                        })

                        console.log("before save")
                        console.log(item.variation_id)


                        global.storage.save({
                            key: 'cart',
                            data: cartContaint
                        })
                        global.cart = cartContaint
                        console.log(global.cart)
                        ////console.log("after save")

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },


    addItemToFavorite(item) {

        //////console.log("add to cart action Cart Management")
        //////console.log(item)

        global.storage.load({
            key: 'favorite',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                ////console.log(ret)

                if (ret.findIndex(x => x.id === item.id) !== -1) {
                    var index = ret.findIndex(x => x.id === item.id)
                    ret.splice(index, 1)

                }
                else {

                    ret.push(item)

                }


                ////console.log("before save")
                global.storage.save({
                    key: 'favorite',
                    data: ret
                })
                ////console.log("after save1")

                ////console.log(ret)

                global.favorite = ret

            })
            .catch(err => {

                //////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        const Favorites = []

                        Favorites.push(item)

                        ////console.log("before save")
                        global.storage.save({
                            key: 'favorite',
                            data: Favorites
                        })
                        global.favorite = Favorites
                        ////console.log(global.favorite)
                        ////console.log("after save")

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },

    getCart() {

        //////console.log("insede get cart ")
        //////console.log(global.cart)

        return global.cart


    },
    getFavorite() {

        ////console.log("insede get cart ")
        ////console.log(global.favorite)

        return global.favorite


    }
})
