import React from 'react';
import {
    Platform,
    Animated,
    ScrollView,
    I18nManager,
    View,
    Text,
    ImageBackground,
    TouchableOpacity,
    Image
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Grid, Row} from "react-native-easy-grid";
import color from '../../constants/colors'
import ApplicationLayout from "../Layout";
import {NAVIGATION_SEARCH_SCREEN} from "../../navigation/types"
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
import Toast, {DURATION} from 'react-native-easy-toast'
import TabedFooter from "../../components/template/TabedFooter";
import firebase, {Notification, NotificationOpen} from "react-native-firebase";


class DefaultHeader extends React.Component {


    constructor(props) {
        super(props);
        // //////console.log(this.props)


        if (this.props.type !== "vendor") {
            if (this.props.type === "order_details" || this.props.type === "home" || this.props.type === "search" || this.props.type === "product") {
                this.state = {
                    hideNavigationBar: false,
                    showFloatingButtonBar: false,
                    opacity: new Animated.Value(0),
                    opacityHeader: new Animated.Value(1)
                }
            } else {
                this.state = {

                    hideNavigationBar: false,
                    showFloatingButtonBar: false,

                }
            }

        }
        else {
            this.state = {
                hideNavigationBar: false,
                showFloatingButtonBar: false,
                opacity: 0,
                opacityHeader: 0,
            }
        }


    }


    onShow() {
        Animated.timing(this.state.opacity, {
            toValue: 1,

            duration: 100,
            useNativeDriver: true,
        }).start()
    }

    onHide() {
        Animated.timing(this.state.opacity, {
            toValue: 0,
            duration: 10,
            useNativeDriver: true,
        }).start()
    }

    onShowH() {
        Animated.timing(this.state.opacityHeader, {
            toValue: 1,

            duration: 10,
            useNativeDriver: true,
        }).start()
    }

    onHideH() {
        Animated.timing(this.state.opacityHeader, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true,
        }).start()
    }

    handleScroll(event) {

        if (this.props.type !== "search") {
            if (event.nativeEvent.contentOffset.y > 30) {
                this.onShow()
                this.onHideH()
                //  this.setState({hideNavigationBar: true})
                this.setState({showFloatingButtonBar: true})

            }
            else {
                this.onHide()

                this.onShowH()

                // this.setState({hideNavigationBar: false})
                // this.setState({showFloatingButtonBar: false})
            }
            //console.log(event.nativeEvent.contentOffset.y);

        }


    }

    render() {

        const {children, ...props} = this.props

        return (

            <ApplicationLayout {...this.props} isLoading={this.props.isLoading}>
                <Grid>
                    <Row size={3.7} style={{

                        marginTop: Platform.OS === "ios" ? wp("5%") : wp("0%")
                    }}>
                        <View style={{
                            flex: 1,
                        }}>


                            {this.props.type !== "vendor" ?

                                this.props.type === "home" || this.props.type === "search" || this.props.type === "product"|| this.props.type === "order_details" ?
                                    <View style={{flex: 1,}}>

                                        <ScrollView
                                            onScroll={this.handleScroll.bind(this)}

                                        >


                                            {!this.props.hideNavigationBar ?
                                                <Animated.View opacity={this.state.opacityHeader} size={0.1} style={{
                                                    flex: 0.1,
                                                    zIndex: 3,
                                                    backgroundColor: 'white',
                                                    borderBottomWidth: 1,
                                                    borderBottomColor: color.GrayTransparent,

                                                    // shadowColor: color.CustomBlack,
                                                    // shadowOpacity: 0.8,
                                                    // shadowRadius: 4,
                                                    // shadowOffset: {
                                                    //     height: 1,
                                                    //     width: 1
                                                    // }
                                                }

                                                }>


                                                    <Grid style={styles.gridHeader}>
                                                        <Col size={1} style={styles.ArrwoLeftContainer}>

                                                            {(this.props.type === "home") ?

                                                                <TouchableOpacity style={{padding: wp("4%")}}
                                                                                  onPress={() => {
                                                                                      this.props.navigation.openDrawer();
                                                                                  }}><Image style={styles.ArrwoLeft}
                                                                                            source={require('./../../assets/images/toggle.png')}/>
                                                                </TouchableOpacity>
                                                                :
                                                                <TouchableOpacity style={{padding: wp("4%")}}
                                                                                  onPress={() => {
                                                                                      this.props.navigation.goBack();
                                                                                  }}>
                                                                    {I18nManager.isRTL ?
                                                                        <Image style={styles.ArrwoLeftBack}
                                                                               source={require('./../../assets/images/arrow_right.png')}/> :

                                                                        <Image style={styles.ArrwoLeftBack}
                                                                               source={require('./../../assets/images/arrow_left.png')}/>
                                                                    }
                                                                </TouchableOpacity>
                                                            }

                                                        </Col>
                                                        <Col size={2} style={styles.logoContainer}>
                                                            <Image style={styles.logo}
                                                                   source={require('./../../assets/images/Logo.png')}/>
                                                        </Col>

                                                        <Col size={1} style={styles.searchContainer}>
                                                            <TouchableOpacity onPress={() => {

                                                                this.props.navigation.navigate(NAVIGATION_SEARCH_SCREEN);
                                                            }
                                                            }>
                                                                <Image style={styles.search}
                                                                       source={require('./../../assets/images/search.png')}/>
                                                            </TouchableOpacity>
                                                        </Col>
                                                    </Grid>
                                                </Animated.View>
                                                : null}
                                            <View size={3.4} style={{flex: 0.9, zIndex: 1}}   {...props} >

                                                {children}

                                            </View>

                                        </ScrollView>
                                    </View>
                                    :
                                    <View style={{flex: 1,}}>

                                        {!this.props.hideNavigationBar ?
                                            <Animated.View size={0.1} style={{
                                                flex: 0.1,
                                                zIndex: 3,
                                                backgroundColor: 'white',
                                                borderBottomWidth: 1,
                                                borderBottomColor: color.GrayTransparent,

                                                // shadowColor: color.CustomBlack,
                                                // shadowOpacity: 0.8,
                                                // shadowRadius: 4,
                                                // shadowOffset: {
                                                //     height: 1,
                                                //     width: 1
                                                // }
                                            }

                                            }>


                                                <Grid style={styles.gridHeader}>
                                                    <Col size={1} style={styles.ArrwoLeftContainer}>

                                                        {(this.props.type === "home") ?

                                                            <TouchableOpacity style={{padding: wp("4%")}}
                                                                              onPress={() => {
                                                                                  this.props.navigation.openDrawer();
                                                                              }}><Image style={styles.ArrwoLeft}
                                                                                        source={require('./../../assets/images/toggle.png')}/>
                                                            </TouchableOpacity>
                                                            :
                                                            <TouchableOpacity style={{padding: wp("4%")}}
                                                                              onPress={() => {
                                                                                  this.props.navigation.goBack();
                                                                              }}>
                                                                {I18nManager.isRTL ?
                                                                    <Image style={styles.ArrwoLeftBack}
                                                                           source={require('./../../assets/images/arrow_right.png')}/> :

                                                                    <Image style={styles.ArrwoLeftBack}
                                                                           source={require('./../../assets/images/arrow_left.png')}/>
                                                                }
                                                            </TouchableOpacity>
                                                        }

                                                    </Col>
                                                    <Col size={2} style={styles.logoContainer}>
                                                        <Image style={styles.logo}
                                                               source={require('./../../assets/images/Logo.png')}/>
                                                    </Col>

                                                    <Col size={1} style={styles.searchContainer}>
                                                        <TouchableOpacity onPress={() => {

                                                            this.props.navigation.navigate(NAVIGATION_SEARCH_SCREEN);
                                                        }
                                                        }>
                                                            <Image style={styles.search}
                                                                   source={require('./../../assets/images/search.png')}/>
                                                        </TouchableOpacity>
                                                    </Col>
                                                </Grid>
                                            </Animated.View>
                                            : null}
                                        <View size={3.4} style={{flex: 0.9, zIndex: 1}}   {...props} >

                                            {children}

                                        </View>
                                    </View>

                                : <View style={{
                                    flex: 1,
                                }}>
                                    <View size={0.1} style={{
                                        flex: 0.1,
                                        zIndex: 3,
                                        backgroundColor: 'white',
                                        borderBottomWidth: 1,
                                        borderBottomColor: color.GrayTransparent,

                                        // shadowColor: color.CustomBlack,
                                        // shadowOpacity: 0.8,
                                        // shadowRadius: 4,
                                        // shadowOffset: {
                                        //     height: 1,
                                        //     width: 1
                                        // }
                                    }

                                    }>

                                        <Grid style={styles.gridHeader}>
                                            <Col size={1} style={styles.ArrwoLeftContainer}>


                                                <TouchableOpacity style={{padding: wp("4%")}}
                                                                  onPress={() => {
                                                                      this.props.navigation.openDrawer();
                                                                  }}><Image style={styles.ArrwoLeft}
                                                                            source={require('./../../assets/images/toggle.png')}/>
                                                </TouchableOpacity>


                                            </Col>
                                            <Col size={2} style={styles.logoContainer}>
                                                <Image style={styles.logo}
                                                       source={require('./../../assets/images/Logo.png')}/>
                                            </Col>

                                            <Col size={1} style={styles.searchContainer}>
                                                <TouchableOpacity onPress={() => {

                                                    this.props.navigation.navigate(NAVIGATION_SEARCH_SCREEN);
                                                }
                                                }>
                                                    <Image style={styles.search}
                                                           source={require('./../../assets/images/search.png')}/>
                                                </TouchableOpacity>


                                            </Col>
                                        </Grid>

                                    </View>
                                    < View size={3.4} style={{flex: 0.9, zIndex: 1}}   {...props} >

                                        {children}


                                    </View>

                                </View>}


                        </View>
                    </Row>
                    {this.props.type !== "vendor" ?
                        <TabedFooter {...this.props}/> : null}

                    {this.state.showFloatingButtonBar ?

                        <Animated.View opacity={this.state.opacity}
                                       style={
                                           [{
                                               zIndex: 4,
                                               alignItems: 'center',
                                               justifyContent: 'center',
                                               shadowColor: color.CustomBlack,
                                               shadowOpacity: 0.3,
                                               shadowOffset: {
                                                   height: 1,
                                                   width: 1
                                               },
                                               shadowRadius: 4,

                                               paddingLeft: wp("6%"),
                                               paddingRight: wp("6%"),
                                               paddingBottom: wp("6%"),
                                               paddingTop: wp("6%"),


                                               width: wp("4%"),
                                               height: wp("4%"),
                                               position: 'absolute',
                                               right: wp("5%"),
                                               top: Platform.OS === "ios" ? hp("5%") : hp("2.5%"),
                                               backgroundColor: color.yellowTransparent,
                                               borderRadius: 50,
                                           }]}>
                            <TouchableOpacity
                                style={{


                                    width: wp("12%"),
                                    height: wp("15%"),
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}

                                onPress={() => {

                                    this.props.navigation.navigate(NAVIGATION_SEARCH_SCREEN);
                                }
                                }
                            >
                                <Image
                                    source={require("./../../assets/images/search.png")}/>
                            </TouchableOpacity>
                        </Animated.View>
                        : null}


                    {this.state.showFloatingButtonBar ?

                        <Animated.View opacity={this.state.opacity}
                                       style={[{


                                           zIndex: 4,
                                           alignItems: 'center',
                                           justifyContent: 'center',
                                           shadowColor: color.CustomBlack,
                                           shadowOpacity: 0.3,
                                           shadowOffset: {
                                               height: 1,
                                               width: 1
                                           },
                                           shadowRadius: 4,
                                           paddingLeft: wp("6%"),
                                           paddingRight: wp("6%"),
                                           paddingBottom: wp("6%"),
                                           paddingTop: wp("6%"),

                                           // padding: wp("6%"),

                                           width: wp("4%"),
                                           height: wp("4%"),
                                           position: 'absolute',
                                           left: wp("9%"),
                                           top: Platform.OS === "ios" ? hp("5%") : hp("2.5%"),
                                           backgroundColor: color.yellowTransparent,
                                           borderRadius: 50
                                       }]}
                        >

                            {(this.props.type === "home" && this.props.type !== "search") ?
                                <TouchableOpacity
                                    style={{


                                        width: wp("12%"),
                                        height: wp("15%"),
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                    onPress={() => {
                                        this.props.navigation.openDrawer();
                                    }}
                                >
                                    <Image
                                        source={require("./../../assets/images/toggle.png")}/>
                                </TouchableOpacity>
                                : <TouchableOpacity
                                    style={{


                                        width: wp("12%"),
                                        height: wp("15%"),
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                    onPress={() => {
                                        this.props.navigation.goBack();
                                    }}
                                >
                                    {I18nManager.isRTL ?
                                        <Image style={styles.ArrwoLeftBack}
                                               source={require('./../../assets/images/arrow_right.png')}/> :

                                        <Image style={styles.ArrwoLeftBack}
                                               source={require('./../../assets/images/arrow_left.png')}/>
                                    }
                                </TouchableOpacity>}
                        </Animated.View>
                        : null}


                    {this.props.type === "product" ? <TouchableOpacity


                            onPress={this.props.AddToCartAction}

                            style={{

                                alignItems: 'flex-start',
                                shadowColor: color.CustomBlack,
                                shadowOpacity: 0.3,
                                shadowOffset: {
                                    height: 1,
                                    width: 1
                                },
                                shadowRadius: 4,
                                padding: wp("5%"),
                                width: wp("30%"),
                                position: 'absolute',
                                right: -wp("14%"),
                                bottom: hp("9%"),
                                backgroundColor: color.yellowTransparent,
                                borderRadius: 40
                            }}>
                            <Image
                                source={require("../../assets/images/cart.png")}/>
                        </TouchableOpacity>
                        : null}

                    {this.props.type === "product" ? <TouchableOpacity

                        onPress={this.props.onPressAddToFavoriteThis}

                        style={{
                            alignItems: 'flex-end',
                            shadowColor: color.CustomBlack,
                            shadowOpacity: 0.3,
                            shadowOffset: {
                                height: 1,
                                width: 1
                            },
                            shadowRadius: 4,
                            padding: wp("5.5%"),
                            width: wp("30%"),
                            position: 'absolute',
                            left: -wp("14%"),
                            bottom: hp("9%"),
                            backgroundColor: color.yellowTransparent,
                            borderRadius: 40
                        }}>
                        <Image
                            source={require("../../assets/images/wishlist_menu.png")}/>
                    </TouchableOpacity> : null}


                </Grid>


            </ApplicationLayout>

        );
    }
}

const styles = {
    gridHeader: {
        height: wp("18%"),


        paddingTop: wp("0%"),
        paddingStart: wp("8%"),
        paddingEnd: wp("8%"),


    }, ArrwoLeft: {

        width: wp('6%'),
        resizeMode: 'contain',
    }, ArrwoLeftBack: {

        width: wp('3%'),
        resizeMode: 'contain',
    }, ArrwoLeftContainer: {
        //

        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
// backgroundColor:'red'


        // flexDirection: 'column',
        // alignItems: 'flex-start',
        // justifyContent: 'flex-end',

    },

    logo: {
        width: wp('25%'),
        resizeMode: 'contain',
    },
    logoContainer: {
        //
        // flexDirection: 'column',
        // justifyContent: 'flex-end',

        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',

    },
    search: {
        width: wp('5%'),
        resizeMode: 'contain',
    },
    searchContainer: {


        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'center',
        //
        // flexDirection: 'column',
        // alignItems: 'flex-end',
        // justifyContent: 'flex-end',

    }

}

export default DefaultHeader;
