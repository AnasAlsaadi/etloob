import React from 'react';
import {View, Text, ImageBackground, TouchableOpacity, I18nManager,Image} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Col, Grid, Row} from "react-native-easy-grid";
import ApplicationLayout from "../Layout";
import Spinner from "react-native-loading-spinner-overlay";
import AppStyles from "../../constants/AppStyles";

import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';

class AuthHeader extends React.Component {



    constructor(props) {
        super(props);
        // //////console.log(this.props)

    }

    render() {

        const {children, ...props} = this.props

        return (

            <ApplicationLayout {...props}>
                <Grid>
                    {/*<Spinner*/}
                        {/*visible={this.props.isLoading}*/}
                        {/*textContent={'Loading...'}*/}
                        {/*textStyle={AppStyles.spinnerText}*/}
                    {/*/>*/}
                    <Row size={0.7}>
                        <Grid style={styles.gridHeader}>
                            <Col size={1.5} style={styles.ArrwoLeftContainer}>

                                {this.props.noBack ? null : <TouchableOpacity onPress={() => {
                                    this.props.navigation.goBack();
                                }}>{I18nManager.isRTL ?
                                        <Image style={styles.ArrwoLeft}
                                               source={require('./../../assets/images/arrow_right.png')}/> :
                                        <Image style={styles.ArrwoLeft}
                                               source={require('./../../assets/images/arrow_left.png')}/>}


                                </TouchableOpacity>}

                            </Col>
                            <Col size={2.5} style={styles.logoContainer}>
                                <Image style={styles.logo} source={require('./../../assets/images/Logo.png')}/>
                            </Col>
                        </Grid>
                    </Row>

                    <Row size={3.3} style={{
                        paddingStart: wp("4%"),
                        paddingEnd: wp("4%"),
                    }} {...props}>

                        {children}

                    </Row>

                    <Row size={0.1}/>
                </Grid>
            </ApplicationLayout>

        );
    }
}

const styles = {
    gridHeader: {
        paddingTop: wp("10%"),
        paddingStart: wp("8%"),
        paddingEnd: wp("8%"),
    }, ArrwoLeft: {
        width: wp('6%'),
        height: wp('6%'),
        resizeMode: 'contain',
    }, ArrwoLeftContainer: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },

    logo: {
        width: wp('50%'),
        resizeMode: 'contain',
    },
    logoContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    }

}

export default AuthHeader;