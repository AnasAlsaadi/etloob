import React from 'react';
import {View, Text, Image,ImageBackground } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Spinner from "react-native-loading-spinner-overlay";
import AppStyles from "../../constants/AppStyles";
import {Grid} from "react-native-easy-grid";


class ApplicationLayout extends React.Component {


    render() {

        const { children, ...props} = this.props

        return (

            <View style={{flex:1}}>
                <Spinner
                    visible={this.props.isLoading}
                    textContent={''}
                    textStyle={AppStyles.spinnerText}
                />
                <ImageBackground
                    style={styles.ImageBackground}
                    source={require('../../assets/images/layout_background_2.jpg')}>
                    <View style={{flex:1}} {...props}>
                        { children }
                    </View>
                </ImageBackground>


            </View>

        );
    }
}

const styles = {
    ImageBackground: {
        height: hp('100%'),
        width: wp('100%'),flex: 1,
        resizeMode: 'contain',
    }


}

export default ApplicationLayout;